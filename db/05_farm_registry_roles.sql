INSERT INTO sc_niva.SEC_drol (ROL_id, ROL_name) VALUES
(1, 'READ'),
(2, 'WRITE'),
(3, 'ADMIN');



--
-- TOC entry 4412 (class 0 OID 31494)
-- Dependencies: 337
-- Data for Name: SEC_DUSER; Type: TABLE DATA; Schema: sc_niva; Owner: niva
--

INSERT INTO sc_niva.SEC_DUSER VALUES (2, 'andalucia', '$2a$10$seKeromMhBaDxpbR1DPRNedwMm6KTUaN3FE5IXhs.eA6ZzUGIVjKO', 'ES');
INSERT INTO sc_niva.SEC_DUSER VALUES (3, 'admin', '$2a$10$/HBJvxzW1djqkO/lAshChuoBj8Gpq8Rr6p6TpeVxoDZNUZ0bXrca2', 'ES');
INSERT INTO sc_niva.SEC_DUSER VALUES (106, 'admin', '$2a$10$Z5RR9su.2USksFFCae748OTfs99rw1ONxXuLFh6jjCJgCaoqcuVx2', 'ES');
INSERT INTO sc_niva.SEC_DUSER VALUES (2378, 'psanchez', '$2a$10$/HBJvxzW1djqkO/lAshChuoBj8Gpq8Rr6p6TpeVxoDZNUZ0bXrca2', 'ES');


--
-- TOC entry 4413 (class 0 OID 31497)
-- Dependencies: 338
-- Data for Name: SEC_DUSER_ROL; Type: TABLE DATA; Schema: sc_niva; Owner: niva
--

INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (2, 1);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (2, 2);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (3, 1);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (3, 2);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (3, 3);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (106, 1);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (106, 2);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (106, 3);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (2378, 1);
INSERT INTO sc_niva.SEC_DUSER_ROL VALUES (2378, 2);

