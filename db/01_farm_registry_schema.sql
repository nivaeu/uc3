
CREATE ROLE niva;

--
-- TOC entry 9 (class 2615 OID 17401)
-- Name: sc_niva; Type: SCHEMA; Schema: -; Owner: niva
--

CREATE SCHEMA sc_niva;


ALTER SCHEMA sc_niva OWNER TO niva;

--
-- TOC entry 4290 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA sc_niva; Type: COMMENT; Schema: -; Owner: niva
--

COMMENT ON SCHEMA sc_niva IS 'sc_niva schema';


--
-- TOC entry 2 (class 3079 OID 23740)
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA sc_niva;


--
-- TOC entry 4292 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 273 (class 1259 OID 28275)
-- Name: ab_dagrblock; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_dagrblock (
    abl_id uuid NOT NULL,
    abl_key character varying(50) NOT NULL,
    pun_id uuid NOT NULL,
    abl_datefrom date,
    abl_dateto date
);


ALTER TABLE sc_niva.ab_dagrblock OWNER TO niva;

--
-- TOC entry 274 (class 1259 OID 28278)
-- Name: ab_dgeometry; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_dgeometry (
    abl_id uuid NOT NULL,
    age_geometry sc_niva.geometry(Geometry,4326) NOT NULL,
    age_surface numeric(10,2),
    age_datefrom date NOT NULL,
    age_dateto date
);


ALTER TABLE sc_niva.ab_dgeometry OWNER TO niva;

--
-- TOC entry 275 (class 1259 OID 28284)
-- Name: ab_dlandcover; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_dlandcover (
    abl_id uuid NOT NULL,
    lan_id smallint NOT NULL,
    alc_datefrom date NOT NULL,
    alc_dateto date
);


ALTER TABLE sc_niva.ab_dlandcover OWNER TO niva;

--
-- TOC entry 276 (class 1259 OID 28287)
-- Name: ab_hagrblock; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_hagrblock (
    abl_id uuid NOT NULL,
    abl_key character varying(50) NOT NULL,
    pun_id uuid NOT NULL,
    abl_datefrom date,
    abl_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.ab_hagrblock OWNER TO niva;

--
-- TOC entry 277 (class 1259 OID 28290)
-- Name: ab_hgeometry; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_hgeometry (
    abl_id uuid NOT NULL,
    age_geometry sc_niva.geometry NOT NULL,
    age_surface numeric(10,2),
    age_datefrom date NOT NULL,
    age_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.ab_hgeometry OWNER TO niva;

--
-- TOC entry 278 (class 1259 OID 28296)
-- Name: ab_hlandcover; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ab_hlandcover (
    abl_id uuid NOT NULL,
    lan_id smallint NOT NULL,
    alc_datefrom date NOT NULL,
    alc_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.ab_hlandcover OWNER TO niva;


--
-- TOC entry 294 (class 1259 OID 28356)
-- Name: co_pactsus; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pactsus (
    acs_id smallint NOT NULL,
    acs_name character varying(255),
    acs_nameeu character varying(255)
);

ALTER TABLE sc_niva.co_pactsus OWNER TO niva;

--
-- TOC entry 294 (class 1259 OID 28356)
-- Name: co_pactmats; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pactmats (
    acm_id smallint NOT NULL,
    phy_id character varying(8) NOT NULL,
    cou_id character varying(2) NOT NULL,
    acs_id smallint NOT NULL,
	acm_percsacs numeric(10,2),
	acm_quanacs numeric(20,2),
	uni_id smallint NOT NULL
	
);


ALTER TABLE sc_niva.co_pactmats OWNER TO niva;

--
-- TOC entry 294 (class 1259 OID 28356)
-- Name: co_pcountry; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcountry (
    cou_id character varying(2) NOT NULL,
    cou_name character varying(255) NOT NULL,
    cou_expirydate date
);


ALTER TABLE sc_niva.co_pcountry OWNER TO niva;

--
-- TOC entry 297 (class 1259 OID 28368)
-- Name: co_pcropprodgroup; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcropprodgroup (
    cpg_id smallint NOT NULL,
    cpg_name character varying(255) NOT NULL,
    cou_id character varying(2) NOT NULL,
    cpg_expirydate date
);


ALTER TABLE sc_niva.co_pcropprodgroup OWNER TO niva;

--
-- TOC entry 296 (class 1259 OID 28362)
-- Name: co_pcropproduct; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcropproduct (
    crp_id character varying(6) NOT NULL,
    crp_name character varying(255) NOT NULL,
    crp_botanicalname character varying(255),
    cpg_id smallint,
    lan_id smallint,
    crp_expirydate date
);


ALTER TABLE sc_niva.co_pcropproduct OWNER TO niva;

--
-- TOC entry 339 (class 1259 OID 29047)
-- Name: co_pcropvariety; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcropvariety (
    crp_id character varying(6) NOT NULL,
    crv_id character varying(5) NOT NULL,
    cou_id character varying(2) NOT NULL,
    crv_name character varying(255) NOT NULL,
    crv_expirydate date
);


ALTER TABLE sc_niva.co_pcropvariety OWNER TO niva;

--
-- TOC entry 298 (class 1259 OID 28374)
-- Name: co_pcultivationdet; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcultivationdet (
    cud_id smallint NOT NULL,
    cud_name character varying(255) NOT NULL,
    cud_expirydate date
);


ALTER TABLE sc_niva.co_pcultivationdet OWNER TO niva;

--
-- TOC entry 299 (class 1259 OID 28377)
-- Name: co_pcultivationsys; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pcultivationsys (
    cus_id smallint NOT NULL,
    cus_name character varying(255) NOT NULL,
    cus_expirydate date
);


ALTER TABLE sc_niva.co_pcultivationsys OWNER TO niva;

--
-- TOC entry 300 (class 1259 OID 28380)
-- Name: co_pfarmertype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfarmertype (
    fat_id smallint NOT NULL,
    fat_name character varying(255) NOT NULL,
    fat_expirydate date
);


ALTER TABLE sc_niva.co_pfarmertype OWNER TO niva;

--
-- TOC entry 301 (class 1259 OID 28383)
-- Name: co_pfarmingtype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfarmingtype (
    frt_id character varying(3) NOT NULL,
    frt_name character varying(255) NOT NULL,
    frt_expirydate date
);


ALTER TABLE sc_niva.co_pfarmingtype OWNER TO niva;

--
-- TOC entry 304 (class 1259 OID 28392)
-- Name: co_pfertilizer; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfertilizer (
    fer_id smallint NOT NULL,
    fer_name character varying(255) NOT NULL,
    fet_id smallint,
    fer_expirydate date
);


ALTER TABLE sc_niva.co_pfertilizer OWNER TO niva;

--
-- TOC entry 305 (class 1259 OID 28395)
-- Name: co_pfertilizertype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfertilizertype (
    fet_id smallint NOT NULL,
    fet_name character varying(255) NOT NULL,
    fet_expirydate date
);


ALTER TABLE sc_niva.co_pfertilizertype OWNER TO niva;

--
-- TOC entry 302 (class 1259 OID 28386)
-- Name: co_pfertilizmethod; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfertilizmethod (
    fem_id smallint NOT NULL,
    fem_name character varying(255) NOT NULL,
    fem_expirydate date
);


ALTER TABLE sc_niva.co_pfertilizmethod OWNER TO niva;

--
-- TOC entry 303 (class 1259 OID 28389)
-- Name: co_pfertiliztype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pfertiliztype (
    frt_id smallint NOT NULL,
    frt_name character varying(255) NOT NULL,
    frt_expirydate date
);


ALTER TABLE sc_niva.co_pfertiliztype OWNER TO niva;

--
-- TOC entry 306 (class 1259 OID 28398)
-- Name: co_pirrigationtype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pirrigationtype (
    irr_id smallint NOT NULL,
    irr_name character varying(255) NOT NULL,
    irr_expirydate date
);


ALTER TABLE sc_niva.co_pirrigationtype OWNER TO niva;

--
-- TOC entry 307 (class 1259 OID 28401)
-- Name: co_plabortype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_plabortype (
    lab_id smallint NOT NULL,
    lab_name character varying(255) NOT NULL,
    lab_expirydate date
);


ALTER TABLE sc_niva.co_plabortype OWNER TO niva;

--
-- TOC entry 308 (class 1259 OID 28404)
-- Name: co_plandcover; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_plandcover (
    lan_id smallint NOT NULL,
    lan_name character varying(255) NOT NULL,
    lan_expirydate date
);


ALTER TABLE sc_niva.co_plandcover OWNER TO niva;

--
-- TOC entry 309 (class 1259 OID 28407)
-- Name: co_plandtenure; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_plandtenure (
    lte_id smallint NOT NULL,
    lte_name character varying(255) NOT NULL,
    lte_expirydate date
);


ALTER TABLE sc_niva.co_plandtenure OWNER TO niva;

--
-- TOC entry 310 (class 1259 OID 28410)
-- Name: co_porganicfarming; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_porganicfarming (
    orf_id smallint NOT NULL,
    orf_name character varying(255) NOT NULL,
    orf_expirydate date
);


ALTER TABLE sc_niva.co_porganicfarming OWNER TO niva;

--
-- TOC entry 312 (class 1259 OID 28416)
-- Name: co_pphytosanequip; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pphytosanequip (
    phe_id smallint NOT NULL,
    phe_name character varying(255) NOT NULL,
    phe_expirydate date
);


ALTER TABLE sc_niva.co_pphytosanequip OWNER TO niva;

--
-- TOC entry 311 (class 1259 OID 28413)
-- Name: co_pphytosanitary; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pphytosanitary (
    phy_id character varying(8) NOT NULL,
    phy_name character varying(255) NOT NULL,
    phy_formulation character varying(255),
    cou_id character varying(2) NOT NULL,
    phy_expirydate date
);


ALTER TABLE sc_niva.co_pphytosanitary OWNER TO niva;

--
-- TOC entry 313 (class 1259 OID 28419)
-- Name: co_pseedtype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pseedtype (
    see_id smallint NOT NULL,
    see_name character varying(255) NOT NULL,
    see_expirydate date
);


ALTER TABLE sc_niva.co_pseedtype OWNER TO niva;

--
-- TOC entry 314 (class 1259 OID 28422)
-- Name: co_punittype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_punittype (
    uni_id smallint NOT NULL,
    uni_code character varying(10) NOT NULL,
    uni_name character varying(255) NOT NULL,
    uni_expirydate date
);


ALTER TABLE sc_niva.co_punittype OWNER TO niva;

--
-- TOC entry 315 (class 1259 OID 28425)
-- Name: co_pwaterorigin; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pwaterorigin (
    wao_id smallint NOT NULL,
    wao_name character varying(255) NOT NULL,
    wao_expirydate date
);


ALTER TABLE sc_niva.co_pwaterorigin OWNER TO niva;

--
-- TOC entry 316 (class 1259 OID 28428)
-- Name: co_pworkunittype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_pworkunittype (
    wut_id smallint NOT NULL,
    wut_name character varying(255) NOT NULL,
    wut_expirydate date
);


ALTER TABLE sc_niva.co_pworkunittype OWNER TO niva;

--
-- TOC entry 295 (class 1259 OID 28359)
-- Name: co_rcou_crp; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.co_rcou_crp (
    crp_id character varying(6) NOT NULL,
    cou_id character varying(2) NOT NULL,
    ccp_couproduct character varying(5) NOT NULL
);


ALTER TABLE sc_niva.co_rcou_crp OWNER TO niva;

--
-- TOC entry 317 (class 1259 OID 28431)
-- Name: ecr_dreportdoc; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.ecr_dreportdoc (
    ecr_id integer NOT NULL,
    ecr_reportcount smallint,
    ecr_description character varying(255),
    ecr_issue timestamp with time zone NOT NULL,
    ecr_type smallint,
    ecr_copy boolean,
    ecr_ctrlrequirement boolean,
    ecr_purpose smallint,
    ecr_linecount smallint,
    ecr_information character varying(255),
    ecr_status smallint,
    ecr_sequence smallint
);


ALTER TABLE sc_niva.ecr_dreportdoc OWNER TO niva;

--
-- TOC entry 318 (class 1259 OID 28437)
-- Name: pr_dfarmer; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dfarmer (
    far_id uuid NOT NULL,
    far_code character varying(14) NOT NULL,
    cou_id character varying(2) NOT NULL,
    fac_id uuid
);


ALTER TABLE sc_niva.pr_dfarmer OWNER TO niva;

--
-- TOC entry 319 (class 1259 OID 28440)
-- Name: pr_dfarmercontact; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dfarmercontact (
    fac_id uuid NOT NULL,
    cou_id character varying(2),
    fac_city character varying(255),
    fac_street character varying(255),
    fac_number character varying(255),
    fac_postcode character varying(255),
    fac_additional character varying(255),
    fac_email1 character varying(255),
    fac_email2 character varying(255),
    fac_phone1 character varying(255),
    fac_phone2 character varying(255)
);


ALTER TABLE sc_niva.pr_dfarmercontact OWNER TO niva;

--
-- TOC entry 320 (class 1259 OID 28446)
-- Name: pr_dfarmerdata; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dfarmerdata (
    far_id uuid NOT NULL,
    fat_id smallint,
    fad_personalid character varying(255) NOT NULL,
    fad_name character varying(255)
);


ALTER TABLE sc_niva.pr_dfarmerdata OWNER TO niva;

--
-- TOC entry 321 (class 1259 OID 28452)
-- Name: pr_dfarmingtype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dfarmingtype (
    pun_id uuid NOT NULL,
    frt_id character varying(3) NOT NULL,
    pft_datefrom date NOT NULL,
    pft_dateto date
);


ALTER TABLE sc_niva.pr_dfarmingtype OWNER TO niva;

--
-- TOC entry 322 (class 1259 OID 28455)
-- Name: pr_dproductionunit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dproductionunit (
    pun_id uuid NOT NULL,
    pun_code character varying(14) NOT NULL,
    pun_key character varying(50) NOT NULL,
    cou_id character varying(2) NOT NULL,
    far_id uuid NOT NULL,
    fac_id uuid,
    pun_datefrom date,
    pun_dateto date
);


ALTER TABLE sc_niva.pr_dproductionunit OWNER TO niva;

--
-- TOC entry 323 (class 1259 OID 28458)
-- Name: pr_dprodworkunit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_dprodworkunit (
    pun_id uuid NOT NULL,
    wut_id smallint NOT NULL,
    puw_awu numeric(8,2),
    puw_datefrom date NOT NULL,
    puw_dateto date
);


ALTER TABLE sc_niva.pr_dprodworkunit OWNER TO niva;

--
-- TOC entry 279 (class 1259 OID 28299)
-- Name: pr_hfarmer; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hfarmer (
    far_id uuid NOT NULL,
    far_code character varying(14) NOT NULL,
    fac_id uuid,
    cou_id character varying(2) NOT NULL,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hfarmer OWNER TO niva;

--
-- TOC entry 280 (class 1259 OID 28302)
-- Name: pr_hfarmercontact; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hfarmercontact (
    fac_id uuid NOT NULL,
    cou_id character varying(2),
    fac_city character varying(255),
    fac_street character varying(255),
    fac_number character varying(255),
    fac_postcode character varying(255),
    fac_additional character varying(255),
    fac_email1 character varying(255),
    fac_email2 character varying(255),
    fac_phone1 character varying(255),
    fac_phone2 character varying(255),
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hfarmercontact OWNER TO niva;

--
-- TOC entry 281 (class 1259 OID 28308)
-- Name: pr_hfarmerdata; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hfarmerdata (
    far_id uuid NOT NULL,
    fat_id smallint,
    fad_personalid character varying(255) NOT NULL,
    fad_name character varying(255),
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hfarmerdata OWNER TO niva;

--
-- TOC entry 282 (class 1259 OID 28314)
-- Name: pr_hfarmingtype; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hfarmingtype (
    pun_id uuid NOT NULL,
    frt_id character varying(3) NOT NULL,
    pft_datefrom date NOT NULL,
    pft_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hfarmingtype OWNER TO niva;

--
-- TOC entry 283 (class 1259 OID 28317)
-- Name: pr_hproductionunit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hproductionunit (
    pun_id uuid NOT NULL,
    pun_code character varying(14) NOT NULL,
    pun_key character varying(50) NOT NULL,
    far_id uuid NOT NULL,
    fac_id uuid,
    cou_id character varying(2) NOT NULL,
    pun_datefrom date,
    pun_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hproductionunit OWNER TO niva;

--
-- TOC entry 284 (class 1259 OID 28320)
-- Name: pr_hprodworkunit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.pr_hprodworkunit (
    pun_id uuid NOT NULL,
    wut_id smallint NOT NULL,
    puw_awu numeric(8,2),
    puw_datefrom date NOT NULL,
    puw_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.pr_hprodworkunit OWNER TO niva;

--
-- TOC entry 324 (class 1259 OID 28461)
-- Name: rev_drevinfo; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rev_drevinfo (
    rev integer NOT NULL,
    rev_tstmp bigint NOT NULL,
    rev_user smallint NOT NULL
);


ALTER TABLE sc_niva.rev_drevinfo OWNER TO niva;

--
-- TOC entry 325 (class 1259 OID 28464)
-- Name: rp_dcropactivity; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropactivity (
    rcp_id uuid NOT NULL,
    aga_agriactivity boolean,
    cus_id smallint,
    cud_id smallint,
    see_id smallint,
    orf_id smallint
);


ALTER TABLE sc_niva.rp_dcropactivity OWNER TO niva;

--
-- TOC entry 326 (class 1259 OID 28467)
-- Name: rp_dcropactivitydet; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropactivitydet (
    rcp_id uuid NOT NULL,
    lab_id smallint NOT NULL,
    cad_startdate date NOT NULL,
    cad_enddate date
);


ALTER TABLE sc_niva.rp_dcropactivitydet OWNER TO niva;

--
-- TOC entry 327 (class 1259 OID 28470)
-- Name: rp_dcropfertilizer; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropfertilizer (
    rcp_id uuid NOT NULL,
    fet_id smallint NOT NULL,
    fer_id smallint,
    frt_id smallint,
    fem_id smallint,
    cfe_dose numeric(8,2),
    uni_id smallint,
    cfe_startdate date NOT NULL,
    cfe_enddate date,
    cfe_expirydate date
);


ALTER TABLE sc_niva.rp_dcropfertilizer OWNER TO niva;

--
-- TOC entry 328 (class 1259 OID 28473)
-- Name: rp_dcropirrigation; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropirrigation (
    rcp_id uuid NOT NULL,
    irr_id smallint NOT NULL,
    wao_id smallint,
    cri_volume numeric(8,2),
    uni_id smallint,
    cri_irrigationdate date NOT NULL
);


ALTER TABLE sc_niva.rp_dcropirrigation OWNER TO niva;

--
-- TOC entry 329 (class 1259 OID 28476)
-- Name: rp_dcropparcel; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropparcel (
    rcp_id uuid NOT NULL,
    rcp_key character varying(50) NOT NULL,
    rfp_id uuid NOT NULL,
    rcp_irrigation boolean NOT NULL,
    crp_id character varying(6) NOT NULL,
    crv_id character varying(5),
    rcp_landscfeatures boolean,
    lte_id smallint,
    rcp_datefrom date,
    rcp_dateto date
);


ALTER TABLE sc_niva.rp_dcropparcel OWNER TO niva;

--
-- TOC entry 330 (class 1259 OID 28479)
-- Name: rp_dcropparcelgeom; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropparcelgeom (
    rcp_id uuid NOT NULL,
    cpg_geometry sc_niva.geometry(Geometry,4326),
    cpg_surface numeric(10,2),
    cpg_datefrom date NOT NULL,
    cpg_dateto date
);


ALTER TABLE sc_niva.rp_dcropparcelgeom OWNER TO niva;

--
-- TOC entry 331 (class 1259 OID 28485)
-- Name: rp_dcropphytosanit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dcropphytosanit (
    rcp_id uuid NOT NULL,
    phy_id character varying(8) NOT NULL,
    cou_id character varying(2) NOT NULL,
    cph_dose numeric(8,2),
    uni_id smallint,
    phe_id smallint,
    cph_startdate date NOT NULL,
    cph_enddate date,
    cph_expirydate date
);


ALTER TABLE sc_niva.rp_dcropphytosanit OWNER TO niva;

--
-- TOC entry 332 (class 1259 OID 28488)
-- Name: rp_dfertilizrec; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dfertilizrec (
    rcp_id uuid NOT NULL,
    fre_n numeric(5,2),
    fre_p numeric(5,2),
    fre_k numeric(5,2),
    fre_fertilizdate date,
    fre_entrydate date NOT NULL,
    fre_expirydate date
);


ALTER TABLE sc_niva.rp_dfertilizrec OWNER TO niva;

--
-- TOC entry 333 (class 1259 OID 28491)
-- Name: rp_dgeometry; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dgeometry (
    rfp_id uuid NOT NULL,
    rge_geometry sc_niva.geometry(Geometry,4326) NOT NULL,
    rge_surface numeric(10,2),
    rge_datefrom date NOT NULL,
    rge_dateto date
);


ALTER TABLE sc_niva.rp_dgeometry OWNER TO niva;

--
-- TOC entry 334 (class 1259 OID 28497)
-- Name: rp_dirrigationrec; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_dirrigationrec (
    rcp_id uuid NOT NULL,
    ire_volume numeric(8,2),
    uni_id smallint,
    ire_irrigationdate date NOT NULL,
    ire_expirydate date
);


ALTER TABLE sc_niva.rp_dirrigationrec OWNER TO niva;

--
-- TOC entry 335 (class 1259 OID 28500)
-- Name: rp_drefplot; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_drefplot (
    rfp_id uuid NOT NULL,
    rfp_key character varying(50) NOT NULL,
    abl_id uuid NOT NULL,
    rfp_refparcel character varying(28) NOT NULL,
    rfp_datefrom date,
    rfp_dateto date
);


ALTER TABLE sc_niva.rp_drefplot OWNER TO niva;

--
-- TOC entry 285 (class 1259 OID 28323)
-- Name: rp_hcropactivity; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropactivity (
    rcp_id uuid NOT NULL,
    aga_agriactivity boolean,
    cus_id smallint,
    cud_id smallint,
    see_id smallint,
    orf_id smallint,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropactivity OWNER TO niva;

--
-- TOC entry 286 (class 1259 OID 28326)
-- Name: rp_hcropactivitydet; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropactivitydet (
    rcp_id uuid NOT NULL,
    lab_id smallint NOT NULL,
    cad_startdate date NOT NULL,
    cad_enddate date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropactivitydet OWNER TO niva;

--
-- TOC entry 287 (class 1259 OID 28329)
-- Name: rp_hcropfertilizer; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropfertilizer (
    rcp_id uuid NOT NULL,
    fet_id smallint NOT NULL,
    fer_id smallint,
    frt_id smallint,
    fem_id smallint,
    cfe_dose numeric(8,2),
    uni_id smallint,
    cfe_startdate date NOT NULL,
    cfe_enddate date,
    cfe_expiry_date date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropfertilizer OWNER TO niva;

--
-- TOC entry 288 (class 1259 OID 28332)
-- Name: rp_hcropirrigation; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropirrigation (
    rcp_id uuid NOT NULL,
    irr_id smallint NOT NULL,
    wao_id smallint,
    cri_volume numeric(8,2),
    uni_id smallint,
    cri_irrigationdate date NOT NULL,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropirrigation OWNER TO niva;

--
-- TOC entry 289 (class 1259 OID 28335)
-- Name: rp_hcropparcel; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropparcel (
    rcp_id uuid NOT NULL,
    rcp_key character varying(50) NOT NULL,
    rfp_id uuid NOT NULL,
    rcp_irrigation boolean NOT NULL,
    crp_id character varying(6) NOT NULL,
    crv_id character varying(5),
    rcp_landscfeatures boolean,
    lte_id smallint,
    rcp_datefrom character varying(50),
    rcp_dateto character varying(50),
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropparcel OWNER TO niva;

--
-- TOC entry 290 (class 1259 OID 28338)
-- Name: rp_hcropparcelgeom; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropparcelgeom (
    rcp_id uuid NOT NULL,
    cpg_geometry sc_niva.geometry,
    cpg_surface numeric(10,2),
    cpg_datefrom date,
    cpg_dateto date NOT NULL,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropparcelgeom OWNER TO niva;

--
-- TOC entry 291 (class 1259 OID 28344)
-- Name: rp_hcropphytosanit; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hcropphytosanit (
    rcp_id uuid NOT NULL,
    phy_id character varying(8) NOT NULL,
    cou_id character varying(2) NOT NULL,
    cph_dose numeric(8,2),
    uni_id smallint,
    phe_id smallint,
    cph_startdate date NOT NULL,
    cph_enddate date,
    cph_expiry_date date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hcropphytosanit OWNER TO niva;

--
-- TOC entry 292 (class 1259 OID 28347)
-- Name: rp_hgeometry; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hgeometry (
    rfp_id uuid NOT NULL,
    rge_geometry sc_niva.geometry NOT NULL,
    rge_surface numeric(10,2),
    rge_datefrom date NOT NULL,
    rge_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hgeometry OWNER TO niva;

--
-- TOC entry 293 (class 1259 OID 28353)
-- Name: rp_hrefplot; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.rp_hrefplot (
    rfp_id uuid NOT NULL,
    rfp_key character varying(50) NOT NULL,
    abl_id uuid NOT NULL,
    rfp_refparcel character varying(28) NOT NULL,
    rfp_datefrom date,
    rfp_dateto date,
    rev integer NOT NULL,
    revtype smallint
);


ALTER TABLE sc_niva.rp_hrefplot OWNER TO niva;

--
-- TOC entry 336 (class 1259 OID 28503)
-- Name: sec_drol; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.sec_drol (
    rol_id smallint NOT NULL,
    rol_name character varying(50) NOT NULL
);


ALTER TABLE sc_niva.sec_drol OWNER TO niva;

--
-- TOC entry 337 (class 1259 OID 28506)
-- Name: sec_duser; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.sec_duser (
    usr_id smallint NOT NULL,
    usr_username character varying(50) NOT NULL,
    usr_password character varying(255) NOT NULL,
    cou_id character varying(2)
);


ALTER TABLE sc_niva.sec_duser OWNER TO niva;

--
-- TOC entry 338 (class 1259 OID 28509)
-- Name: sec_duser_rol; Type: TABLE; Schema: sc_niva; Owner: niva
--

CREATE TABLE sc_niva.sec_duser_rol (
    usr_id smallint NOT NULL,
    rol_id smallint NOT NULL
);


ALTER TABLE sc_niva.sec_duser_rol OWNER TO niva;

--
-- TOC entry 209 (class 1259 OID 28147)
-- Name: seq_ecr_dreportdoc; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_ecr_dreportdoc
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_ecr_dreportdoc OWNER TO niva;

--
-- TOC entry 210 (class 1259 OID 28149)
-- Name: seq_pr_dfarmer_at; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_at
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_at OWNER TO niva;

--
-- TOC entry 211 (class 1259 OID 28151)
-- Name: seq_pr_dfarmer_be; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_be
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_be OWNER TO niva;

--
-- TOC entry 212 (class 1259 OID 28153)
-- Name: seq_pr_dfarmer_bg; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_bg
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_bg OWNER TO niva;

--
-- TOC entry 213 (class 1259 OID 28155)
-- Name: seq_pr_dfarmer_ch; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_ch
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_ch OWNER TO niva;

--
-- TOC entry 214 (class 1259 OID 28157)
-- Name: seq_pr_dfarmer_cy; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_cy
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_cy OWNER TO niva;

--
-- TOC entry 215 (class 1259 OID 28159)
-- Name: seq_pr_dfarmer_cz; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_cz
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_cz OWNER TO niva;

--
-- TOC entry 216 (class 1259 OID 28161)
-- Name: seq_pr_dfarmer_de; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_de
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_de OWNER TO niva;

--
-- TOC entry 217 (class 1259 OID 28163)
-- Name: seq_pr_dfarmer_dk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_dk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_dk OWNER TO niva;

--
-- TOC entry 218 (class 1259 OID 28165)
-- Name: seq_pr_dfarmer_ee; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_ee
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_ee OWNER TO niva;

--
-- TOC entry 219 (class 1259 OID 28167)
-- Name: seq_pr_dfarmer_el; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_el
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_el OWNER TO niva;

--
-- TOC entry 220 (class 1259 OID 28169)
-- Name: seq_pr_dfarmer_es; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_es
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_es OWNER TO niva;

--
-- TOC entry 221 (class 1259 OID 28171)
-- Name: seq_pr_dfarmer_fi; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_fi
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_fi OWNER TO niva;

--
-- TOC entry 222 (class 1259 OID 28173)
-- Name: seq_pr_dfarmer_fr; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_fr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_fr OWNER TO niva;

--
-- TOC entry 223 (class 1259 OID 28175)
-- Name: seq_pr_dfarmer_hr; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_hr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_hr OWNER TO niva;

--
-- TOC entry 224 (class 1259 OID 28177)
-- Name: seq_pr_dfarmer_hu; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_hu
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_hu OWNER TO niva;

--
-- TOC entry 225 (class 1259 OID 28179)
-- Name: seq_pr_dfarmer_ie; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_ie
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_ie OWNER TO niva;

--
-- TOC entry 226 (class 1259 OID 28181)
-- Name: seq_pr_dfarmer_il; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_il
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_il OWNER TO niva;

--
-- TOC entry 227 (class 1259 OID 28183)
-- Name: seq_pr_dfarmer_it; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_it
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_it OWNER TO niva;

--
-- TOC entry 228 (class 1259 OID 28185)
-- Name: seq_pr_dfarmer_lt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_lt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_lt OWNER TO niva;

--
-- TOC entry 229 (class 1259 OID 28187)
-- Name: seq_pr_dfarmer_lu; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_lu
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_lu OWNER TO niva;

--
-- TOC entry 230 (class 1259 OID 28189)
-- Name: seq_pr_dfarmer_lv; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_lv
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_lv OWNER TO niva;

--
-- TOC entry 231 (class 1259 OID 28191)
-- Name: seq_pr_dfarmer_mt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_mt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_mt OWNER TO niva;

--
-- TOC entry 232 (class 1259 OID 28193)
-- Name: seq_pr_dfarmer_nl; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_nl
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_nl OWNER TO niva;

--
-- TOC entry 233 (class 1259 OID 28195)
-- Name: seq_pr_dfarmer_no; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_no
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_no OWNER TO niva;

--
-- TOC entry 234 (class 1259 OID 28197)
-- Name: seq_pr_dfarmer_pl; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_pl
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_pl OWNER TO niva;

--
-- TOC entry 235 (class 1259 OID 28199)
-- Name: seq_pr_dfarmer_pt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_pt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_pt OWNER TO niva;

--
-- TOC entry 236 (class 1259 OID 28201)
-- Name: seq_pr_dfarmer_ro; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_ro
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_ro OWNER TO niva;

--
-- TOC entry 237 (class 1259 OID 28203)
-- Name: seq_pr_dfarmer_se; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_se
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_se OWNER TO niva;

--
-- TOC entry 238 (class 1259 OID 28205)
-- Name: seq_pr_dfarmer_si; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_si
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_si OWNER TO niva;

--
-- TOC entry 239 (class 1259 OID 28207)
-- Name: seq_pr_dfarmer_sk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_sk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_sk OWNER TO niva;

--
-- TOC entry 240 (class 1259 OID 28209)
-- Name: seq_pr_dfarmer_uk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dfarmer_uk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dfarmer_uk OWNER TO niva;

--
-- TOC entry 241 (class 1259 OID 28211)
-- Name: seq_pr_dproductionunit_at; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_at
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_at OWNER TO niva;

--
-- TOC entry 242 (class 1259 OID 28213)
-- Name: seq_pr_dproductionunit_be; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_be
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_be OWNER TO niva;

--
-- TOC entry 243 (class 1259 OID 28215)
-- Name: seq_pr_dproductionunit_bg; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_bg
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_bg OWNER TO niva;

--
-- TOC entry 244 (class 1259 OID 28217)
-- Name: seq_pr_dproductionunit_ch; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_ch
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_ch OWNER TO niva;

--
-- TOC entry 245 (class 1259 OID 28219)
-- Name: seq_pr_dproductionunit_cy; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_cy
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_cy OWNER TO niva;

--
-- TOC entry 246 (class 1259 OID 28221)
-- Name: seq_pr_dproductionunit_cz; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_cz
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_cz OWNER TO niva;

--
-- TOC entry 247 (class 1259 OID 28223)
-- Name: seq_pr_dproductionunit_de; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_de
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_de OWNER TO niva;

--
-- TOC entry 248 (class 1259 OID 28225)
-- Name: seq_pr_dproductionunit_dk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_dk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_dk OWNER TO niva;

--
-- TOC entry 249 (class 1259 OID 28227)
-- Name: seq_pr_dproductionunit_ee; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_ee
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_ee OWNER TO niva;

--
-- TOC entry 250 (class 1259 OID 28229)
-- Name: seq_pr_dproductionunit_el; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_el
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_el OWNER TO niva;

--
-- TOC entry 251 (class 1259 OID 28231)
-- Name: seq_pr_dproductionunit_es; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_es
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_es OWNER TO niva;

--
-- TOC entry 252 (class 1259 OID 28233)
-- Name: seq_pr_dproductionunit_fi; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_fi
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_fi OWNER TO niva;

--
-- TOC entry 253 (class 1259 OID 28235)
-- Name: seq_pr_dproductionunit_fr; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_fr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_fr OWNER TO niva;

--
-- TOC entry 254 (class 1259 OID 28237)
-- Name: seq_pr_dproductionunit_hr; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_hr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_hr OWNER TO niva;

--
-- TOC entry 255 (class 1259 OID 28239)
-- Name: seq_pr_dproductionunit_hu; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_hu
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_hu OWNER TO niva;

--
-- TOC entry 256 (class 1259 OID 28241)
-- Name: seq_pr_dproductionunit_ie; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_ie
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_ie OWNER TO niva;

--
-- TOC entry 257 (class 1259 OID 28243)
-- Name: seq_pr_dproductionunit_il; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_il
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_il OWNER TO niva;

--
-- TOC entry 258 (class 1259 OID 28245)
-- Name: seq_pr_dproductionunit_it; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_it
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_it OWNER TO niva;

--
-- TOC entry 259 (class 1259 OID 28247)
-- Name: seq_pr_dproductionunit_lt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_lt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_lt OWNER TO niva;

--
-- TOC entry 260 (class 1259 OID 28249)
-- Name: seq_pr_dproductionunit_lu; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_lu
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_lu OWNER TO niva;

--
-- TOC entry 261 (class 1259 OID 28251)
-- Name: seq_pr_dproductionunit_lv; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_lv
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_lv OWNER TO niva;

--
-- TOC entry 262 (class 1259 OID 28253)
-- Name: seq_pr_dproductionunit_mt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_mt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_mt OWNER TO niva;

--
-- TOC entry 263 (class 1259 OID 28255)
-- Name: seq_pr_dproductionunit_nl; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_nl
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_nl OWNER TO niva;

--
-- TOC entry 264 (class 1259 OID 28257)
-- Name: seq_pr_dproductionunit_no; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_no
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_no OWNER TO niva;

--
-- TOC entry 265 (class 1259 OID 28259)
-- Name: seq_pr_dproductionunit_pl; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_pl
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_pl OWNER TO niva;

--
-- TOC entry 266 (class 1259 OID 28261)
-- Name: seq_pr_dproductionunit_pt; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_pt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_pt OWNER TO niva;

--
-- TOC entry 267 (class 1259 OID 28263)
-- Name: seq_pr_dproductionunit_ro; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_ro
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_ro OWNER TO niva;

--
-- TOC entry 268 (class 1259 OID 28265)
-- Name: seq_pr_dproductionunit_se; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_se
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_se OWNER TO niva;

--
-- TOC entry 269 (class 1259 OID 28267)
-- Name: seq_pr_dproductionunit_si; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_si
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_si OWNER TO niva;

--
-- TOC entry 270 (class 1259 OID 28269)
-- Name: seq_pr_dproductionunit_sk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_dproductionunit_sk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_dproductionunit_sk OWNER TO niva;

--
-- TOC entry 271 (class 1259 OID 28271)
-- Name: seq_pr_productionunit_uk; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_pr_productionunit_uk
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_pr_productionunit_uk OWNER TO niva;

--
-- TOC entry 272 (class 1259 OID 28273)
-- Name: seq_rev_envers; Type: SEQUENCE; Schema: sc_niva; Owner: niva
--

CREATE SEQUENCE sc_niva.seq_rev_envers
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sc_niva.seq_rev_envers OWNER TO niva;

--
-- TOC entry 3946 (class 2606 OID 28513)
-- Name: ab_dagrblock pk_ab_dagrblock; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dagrblock
    ADD CONSTRAINT pk_ab_dagrblock PRIMARY KEY (abl_id);


--
-- TOC entry 3948 (class 2606 OID 28515)
-- Name: ab_dgeometry pk_ab_dgeometry; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dgeometry
    ADD CONSTRAINT pk_ab_dgeometry PRIMARY KEY (abl_id, age_datefrom);


--
-- TOC entry 3950 (class 2606 OID 28517)
-- Name: ab_dlandcover pk_ab_dlandcover; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dlandcover
    ADD CONSTRAINT pk_ab_dlandcover PRIMARY KEY (abl_id, lan_id, alc_datefrom);


--
-- TOC entry 3952 (class 2606 OID 28519)
-- Name: ab_hagrblock pk_ab_hagrblock; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hagrblock
    ADD CONSTRAINT pk_ab_hagrblock PRIMARY KEY (abl_id, rev);


--
-- TOC entry 3954 (class 2606 OID 28521)
-- Name: ab_hgeometry pk_ab_hgeometry; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hgeometry
    ADD CONSTRAINT pk_ab_hgeometry PRIMARY KEY (rev, abl_id, age_datefrom);


--
-- TOC entry 3956 (class 2606 OID 28523)
-- Name: ab_hlandcover pk_ab_hlandcover; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hlandcover
    ADD CONSTRAINT pk_ab_hlandcover PRIMARY KEY (rev, abl_id, lan_id, alc_datefrom);


--
-- TOC entry 3988 (class 2606 OID 28555)
-- Name: co_pactsus pk_co_pactsus; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactsus
    ADD CONSTRAINT pk_co_pactsus PRIMARY KEY (acs_id);


--
-- TOC entry 3988 (class 2606 OID 28555)
-- Name: co_pactmats pk_co_pactmats; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactmats
    ADD CONSTRAINT pk_co_pactmats PRIMARY KEY (acm_id);
	
	
--
-- TOC entry 3988 (class 2606 OID 28555)
-- Name: co_pcountry pk_co_country; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcountry
    ADD CONSTRAINT pk_co_country PRIMARY KEY (cou_id);


--
-- TOC entry 3994 (class 2606 OID 28561)
-- Name: co_pcropprodgroup pk_co_pcropprodgroup; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropprodgroup
    ADD CONSTRAINT pk_co_pcropprodgroup PRIMARY KEY (cpg_id);


--
-- TOC entry 3992 (class 2606 OID 29752)
-- Name: co_pcropproduct pk_co_pcropproduct; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropproduct
    ADD CONSTRAINT pk_co_pcropproduct PRIMARY KEY (crp_id);


--
-- TOC entry 4082 (class 2606 OID 29771)
-- Name: co_pcropvariety pk_co_pcropvariety; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropvariety
    ADD CONSTRAINT pk_co_pcropvariety PRIMARY KEY (crp_id, cou_id, crv_id);


--
-- TOC entry 3996 (class 2606 OID 28565)
-- Name: co_pcultivationdet pk_co_pcultivationdet; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcultivationdet
    ADD CONSTRAINT pk_co_pcultivationdet PRIMARY KEY (cud_id);


--
-- TOC entry 3998 (class 2606 OID 28567)
-- Name: co_pcultivationsys pk_co_pcultivationsys; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcultivationsys
    ADD CONSTRAINT pk_co_pcultivationsys PRIMARY KEY (cus_id);


--
-- TOC entry 4000 (class 2606 OID 28569)
-- Name: co_pfarmertype pk_co_pfarmertype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfarmertype
    ADD CONSTRAINT pk_co_pfarmertype PRIMARY KEY (fat_id);


--
-- TOC entry 4002 (class 2606 OID 28571)
-- Name: co_pfarmingtype pk_co_pfarmingtype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfarmingtype
    ADD CONSTRAINT pk_co_pfarmingtype PRIMARY KEY (frt_id);


--
-- TOC entry 4008 (class 2606 OID 28577)
-- Name: co_pfertilizer pk_co_pfertilizer; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfertilizer
    ADD CONSTRAINT pk_co_pfertilizer PRIMARY KEY (fer_id);


--
-- TOC entry 4010 (class 2606 OID 28579)
-- Name: co_pfertilizertype pk_co_pfertilizertype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfertilizertype
    ADD CONSTRAINT pk_co_pfertilizertype PRIMARY KEY (fet_id);


--
-- TOC entry 4004 (class 2606 OID 28573)
-- Name: co_pfertilizmethod pk_co_pfertilizmethod; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfertilizmethod
    ADD CONSTRAINT pk_co_pfertilizmethod PRIMARY KEY (fem_id);


--
-- TOC entry 4006 (class 2606 OID 28575)
-- Name: co_pfertiliztype pk_co_pfertiliztype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfertiliztype
    ADD CONSTRAINT pk_co_pfertiliztype PRIMARY KEY (frt_id);


--
-- TOC entry 4012 (class 2606 OID 28581)
-- Name: co_pirrigationtype pk_co_pirrigationtype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pirrigationtype
    ADD CONSTRAINT pk_co_pirrigationtype PRIMARY KEY (irr_id);


--
-- TOC entry 4014 (class 2606 OID 28583)
-- Name: co_plabortype pk_co_plabortype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_plabortype
    ADD CONSTRAINT pk_co_plabortype PRIMARY KEY (lab_id);


--
-- TOC entry 4016 (class 2606 OID 28585)
-- Name: co_plandcover pk_co_plandcover; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_plandcover
    ADD CONSTRAINT pk_co_plandcover PRIMARY KEY (lan_id);


--
-- TOC entry 4018 (class 2606 OID 28587)
-- Name: co_plandtenure pk_co_plandtenure; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_plandtenure
    ADD CONSTRAINT pk_co_plandtenure PRIMARY KEY (lte_id);


--
-- TOC entry 4020 (class 2606 OID 28589)
-- Name: co_porganicfarming pk_co_porganicfarming; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_porganicfarming
    ADD CONSTRAINT pk_co_porganicfarming PRIMARY KEY (orf_id);


--
-- TOC entry 4024 (class 2606 OID 28593)
-- Name: co_pphytosanequip pk_co_pphytosanequip; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pphytosanequip
    ADD CONSTRAINT pk_co_pphytosanequip PRIMARY KEY (phe_id);


--
-- TOC entry 4022 (class 2606 OID 28591)
-- Name: co_pphytosanitary pk_co_pphytosanitary; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pphytosanitary
    ADD CONSTRAINT pk_co_pphytosanitary PRIMARY KEY (phy_id);


--
-- TOC entry 4026 (class 2606 OID 28595)
-- Name: co_pseedtype pk_co_pseedtype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pseedtype
    ADD CONSTRAINT pk_co_pseedtype PRIMARY KEY (see_id);


--
-- TOC entry 4028 (class 2606 OID 28597)
-- Name: co_punittype pk_co_punittype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_punittype
    ADD CONSTRAINT pk_co_punittype PRIMARY KEY (uni_id);


--
-- TOC entry 4030 (class 2606 OID 28599)
-- Name: co_pwaterorigin pk_co_pwaterorigin; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pwaterorigin
    ADD CONSTRAINT pk_co_pwaterorigin PRIMARY KEY (wao_id);


--
-- TOC entry 4032 (class 2606 OID 28601)
-- Name: co_pworkunittype pk_co_pworkunittype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pworkunittype
    ADD CONSTRAINT pk_co_pworkunittype PRIMARY KEY (wut_id);


--
-- TOC entry 3990 (class 2606 OID 29769)
-- Name: co_rcou_crp pk_co_rcou_crp; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_rcou_crp
    ADD CONSTRAINT pk_co_rcou_crp PRIMARY KEY (cou_id, crp_id, ccp_couproduct);


--
-- TOC entry 4034 (class 2606 OID 28603)
-- Name: ecr_dreportdoc pk_ecr_dreportdoc; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ecr_dreportdoc
    ADD CONSTRAINT pk_ecr_dreportdoc PRIMARY KEY (ecr_id);


--
-- TOC entry 4036 (class 2606 OID 28605)
-- Name: pr_dfarmer pk_pr_dfarmer; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmer
    ADD CONSTRAINT pk_pr_dfarmer PRIMARY KEY (far_id);


--
-- TOC entry 4040 (class 2606 OID 28609)
-- Name: pr_dfarmercontact pk_pr_dfarmercontact; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmercontact
    ADD CONSTRAINT pk_pr_dfarmercontact PRIMARY KEY (fac_id);


--
-- TOC entry 4042 (class 2606 OID 28611)
-- Name: pr_dfarmerdata pk_pr_dfarmerdata; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmerdata
    ADD CONSTRAINT pk_pr_dfarmerdata PRIMARY KEY (far_id);


--
-- TOC entry 4044 (class 2606 OID 28613)
-- Name: pr_dfarmingtype pk_pr_dfarmingtype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmingtype
    ADD CONSTRAINT pk_pr_dfarmingtype PRIMARY KEY (pun_id, frt_id, pft_datefrom);


--
-- TOC entry 4046 (class 2606 OID 28615)
-- Name: pr_dproductionunit pk_pr_dproductionunit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dproductionunit
    ADD CONSTRAINT pk_pr_dproductionunit PRIMARY KEY (pun_id);


--
-- TOC entry 4050 (class 2606 OID 28619)
-- Name: pr_dprodworkunit pk_pr_dprodworkunit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dprodworkunit
    ADD CONSTRAINT pk_pr_dprodworkunit PRIMARY KEY (pun_id, wut_id, puw_datefrom);


--
-- TOC entry 3958 (class 2606 OID 28525)
-- Name: pr_hfarmer pk_pr_hfarmer; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmer
    ADD CONSTRAINT pk_pr_hfarmer PRIMARY KEY (far_id, rev);


--
-- TOC entry 3960 (class 2606 OID 28527)
-- Name: pr_hfarmercontact pk_pr_hfarmercontact; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmercontact
    ADD CONSTRAINT pk_pr_hfarmercontact PRIMARY KEY (fac_id, rev);


--
-- TOC entry 3962 (class 2606 OID 28529)
-- Name: pr_hfarmerdata pk_pr_hfarmerdata; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmerdata
    ADD CONSTRAINT pk_pr_hfarmerdata PRIMARY KEY (rev, far_id);


--
-- TOC entry 3964 (class 2606 OID 28531)
-- Name: pr_hfarmingtype pk_pr_hfarmingtype; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmingtype
    ADD CONSTRAINT pk_pr_hfarmingtype PRIMARY KEY (pun_id, frt_id, pft_datefrom, rev);


--
-- TOC entry 3966 (class 2606 OID 28533)
-- Name: pr_hproductionunit pk_pr_hproductionunit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hproductionunit
    ADD CONSTRAINT pk_pr_hproductionunit PRIMARY KEY (pun_id, rev);


--
-- TOC entry 3968 (class 2606 OID 28535)
-- Name: pr_hprodworkunit pk_pr_hprodworkunit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hprodworkunit
    ADD CONSTRAINT pk_pr_hprodworkunit PRIMARY KEY (pun_id, wut_id, puw_datefrom, rev);


--
-- TOC entry 4052 (class 2606 OID 28621)
-- Name: rev_drevinfo pk_rev_drevinfo; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rev_drevinfo
    ADD CONSTRAINT pk_rev_drevinfo PRIMARY KEY (rev);


--
-- TOC entry 4054 (class 2606 OID 28623)
-- Name: rp_dcropactivity pk_rp_dcropactivity; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT pk_rp_dcropactivity PRIMARY KEY (rcp_id);


--
-- TOC entry 4056 (class 2606 OID 28625)
-- Name: rp_dcropactivitydet pk_rp_dcropactivitydet; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivitydet
    ADD CONSTRAINT pk_rp_dcropactivitydet PRIMARY KEY (rcp_id, lab_id, cad_startdate);


--
-- TOC entry 4058 (class 2606 OID 28627)
-- Name: rp_dcropfertilizer pk_rp_dcropfertilizer; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT pk_rp_dcropfertilizer PRIMARY KEY (rcp_id, cfe_startdate, fet_id);


--
-- TOC entry 4060 (class 2606 OID 28629)
-- Name: rp_dcropirrigation pk_rp_dcropirrigation; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropirrigation
    ADD CONSTRAINT pk_rp_dcropirrigation PRIMARY KEY (rcp_id, cri_irrigationdate);


--
-- TOC entry 4062 (class 2606 OID 28631)
-- Name: rp_dcropparcel pk_rp_dcropparcel; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcel
    ADD CONSTRAINT pk_rp_dcropparcel PRIMARY KEY (rcp_id);


--
-- TOC entry 4064 (class 2606 OID 28633)
-- Name: rp_dcropparcelgeom pk_rp_dcropparcelgeom; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcelgeom
    ADD CONSTRAINT pk_rp_dcropparcelgeom PRIMARY KEY (rcp_id, cpg_datefrom);


--
-- TOC entry 4066 (class 2606 OID 28635)
-- Name: rp_dcropphytosanit pk_rp_dcropphytosanit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT pk_rp_dcropphytosanit PRIMARY KEY (rcp_id, phy_id, cou_id, cph_startdate);


--
-- TOC entry 4068 (class 2606 OID 28637)
-- Name: rp_dfertilizrec pk_rp_dfertilizrec; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dfertilizrec
    ADD CONSTRAINT pk_rp_dfertilizrec PRIMARY KEY (rcp_id, fre_entrydate);


--
-- TOC entry 4070 (class 2606 OID 28639)
-- Name: rp_dgeometry pk_rp_dgeometry; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dgeometry
    ADD CONSTRAINT pk_rp_dgeometry PRIMARY KEY (rfp_id, rge_datefrom);


--
-- TOC entry 4072 (class 2606 OID 28641)
-- Name: rp_dirrigationrec pk_rp_dirrigationrec; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dirrigationrec
    ADD CONSTRAINT pk_rp_dirrigationrec PRIMARY KEY (rcp_id, ire_irrigationdate);


--
-- TOC entry 4074 (class 2606 OID 28643)
-- Name: rp_drefplot pk_rp_drefplot; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_drefplot
    ADD CONSTRAINT pk_rp_drefplot PRIMARY KEY (rfp_id);


--
-- TOC entry 3970 (class 2606 OID 28537)
-- Name: rp_hcropactivity pk_rp_hcropactivity; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropactivity
    ADD CONSTRAINT pk_rp_hcropactivity PRIMARY KEY (rcp_id, rev);


--
-- TOC entry 3972 (class 2606 OID 28539)
-- Name: rp_hcropactivitydet pk_rp_hcropactivitydet; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropactivitydet
    ADD CONSTRAINT pk_rp_hcropactivitydet PRIMARY KEY (rcp_id, lab_id, cad_startdate, rev);


--
-- TOC entry 3974 (class 2606 OID 28541)
-- Name: rp_hcropfertilizer pk_rp_hcropfertilizer; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropfertilizer
    ADD CONSTRAINT pk_rp_hcropfertilizer PRIMARY KEY (rcp_id, cfe_startdate, rev, fet_id);


--
-- TOC entry 3976 (class 2606 OID 28543)
-- Name: rp_hcropirrigation pk_rp_hcropirrigation; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropirrigation
    ADD CONSTRAINT pk_rp_hcropirrigation PRIMARY KEY (rev, rcp_id, cri_irrigationdate);


--
-- TOC entry 3978 (class 2606 OID 28545)
-- Name: rp_hcropparcel pk_rp_hcropparcel; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropparcel
    ADD CONSTRAINT pk_rp_hcropparcel PRIMARY KEY (rcp_id, rev);


--
-- TOC entry 3980 (class 2606 OID 28547)
-- Name: rp_hcropparcelgeom pk_rp_hcropparcelgeom; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropparcelgeom
    ADD CONSTRAINT pk_rp_hcropparcelgeom PRIMARY KEY (rcp_id, cpg_dateto, rev);


--
-- TOC entry 3982 (class 2606 OID 28549)
-- Name: rp_hcropphytosanit pk_rp_hcropphytosanit; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropphytosanit
    ADD CONSTRAINT pk_rp_hcropphytosanit PRIMARY KEY (cph_startdate, rev, rcp_id, phy_id, cou_id);


--
-- TOC entry 3984 (class 2606 OID 28551)
-- Name: rp_hgeometry pk_rp_hgeometry; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hgeometry
    ADD CONSTRAINT pk_rp_hgeometry PRIMARY KEY (rfp_id, rev, rge_datefrom);


--
-- TOC entry 3986 (class 2606 OID 28553)
-- Name: rp_hrefplot pk_rp_hrefplot; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hrefplot
    ADD CONSTRAINT pk_rp_hrefplot PRIMARY KEY (rfp_id, rev);


--
-- TOC entry 4076 (class 2606 OID 28645)
-- Name: sec_drol pk_sec_drol; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_drol
    ADD CONSTRAINT pk_sec_drol PRIMARY KEY (rol_id);


--
-- TOC entry 4078 (class 2606 OID 28647)
-- Name: sec_duser pk_sec_duser; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_duser
    ADD CONSTRAINT pk_sec_duser PRIMARY KEY (usr_id);


--
-- TOC entry 4080 (class 2606 OID 28649)
-- Name: sec_duser_rol pk_sec_duser_rol; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_duser_rol
    ADD CONSTRAINT pk_sec_duser_rol PRIMARY KEY (rol_id, usr_id);


--
-- TOC entry 4038 (class 2606 OID 28607)
-- Name: pr_dfarmer ux_pr_dfarmer_code; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmer
    ADD CONSTRAINT ux_pr_dfarmer_code UNIQUE (far_code);


--
-- TOC entry 4048 (class 2606 OID 28617)
-- Name: pr_dproductionunit ux_pr_dproductionunit_code; Type: CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dproductionunit
    ADD CONSTRAINT ux_pr_dproductionunit_code UNIQUE (pun_code);


--
-- TOC entry 4083 (class 2606 OID 28650)
-- Name: ab_dagrblock fk_ab_dagrblock_pr_dproductionunit; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dagrblock
    ADD CONSTRAINT fk_ab_dagrblock_pr_dproductionunit FOREIGN KEY (pun_id) REFERENCES sc_niva.pr_dproductionunit(pun_id);


--
-- TOC entry 4084 (class 2606 OID 28655)
-- Name: ab_dgeometry fk_ab_dgeometry_ab_dagrblock; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dgeometry
    ADD CONSTRAINT fk_ab_dgeometry_ab_dagrblock FOREIGN KEY (abl_id) REFERENCES sc_niva.ab_dagrblock(abl_id);


--
-- TOC entry 4085 (class 2606 OID 28660)
-- Name: ab_dlandcover fk_ab_dlandcover_ab_dagrblock; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dlandcover
    ADD CONSTRAINT fk_ab_dlandcover_ab_dagrblock FOREIGN KEY (abl_id) REFERENCES sc_niva.ab_dagrblock(abl_id);


--
-- TOC entry 4086 (class 2606 OID 28665)
-- Name: ab_dlandcover fk_ab_dlandcover_co_plandcover; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_dlandcover
    ADD CONSTRAINT fk_ab_dlandcover_co_plandcover FOREIGN KEY (lan_id) REFERENCES sc_niva.co_plandcover(lan_id);


--
-- TOC entry 4087 (class 2606 OID 28670)
-- Name: ab_hagrblock fk_ab_hagrblock_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hagrblock
    ADD CONSTRAINT fk_ab_hagrblock_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4088 (class 2606 OID 28675)
-- Name: ab_hgeometry fk_ab_hgeometry_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hgeometry
    ADD CONSTRAINT fk_ab_hgeometry_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4089 (class 2606 OID 28680)
-- Name: ab_hlandcover fk_ab_hlandcover_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.ab_hlandcover
    ADD CONSTRAINT fk_ab_hlandcover_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4108 (class 2606 OID 28785)
-- Name: co_pactmats fk_co_pactmats_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactmats
    ADD CONSTRAINT fk_co_pactmats_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);

--
-- TOC entry 4108 (class 2606 OID 28785)
-- Name: co_pactmats fk_co_pactmats_co_pphytosanitary; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactmats
    ADD CONSTRAINT fk_co_pactmats_co_pphytosanitary FOREIGN KEY (phy_id) REFERENCES sc_niva.co_pphytosanitary(phy_id);

--
-- TOC entry 4108 (class 2606 OID 28785)
-- Name: co_pactmats fk_co_pactmats_co_punittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactmats
    ADD CONSTRAINT fk_co_pactmats_co_punittype FOREIGN KEY (uni_id) REFERENCES sc_niva.co_punittype(uni_id);

--
-- TOC entry 4108 (class 2606 OID 28785)
-- Name: co_pactmats fk_co_pactmats_co_pactsus; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pactmats
    ADD CONSTRAINT fk_co_pactmats_co_pactsus FOREIGN KEY (acs_id) REFERENCES sc_niva.co_pactsus(acs_id);

--
-- TOC entry 4106 (class 2606 OID 28765)
-- Name: co_pcropprodgroup fk_co_pcropprodgroup_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropprodgroup
    ADD CONSTRAINT fk_co_pcropprodgroup_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4105 (class 2606 OID 28760)
-- Name: co_pcropproduct fk_co_pcropproduct_co_pcropprodgroup; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropproduct
    ADD CONSTRAINT fk_co_pcropproduct_co_pcropprodgroup FOREIGN KEY (cpg_id) REFERENCES sc_niva.co_pcropprodgroup(cpg_id);

--
-- TOC entry 4105 (class 2606 OID 28760)
-- Name: co_pcropproduct fk_co_pcropproduct_co_plandcover; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropproduct
    ADD CONSTRAINT fk_co_pcropproduct_co_pland_cover FOREIGN KEY (lan_id) REFERENCES sc_niva.co_plandcover(lan_id);



--
-- TOC entry 4152 (class 2606 OID 29052)
-- Name: co_pcropvariety fk_co_pcropvariety_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropvariety
    ADD CONSTRAINT fk_co_pcropvariety_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4153 (class 2606 OID 29772)
-- Name: co_pcropvariety fk_co_pcropvariety_co_pcropproduct_02; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pcropvariety
    ADD CONSTRAINT fk_co_pcropvariety_co_pcropproduct_02 FOREIGN KEY (crp_id) REFERENCES sc_niva.co_pcropproduct(crp_id);


--
-- TOC entry 4107 (class 2606 OID 28780)
-- Name: co_pfertilizer fk_co_pfertilizer_co_pfertilizertype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pfertilizer
    ADD CONSTRAINT fk_co_pfertilizer_co_pfertilizertype FOREIGN KEY (fet_id) REFERENCES sc_niva.co_pfertilizertype(fet_id);


--
-- TOC entry 4108 (class 2606 OID 28785)
-- Name: co_pphytosanitary fk_co_pphytosanitary_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.co_pphytosanitary
    ADD CONSTRAINT fk_co_pphytosanitary_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4109 (class 2606 OID 28790)
-- Name: pr_dfarmer fk_pr_dfarmer_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmer
    ADD CONSTRAINT fk_pr_dfarmer_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4110 (class 2606 OID 28795)
-- Name: pr_dfarmerdata fk_pr_dfarmerdata_co_pfarmertype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmerdata
    ADD CONSTRAINT fk_pr_dfarmerdata_co_pfarmertype FOREIGN KEY (fat_id) REFERENCES sc_niva.co_pfarmertype(fat_id);


--
-- TOC entry 4111 (class 2606 OID 28800)
-- Name: pr_dfarmerdata fk_pr_dfarmerdata_pr_dfarmer; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmerdata
    ADD CONSTRAINT fk_pr_dfarmerdata_pr_dfarmer FOREIGN KEY (far_id) REFERENCES sc_niva.pr_dfarmer(far_id);


--
-- TOC entry 4112 (class 2606 OID 28805)
-- Name: pr_dfarmingtype fk_pr_dfarmingtype_co_pfarmingtype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmingtype
    ADD CONSTRAINT fk_pr_dfarmingtype_co_pfarmingtype FOREIGN KEY (frt_id) REFERENCES sc_niva.co_pfarmingtype(frt_id);


--
-- TOC entry 4113 (class 2606 OID 28810)
-- Name: pr_dfarmingtype fk_pr_dfarmingtype_pr_dproductionunit; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dfarmingtype
    ADD CONSTRAINT fk_pr_dfarmingtype_pr_dproductionunit FOREIGN KEY (pun_id) REFERENCES sc_niva.pr_dproductionunit(pun_id);


--
-- TOC entry 4114 (class 2606 OID 28815)
-- Name: pr_dproductionunit fk_pr_dproductionunit_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dproductionunit
    ADD CONSTRAINT fk_pr_dproductionunit_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4115 (class 2606 OID 28820)
-- Name: pr_dproductionunit fk_pr_dproductionunit_pr_dfarmer; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dproductionunit
    ADD CONSTRAINT fk_pr_dproductionunit_pr_dfarmer FOREIGN KEY (far_id) REFERENCES sc_niva.pr_dfarmer(far_id);


--
-- TOC entry 4116 (class 2606 OID 28825)
-- Name: pr_dprodworkunit fk_pr_dprodworkunit_co_pworkunittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dprodworkunit
    ADD CONSTRAINT fk_pr_dprodworkunit_co_pworkunittype FOREIGN KEY (wut_id) REFERENCES sc_niva.co_pworkunittype(wut_id);


--
-- TOC entry 4117 (class 2606 OID 28830)
-- Name: pr_dprodworkunit fk_pr_dprodworkunit_pr_dproductionunit; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_dprodworkunit
    ADD CONSTRAINT fk_pr_dprodworkunit_pr_dproductionunit FOREIGN KEY (pun_id) REFERENCES sc_niva.pr_dproductionunit(pun_id);


--
-- TOC entry 4090 (class 2606 OID 28685)
-- Name: pr_hfarmer fk_pr_hfarmer_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmer
    ADD CONSTRAINT fk_pr_hfarmer_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4091 (class 2606 OID 28690)
-- Name: pr_hfarmercontact fk_pr_hfarmercontact_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmercontact
    ADD CONSTRAINT fk_pr_hfarmercontact_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4092 (class 2606 OID 28695)
-- Name: pr_hfarmerdata fk_pr_hfarmerdata_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmerdata
    ADD CONSTRAINT fk_pr_hfarmerdata_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4093 (class 2606 OID 28700)
-- Name: pr_hfarmingtype fk_pr_hfarmingtype_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hfarmingtype
    ADD CONSTRAINT fk_pr_hfarmingtype_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4094 (class 2606 OID 28705)
-- Name: pr_hproductionunit fk_pr_hproductionunit_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hproductionunit
    ADD CONSTRAINT fk_pr_hproductionunit_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4095 (class 2606 OID 28710)
-- Name: pr_hprodworkunit fk_pr_hprodworkunit_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.pr_hprodworkunit
    ADD CONSTRAINT fk_pr_hprodworkunit_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4137 (class 2606 OID 29763)
-- Name: rp_dcropparcel fk_rp_cropparcel_co_pcropproduct; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcel
    ADD CONSTRAINT fk_rp_cropparcel_co_pcropproduct FOREIGN KEY (crp_id) REFERENCES sc_niva.co_pcropproduct(crp_id);


--
-- TOC entry 4118 (class 2606 OID 28835)
-- Name: rp_dcropactivity fk_rp_dcropactivity_co_pcultivationdet; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT fk_rp_dcropactivity_co_pcultivationdet FOREIGN KEY (cud_id) REFERENCES sc_niva.co_pcultivationdet(cud_id);


--
-- TOC entry 4119 (class 2606 OID 28840)
-- Name: rp_dcropactivity fk_rp_dcropactivity_co_pcultivationsys; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT fk_rp_dcropactivity_co_pcultivationsys FOREIGN KEY (cus_id) REFERENCES sc_niva.co_pcultivationsys(cus_id);


--
-- TOC entry 4120 (class 2606 OID 28845)
-- Name: rp_dcropactivity fk_rp_dcropactivity_co_porganicfarming; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT fk_rp_dcropactivity_co_porganicfarming FOREIGN KEY (orf_id) REFERENCES sc_niva.co_porganicfarming(orf_id);


--
-- TOC entry 4121 (class 2606 OID 28850)
-- Name: rp_dcropactivity fk_rp_dcropactivity_co_pseedtype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT fk_rp_dcropactivity_co_pseedtype FOREIGN KEY (see_id) REFERENCES sc_niva.co_pseedtype(see_id);


--
-- TOC entry 4122 (class 2606 OID 28855)
-- Name: rp_dcropactivity fk_rp_dcropactivity_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivity
    ADD CONSTRAINT fk_rp_dcropactivity_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4123 (class 2606 OID 28860)
-- Name: rp_dcropactivitydet fk_rp_dcropactivitydet_co_plabortype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivitydet
    ADD CONSTRAINT fk_rp_dcropactivitydet_co_plabortype FOREIGN KEY (lab_id) REFERENCES sc_niva.co_plabortype(lab_id);


--
-- TOC entry 4124 (class 2606 OID 28865)
-- Name: rp_dcropactivitydet fk_rp_dcropactivitydet_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropactivitydet
    ADD CONSTRAINT fk_rp_dcropactivitydet_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4127 (class 2606 OID 28880)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_co_pfertilizer; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_co_pfertilizer FOREIGN KEY (fer_id) REFERENCES sc_niva.co_pfertilizer(fer_id);


--
-- TOC entry 4128 (class 2606 OID 28885)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_co_pfertilizertype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_co_pfertilizertype FOREIGN KEY (fet_id) REFERENCES sc_niva.co_pfertilizertype(fet_id);


--
-- TOC entry 4125 (class 2606 OID 28870)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_co_pfertilizmethod; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_co_pfertilizmethod FOREIGN KEY (fem_id) REFERENCES sc_niva.co_pfertilizmethod(fem_id);


--
-- TOC entry 4126 (class 2606 OID 28875)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_co_pfertiliztype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_co_pfertiliztype FOREIGN KEY (frt_id) REFERENCES sc_niva.co_pfertiliztype(frt_id);


--
-- TOC entry 4129 (class 2606 OID 28890)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_co_punittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_co_punittype FOREIGN KEY (uni_id) REFERENCES sc_niva.co_punittype(uni_id);


--
-- TOC entry 4130 (class 2606 OID 28895)
-- Name: rp_dcropfertilizer fk_rp_dcropfertilizer_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropfertilizer
    ADD CONSTRAINT fk_rp_dcropfertilizer_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4131 (class 2606 OID 28900)
-- Name: rp_dcropirrigation fk_rp_dcropirrigation_co_pirrigationtype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropirrigation
    ADD CONSTRAINT fk_rp_dcropirrigation_co_pirrigationtype FOREIGN KEY (irr_id) REFERENCES sc_niva.co_pirrigationtype(irr_id);


--
-- TOC entry 4132 (class 2606 OID 28905)
-- Name: rp_dcropirrigation fk_rp_dcropirrigation_co_punittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropirrigation
    ADD CONSTRAINT fk_rp_dcropirrigation_co_punittype FOREIGN KEY (uni_id) REFERENCES sc_niva.co_punittype(uni_id);


--
-- TOC entry 4133 (class 2606 OID 28910)
-- Name: rp_dcropirrigation fk_rp_dcropirrigation_co_pwaterorigin; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropirrigation
    ADD CONSTRAINT fk_rp_dcropirrigation_co_pwaterorigin FOREIGN KEY (wao_id) REFERENCES sc_niva.co_pwaterorigin(wao_id);


--
-- TOC entry 4134 (class 2606 OID 28915)
-- Name: rp_dcropirrigation fk_rp_dcropirrigation_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropirrigation
    ADD CONSTRAINT fk_rp_dcropirrigation_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4135 (class 2606 OID 28925)
-- Name: rp_dcropparcel fk_rp_dcropparcel_co_plandtenure; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcel
    ADD CONSTRAINT fk_rp_dcropparcel_co_plandtenure FOREIGN KEY (lte_id) REFERENCES sc_niva.co_plandtenure(lte_id);


--
-- TOC entry 4136 (class 2606 OID 28930)
-- Name: rp_dcropparcel fk_rp_dcropparcel_rp_drefplot; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcel
    ADD CONSTRAINT fk_rp_dcropparcel_rp_drefplot FOREIGN KEY (rfp_id) REFERENCES sc_niva.rp_drefplot(rfp_id);


--
-- TOC entry 4138 (class 2606 OID 28935)
-- Name: rp_dcropparcelgeom fk_rp_dcropparcelgeom_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropparcelgeom
    ADD CONSTRAINT fk_rp_dcropparcelgeom_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4139 (class 2606 OID 28940)
-- Name: rp_dcropphytosanit fk_rp_dcropphytosanit_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT fk_rp_dcropphytosanit_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4141 (class 2606 OID 28950)
-- Name: rp_dcropphytosanit fk_rp_dcropphytosanit_co_pphytosanequip; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT fk_rp_dcropphytosanit_co_pphytosanequip FOREIGN KEY (phe_id) REFERENCES sc_niva.co_pphytosanequip(phe_id);


--
-- TOC entry 4140 (class 2606 OID 28945)
-- Name: rp_dcropphytosanit fk_rp_dcropphytosanit_co_pphytosanitary; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT fk_rp_dcropphytosanit_co_pphytosanitary FOREIGN KEY (phy_id) REFERENCES sc_niva.co_pphytosanitary(phy_id);


--
-- TOC entry 4142 (class 2606 OID 28955)
-- Name: rp_dcropphytosanit fk_rp_dcropphytosanit_co_punittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT fk_rp_dcropphytosanit_co_punittype FOREIGN KEY (uni_id) REFERENCES sc_niva.co_punittype(uni_id);


--
-- TOC entry 4143 (class 2606 OID 28960)
-- Name: rp_dcropphytosanit fk_rp_dcropphytosanit_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dcropphytosanit
    ADD CONSTRAINT fk_rp_dcropphytosanit_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4144 (class 2606 OID 28965)
-- Name: rp_dfertilizrec fk_rp_dfertilizrec_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dfertilizrec
    ADD CONSTRAINT fk_rp_dfertilizrec_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4145 (class 2606 OID 28970)
-- Name: rp_dgeometry fk_rp_dgeometry_rp_drefplot; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dgeometry
    ADD CONSTRAINT fk_rp_dgeometry_rp_drefplot FOREIGN KEY (rfp_id) REFERENCES sc_niva.rp_drefplot(rfp_id);


--
-- TOC entry 4146 (class 2606 OID 28975)
-- Name: rp_dirrigationrec fk_rp_dirrigationrec_co_punittype; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dirrigationrec
    ADD CONSTRAINT fk_rp_dirrigationrec_co_punittype FOREIGN KEY (uni_id) REFERENCES sc_niva.co_punittype(uni_id);


--
-- TOC entry 4147 (class 2606 OID 28980)
-- Name: rp_dirrigationrec fk_rp_dirrigationrec_rp_dcropparcel; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_dirrigationrec
    ADD CONSTRAINT fk_rp_dirrigationrec_rp_dcropparcel FOREIGN KEY (rcp_id) REFERENCES sc_niva.rp_dcropparcel(rcp_id);


--
-- TOC entry 4148 (class 2606 OID 28985)
-- Name: rp_drefplot fk_rp_drefplot_ab_dagrblock; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_drefplot
    ADD CONSTRAINT fk_rp_drefplot_ab_dagrblock FOREIGN KEY (abl_id) REFERENCES sc_niva.ab_dagrblock(abl_id);


--
-- TOC entry 4096 (class 2606 OID 28715)
-- Name: rp_hcropactivity fk_rp_hcropactivity_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropactivity
    ADD CONSTRAINT fk_rp_hcropactivity_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4097 (class 2606 OID 28720)
-- Name: rp_hcropactivitydet fk_rp_hcropactivitydet_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropactivitydet
    ADD CONSTRAINT fk_rp_hcropactivitydet_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4098 (class 2606 OID 28725)
-- Name: rp_hcropfertilizer fk_rp_hcropfertilizer_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropfertilizer
    ADD CONSTRAINT fk_rp_hcropfertilizer_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4099 (class 2606 OID 28730)
-- Name: rp_hcropirrigation fk_rp_hcropirrigation_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropirrigation
    ADD CONSTRAINT fk_rp_hcropirrigation_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4100 (class 2606 OID 28735)
-- Name: rp_hcropparcel fk_rp_hcropparcel_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropparcel
    ADD CONSTRAINT fk_rp_hcropparcel_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4101 (class 2606 OID 28740)
-- Name: rp_hcropparcelgeom fk_rp_hcropparcelgeom_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropparcelgeom
    ADD CONSTRAINT fk_rp_hcropparcelgeom_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4102 (class 2606 OID 28745)
-- Name: rp_hcropphytosanit fk_rp_hcropphytosanit_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hcropphytosanit
    ADD CONSTRAINT fk_rp_hcropphytosanit_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4103 (class 2606 OID 28750)
-- Name: rp_hgeometry fk_rp_hgeometry_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hgeometry
    ADD CONSTRAINT fk_rp_hgeometry_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4104 (class 2606 OID 28755)
-- Name: rp_hrefplot fk_rp_hrefplot_rev_drevinfo; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.rp_hrefplot
    ADD CONSTRAINT fk_rp_hrefplot_rev_drevinfo FOREIGN KEY (rev) REFERENCES sc_niva.rev_drevinfo(rev);


--
-- TOC entry 4149 (class 2606 OID 28990)
-- Name: sec_duser fk_sec_duser_co_pcountry; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_duser
    ADD CONSTRAINT fk_sec_duser_co_pcountry FOREIGN KEY (cou_id) REFERENCES sc_niva.co_pcountry(cou_id);


--
-- TOC entry 4150 (class 2606 OID 28995)
-- Name: sec_duser_rol fk_sec_duser_rol_sec_drol; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_duser_rol
    ADD CONSTRAINT fk_sec_duser_rol_sec_drol FOREIGN KEY (rol_id) REFERENCES sc_niva.sec_drol(rol_id);


--
-- TOC entry 4151 (class 2606 OID 29000)
-- Name: sec_duser_rol fk_sec_duser_rol_sec_duser; Type: FK CONSTRAINT; Schema: sc_niva; Owner: niva
--

ALTER TABLE ONLY sc_niva.sec_duser_rol
    ADD CONSTRAINT fk_sec_duser_rol_sec_duser FOREIGN KEY (usr_id) REFERENCES sc_niva.sec_duser(usr_id);


--
-- TOC entry 4291 (class 0 OID 0)
-- Dependencies: 9
-- Name: SCHEMA sc_niva; Type: ACL; Schema: -; Owner: niva
--

-- GRANT USAGE ON SCHEMA sc_niva TO niva_ap;


-- Completed on 2021-04-21 09:50:35

--
-- PostgreSQL database dump complete
--

