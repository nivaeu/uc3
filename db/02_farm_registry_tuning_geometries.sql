ALTER TABLE sc_niva.AB_dgeometry ALTER COLUMN AGE_geometry TYPE sc_niva.geometry(geometry, 4326);
ALTER TABLE sc_niva.RP_dgeometry ALTER COLUMN RGE_geometry TYPE sc_niva.geometry(geometry, 4326);
ALTER TABLE sc_niva.RP_dcropparcelgeom ALTER COLUMN CPG_geometry TYPE sc_niva.geometry(geometry, 4326);
