# Swagger\Client\DeleteEntitiesApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteByCodeUsingDELETE**](DeleteEntitiesApi.md#deleteByCodeUsingDELETE) | **DELETE** /delete/agriculturalProducerParties/country/{country}/personalID/{personalID}/code/{code} | Deletes an Agricultural Producer Party by its country, personalID and far_code
[**deleteUsingDELETE**](DeleteEntitiesApi.md#deleteUsingDELETE) | **DELETE** /delete/agriculturalProducerParties/code/{code} | Deletes an Agricultural Producer Party by its code
[**deleteUsingDELETE1**](DeleteEntitiesApi.md#deleteUsingDELETE1) | **DELETE** /delete/agriculturalProducerParties/country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey}/agriculturalBlock/{agrBlock}/referencePlot/{refPlot} | Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock
[**deleteUsingDELETE2**](DeleteEntitiesApi.md#deleteUsingDELETE2) | **DELETE** /delete/agriculturalProducerParties/country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey}/agriculturalBlock/{agrBlock} | Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock
[**deleteUsingDELETE3**](DeleteEntitiesApi.md#deleteUsingDELETE3) | **DELETE** /delete/agriculturalProducerParties/country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey} | Deletes an Agricultural Producer Party by its country, personalID and APUKey
[**deleteUsingDELETE4**](DeleteEntitiesApi.md#deleteUsingDELETE4) | **DELETE** /delete/agriculturalProducerParties/country/{country}/personalID/{personalID} | Deletes an Agricultural Producer Party by its country and personalID


# **deleteByCodeUsingDELETE**
> \Swagger\Client\Model\Response deleteByCodeUsingDELETE($code, $country, $personal_id)

Deletes an Agricultural Producer Party by its country, personalID and far_code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->deleteByCodeUsingDELETE($code, $country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteByCodeUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsingDELETE**
> \Swagger\Client\Model\Response deleteUsingDELETE($code)

Deletes an Agricultural Producer Party by its code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code

try {
    $result = $apiInstance->deleteUsingDELETE($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteUsingDELETE: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsingDELETE1**
> \Swagger\Client\Model\Response deleteUsingDELETE1($agr_block, $apu_key, $country, $personal_id, $ref_plot)

Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agr_block = "agr_block_example"; // string | agrBlock
$apu_key = "apu_key_example"; // string | apuKey
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID
$ref_plot = "ref_plot_example"; // string | refPlot

try {
    $result = $apiInstance->deleteUsingDELETE1($agr_block, $apu_key, $country, $personal_id, $ref_plot);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteUsingDELETE1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agr_block** | **string**| agrBlock |
 **apu_key** | **string**| apuKey |
 **country** | **string**| country |
 **personal_id** | **string**| personalID |
 **ref_plot** | **string**| refPlot |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsingDELETE2**
> \Swagger\Client\Model\Response deleteUsingDELETE2($agr_block, $apu_key, $country, $personal_id)

Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agr_block = "agr_block_example"; // string | agrBlock
$apu_key = "apu_key_example"; // string | apuKey
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->deleteUsingDELETE2($agr_block, $apu_key, $country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteUsingDELETE2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agr_block** | **string**| agrBlock |
 **apu_key** | **string**| apuKey |
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsingDELETE3**
> \Swagger\Client\Model\Response deleteUsingDELETE3($apu_key, $country, $personal_id)

Deletes an Agricultural Producer Party by its country, personalID and APUKey

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$apu_key = "apu_key_example"; // string | apuKey
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->deleteUsingDELETE3($apu_key, $country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteUsingDELETE3: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apu_key** | **string**| apuKey |
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUsingDELETE4**
> \Swagger\Client\Model\Response deleteUsingDELETE4($country, $personal_id)

Deletes an Agricultural Producer Party by its country and personalID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\DeleteEntitiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->deleteUsingDELETE4($country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DeleteEntitiesApi->deleteUsingDELETE4: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

