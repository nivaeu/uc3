# Swagger\Client\CropPlotsApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCodeListUsingGET1**](CropPlotsApi.md#getCodeListUsingGET1) | **GET** /query/cropPlots | Returns all Crop Plots code list
[**getCropPlotsByFarmerKeyUsingGET**](CropPlotsApi.md#getCropPlotsByFarmerKeyUsingGET) | **GET** /query/cropPlots/country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey}/key/{key} | Returns all Crop Plots by farmer personalID
[**getCropPlotsByFarmerUsingGET**](CropPlotsApi.md#getCropPlotsByFarmerUsingGET) | **GET** /query/cropPlots/country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey} | Returns all Crop Plots by farmer personalID
[**getUsingGET7**](CropPlotsApi.md#getUsingGET7) | **GET** /query/cropPlots/details | Returns all Crop Plots


# **getCodeListUsingGET1**
> \Swagger\Client\Model\CropPlot[] getCodeListUsingGET1()

Returns all Crop Plots code list

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\CropPlotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCodeListUsingGET1();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CropPlotsApi->getCodeListUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CropPlot[]**](../Model/CropPlot.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCropPlotsByFarmerKeyUsingGET**
> \Swagger\Client\Model\CropPlot[] getCropPlotsByFarmerKeyUsingGET($agricultural_production_unit_key, $country, $key, $personal_id)

Returns all Crop Plots by farmer personalID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\CropPlotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agricultural_production_unit_key = "agricultural_production_unit_key_example"; // string | AgriculturalProductionUnitKey
$country = "country_example"; // string | country
$key = "key_example"; // string | key
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->getCropPlotsByFarmerKeyUsingGET($agricultural_production_unit_key, $country, $key, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CropPlotsApi->getCropPlotsByFarmerKeyUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agricultural_production_unit_key** | **string**| AgriculturalProductionUnitKey |
 **country** | **string**| country |
 **key** | **string**| key |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\CropPlot[]**](../Model/CropPlot.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCropPlotsByFarmerUsingGET**
> \Swagger\Client\Model\CropPlot[] getCropPlotsByFarmerUsingGET($agricultural_production_unit_key, $country, $personal_id)

Returns all Crop Plots by farmer personalID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\CropPlotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$agricultural_production_unit_key = "agricultural_production_unit_key_example"; // string | AgriculturalProductionUnitKey
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->getCropPlotsByFarmerUsingGET($agricultural_production_unit_key, $country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CropPlotsApi->getCropPlotsByFarmerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agricultural_production_unit_key** | **string**| AgriculturalProductionUnitKey |
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\CropPlot[]**](../Model/CropPlot.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUsingGET7**
> \Swagger\Client\Model\CropPlot[] getUsingGET7()

Returns all Crop Plots

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\CropPlotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getUsingGET7();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CropPlotsApi->getUsingGET7: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\CropPlot[]**](../Model/CropPlot.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

