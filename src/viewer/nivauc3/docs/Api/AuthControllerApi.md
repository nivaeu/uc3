# Swagger\Client\AuthControllerApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticateUserUsingPOST**](AuthControllerApi.md#authenticateUserUsingPOST) | **POST** /auth/login | authenticateUser
[**registerUserUsingPOST**](AuthControllerApi.md#registerUserUsingPOST) | **POST** /auth/register | Only available with Admin role


# **authenticateUserUsingPOST**
> object authenticateUserUsingPOST($login_request)

authenticateUser

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AuthControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login_request = new \Swagger\Client\Model\LoginRequest(); // \Swagger\Client\Model\LoginRequest | loginRequest

try {
    $result = $apiInstance->authenticateUserUsingPOST($login_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthControllerApi->authenticateUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login_request** | [**\Swagger\Client\Model\LoginRequest**](../Model/LoginRequest.md)| loginRequest |

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **registerUserUsingPOST**
> object registerUserUsingPOST($sign_up_request)

Only available with Admin role

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AuthControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sign_up_request = new \Swagger\Client\Model\SignupRequest(); // \Swagger\Client\Model\SignupRequest | signUpRequest

try {
    $result = $apiInstance->registerUserUsingPOST($sign_up_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthControllerApi->registerUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sign_up_request** | [**\Swagger\Client\Model\SignupRequest**](../Model/SignupRequest.md)| signUpRequest |

### Return type

**object**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

