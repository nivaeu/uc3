# Swagger\Client\ReferencePlotsApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUsingGET9**](ReferencePlotsApi.md#getUsingGET9) | **GET** /query/referencePlots/details | Returns all Reference Plots


# **getUsingGET9**
> \Swagger\Client\Model\ReferencePlot[] getUsingGET9()

Returns all Reference Plots

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ReferencePlotsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getUsingGET9();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ReferencePlotsApi->getUsingGET9: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\ReferencePlot[]**](../Model/ReferencePlot.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

