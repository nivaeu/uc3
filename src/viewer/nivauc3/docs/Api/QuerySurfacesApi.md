# Swagger\Client\QuerySurfacesApi

All URIs are relative to *https://svjc-pre-niva.ttec.es/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSurfaceABByFarmerIrrigationUsingGET**](QuerySurfacesApi.md#getSurfaceABByFarmerIrrigationUsingGET) | **GET** /query/surfaces/farmer/{farmer}/irrigation/{irrigation} | Returns total AB surface by farmer and irrigation
[**getSurfaceABByFarmerLandCoverUsingGET**](QuerySurfacesApi.md#getSurfaceABByFarmerLandCoverUsingGET) | **GET** /query/surfaces/farmer/{farmer}/landCover/{landCover} | Returns total AB surface by farmer and land cover
[**getSurfaceABByFarmerUsingGET**](QuerySurfacesApi.md#getSurfaceABByFarmerUsingGET) | **GET** /query/surfaces/farmer/{farmer}/agrblock | Returns total AB surface by farmer
[**getSurfaceABByLandCoverUsingGET**](QuerySurfacesApi.md#getSurfaceABByLandCoverUsingGET) | **GET** /query/surfaces/landCover/{landCover} | Returns total AB surface by farmer and land cover
[**getSurfaceABUsingGET**](QuerySurfacesApi.md#getSurfaceABUsingGET) | **GET** /query/surfaces/agrblock | Returns total AB surface
[**getSurfaceByIrrigationUsingGET**](QuerySurfacesApi.md#getSurfaceByIrrigationUsingGET) | **GET** /query/surfaces/irrigation/{irrigation} | Returns total surface by irrigation
[**getSurfaceCPUsingGET**](QuerySurfacesApi.md#getSurfaceCPUsingGET) | **GET** /query/surfaces/cropparcel | Returns total CP surface
[**getSurfaceFCByFarmerUsingGET**](QuerySurfacesApi.md#getSurfaceFCByFarmerUsingGET) | **GET** /query/surfaces/farmer/{farmer}/cropparcel | Returns total CP surface by farmer
[**getSurfaceRPByFarmerUsingGET**](QuerySurfacesApi.md#getSurfaceRPByFarmerUsingGET) | **GET** /query/surfaces/farmer/{farmer}/refplot | Returns total RP surface by farmer
[**getSurfaceRPUsingGET**](QuerySurfacesApi.md#getSurfaceRPUsingGET) | **GET** /query/surfaces/refplot | Returns total RP surface


# **getSurfaceABByFarmerIrrigationUsingGET**
> double getSurfaceABByFarmerIrrigationUsingGET($farmer, $irrigation)

Returns total AB surface by farmer and irrigation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$farmer = "farmer_example"; // string | farmer
$irrigation = 56; // int | irrigation

try {
    $result = $apiInstance->getSurfaceABByFarmerIrrigationUsingGET($farmer, $irrigation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceABByFarmerIrrigationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **farmer** | **string**| farmer |
 **irrigation** | **int**| irrigation |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceABByFarmerLandCoverUsingGET**
> double getSurfaceABByFarmerLandCoverUsingGET($farmer, $land_cover)

Returns total AB surface by farmer and land cover

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$farmer = "farmer_example"; // string | farmer
$land_cover = 56; // int | landCover

try {
    $result = $apiInstance->getSurfaceABByFarmerLandCoverUsingGET($farmer, $land_cover);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceABByFarmerLandCoverUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **farmer** | **string**| farmer |
 **land_cover** | **int**| landCover |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceABByFarmerUsingGET**
> double getSurfaceABByFarmerUsingGET($farmer)

Returns total AB surface by farmer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$farmer = "farmer_example"; // string | farmer

try {
    $result = $apiInstance->getSurfaceABByFarmerUsingGET($farmer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceABByFarmerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **farmer** | **string**| farmer |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceABByLandCoverUsingGET**
> double getSurfaceABByLandCoverUsingGET($land_cover)

Returns total AB surface by farmer and land cover

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$land_cover = 56; // int | landCover

try {
    $result = $apiInstance->getSurfaceABByLandCoverUsingGET($land_cover);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceABByLandCoverUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **land_cover** | **int**| landCover |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceABUsingGET**
> double getSurfaceABUsingGET()

Returns total AB surface

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getSurfaceABUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceABUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceByIrrigationUsingGET**
> double getSurfaceByIrrigationUsingGET($irrigation)

Returns total surface by irrigation

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$irrigation = 56; // int | irrigation

try {
    $result = $apiInstance->getSurfaceByIrrigationUsingGET($irrigation);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceByIrrigationUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **irrigation** | **int**| irrigation |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceCPUsingGET**
> double getSurfaceCPUsingGET()

Returns total CP surface

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getSurfaceCPUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceCPUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceFCByFarmerUsingGET**
> double getSurfaceFCByFarmerUsingGET($farmer)

Returns total CP surface by farmer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$farmer = "farmer_example"; // string | farmer

try {
    $result = $apiInstance->getSurfaceFCByFarmerUsingGET($farmer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceFCByFarmerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **farmer** | **string**| farmer |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceRPByFarmerUsingGET**
> double getSurfaceRPByFarmerUsingGET($farmer)

Returns total RP surface by farmer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$farmer = "farmer_example"; // string | farmer

try {
    $result = $apiInstance->getSurfaceRPByFarmerUsingGET($farmer);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceRPByFarmerUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **farmer** | **string**| farmer |

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getSurfaceRPUsingGET**
> double getSurfaceRPUsingGET()

Returns total RP surface

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getSurfaceRPUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QuerySurfacesApi->getSurfaceRPUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

**double**

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

