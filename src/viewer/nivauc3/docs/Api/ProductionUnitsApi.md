# Swagger\Client\ProductionUnitsApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUsingGET**](ProductionUnitsApi.md#deleteUsingGET) | **GET** /query/agriculturalProductionUnits/{code} | Deletes an Agricultural Production Unit by its code
[**getByCodeUsingGET1**](ProductionUnitsApi.md#getByCodeUsingGET1) | **GET** /query/agriculturalProductionUnits/code/{code} | Returns an Agricultural Production Unit by its code
[**getByCountryAndPersonalIDAndIntervalUsingGET**](ProductionUnitsApi.md#getByCountryAndPersonalIDAndIntervalUsingGET) | **GET** /query/agriculturalProductionUnits/country/{country}/personalID/{personalID}/dateFrom/{dateFrom}/dateTo/{dateTo} | Returns all Production Units by country code, personalID, dateFrom and dateTo
[**getByCountryAndPersonalIDUsingGET**](ProductionUnitsApi.md#getByCountryAndPersonalIDUsingGET) | **GET** /query/agriculturalProductionUnits/country/{country}/personalID/{personalID} | Returns all Production Units by country code and personalID
[**getCodeListUsingGET**](ProductionUnitsApi.md#getCodeListUsingGET) | **GET** /query/agriculturalProductionUnits | Returns all Agricultural Production Units
[**getUsingGET1**](ProductionUnitsApi.md#getUsingGET1) | **GET** /query/agriculturalProductionUnits/details | Returns all Agricultural Production Units


# **deleteUsingGET**
> \Swagger\Client\Model\Response deleteUsingGET($code)

Deletes an Agricultural Production Unit by its code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code

try {
    $result = $apiInstance->deleteUsingGET($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->deleteUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByCodeUsingGET1**
> \Swagger\Client\Model\AgriculturalProductionUnit getByCodeUsingGET1($code)

Returns an Agricultural Production Unit by its code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code

try {
    $result = $apiInstance->getByCodeUsingGET1($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->getByCodeUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |

### Return type

[**\Swagger\Client\Model\AgriculturalProductionUnit**](../Model/AgriculturalProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByCountryAndPersonalIDAndIntervalUsingGET**
> \Swagger\Client\Model\PrProductionUnit[] getByCountryAndPersonalIDAndIntervalUsingGET($country, $date_from, $dateto, $personal_id)

Returns all Production Units by country code, personalID, dateFrom and dateTo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$country = "country_example"; // string | country
$date_from = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | dateFrom
$dateto = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | dateto
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->getByCountryAndPersonalIDAndIntervalUsingGET($country, $date_from, $dateto, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->getByCountryAndPersonalIDAndIntervalUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| country |
 **date_from** | **\DateTime**| dateFrom |
 **dateto** | **\DateTime**| dateto |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\PrProductionUnit[]**](../Model/PrProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByCountryAndPersonalIDUsingGET**
> \Swagger\Client\Model\PrProductionUnit[] getByCountryAndPersonalIDUsingGET($country, $personal_id)

Returns all Production Units by country code and personalID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->getByCountryAndPersonalIDUsingGET($country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->getByCountryAndPersonalIDUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\PrProductionUnit[]**](../Model/PrProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCodeListUsingGET**
> \Swagger\Client\Model\AgriculturalProductionUnit[] getCodeListUsingGET($params)

Returns all Agricultural Production Units

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = new \stdClass; // object | params

try {
    $result = $apiInstance->getCodeListUsingGET($params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->getCodeListUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | [**object**](../Model/.md)| params |

### Return type

[**\Swagger\Client\Model\AgriculturalProductionUnit[]**](../Model/AgriculturalProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUsingGET1**
> \Swagger\Client\Model\AgriculturalProductionUnit[] getUsingGET1($params)

Returns all Agricultural Production Units

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductionUnitsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = new \stdClass; // object | params

try {
    $result = $apiInstance->getUsingGET1($params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductionUnitsApi->getUsingGET1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | [**object**](../Model/.md)| params |

### Return type

[**\Swagger\Client\Model\AgriculturalProductionUnit[]**](../Model/AgriculturalProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

