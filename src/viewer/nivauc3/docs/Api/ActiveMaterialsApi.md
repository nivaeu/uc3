# Swagger\Client\ActiveMaterialsApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUsingPOST**](ActiveMaterialsApi.md#createUsingPOST) | **POST** /load/dataList/ActiveMaterials | Inserts data into DB


# **createUsingPOST**
> \Swagger\Client\Model\Response createUsingPOST($data)

Inserts data into DB

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\ActiveMaterialsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Swagger\Client\Model\ActMatsMessage(); // \Swagger\Client\Model\ActMatsMessage | data

try {
    $result = $apiInstance->createUsingPOST($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ActiveMaterialsApi->createUsingPOST: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\ActMatsMessage**](../Model/ActMatsMessage.md)| data |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

