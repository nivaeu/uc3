# Swagger\Client\AgriculturalProducerPartiesApi

All URIs are relative to *https://svjc-pre-niva.ttec.es:8080/FarmRegistry*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUsingPOST5**](AgriculturalProducerPartiesApi.md#createUsingPOST5) | **POST** /load/agriculturalProducerParties | Inserts data into DB
[**getAgriculturalProductionUnitByCodeUsingGET**](AgriculturalProducerPartiesApi.md#getAgriculturalProductionUnitByCodeUsingGET) | **GET** /query/agriculturalProducerParties/code/{code}/AgriculturalProductionUnits/{productionUnitKey} | Returns an Agricultural Production Unit by its code of an Agricultural Producer Party by its key
[**getAgriculturalProductionUnitsByCodeUsingGET**](AgriculturalProducerPartiesApi.md#getAgriculturalProductionUnitsByCodeUsingGET) | **GET** /query/agriculturalProducerParties/code/{code}/AgriculturalProductionUnits | Returns all Agricultural Production Units of an Agricultural Producer Party by its code
[**getByCodeUsingGET**](AgriculturalProducerPartiesApi.md#getByCodeUsingGET) | **GET** /query/agriculturalProducerParties/code/{code} | Returns an Agricultural Producer Party by its Farm Registry Code
[**getByCountryUsingGET**](AgriculturalProducerPartiesApi.md#getByCountryUsingGET) | **GET** /query/agriculturalProducerParties/country/{country} | Returns an Agricultural Producer Party by its Country and Personal ID
[**getByPersonalIdCountryUsingGET**](AgriculturalProducerPartiesApi.md#getByPersonalIdCountryUsingGET) | **GET** /query/agriculturalProducerParties/country/{country}/personalID/{personalID} | Returns an Agricultural Producer Party by its Country and Personal ID
[**getDetailsUsingGET**](AgriculturalProducerPartiesApi.md#getDetailsUsingGET) | **GET** /query/agriculturalProducerParties/details | Returns all Agricultural Producer Parties main details
[**getUsingGET**](AgriculturalProducerPartiesApi.md#getUsingGET) | **GET** /query/agriculturalProducerParties | Returns all Agricultural Producer Parties codes


# **createUsingPOST5**
> \Swagger\Client\Model\Response createUsingPOST5($data)

Inserts data into DB

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$data = new \Swagger\Client\Model\CropReportMessage(); // \Swagger\Client\Model\CropReportMessage | data

try {
    $result = $apiInstance->createUsingPOST5($data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->createUsingPOST5: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**\Swagger\Client\Model\CropReportMessage**](../Model/CropReportMessage.md)| data |

### Return type

[**\Swagger\Client\Model\Response**](../Model/Response.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAgriculturalProductionUnitByCodeUsingGET**
> \Swagger\Client\Model\AgriculturalProductionUnit getAgriculturalProductionUnitByCodeUsingGET($code, $production_unit_key)

Returns an Agricultural Production Unit by its code of an Agricultural Producer Party by its key

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code
$production_unit_key = "production_unit_key_example"; // string | productionUnitKey

try {
    $result = $apiInstance->getAgriculturalProductionUnitByCodeUsingGET($code, $production_unit_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getAgriculturalProductionUnitByCodeUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |
 **production_unit_key** | **string**| productionUnitKey |

### Return type

[**\Swagger\Client\Model\AgriculturalProductionUnit**](../Model/AgriculturalProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAgriculturalProductionUnitsByCodeUsingGET**
> \Swagger\Client\Model\AgriculturalProductionUnit[] getAgriculturalProductionUnitsByCodeUsingGET($code)

Returns all Agricultural Production Units of an Agricultural Producer Party by its code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code

try {
    $result = $apiInstance->getAgriculturalProductionUnitsByCodeUsingGET($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getAgriculturalProductionUnitsByCodeUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |

### Return type

[**\Swagger\Client\Model\AgriculturalProductionUnit[]**](../Model/AgriculturalProductionUnit.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByCodeUsingGET**
> \Swagger\Client\Model\AgriculturalProducerParty getByCodeUsingGET($code)

Returns an Agricultural Producer Party by its Farm Registry Code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$code = "code_example"; // string | code

try {
    $result = $apiInstance->getByCodeUsingGET($code);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getByCodeUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **string**| code |

### Return type

[**\Swagger\Client\Model\AgriculturalProducerParty**](../Model/AgriculturalProducerParty.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByCountryUsingGET**
> \Swagger\Client\Model\PrFarmer[] getByCountryUsingGET($country)

Returns an Agricultural Producer Party by its Country and Personal ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$country = "country_example"; // string | country

try {
    $result = $apiInstance->getByCountryUsingGET($country);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getByCountryUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| country |

### Return type

[**\Swagger\Client\Model\PrFarmer[]**](../Model/PrFarmer.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getByPersonalIdCountryUsingGET**
> \Swagger\Client\Model\AgriculturalProducerParty getByPersonalIdCountryUsingGET($country, $personal_id)

Returns an Agricultural Producer Party by its Country and Personal ID

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$country = "country_example"; // string | country
$personal_id = "personal_id_example"; // string | personalID

try {
    $result = $apiInstance->getByPersonalIdCountryUsingGET($country, $personal_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getByPersonalIdCountryUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| country |
 **personal_id** | **string**| personalID |

### Return type

[**\Swagger\Client\Model\AgriculturalProducerParty**](../Model/AgriculturalProducerParty.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDetailsUsingGET**
> \Swagger\Client\Model\AgriculturalProducerParty[] getDetailsUsingGET($params)

Returns all Agricultural Producer Parties main details

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = new \stdClass; // object | params

try {
    $result = $apiInstance->getDetailsUsingGET($params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getDetailsUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | [**object**](../Model/.md)| params |

### Return type

[**\Swagger\Client\Model\AgriculturalProducerParty[]**](../Model/AgriculturalProducerParty.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUsingGET**
> \Swagger\Client\Model\AgriculturalProducerParty[] getUsingGET($params)

Returns all Agricultural Producer Parties codes

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: BearerToken
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');

$apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = new \stdClass; // object | params

try {
    $result = $apiInstance->getUsingGET($params);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AgriculturalProducerPartiesApi->getUsingGET: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **params** | [**object**](../Model/.md)| params |

### Return type

[**\Swagger\Client\Model\AgriculturalProducerParty[]**](../Model/AgriculturalProducerParty.md)

### Authorization

[BearerToken](../../README.md#BearerToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

