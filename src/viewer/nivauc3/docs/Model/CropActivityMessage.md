# CropActivityMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crop_activities** | [**\Swagger\Client\Model\RpCropActivityMessage[]**](RpCropActivityMessage.md) |  | [optional] 
**rpcrop_activity_message** | [**\Swagger\Client\Model\RpCropActivityMessage[]**](RpCropActivityMessage.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


