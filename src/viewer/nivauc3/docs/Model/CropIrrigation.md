# CropIrrigation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | **int** |  | [optional] 
**unit** | **int** |  | [optional] 
**volume** | **float** |  | [optional] 
**water_origin** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


