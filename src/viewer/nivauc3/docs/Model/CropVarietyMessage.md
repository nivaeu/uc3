# CropVarietyMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crop_variety** | [**\Swagger\Client\Model\CoCropVariety[]**](CoCropVariety.md) |  | [optional] 
**co_pcrop_variety** | [**\Swagger\Client\Model\CoCropVariety[]**](CoCropVariety.md) |  | [optional] 
**cocrop_variety** | [**\Swagger\Client\Model\CoCropVariety[]**](CoCropVariety.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


