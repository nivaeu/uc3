# CoActMats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cou_id** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**percentage** | **float** |  | [optional] 
**phytosanitary** | **string** |  | [optional] 
**quantity** | **float** |  | [optional] 
**substance** | **int** |  | [optional] 
**unit** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


