# CropFertilizer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dose** | **float** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**fertilization_method** | **int** |  | [optional] 
**fertilization_type** | **int** |  | [optional] 
**id** | **int** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | **int** |  | [optional] 
**unit** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


