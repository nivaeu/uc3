# Workunit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**awu** | **float** |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


