# FertilizerMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fertilizer** | [**\Swagger\Client\Model\CoFertilizer[]**](CoFertilizer.md) |  | [optional] 
**co_pfertilizer** | [**\Swagger\Client\Model\CoFertilizer[]**](CoFertilizer.md) |  | [optional] 
**cofertilizer** | [**\Swagger\Client\Model\CoFertilizer[]**](CoFertilizer.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


