# PhytosanitaryMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phytosanitary** | [**\Swagger\Client\Model\CoPhytosanitary[]**](CoPhytosanitary.md) |  | [optional] 
**co_pphytosanitary** | [**\Swagger\Client\Model\CoPhytosanitary[]**](CoPhytosanitary.md) |  | [optional] 
**cophytosanitary** | [**\Swagger\Client\Model\CoPhytosanitary[]**](CoPhytosanitary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


