# RpCropActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_activity** | **bool** |  | [optional] 
**id_cultivation_detail** | **int** |  | [optional] 
**id_cultivation_system** | **int** |  | [optional] 
**id_organic_farming** | **int** |  | [optional] 
**id_seed_type** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


