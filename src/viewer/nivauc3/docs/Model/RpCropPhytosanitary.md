# RpCropPhytosanitary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dose** | **float** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id_country** | **string** |  | [optional] 
**id_phytosanitary** | **string** |  | [optional] 
**id_phytosanitary_equipment** | **int** |  | [optional] 
**id_unit_type** | **int** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


