# StructuredAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional** | **string** |  | [optional] 
**building_number** | **string** |  | [optional] 
**city_name** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**email1** | **string** |  | [optional] 
**email2** | **string** |  | [optional] 
**phone1** | **string** |  | [optional] 
**phone2** | **string** |  | [optional] 
**postcode** | **string** |  | [optional] 
**street_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


