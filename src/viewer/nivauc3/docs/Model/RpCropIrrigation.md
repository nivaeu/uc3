# RpCropIrrigation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id_irrigation_type** | **int** |  | [optional] 
**id_unit_type** | **int** |  | [optional] 
**id_water_origin** | **int** |  | [optional] 
**irrigation_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**volume** | **float** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


