# Response

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errors** | **string[]** |  | [optional] 
**status** | **int** |  | [optional] 
**timestamp** | [**\Swagger\Client\Model\Timestamp**](Timestamp.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


