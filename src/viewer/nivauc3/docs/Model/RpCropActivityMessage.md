# RpCropActivityMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_activity** | **bool** |  | [optional] 
**agricultural_block_key** | **string** |  | [optional] 
**agricultural_producer_party_country** | **string** |  | [optional] 
**agricultural_producer_party_personal_id** | **string** |  | [optional] 
**agricultural_production_unit_code** | **string** |  | [optional] 
**agricultural_production_unit_key** | **string** |  | [optional] 
**cultivation_detail** | **int** |  | [optional] 
**cultivation_system** | **int** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**labor_type** | **int** |  | [optional] 
**organic_farming** | **int** |  | [optional] 
**reference_crop_parcel_key** | **string** |  | [optional] 
**reference_parcel** | **string** |  | [optional] 
**seed_type** | **int** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


