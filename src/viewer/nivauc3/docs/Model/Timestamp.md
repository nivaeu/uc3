# Timestamp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **int** |  | [optional] 
**day** | **int** |  | [optional] 
**hours** | **int** |  | [optional] 
**minutes** | **int** |  | [optional] 
**month** | **int** |  | [optional] 
**nanos** | **int** |  | [optional] 
**seconds** | **int** |  | [optional] 
**time** | **int** |  | [optional] 
**timezone_offset** | **int** |  | [optional] 
**year** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


