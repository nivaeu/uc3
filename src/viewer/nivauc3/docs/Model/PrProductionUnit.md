# PrProductionUnit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_block** | [**\Swagger\Client\Model\AbAgriculturalBlock[]**](AbAgriculturalBlock.md) |  | [optional] 
**code** | **string** |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**farmer** | [**\Swagger\Client\Model\PrFarmer**](PrFarmer.md) |  | [optional] 
**farmer_contact** | [**\Swagger\Client\Model\PrFarmerContact**](PrFarmerContact.md) |  | [optional] 
**farming_type** | [**\Swagger\Client\Model\PrFarmingType[]**](PrFarmingType.md) |  | [optional] 
**id_country** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**production_workunit** | [**\Swagger\Client\Model\PrProductionWorkunit[]**](PrProductionWorkunit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


