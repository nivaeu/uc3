# RpCropParcel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crop_activity** | [**\Swagger\Client\Model\RpCropActivity**](RpCropActivity.md) |  | [optional] 
**crop_activity_detail** | [**\Swagger\Client\Model\RpCropActivityDetail[]**](RpCropActivityDetail.md) |  | [optional] 
**crop_fertilizer** | [**\Swagger\Client\Model\RpCropFertilizer[]**](RpCropFertilizer.md) |  | [optional] 
**crop_irrigation** | [**\Swagger\Client\Model\RpCropIrrigation[]**](RpCropIrrigation.md) |  | [optional] 
**crop_phytosanitary** | [**\Swagger\Client\Model\RpCropPhytosanitary[]**](RpCropPhytosanitary.md) |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\RpCropParcelGeometry[]**](RpCropParcelGeometry.md) |  | [optional] 
**id_crop_product** | **string** |  | [optional] 
**id_crop_variety** | **string** |  | [optional] 
**id_land_tenure** | **int** |  | [optional] 
**irrigation** | **bool** |  | [optional] 
**key** | **string** |  | [optional] 
**landscape_features** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


