# PrFarmerContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**additional** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**email1** | **string** |  | [optional] 
**email2** | **string** |  | [optional] 
**farmer** | [**\Swagger\Client\Model\PrFarmer**](PrFarmer.md) |  | [optional] 
**id_country** | **string** |  | [optional] 
**number** | **string** |  | [optional] 
**phone1** | **string** |  | [optional] 
**phone2** | **string** |  | [optional] 
**postcode** | **string** |  | [optional] 
**production_unit** | [**\Swagger\Client\Model\PrProductionUnit**](PrProductionUnit.md) |  | [optional] 
**street** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


