# AgriculturalProducerParty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_production_unit** | [**\Swagger\Client\Model\AgriculturalProductionUnit[]**](AgriculturalProductionUnit.md) |  | [optional] 
**classification** | **int** |  | [optional] 
**code** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**personal_id** | **string** |  | [optional] 
**structured_address** | [**\Swagger\Client\Model\StructuredAddress**](StructuredAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


