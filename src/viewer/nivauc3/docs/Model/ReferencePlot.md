# ReferencePlot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**field_crop** | [**\Swagger\Client\Model\FieldCrop[]**](FieldCrop.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\Geometry[]**](Geometry.md) |  | [optional] 
**key** | **string** |  | [optional] 
**reference_parcel** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


