# FieldCrop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activity** | [**\Swagger\Client\Model\Activity**](Activity.md) |  | [optional] 
**activity_detail** | [**\Swagger\Client\Model\ActivityDetail[]**](ActivityDetail.md) |  | [optional] 
**crop_irrigation** | [**\Swagger\Client\Model\CropIrrigation[]**](CropIrrigation.md) |  | [optional] 
**crop_product** | **string** |  | [optional] 
**crop_variety** | **string** |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**fertilizer** | [**\Swagger\Client\Model\CropFertilizer[]**](CropFertilizer.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\Geometry[]**](Geometry.md) |  | [optional] 
**irrigation** | **bool** |  | [optional] 
**key** | **string** |  | [optional] 
**land_tenure** | **int** |  | [optional] 
**landscape_features** | **bool** |  | [optional] 
**phytosanitary** | [**\Swagger\Client\Model\CropPhytosanitary[]**](CropPhytosanitary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


