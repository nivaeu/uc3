# PrFarmingType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id_farming_type** | **string** |  | [optional] 
**production_unit** | [**\Swagger\Client\Model\PrProductionUnit**](PrProductionUnit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


