# CropReportDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**control_requirement** | **bool** |  | [optional] 
**copy** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**information** | **string** |  | [optional] 
**issue** | [**\Swagger\Client\Model\Timestamp**](Timestamp.md) |  | [optional] 
**line_count** | **int** |  | [optional] 
**purpose** | **int** |  | [optional] 
**report_count** | **int** |  | [optional] 
**sequence** | **int** |  | [optional] 
**status** | **int** |  | [optional] 
**type** | **int** |  | [optional] 
**id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


