# CropProductMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crop_product** | [**\Swagger\Client\Model\CoCropProduct[]**](CoCropProduct.md) |  | [optional] 
**co_pcrop_product** | [**\Swagger\Client\Model\CoCropProduct[]**](CoCropProduct.md) |  | [optional] 
**cocrop_product** | [**\Swagger\Client\Model\CoCropProduct[]**](CoCropProduct.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


