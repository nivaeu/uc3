# CoCropProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**botanical_name** | **string** |  | [optional] 
**expiry_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id** | **string** |  | [optional] 
**id_group** | **int** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


