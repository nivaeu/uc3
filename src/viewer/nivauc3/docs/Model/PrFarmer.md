# PrFarmer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** |  | [optional] 
**farmer_contact** | [**\Swagger\Client\Model\PrFarmerContact**](PrFarmerContact.md) |  | [optional] 
**farmer_data** | [**\Swagger\Client\Model\PrFarmerData**](PrFarmerData.md) |  | [optional] 
**id_country** | **string** |  | [optional] 
**production_unit** | [**\Swagger\Client\Model\PrProductionUnit[]**](PrProductionUnit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


