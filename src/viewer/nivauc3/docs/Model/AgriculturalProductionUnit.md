# AgriculturalProductionUnit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**crop_plot** | [**\Swagger\Client\Model\CropPlot[]**](CropPlot.md) |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**farming_type** | [**\Swagger\Client\Model\FarmingType[]**](FarmingType.md) |  | [optional] 
**key** | **string** |  | [optional] 
**structured_address** | [**\Swagger\Client\Model\StructuredAddress**](StructuredAddress.md) |  | [optional] 
**workunit** | [**\Swagger\Client\Model\Workunit[]**](Workunit.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


