# Activity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_activity** | **bool** |  | [optional] 
**cultivation_detail** | **int** |  | [optional] 
**cultivation_system** | **int** |  | [optional] 
**organic_farming** | **int** |  | [optional] 
**seed_type** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


