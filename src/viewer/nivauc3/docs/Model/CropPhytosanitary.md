# CropPhytosanitary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **string** |  | [optional] 
**dose** | **float** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**equipment** | **int** |  | [optional] 
**id** | **string** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**unit** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


