# CoCropVariety

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiry_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id** | **string** |  | [optional] 
**id_country** | **string** |  | [optional] 
**id_product** | **string** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


