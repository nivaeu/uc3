# CropPlot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end** | [**\DateTime**](\DateTime.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\Geometry[]**](Geometry.md) |  | [optional] 
**key** | **string** |  | [optional] 
**reference_plot** | [**\Swagger\Client\Model\ReferencePlot[]**](ReferencePlot.md) |  | [optional] 
**soil_type** | [**\Swagger\Client\Model\SoilType[]**](SoilType.md) |  | [optional] 
**start** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


