# RpCropActivityDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id_labor_type** | **int** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


