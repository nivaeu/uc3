# PrFarmerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**farmer** | [**\Swagger\Client\Model\PrFarmer**](PrFarmer.md) |  | [optional] 
**id_farmer_type** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**personal_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


