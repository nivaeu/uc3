# RpReferencePlot

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**crop_parcel** | [**\Swagger\Client\Model\RpCropParcel[]**](RpCropParcel.md) |  | [optional] 
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\RpGeometry[]**](RpGeometry.md) |  | [optional] 
**key** | **string** |  | [optional] 
**reference_parcel** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


