# CoPhytosanitary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiry_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**formulation** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**id_country** | **string** |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


