# RpCropFertilizer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dose** | **float** |  | [optional] 
**end_date** | [**\DateTime**](\DateTime.md) |  | [optional] 
**id_fertilization_method** | **int** |  | [optional] 
**id_fertilization_type** | **int** |  | [optional] 
**id_fertilizer** | **int** |  | [optional] 
**id_fertilizer_type** | **int** |  | [optional] 
**id_unit_type** | **int** |  | [optional] 
**start_date** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


