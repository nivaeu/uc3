# CropReportMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agricultural_producer_party** | [**\Swagger\Client\Model\AgriculturalProducerParty[]**](AgriculturalProducerParty.md) |  | [optional] 
**crop_report_document** | [**\Swagger\Client\Model\CropReportDocument**](CropReportDocument.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


