# ActSusMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**co_act_sus** | [**\Swagger\Client\Model\CoActSus[]**](CoActSus.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


