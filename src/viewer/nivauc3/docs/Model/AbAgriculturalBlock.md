# AbAgriculturalBlock

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_from** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_to** | [**\DateTime**](\DateTime.md) |  | [optional] 
**geometry** | [**\Swagger\Client\Model\AbGeometry[]**](AbGeometry.md) |  | [optional] 
**key** | **string** |  | [optional] 
**land_cover** | [**\Swagger\Client\Model\AbLandCover[]**](AbLandCover.md) |  | [optional] 
**reference_plot** | [**\Swagger\Client\Model\RpReferencePlot[]**](RpReferencePlot.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


