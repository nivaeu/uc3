<?php


function parseCoordinates($coordinates_string) {

    $return = str_replace("))],[((", "]],[[", $coordinates_string);
    $return = str_replace("]]],[((", "]],[[", $coordinates_string);
    
    //$return = str_replace("ON,(((", "[", $return);*/
    
    
    //$return = str_replace(",[((", ",[[", $return);
    $return = str_replace(")]", "]]", $return);
    //$return = str_replace("],[[", "]],[[", $return);
    

    
    return $return;
}

function parseaReferencePlots($reference_plot, $apu_code, $num_apu_code, $mostrar_rp=true, $mostrar_fc=true) {

    //echo "<br>**".print_r($reference_plot, true)."**";

    $i = 0;
    $arr_result = array();
    if (($reference_plot != false) && (count($reference_plot) > 0)) {
        foreach ($reference_plot as $rp) {

            $surface = 0;
            $coords_string = "";
            $coord_ini = array();
            if (count($rp["geometry"]) > 0) {
                foreach ($rp["geometry"] as $geo) {
                    //echo "<br />GEO: ".print_r($geo["coordinates"], true);
                    $surface = $geo["surface"];


                    foreach ($geo["coordinates"] as $geoitems) {
                        //echo "<br /><b>GEO ITEMS</b>: ".print_r($geoitems, true);
                        foreach ($geoitems as $coordspolygons) {
                            //echo "<br /><b>COORDS ".$afu["code"]."</b>: ".print_r($coordspolygons, true);
                            
                            if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            if (count(explode("(((", $coordspolygons[1])) > 1) {
                                $coords_string.= "[[".str_replace("(((", "", $coordspolygons[1]).",".$coordspolygons[2]."]";
                            }
                            else {
                                $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            }
                            $coords_string = parseCoordinates($coords_string);
                            
                            //echo "<br /><b>COORDS</b>: ".print_r($coordspolygons, true);
                            /*if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            $coords_string = parseCoordinates($coords_string);*/
                        }
                    }

                }
            }
            $arr_result[$i]["rpgeometry"]["coords"] = "";
            if ($mostrar_rp)
                $arr_result[$i]["rpgeometry"]["coords"] = "".$coords_string."";
            $exp1 = explode("]", $coords_string);
            $exp2 = explode(",", $exp1[0]);
            $arr_result[$i]["rpgeometry"]["latini"] = str_replace("[", "", $exp2[0]);
            $arr_result[$i]["rpgeometry"]["lngini"] = $exp2[1];
            $arr_result[$i]["rpgeometry"]["type"] = "Polygon";
            if (count(explode("]],[[", $coords_string)) > 1)
                $arr_result[$i]["rpgeometry"]["type"] = "MultiPolygon";
            $arr_result[$i]["rpgeometry"]["surface"] = $surface;

            $arr_result[$i]["key"] = $rp["key"];
            $arr_result[$i]["reference_parcel"] = $rp["reference_parcel"];
            $arr_result[$i]["date_from"] = $rp["date_from"];
            // referencias al nivel superior
            $arr_result[$i]["apu_code"] = $apu_code;
            $arr_result[$i]["num_apu_code"] = $num_apu_code;

            $arr_field_crops = array();
            $arr_field_crops = parseaFieldCrops($rp, $mostrar_fc);

            $arr_result[$i]["field_crops"] = $arr_field_crops;

            $arr_field_crops_stats = array();
            $arr_field_crops_stats = parseaFieldCropsStats($rp);
            $arr_result[$i]["field_crops_stats"] = $arr_field_crops_stats;

            $i++;
        }
    }

    //echo "<br /><br />";
    //print_r($arr_result);//die;

    return $arr_result;
}


function parseaFieldCrops($rp, $mostrar_fc=true) {


    $i = 0;
    $arr_result = array();
    if (($rp["field_crop"] != false) && (count($rp["field_crop"]) > 0)) {
        foreach ($rp["field_crop"] as $fc) {

            //echo "<b>";
            //print_r($fc);
            //echo "</b>";

            $surface = 0;
            $coords_string = "";
            $coord_ini = array();
            if (count($fc["geometry"]) > 0) {
                foreach ($fc["geometry"] as $geo) {
                    //echo "<br />GEO: ".print_r($geo["coordinates"], true);
                    $surface = $geo["surface"];

                    foreach ($geo["coordinates"] as $geoitems) {
                        //echo "<br /><b>GEO ITEMS</b>: ".print_r($geoitems, true);
                        foreach ($geoitems as $coordspolygons) {
                            //echo "<br /><b>COORDS ".$afu["code"]."</b>: ".print_r($coordspolygons, true);
                            
                            if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            if (count(explode("(((", $coordspolygons[1])) > 1) {
                                $coords_string.= "[[".str_replace("(((", "", $coordspolygons[1]).",".$coordspolygons[2]."]";
                            }
                            else {
                                $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            }
                            $coords_string = parseCoordinates($coords_string);
                            
                            //echo "<br /><b>COORDS</b>: ".print_r($coordspolygons, true);
                            /*if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            $coords_string = parseCoordinates($coords_string);*/
                        }
                    }

                }
            }

            if ($mostrar_fc) {
                $arr_result[$i]["fcgeometry"]["coords"] = "".$coords_string."";
                $arr_result[$i]["fcgeometry"]["type"] = "Polygon";
                if (count(explode("]],[[", $coords_string)) > 1)
                    $arr_result[$i]["type"] = "MultiPolygon";
                $arr_result[$i]["fcgeometry"]["surface"] = $surface;

                $arr_result[$i]["type"] = "Polygon";
                if (count(explode("]],[[", $coords_string)) > 1)
                    $arr_result[$i]["type"] = "MultiPolygon";
                $arr_result[$i]["key"] = $fc["key"];
                $arr_result[$i]["irrigation"] = $fc["irrigation"];
                $arr_result[$i]["crop_product"] = $fc["crop_product"];
                $arr_result[$i]["land_tenure"] = $fc["land_tenure"];
                $arr_result[$i]["activity_detail"] = $fc["activity_detail"];
                // parse irrigation
                $arr_irrigation = array();
                $arr_irrigation = parseFCIrrigation($fc["crop_irrigation"]);
                $arr_result[$i]["crop_irrigation"] = $arr_irrigation;
                // parse phytosanitary
                $arr_phytosanitary = array();
                $arr_phytosanitary = parseFCPhytosanitary($fc["phytosanitary"]);
                $arr_result[$i]["phytosanitary"] = $arr_phytosanitary;
                $arr_result[$i]["fertilizer"] = $fc["fertilizer"];
                // parse fertilizer
                $arr_fertilizer = array();
                $arr_fertilizer = parseFCFertilizer($fc["fertilizer"]);
                $arr_result[$i]["fertilizer"] = $arr_fertilizer;

                //echo "<br />construyo crop irrigation **";

                //print_r($rp["field_crop"]);
                //echo "**";
                //print_r($fc);die;
            }
            $i++;
        }
    }
    //echo "<br /><br />FC: <br />";
    //print_r($arr_result);

    return $arr_result;    
}

function parseaFieldCropsStats($rp) {

    $i = 0;
    $arr_result = array();
    if (($rp["field_crop"] != false) && (count($rp["field_crop"]) > 0)) {
        foreach ($rp["field_crop"] as $fc) {

            //echo "<b>";
            //print_r($fc);
            //echo "</b>";

            $surface = 0;
            $coords_string = "";
            $coord_ini = array();
            if (count($fc["geometry"]) > 0) {
                foreach ($fc["geometry"] as $geo) {
                    //echo "<br />GEO: ".print_r($geo["coordinates"], true);
                    $surface = $geo["surface"];

                    foreach ($geo["coordinates"] as $geoitems) {
                        //echo "<br /><b>GEO ITEMS</b>: ".print_r($geoitems, true);
                        foreach ($geoitems as $coordspolygons) {
                            //echo "<br /><b>COORDS ".$afu["code"]."</b>: ".print_r($coordspolygons, true);
                            
                            if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            if (count(explode("(((", $coordspolygons[1])) > 1) {
                                $coords_string.= "[[".str_replace("(((", "", $coordspolygons[1]).",".$coordspolygons[2]."]";
                            }
                            else {
                                $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            }
                            $coords_string = parseCoordinates($coords_string);
                            
                            //echo "<br /><b>COORDS</b>: ".print_r($coordspolygons, true);
                            /*if ($coords_string == "") {
                                $coord_ini[0] = $coordspolygons[0];
                                $coord_ini[1] = $coordspolygons[1];
                            }
                            if ($coords_string != "") {
                                $coords_string.= ",";
                            }
                            $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                            $coords_string = parseCoordinates($coords_string);*/
                        }
                    }

                }
            }

            $arr_result[$i]["fcgeometry"]["coords"] = "".$coords_string."";
            $arr_result[$i]["fcgeometry"]["type"] = "Polygon";
            if (count(explode("]],[[", $coords_string)) > 1)
                $arr_result[$i]["type"] = "MultiPolygon";
            $arr_result[$i]["fcgeometry"]["surface"] = $surface;

            $arr_result[$i]["type"] = "Polygon";
            if (count(explode("]],[[", $coords_string)) > 1)
                $arr_result[$i]["type"] = "MultiPolygon";
            $arr_result[$i]["key"] = $fc["key"];
            $arr_result[$i]["irrigation"] = $fc["irrigation"];
            $arr_result[$i]["crop_product"] = $fc["crop_product"];
            $arr_result[$i]["land_tenure"] = $fc["land_tenure"];
            $arr_result[$i]["activity_detail"] = $fc["activity_detail"];
            $arr_irrigation = array();
            $arr_irrigation = parseFCIrrigation($fc["crop_irrigation"]);
            $arr_result[$i]["crop_irrigation"] = $arr_irrigation;
            $arr_phytosanitary = array();
            $arr_phytosanitary = parseFCPhytosanitary($fc["phytosanitary"]);
            $arr_result[$i]["phytosanitary"] = $arr_phytosanitary;
            $arr_result[$i]["fertilizer"] = $fc["fertilizer"];

            $i++;
        }
    }
    //echo "<br /><br />FC: <br />";
    //print_r($arr_result);

    return $arr_result;      
}


function parseFCIrrigation($fc_irrigation) {

    $return = array();

    $i = 0;
    if (($fc_irrigation != false) && (count($fc_irrigation) > 0)) {
        foreach ($fc_irrigation as $ir) {

            //echo "<br />arrirr: ".print_r($ir, true);
            $d = print_r($ir, true);
            //echo "<br />Date: ".$ir["date"]->date;
            //echo "<br />Type: ".$ir["type"];
            //echo "<br />Unit: ".$ir["unit"];
            //echo "<br />Volume: ".$ir["volume"];
            //echo "<br />Water origin: ".$ir["water_origin"];

            $date = print_r($ir["date"],true);
            $exp1date = explode("]", $date);
            $exp2date = str_replace(" => ", "", explode("[", $exp1date[1]));
            $exp3date = explode(" ", $exp2date[0]);

            //return[$i]["date"] = $ir["date"]->date;
            $return[$i]["date"] = $exp3date[0];
            $return[$i]["type"] = $ir["type"];
            $return[$i]["unit"] = $ir["unit"];
            $return[$i]["volume"] = $ir["volume"];
            $return[$i]["water_origin"] = $ir["water_origin"];

            $i++;
        }
    }

    return $return;
}

function parseFCPhytosanitary($fc_phytosanitary) {

    $return = array();

    $i = 0;
    if (($fc_phytosanitary != false) && (count($fc_phytosanitary) > 0)) {
        foreach ($fc_phytosanitary as $ph) {

            //echo "<br />arrirr: ".print_r($ph, true);
            $d = print_r($ph, true);
            //echo "<br />date: ".print_r($ph["start_date"],true);

            $start_date = print_r($ph["start_date"],true);
            $end_date = print_r($ph["end_date"],true);
            //echo "<br /> ** ".$start_date." **** ";
            
            $exp1startdate = explode("]", $start_date);
            $exp2startdate = str_replace(" => ", "", explode("[", $exp1startdate[1]));
            $exp3startdate = explode(" ", $exp2startdate[0]);
            //echo "<br />date: /*/*".$exp3startdate[0]."*/*/";
            $exp1enddate = explode("]", $end_date);
            $exp2enddate = str_replace(" => ", "", explode("[", $exp1enddate[1]));
            $exp3enddate = explode(" ", $exp2enddate[0]);

            //$return[$i]["start_date"] = $ph["start_date"]->date;
            $return[$i]["start_date"] = $exp3startdate[0];
            //$return[$i]["end_date"] = $ph["end_date"]->date;
            $return[$i]["end_date"] = $exp3enddate[0];
            $return[$i]["country"] = $ph["country"];
            $return[$i]["dose"] = $ph["dose"];
            $return[$i]["equipment"] = $ph["equipment"];
            $return[$i]["id"] = $ph["id"];
            $return[$i]["unit"] = $ph["unit"];

            $i++;
        }
    }

    return $return;
}

function parseFCFertilizer($fc_fertilizer) {

    $return = array();
    //echo "<br /><hr /><br />";
    //print_r($fc_fertilizer);
    $i = 0;
    if (($fc_fertilizer != false) && (count($fc_fertilizer) > 0)) {
        foreach ($fc_fertilizer as $fe) {

            //echo "<br />arrfer: ".print_r($ph, true);
            $d = print_r($fe, true);
            //echo "<br />date: ".print_r($ph["start_date"],true);

            $start_date = print_r($fe["start_date"],true);
            $end_date = print_r($fe["end_date"],true);
            //$expiry_date = print_r($fe["expiry_date"],true);
            //echo "<br /> ** ".$start_date." **** ";
            
            $exp1startdate = explode("]", $start_date);
            $exp2startdate = str_replace(" => ", "", explode("[", $exp1startdate[1]));
            $exp3startdate = explode(" ", $exp2startdate[0]);
            //echo "<br />date: /*/*".$exp3startdate[0]."*/*/";
            $exp1enddate = explode("]", $end_date);
            $exp2enddate = str_replace(" => ", "", explode("[", $exp1enddate[1]));
            $exp3enddate = explode(" ", $exp2enddate[0]);

            //$exp1expirydate = explode("]", $expiry_date);
            //$exp2expirydate = str_replace(" => ", "", explode("[", $exp1expirydate[1]));
            //$exp3expirydate = explode(" ", $exp2expirydate[0]);

            //$return[$i]["start_date"] = $ph["start_date"]->date;
            $return[$i]["start_date"] = $exp3startdate[0];
            //$return[$i]["end_date"] = $ph["end_date"]->date;
            $return[$i]["end_date"] = $exp3enddate[0];
            //$return[$i]["expiry_date"] = $exp3expirydate[0];

            $return[$i]["fertilization_method"] = $fe["fertilization_method"];
            $return[$i]["fertilization_type"] = $fe["fertilization_type"];
            $return[$i]["dose"] = $fe["dose"];
            $return[$i]["id"] = $fe["id"];
            $return[$i]["type"] = $fe["type"];
            $return[$i]["unit"] = $fe["unit"];

            $i++;
        }
    }

    return $return;
}

?>