<?php

class Settings {

    var $_siteDefaultPage = "out/out.main.farmer.php";
    var $_siteName = "NIVA UC3";
    // estilo por defecto
    var $_style = "black";
    // idioma por defecto
    var $_language = "Spanish";
    var $_rootDir = "../";
    var $_contentDir = "../content/";
    var $_imagesDir = "../images/";
    var $_userImagesDir = "users/";
    var $_iconImagesDir = "icons/";
    var $_flagImagesDir = "flags/";
  	var $_expireTime = 12000;
  	var $_separatorArray = "##";
  	

    // ------------------------------- Mapbox access token ------------------------------------
    var $_mapbox_accesstoken = 'pk.eyJ1IjoidmlwZXJpb3JpYnVzIiwiYSI6ImNrOXp3ZHFuMjBpd2gzbG52YWo2Y2JrZnEifQ.p9d6loHUABC0wvgIzSGOww';

    // ------------------------------- Opciones de maquetaci�n -------------------------------
    // ---- AB
    var $_color_ab_borde = "#FF0000";
    var $_color_ab_relleno = "#FF0000";
    var $_color_ab_relleno_seleccionado = "#00B0FF";
    var $_opacidad_ab_relleno = 0.5;
    var $_opacidad_ab_relleno_hover = 0.6;
    var $_grosor_ab_borde = 0.9;
    // ---- RP
    var $_color_rp_borde = "#FF00FF";
    var $_color_rp_relleno = "#FF00FF";
    var $_color_rp_relleno_seleccionado = "#00B0FF";
    var $_opacidad_rp_relleno = 0.7;
    var $_opacidad_rp_relleno_hover = 0.6;
    var $_opacidad_rp_relleno_seleccionado = 0.5;
    var $_grosor_rp_borde = 0.8;
    // ---- FC
    var $_color_fc_borde = "#00FF80";
    var $_color_fc_relleno = "#00FF80";
    var $_color_fc_relleno_seleccionado = "#00B0FF";
    var $_opacidad_fc_relleno = 0.4;
    var $_opacidad_fc_relleno_hover = 0.7;
    var $_opacidad_fc_relleno_seleccionado = 0.5;
    var $_grosor_fc_borde = 0.8;

    var $_maxtablesorangesize = 50;

    // land covers array de colores
    var $_landcovercolors = array();
    // irrigation array de colores
    var $_irrigationcolors = array();
    // arr catalog irrigation
    var $_arrCatalogIrrigation = array();
    // arr catalog irrigation legend
    var $_arrCatalogIrrigationLegend = array();

    var $_mostrar_legend_barras = true;
    var $_mostrar_legend_pie = true;
}

$settings = new Settings();

// landcover colors
//$settings->_landcovercolors[0] = "#8B0000";
//$settings->_landcovercolors[0] = "#FF66B2";
$settings->_landcovercolors[0] = "#990000";
$settings->_landcovercolors[1] = "#B8860B";
//$settings->_landcovercolors[2] = "#DC143C";
$settings->_landcovercolors[2] = "#FFCCCC";
$settings->_landcovercolors[3] = "#CD853F";

// irrigation colors
$settings->_irrigationcolors[0] = "#CC6600";
$settings->_irrigationcolors[1] = "#00CC00";

// arr catalog irrigation
$settings->_arrCatalogIrrigation[0] = 0;
$settings->_arrCatalogIrrigation[1] = 1;

// arr catalog irrigation legend
$settings->_arrCatalogIrrigationLegend[0] = "Non-irrigated";
$settings->_arrCatalogIrrigationLegend[1] = "Irrigated";


?>
