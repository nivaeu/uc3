<?php

function renameFile($old, $new)
{
	return rename($old, $new);
}

function removeFile($file)
{
	return unlink($file);
}

function copyFile($source, $target)
{
	
	return copy($source, $target);
}

function moveFile($source, $target)
{
	if (!copyFile($source, $target))
		return false;
	return removeFile($source);
}

function renameDir($old, $new)
{
	return rename($old, $new);
}

function makeDir($path)
{
	if (strncmp($path, DIRECTORY_SEPARATOR, 1) == 0) {
		$mkfolder = DIRECTORY_SEPARATOR;
	}
	else {
		$mkfolder = "";
	}
	$path = preg_split( "/[\\\\\/]/" , $path );
	for(  $i=0 ; isset( $path[$i] ) ; $i++ )
	{
		if(!strlen(trim($path[$i])))continue;
		$mkfolder .= $path[$i];
		if( !is_dir( $mkfolder ) ){
			$res=mkdir( "$mkfolder" ,  0777);
			if (!$res) return false;
		}
		$mkfolder .= DIRECTORY_SEPARATOR;
	}

	return true;
}

function removeDir($path)
{
	$handle = opendir($path);
	while ($entry = readdir($handle) )
	{
		if ($entry == ".." || $entry == ".")
			continue;
		else if (is_dir($path . $entry))
		{
			if (!removeDir($path . $entry . "/"))
				return false;
		}
		else
		{
			if (!unlink($path . $entry))
				return false;
		}
	}
	closedir($handle);
	return rmdir($path);
}

function copyDir($sourcePath, $targetPath)
{
	if (mkdir($targetPath, 0777))
	{
		$handle = opendir($sourcePath);
		while ($entry = readdir($handle) )
		{
			if ($entry == ".." || $entry == ".")
				continue;
			else if (is_dir($sourcePath . $entry))
			{
				if (!copyDir($sourcePath . $entry . "/", $targetPath . $entry . "/"))
					return false;
			}
			else
			{
				if (!copy($sourcePath . $entry, $targetPath . $entry))
					return false;
			}
		}
		closedir($handle);
	}
	else
		return false;
	
	return true;
}

function moveDir($sourcePath, $targetPath)
{
	if (!copyDir($sourcePath, $targetPath))
		return false;
	return removeDir($sourcePath);
}

function escribir_trazas($mensaje, $archivo){

	$fp = fopen($archivo,"a+");
	fwrite($fp, $mensaje);
	fclose($fp);
	return true;
}

function escribir_trazas_movimiento($mensaje, $archivo){

	$fp = fopen($archivo,"w");
	fwrite($fp, $mensaje);
	fclose($fp);
	return true;
}

// funci�n que crea un archivo excel y le a�ade el contenido, que se lo paso en $arraydata.
function createExcel($filename, $arrydata) {
	global $settings;
  $excelfile = $settings->_contentDir.$settings->_excelInforms.$filename;
	$fp = fopen($excelfile, "wb");  
	if (!is_resource($fp)) {  
		printMLText("error_occured");
    return false;  
	}
  fwrite($fp, serialize($arrydata));
	fclose($fp);

/*
	header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");  
	header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");  
	header ("Cache-Control: no-cache, must-revalidate");  
	header ("Pragma: no-cache");  
	header ("Content-type: application/x-msexcel");  
	header ("Content-Disposition: attachment; filename=\"" . $filename . "\"" );
*/
	//readfile($excelfile);
  return true;
}

?>
