<?php


$bearer = "";
if ((isset($_GET["token"])) && (strlen($_GET["token"]) > 0)) {
    $bearer = sanitizeString($_GET["token"]);
}


//$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyY2FtcGFuYSIsImlhdCI6MTYzMjk5NTkyOCwiZXhwIjoxNjMzMDAxOTI4fQ.i5ESsiaV3YAP7xdYBBOBnCi4PexOZWvovQZmUM6KQYB_3kVALBtWSXC8X_KCD9LHZj35QLkKYVuCcCijyOy0Sw');
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', "Bearer ".$bearer."");

$apiInstance = new Swagger\Client\Api\QueryCatalogsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCountriesUsingGET();
    //print_r($result);
} catch (Exception $e) {
    //echo 'Exception when calling QueryCatalogsApi->getCountriesUsingGET: ', $e->getMessage(), PHP_EOL;
    header("Location: ../out/out.login.php");
}


?>