<?php

function getReadableDate($timestamp) {
	return date("d.m.Y", $timestamp);
}

function getLongReadableDate($timestamp) {
	return date("d.m.Y - H:i", $timestamp);
}


function redondear_dos_decimal($valor)
{
    $float_redondeado = round($valor * 100) / 100;
    return $float_redondeado;
}

function traduce_numero_real_excel($string)
{
    $return = str_replace(".", "", $string);
    $return = str_replace(",", ".", $return);
    return $return;
}

// Devuelve el mktime asociado a una fecha dada en formato dd/mm/yyyy o d/m/yyyy
function tratarFecha($date) {
    $arr = explode("/", $date);
    if (($arr[0] < 10) && (strlen($arr[0]) == 2)) {
        $arrDay = explode("0",$arr[0]);
        $day = $arrDay[1];
    }
    else $day = $arr[0];
    if (($arr[1] < 10) && (strlen($arr[0]) == 2)){
        $arrMonth = explode("0",$arr[1]);
        $month = $arrMonth[1];
    }
    else $month = $arr[1];

    //print $arr[1]." - ".$arr[0]." - ".$arr[2];
    return mktime(0, 0, 0, $month, $day, $arr[2]);
}


function traducirMes($string_mes) {

    switch ($string_mes) {
        case "1":
            $mes = 'ENE';
            break;
        case "2":
            $mes = 'FEB';
            break;
        case "3":
            $mes = 'MAR';
            break;
        case "4":
            $mes = 'ABR';
            break;
        case "5":
            $mes = 'MAY';
            break;
        case "6":
            $mes = 'JUN';
            break;
        case "7":
            $mes = 'JUL';
            break;
        case "8":
            $mes = 'AGO';
            break;
        case "9":
            $mes = 'SEP';
            break;
        case "10":
            $mes = 'OCT';
            break;
        case "11":
            $mes = 'NOV';
            break;
        case "12":
            $mes = 'DIC';
            break;
        default: $mes = 0;
    }
    return $mes;
}


function tratarMes($string_mes) {

    switch ($string_mes) {
        case "Ene":
            $mes = '01';
            break;
        case "Feb":
            $mes = '02';
            break;
        case "Mar":
            $mes = '03';
            break;
        case "Abr":
            $mes = '04';
            break;
        case "May":
            $mes = '05';
            break;
        case "Jun":
            $mes = '06';
            break;
        case "Jul":
            $mes = '07';
            break;
        case "Ago":
            $mes = '08';
            break;
        case "Sep":
            $mes = '09';
            break;
        case "Oct":
            $mes = '10';
            break;
        case "Nov":
            $mes = '11';
            break;
        case "Dic":
            $mes = '12';
            break;
        default: $mes = 0;
    }
    return $mes;
}

function tratarMesIngles($string_mes) {

    switch ($string_mes) {
        case "Jan":
            $mes = '01';
            break;
        case "Feb":
            $mes = '02';
            break;
        case "Mar":
            $mes = '03';
            break;
        case "Apr":
            $mes = '04';
            break;
        case "May":
            $mes = '05';
            break;
        case "Jun":
            $mes = '06';
            break;
        case "Jul":
            $mes = '07';
            break;
        case "Aug":
            $mes = '08';
            break;
        case "Sep":
            $mes = '09';
            break;
        case "Oct":
            $mes = '10';
            break;
        case "Nov":
            $mes = '11';
            break;
        case "Dec":
            $mes = '12';
            break;
        default: $mes = 0;
    }
    return $mes;
}

function tratarMesCorto($string_mes) {

    switch ($string_mes) {
        case "ene":
            $mes = '01';
            break;
        case "feb":
            $mes = '02';
            break;
        case "mar":
            $mes = '03';
            break;
        case "abr":
            $mes = '04';
            break;
        case "may":
            $mes = '05';
            break;
        case "jun":
            $mes = '06';
            break;
        case "jul":
            $mes = '07';
            break;
        case "ago":
            $mes = '08';
            break;
        case "sep":
            $mes = '09';
            break;
        case "oct":
            $mes = '10';
            break;
        case "nov":
            $mes = '11';
            break;
        case "dic":
            $mes = '12';
            break;
        default: $mes = 0;
    }
    return $mes;
}

function tratarMesLargo($string_mes) {

    switch ($string_mes) {
        case "enero":
            $mes = '01';
            break;
        case "febrero":
            $mes = '02';
            break;
        case "marzo":
            $mes = '03';
            break;
        case "abril":
            $mes = '04';
            break;
        case "mayo":
            $mes = '05';
            break;
        case "junio":
            $mes = '06';
            break;
        case "julio":
            $mes = '07';
            break;
        case "agosto":
            $mes = '08';
            break;
        case "septiembre":
            $mes = '09';
            break;
        case "octubre":
            $mes = '10';
            break;
        case "noviembre":
            $mes = '11';
            break;
        case "diciembre":
            $mes = '12';
            break;
        default: $mes = 0;
    }
    return $mes;
}


// Devuelve una fecha en formato hh:mm pasada en formato mktime
function tratarFechaAHorasMinutos($date) {
    if (($date == 0) || ($date == ""))
        return "No definida";
    else
        return date("H",$date).":".date("i",$date);
}

// Devuelve una fecha en formato dd/mm/yyy pasada en formato mktime
// Importante : para ser consecuente con la conversi�n en sentido contrario, no se ponen los ceros iniciales
function tratarFechaADiasMesAnyo($date) {
    if (($date == 0) || ($date == ""))
        return "No definida";
    else
        return date("d",$date)."/".date("m",$date)."/".date("Y",$date);
}

// Devuelve una fecha en formato dd/mm/yyy pasada en formato mktime
// Importante : para ser consecuente con la conversi�n en sentido contrario, no se ponen los ceros iniciales
function tratarFechaADiasMesAnyoCerosIniciales($date) {
    if (($date == 0) || ($date == ""))
        return "No definida";
    else
        return date("d",$date)."/".date("m",$date)."/".date("y",$date);
}

function sanitizeString($string) {

	//$string = (string) $string;
	//if (get_magic_quotes_gpc()) {
	//	$string = stripslashes($string);
	//}

    $string = str_replace("&#243;", "OOOOOOOOOO", $string);

	$string = str_replace("\\", "\\\\", $string);
	$string = str_replace("--", "\-\-", $string);
	$string = str_replace(";", "\;", $string);
	// Use HTML entities to represent the other characters that have special
	// meaning in SQL. These can be easily converted back to ASCII / UTF-8
	// with a decode function if need be.
	$string = str_replace("&", "&amp;", $string);
	$string = str_replace("%", "&#0037;", $string); // percent
	$string = str_replace("\"", "&quot;", $string); // double quote
	$string = str_replace("/*", "&#0047;&#0042;", $string); // start of comment
	$string = str_replace("*/", "&#0042;&#0047;", $string); // end of comment
	$string = str_replace("<", "&lt;", $string);
	$string = str_replace(">", "&gt;", $string);
	$string = str_replace("=", "&#0061;", $string);
	$string = str_replace(")", "&#0041;", $string);
	$string = str_replace("(", "&#0040;", $string);
	$string = str_replace("'", "&#0039;", $string);
    $string = str_replace("+", "&#0043;", $string);

    $string = str_replace("á", "&aacute;", $string);
    $string = str_replace("é", "&eacute;", $string);
    $string = str_replace("í", "&iacute;", $string);
    $string = str_replace("ó", "&oacute;", $string);
    $string = str_replace("ú", "&uacute;", $string);
    $string = str_replace("Á", "&Aacute;", $string);
    $string = str_replace("É", "&Eacute;", $string);
    $string = str_replace("Í", "&Iacute;", $string);
    $string = str_replace("Ó", "&Oacute;", $string);
    $string = str_replace("Ú", "&Uacute;", $string);
    $string = str_replace("ñ", "&ntilde;", $string);
    $string = str_replace("Ñ", "&Ntilde;", $string);
    $string = str_replace("ö", "o", $string);
    $string = str_replace("ü", "u", $string);
	return $string;
}


function sanitizePhotoName($string) {
    $string = ($string);
    $string = strtolower($string);
    $string = str_replace("&nbsp;","_",$string);
    $string = str_replace(" ","_",$string);
    $string = sanitizeString($string);
    $string = sanitizeAcutes($string);
    return $string;
}


function acutesPlanos($string) {
	$string = (string) $string;
    $string = str_replace("á", "a", $string);
    $string = str_replace("é", "e", $string);
    $string = str_replace("í", "i", $string);
    $string = str_replace("ó", "o", $string);
    $string = str_replace("ú", "u", $string);
    $string = str_replace("Á", "A", $string);
    $string = str_replace("É", "E", $string);
    $string = str_replace("Í", "I", $string);
    $string = str_replace("Ó", "O", $string);
    $string = str_replace("Ú", "U", $string);
	$string = str_replace("'", "", $string);
    return $string;
}

function acutesDecode($string) {
	$string = (string) $string;
  $string = str_replace("á", "a", $string);
  $string = str_replace("é", "e", $string);
  $string = str_replace("�", "i", $string);
  $string = str_replace("ó", "o", $string);
  $string = str_replace("i�", "o", $string);
  $string = str_replace("-", "", $string);
  return $string;
}

function mydmsDecodeString($string) {

	$string = (string)$string;

	$string = str_replace("&amp;", "&", $string);
	$string = str_replace("&#0037;", "%", $string); // percent
	$string = str_replace("&#37;", "%", $string); // percent
	$string = str_replace("&quot;", "\"", $string); // double quote
	$string = str_replace("&#0047;&#0042;", "/*", $string); // start of comment
	$string = str_replace("&#0042;&#0047;", "*/", $string); // end of comment
	$string = str_replace("&lt;", "<", $string);
	$string = str_replace("&gt;", ">", $string);
	$string = str_replace("&#0061;", "=", $string);
	$string = str_replace("&#0041;", ")", $string);
	$string = str_replace("&#0040;", "(", $string);
	$string = str_replace("&#0039;", "'", $string);
	$string = str_replace("&#0043;", "+", $string);
	$string = str_replace("&#41;", ")", $string);
	$string = str_replace("&#40;", "(", $string);

    $string = str_replace("&aacute;", "á", $string);
    $string = str_replace("&eacute;", "é", $string);
    $string = str_replace("&iacute;", "í", $string);
    $string = str_replace("&oacute;", "ó", $string);
    $string = str_replace("&uacute;", "ú", $string);
    $string = str_replace("&Aacute;", "Á", $string);
    $string = str_replace("&Eacute;", "É", $string);
    $string = str_replace("&Iacute;", "Í", $string);
    $string = str_replace("&Oacute;", "Ó", $string);
    $string = str_replace("&Uacute;", "Ú", $string);
    $string = str_replace("&ntilde;", "n", $string);
    $string = str_replace("&Ntilde;", "Ñ", $string);

	return $string;
}

function ultimo_dia_mes($fecha) {
    $mes = date("n",$fecha);
    if ($mes == 2)
        return 28;
    elseif (($mes == 11) || ($mes == 4) || ($mes == 6) || ($mes == 9))
        return 30;
    else
        return 31;
}

function contarMeses($mktime) {
    $mes_inicial = date("n",$mktime);
    $anyo_inicial = date("Y",$mktime);
    $mes_actual = date("n",time());
    $anyo_actual = date("Y",time());
    if ($anyo_inicial == $anyo_actual)
        $meses = $mes_actual - $mes_inicial;
    else {
        $distancia_anyos = $anyo_actual - $anyo_inicial;
        if ($distancia_anyos == 1)
            $meses = 12 - $mes_inicial + $mes_actual;
        else {
            $meses = 12 - $mes_inicial + $mes_actual;
            $meses = $meses + (12 * ($distancia_anyos - 1));
        }
    }
    return $meses;
}


function tratarFechaPostgre($fecha_ini) {

    $expfecha1 = explode(" ", $fecha_ini);
	$expfecha2 = explode(":", $expfecha1[1]);
	$expfecha3 = explode("-", $expfecha1[0]);
	$anyo_actual = $expfecha3[0];
	$mes_actual = $expfecha3[1];
	$dia_actual = $expfecha3[2];
	$hora_actual = intval($expfecha2[0]);
	$minuto_actual = intval($expfecha2[1]);
    $mktime_actual = mktime($hora_actual, $minuto_actual, 0, $mes_actual, $dia_actual, $anyo_actual);
    
    return $mktime_actual;
}

function ordenarArray($campo, $arr, $order=SORT_DESC) {

    $result = array();
    $aux = array();    
    foreach ($arr as $key => $row) {
        $aux[$key] = $row[$campo];
    }

    array_multisort($aux, $order, $arr);

    return $arr;
}

?>
