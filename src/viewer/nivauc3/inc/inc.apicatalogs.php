<?php

// inc.apicatalogs.php


$apiInstanceCatalogs = new Swagger\Client\Api\QueryCatalogsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);


// --------------------------------------------------------------------------------------------------
// crop product

$i = 0;
$arrCatalogCropProduct = array();
$resultCatalogCropProduct = $apiInstanceCatalogs->getCropProductsUsingGET();
//print_r($resultCatalogIrrigationType);//die;
if (($resultCatalogCropProduct != false) && (count($resultCatalogCropProduct) > 0)) {
    foreach ($resultCatalogCropProduct as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogCropProduct[$data["id"]] = $data["name"];
        $i++;
    }
}

// --------------------------------------------------------------------------------------------------
// fertilization methods

$i = 0;
$arrCatalogFertilizationMethods = array();
$resultCatalogFertilizationMethods = $apiInstanceCatalogs->getFertilizationMethodsUsingGET();
//print_r($resultCatalogFertilizer);//die;
if (($resultCatalogFertilizationMethods != false) && (count($resultCatalogFertilizationMethods) > 0)) {
    foreach ($resultCatalogFertilizationMethods as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogFertilizationMethods[$data["id"]] = $data["name"];
        $i++;
    }
}

// --------------------------------------------------------------------------------------------------
// fertilization types

$i = 0;
$arrCatalogFertilizationTypes = array();
$resultCatalogFertilizationTypes = $apiInstanceCatalogs->getFertilizationTypesUsingGET();
//print_r($resultCatalogFertilizer);//die;
if (($resultCatalogFertilizationTypes != false) && (count($resultCatalogFertilizationTypes) > 0)) {
    foreach ($resultCatalogFertilizationTypes as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogFertilizationTypes[$data["id"]] = $data["name"];
        $i++;
    }
}

// --------------------------------------------------------------------------------------------------
// fertilizer

$i = 0;
$arrCatalogFertilizer = array();
$resultCatalogFertilizer = $apiInstanceCatalogs->getFertilizersUsingGET();
//print_r($resultCatalogFertilizer);//die;
if (($resultCatalogFertilizer != false) && (count($resultCatalogFertilizer) > 0)) {
    foreach ($resultCatalogFertilizer as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogFertilizer[$data["id"]] = $data["name"];
        $i++;
    }
}

// --------------------------------------------------------------------------------------------------
// fertilizer types

$i = 0;
$arrCatalogFertilizerTypes = array();
$resultCatalogFertilizerTypes = $apiInstanceCatalogs->getFertilizerTypesUsingGET();
//print_r($resultCatalogFertilizer);//die;
if (($resultCatalogFertilizerTypes != false) && (count($resultCatalogFertilizerTypes) > 0)) {
    foreach ($resultCatalogFertilizerTypes as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogFertilizerTypes[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// irrigation type

$i = 0;
$arrCatalogIrrigationType = array();
$resultCatalogIrrigationType = $apiInstanceCatalogs->getIrrigationTypesUsingGET();
//print_r($resultCatalogIrrigationType);//die;
if (($resultCatalogIrrigationType != false) && (count($resultCatalogIrrigationType) > 0)) {
    foreach ($resultCatalogIrrigationType as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogIrrigationType[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// land cover

$i = 0;
$arrCatalogLandCover = array();
$resultCatalogLandCover = $apiInstanceCatalogs->getLandCoversUsingGET();
//print_r($resultCatalogLandCover);//die;
if (($resultCatalogLandCover != false) && (count($resultCatalogLandCover) > 0)) {
    foreach ($resultCatalogLandCover as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogLandCover[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// phytosanitary

$i = 0;
$arrCatalogPhytosanitary = array();
$resultCatalogPhytosanitary = $apiInstanceCatalogs->getPhytosanitariesUsingGET();
//print_r($resultCatalogPhytosanitary);//die;
if (($resultCatalogPhytosanitary != false) && (count($resultCatalogPhytosanitary) > 0)) {
    foreach ($resultCatalogPhytosanitary as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogPhytosanitary[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// phytosanitary equipment

$i = 0;
$arrCatalogPhytosanitaryEquipment = array();
$resultCatalogPhytosanitaryEquipment = $apiInstanceCatalogs->getPhytosanitaryEquipmentUsingGET();
//print_r($resultCatalogPhytosanitaryEquipment);//die;
if (($resultCatalogPhytosanitaryEquipment != false) && (count($resultCatalogPhytosanitaryEquipment) > 0)) {
    foreach ($resultCatalogPhytosanitaryEquipment as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogPhytosanitaryEquipment[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// unit type

$i = 0;
$arrCatalogUnitType = array();
$resultCatalogUnitType = $apiInstanceCatalogs->getUnitTypesUsingGET();
//print_r($resultCatalogPhytosanitaryEquipment);//die;
if (($resultCatalogUnitType != false) && (count($resultCatalogUnitType) > 0)) {
    foreach ($resultCatalogUnitType as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogUnitType[$data["id"]] = $data["name"];
        $i++;
    }
}


// --------------------------------------------------------------------------------------------------
// water origin

$i = 0;
$arrCatalogWaterOrigin = array();
$resultCatalogWaterOrigin = $apiInstanceCatalogs->getWaterOriginsUsingGET();
//print_r($resultCatalogPhytosanitaryEquipment);//die;
if (($resultCatalogWaterOrigin != false) && (count($resultCatalogWaterOrigin) > 0)) {
    foreach ($resultCatalogWaterOrigin as $data) {
        //echo "<br />".$rcpe["id"]." ".$rcpe["name"];
        $arrCatalogWaterOrigin[$data["id"]] = $data["name"];
        $i++;
    }
}



?>