<?php



function htmlStartPage($back=false) {

    global $settings;

    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

        <title>NIVA UC3</title>

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>

        <script src="../js/js_chartjs.js"></script>

        <!-- General stylesheet -->
        <link rel="stylesheet" href="../style/styles.css">
        <!-- Tables stylesheet -->
        <link rel="stylesheet" href="../style/styles_tables.css">
        <!-- Additional CSS Files -->
        <link rel="stylesheet" href="../assets/css/fontawesome.css">
        <!--<link rel="stylesheet" href="../assets/css/templatemo-style.css">-->
        <link rel="stylesheet" href="../assets/css/owl.css">
        <!-- Leaflet JavaScript library -->
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossorigin=""/>

        <!-- favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="../assets/images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">            

        <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
        <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
        <!--<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
            integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
            crossorigin=""></script>-->
        
        <!-- Bootstrap stylesheet -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    </head>

    <body class="is-preload">

        <!-- Wrapper -->
        <div id="wrapper">

        <!-- Main -->
            <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                <div class="logo">
                    <img src="../assets/images/EU_logo.png" style="width:180px;margin-right:50px;" />
                    NIVA UC3
                </div>
                <div style="float:right;width:130px;margin-top:20px;margin-right:0px;border:0px solid #000;color: #fff;" alt="logoder">
                    <a href="../out/out.login.php" style="color: #fff; font-weight: normal; font-size: 19px; text-decoration: none">Logout&nbsp;&nbsp;&nbsp;<img src="../assets/images/logout.png" style="width:25px;" /></a>
                    <?php
                    if ($back != false) {
                        ?>
                        <br /><br /><br />
                        <a href="../out/out.menu.php?token=<?=$back?>" style="color: #fff; font-weight: bold; font-size: 15px; text-decoration: none">Main menu</a>
                        <?php
                    }
                    ?>
                </div>
                </header>

                <section class="simple-post">
                    <div class="container-fluid">

    <?php

}

function htmlEndPage() {

    ?>
                    </div>
                </section>

            </div>
            </div>


        </div>

        <br />
        <br />
        <br />
        <div class="footer">
            <br />
            <p>&copy; 2022 NIVA | All rights reserved</p>
        </div>        
    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <script src="../assets/js/browser.min.js"></script>
        <script src="../assets/js/breakpoints.min.js"></script>
        <script src="../assets/js/transition.js"></script>
        <script src="../assets/js/owl-carousel.js"></script>
        <script src="../assets/js/custom.js"></script>
        

    </body>

    </html>

    <?php

}


function getDiv($class, $id, $adicionales, $content) {
    $div = "<div ";
    if ($class != "")
      $div.= "class=\"".$class."\" ";
    if ($id != "")
      $div.= "id=\"".$id."\" ";
    return $div.$adicionales.">\n".$content."\n</div>\n";
}

function getSpan($class, $id, $adicionales, $content) {
    $span = "<span ";
    if ($class != "")
        $span.= "class=\"".$class."\" ";
    if ($id != "")
        $span.= "id=\"".$id."\" ";
    return $span.$adicionales.">".$content."</span>";
}

function getCeldaInputFile() {
    $return = "<span class=\"celda_input\">";
    $return.= "<input type=\"File\" name=\"archivos[]\" />";
    $return.= "</span>";
return $return;
}

function getInputTag($type, $class, $name, $id, $value, $title, $readonly=false, $adicionales="") {
    $input = "<input type=\"".$type."\" ";
    if ($class != "")
        $input.= "class=\"".$class."\" ";
    if ($name != "")
        $input.= "name=\"".$name."\" ";
    if ($id != "")
        $input.= "id=\"".$id."\" ";
    if ($value != "")
        $input.= "value=\"".$value."\" ";
    if ($title != "")
        $input.= "title=\"".$title."\" ";

    //$input = "<input type=\"".$type."\" class=\"".$class."\" name=\"".$name."\" id=\"".$id."\" value=\"".$value."\" title=\"".$title."\" ";
    if ($readonly)
        $input.= "READONLY ";
    $input.= $adicionales." />\n";
    return $input;
}

function getTextarea($cols, $rows, $class, $name, $id, $value, $title, $readonly=false, $adicionales="") {
    $input = "<textarea ";
    if ($cols != 0)
        $input.= "cols=".$cols." ";
    if ($rows != 0)
        $input.= "rows=".$rows." ";
    if ($class != "")
        $input.= "class=\"".$class."\" ";
    if ($name != "")
        $input.= "name=\"".$name."\" ";
    if ($id != "")
        $input.= "id=\"".$id."\" ";
    if ($value != "")
        $input.= "value=\"".$value."\" ";
    if ($title != "")
        $input.= "title=\"".$title."\" ";

    //$input = "<input type=\"".$type."\" class=\"".$class."\" name=\"".$name."\" id=\"".$id."\" value=\"".$value."\" title=\"".$title."\" ";
    if ($readonly)
        $input.= "READONLY ";
    $input.= $adicionales." >\n";
    $input.= $value."\n";
    $input.= "</textarea>\n";
    return $input;

}

function muestraCalendarioPopup($nombre_input, $idcalendar, $value="") {
    return "<span class=\"celda_input\" id=\"fila\"><input class=\"fecha\" id=\"".$idcalendar."\" type=\"text\" READONLY name=\"".$nombre_input."\" value=\"".$value."\" title=\"DD/MM/YY\" onClick=\"displayCalendarFor('".$idcalendar."')\"><a href=\"javascript:void(0)\" onClick=\"displayCalendarFor('".$idcalendar."')\">&nbsp;>></a></span><span class=\"espacio_calendario\" id=\"espacio_calendario\">&nbsp;</span>";
    //return "<span class=\"celda_input\" id=\"fila\"><input class=\"fecha\" id=\"".$idcalendar."\" type=\"text\" READONLY name=\"".$nombre_input."\" value=\"".$value."\" title=\"DD/MM/YY\" onClick=\"displayCalendarFor('".$idcalendar."')\"><a href=\"javascript:void(0)\" onClick=\"displayCalendarFor('".$idcalendar."')\">&nbsp;>></a></span><span class=\"espacio_calendario\" id=\"".$idcalendar."\">&nbsp;</span>";
}

function getFormStart($name, $id, $action, $method, $enctype, $adicionales) {
    $form = "<form name=\"".$name."\" ";
    if ($id != "")
        $form.= "id=\"".$id."\" ";
    if ($action != "")
        $form.= "action=\"".$action."\" ";
    if ($method != "")
        $form.= "method=\"".$method."\" ";
    if ($enctype != "")
        $form.= "enctype=\"".$enctype."\" ";
    $form.= $adicionales." >\n";
    return $form;
}

function getFormEnd() {
    return "</form>\n";
}

function getSelectStart($name, $id, $adicionales=""){
    return "<select name='".$name."' id='".$id."' ".$adicionales.">\n";
}

function getSelectEnd(){
    return "</select>\n";
}

function getOption($value, $text, $disabled=false, $selected=false, $adicionales=""){
    if (!$disabled && !$selected)
        $return = "\t<option value='".$value."' ".$adicionales.">".$text."</option>\n";
    elseif(!$disabled&&$selected)
        $return = "\t<option value='".$value."' selected='selected' ".$adicionales.">".$text."</option>\n";
    else
        $return = "\t<option value='".$value."' disabled ".$adicionales.">".$text."</option>\n";
    return $return;
}

function getImageTag($src, $class, $id, $alt, $title, $adicionales) {
    $return = "<img src=\"".$src."\" ";
    if ($class != "")
        $return.= "class=\"".$class."\" ";
    if ($id != "")
        $return.= "id=\"".$id."\" ";
    if ($alt != "")
        $return.= "alt=\"".$alt."\" ";
    if ($title != "")
        $return.= "title=\"".$title."\" ";
    $return.= $adicionales." />\n";
    return $return;
}

function getFontTag($class, $id, $value, $size, $face, $color) {
    $return = "<font class=\"".$class."\" ";
    if ($id != "")
        $return.= "id=\"".$id."\" ";
    if ($size != "")
        $return.= "size=\"".$size."\" ";
    if ($face != "")
        $return.= "face=\"".$face."\" ";
    if ($color != "")
        $return.= "color=\"".$color."\" ";
    $return.= ">".$value."</font>\n";
    return $return;
}

function getDivStart($class, $id, $adicionales) {
    $div = "<br /><div ";
    if ($class != "")
        $div.= "class=\"".$class."\" ";
    if ($id != "")
        $div.= "id=\"".$id."\" ";
    return $div.$adicionales.">\n";
}

function getDivEnd() {
    return "</div>\n</ br>";
}

function getEncabezadoStart($nivel, $medio=false) {
    $return = "";
    if ($medio != false)
        $return.= "\n<div class=\"encabezado_medio".$nivel."\">\n";
    else
        $return.= "\n<div class=\"encabezado".$nivel."\">\n";
    return $return;
}

function getEncabezadoEnd() {
    return "</div>\n";
}

function getCeldaDatos($nivel, $nombre) {
    return "<span class=\"celda_datos".$nivel."\">".$nombre."</span>\n";
}

function getHTMLMainSummary($arr_polygons, $pun_code_search) {

    $return = "<table class=\"table_summary\">";
    $return.= "<tr>";
    $return.= "<th>Production Unit</th>";
    $return.= "<th>Country</th>";
    $return.= "<th>Key</th>";
    $return.= "<th>Surface</th>";
    $return.= "</tr>";

    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            $return.= "<tr>";
            $return.= "<td>".$pun_code_search."</td>";
            $return.= "<td>".$ap["country"]."</td>";
            $return.= "<td>".$ap["key"]."</td>";
            $return.= "<td>".$ap["surface"]." ha</td>";
            $return.= "</tr>";
        }
    }


    $return.= "</table>";


    return $return;
}

function getHTMLMainFarmerSummary($arr_polygons, $far_personalid_search) {

    $return = "<table class=\"table_summary\">";
    $return.= "<tr>";
    $return.= "<th>Farmer</th>";
    $return.= "<th>Production Unit</th>";
    $return.= "<th>Country</th>";
    $return.= "<th>Key</th>";
    $return.= "<th>Surface</th>";
    $return.= "</tr>";

    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            $return.= "<tr>";
            $return.= "<td>".$far_personalid_search."</td>";
            $return.= "<td>".$ap["apu_code"]."</td>";
            $return.= "<td>".$ap["country"]."</td>";
            $return.= "<td>".$ap["key"]."</td>";
            $return.= "<td>".$ap["surface"]." ha</td>";
            $return.= "</tr>";
        }
    }


    $return.= "</table>";


    return $return;
}

function getHTMLEncabezadoMapa($fad_personalid_search, $parcel_search, $bearer, $mostrar_ab, $mostrar_rp, $mostrar_fc, $refparcel_search, $num_apu_code) {

    global $settings, $arrCatalogLandCover, $arr_polygons;

    /*$landcovercolors = array();
    $landcovercolors[0] = "#8B0000";
    $landcovercolors[1] = "#B8860B";
    $landcovercolors[2] = "#DC143C";
    $landcovercolors[3] = "#CD853F";*/

    //echo getDiv("div_contenedor_formulario_capas", "", "", getHTMLFormularioCapas($fad_personalid_search, $bearer, $mostrar_ab, $mostrar_rp, $mostrar_fc));
    //echo getDiv("div_contenedor_contenedor_coordenadas", "", "", getHTMLContenedorCoordenadas());

    echo "<table class=\"table_content_summary_preview\">";
    echo "<tr>";
    echo "<td>".getHTMLFormularioCapas($fad_personalid_search, $parcel_search, $bearer, $mostrar_ab, $mostrar_rp, $mostrar_fc, $refparcel_search, $num_apu_code)."</td>";
    echo "<td>".getHTMLContenedorCoordenadas($mostrar_ab, $mostrar_rp, $mostrar_fc)."</td>";
    echo "</tr>";
    echo "</table>";
    echo "<br />";

    if (($mostrar_ab) && (!$mostrar_rp) && (!$mostrar_fc)) {
        
        $landcovers = getLandCoversFromPolygons($arr_polygons);


        //print_r($arrCatalogLandCover);
        //print_r($landcovercolors);

        $i = 1;
        echo "<div class=\"div_legend_main_farmer\">";
        if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
            foreach ($arrCatalogLandCover as $aclc) {
                if (in_array($arrCatalogLandCover[$i], $landcovers)) {
                    echo "<span class=\"span_piechart_legend_color\" style=\"background-color:".$settings->_landcovercolors[($i-1)]."\">&nbsp;</span>";
                    echo "<span class=\"span_piechart_legend_name\">".$aclc."</span>";
                }
                $i++;
            }
        }
        echo "</div>";

        /*echo "<div class=\"div_legend\">";
        $i = 0;
        if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
            foreach ($arrCatalogLandCover as $aclc) {
                if ($arr_data["total"]["landcover"][$keysArrCatalogLandCover[$i]] != 0) {
                    echo "<span class=\"span_piechart_legend_color\" style=\"background-color:".$landcovercolors[$i]."\">&nbsp;</span>";
                    echo "<span class=\"span_piechart_legend_name\">".$aclc."</span><br />";
                }
                    
                $i++;
            }
        }
        echo "</div>";*/

    }

    if ($mostrar_fc) {
        echo "<table class=\"table_content_summary_preview_activities\">";
        echo "<tr>";
        /*echo "<td>  
                    <a data-toggle=\"popover_irrigation\" 
                            title=\"<b>Irrigation</b>\" 
                            data-bs-html=\"true\"
                            data-content=\"Ref Parcel: 4105300000000000030012000001\" 
                            data-placement=\"bottom\" 
                            class=\"btn_irrigation\"
                            id=\"id_btn_irrigation\">
                        <img src=\"../assets/images/irrigation_icon.png\" class=\"avatar_activities\" />
                    </a>
            </td>";
        echo "<td>  
                    <a data-toggle=\"popover_fertilizer\" 
                            title=\"Fertilizer\" 
                            data-bs-html=\"true\"
                            data-content=\"Ref Parcel: 4105300000000000030012000001\" 
                            data-placement=\"bottom\" 
                            class=\"btn_fertilizer\"
                            id=\"id_btn_fertilizer\">
                        <img src=\"../assets/images/fertilizer_icon.png\" class=\"avatar_activities\" />
                    </a>
            </td>";
        echo "<td>
                    <a data-toggle=\"popover_phytosanitary\" 
                            title=\"Phytosanitary\" 
                            data-bs-html=\"true\"
                            data-content=\"Ref Parcel: 4105300000000000030012000001\" 
                            data-placement=\"bottom\" 
                            class=\"btn_phytosanitary\"
                            id=\"id_btn_phytosanitary\">
                        <img src=\"../assets/images/phytosanitary_icon.png\" class=\"avatar_activities\" />
                    </a>
            </td>";
        echo "<td>
                    <a data-toggle=\"popover_activity\" 
                            title=\"Activity\" 
                            data-bs-html=\"true\"
                            data-content=\"Ref Parcel: 4105300000000000030012000001\" 
                            data-placement=\"bottom\" 
                            class=\"btn_activity\"
                            id=\"id_btn_activity\">
                        <img src=\"../assets/images/activity_icon.png\" class=\"avatar_activities\" />
                    </a>
            </td>";*/


        echo "<td>
                <img src=\"../assets/images/phytosanitary_icon.png\" class=\"avatar_activities\" id=\"id_avatar_phytosanitaries\" />
            </td>";
        echo "<td>
                <img src=\"../assets/images/irrigation_icon.png\" class=\"avatar_activities\" id=\"id_avatar_irrigation\" />
            </td>";
        echo "<td>
                <img src=\"../assets/images/fertilizer_icon.png\" class=\"avatar_activities\" id=\"id_avatar_fertilizer\" />
            </td>";
        echo "</tr>";
        echo "</table>";

        echo getHTMLToast("id_toast_phytosanitaries", "false", "Phytosanitaries", "id_toast_phytosanitaries_body");
        echo getHTMLToast("id_toast_irrigation", "false", "Irrigation", "id_toast_irrigation_body");
        echo getHTMLToast("id_toast_fertilizer", "false", "Fertilizer", "id_toast_fertilizer_body");

        echo getHTMLDiv("id_div_phytosanitaries", "false", "Phytosanitaries", "id_div_phytosanitaries_body");
        echo getHTMLDiv("id_div_irrigation", "false", "Irrigation", "id_div_irrigation_body");
        echo getHTMLDiv("id_div_fertilizer", "false", "Fertilizer", "id_div_fertilizer_body");





        ?>
        <!--<div class="toast" data-autohide="false" id="id_toast_activities">
            <div class="toast-header">
                Activities
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body" id="id_toast_activities_body">
            </div>
        </div>-->
        <?php


    }
    if ($parcel_search != "") {
        echo "<br /><div class=\"center\"><img src=\"../assets/images/circulo_seleccionado.png\" width=20 />&nbsp;".$parcel_search."&nbsp;selected</div>";
    }
    
    ?>



    <script>
    $(document).ready(function(){
        $("#id_avatar_phytosanitaries").click(function(){
            if ($("#id_avatar_phytosanitaries").is(":visible")) {
                $('#id_div_phytosanitaries').hide();
            }
            if ($("#id_avatar_phytosanitaries").not(":visible")) {
                $('#id_div_phytosanitaries').show();
            }
            //$('#id_toast_phytosanitaries').toast('show');
            //$('#id_div_phytosanitaries').show();
            $('#id_div_irrigation').hide();
            $('#id_div_fertilizer').hide();
        });    
        $("#id_avatar_irrigation").click(function(){
            if ($("#id_avatar_irrigation").is(":visible")) {
                $('#id_div_irrigation').hide();
            }
            if ($("#id_avatar_irrigation").not(":visible")) {
                $('#id_div_irrigation').show();
            }
            //$('#id_toast_phytosanitaries').toast('show');
            //$('#id_div_phytosanitaries').show();
            $('#id_div_phytosanitaries').hide();
            $('#id_div_fertilizer').hide();
        });    
        $("#id_avatar_fertilizer").click(function(){
            if ($("#id_avatar_fertilizer").is(":visible")) {
                $('#id_div_fertilizer').hide();
            }
            if ($("#id_avatar_fertilizer").not(":visible")) {
                $('#id_div_fertilizer').show();
            }
            //$('#id_toast_phytosanitaries').toast('show');
            //$('#id_div_phytosanitaries').show();
            $('#id_div_phytosanitaries').hide();
            $('#id_div_irrigation').hide();
        });   
    });

    </script>
    <?php

}

function getHTMLToast($id_toast, $autohide, $header, $id_body) {
    
    $return = "<toaster-container toaster-options=\"{'position-class': 'toast-container','time-out': 3000, 'close-button':true}\"></toaster-container>";
    $return.= "<div class=\"toast\" data-autohide=\"".$autohide."\" id=\"".$id_toast."\">";
    $return.= "<div class=\"toast-header\">";
    $return.= $header;
    $return.= "<button type=\"button\" class=\"ml-2 mb-1 close\" data-dismiss=\"toast\">&times;</button>";
    $return.= "</div>";
    $return.= "<div class=\"toast-body\" id=\"".$id_body."\"></div>";
    $return.= "</div>";
    
    return $return;
}

function getHTMLDiv($id_div, $autohide, $header, $id_body) {
    
    $return = "<div class=\"div_cerrado\" id=\"".$id_div."\">";
    //$return.= "<br /><h4 class=\"class_h4\">".$header."</h4><br />";
    $return.= "<div class=\"div-body\" id=\"".$id_body."\"></div>";
    $return.= "</div>";

    return $return;
}

function getHTMLContenedorCoordenadas($mostrar_ab, $mostrar_rp, $mostrar_fc) {
    
    $return = "<pre id=\"contenedor_coordenadas\">";

    $return.= getSpan("", "", "", "CODE");
    $return.= getSpan("span_data", "span_data_id_code", "", "&nbsp;");
    $return.= getSpan("", "", "", "KEY");
    $return.= getSpan("span_data", "span_data_id_key", "", "&nbsp;");
    $return.= getSpan("", "", "", "SURFACE");
    $return.= getSpan("span_data", "span_data_id_surface", "", "&nbsp;");

    if ($mostrar_rp) {
        $return.= "<br />";
        $return.= getSpan("", "", "", "REF. PARCEL");
        $return.= getSpan("span_data", "span_data_id_refparcel", "", "&nbsp;");
    }

    if ($mostrar_fc) {
        $return.= "<br />";
        $return.= getSpan("", "", "", "REF. PARCEL");
        $return.= getSpan("span_data", "span_data_id_refparcel", "", "&nbsp;");        
        $return.= getSpan("", "", "", "CP KEY");
        $return.= getSpan("span_data", "span_data_id_fckey", "", "&nbsp;");        
        $return.= "<br />";
        $return.= getSpan("", "", "", "CP SURFACE");
        $return.= getSpan("span_data", "span_data_id_fcsurface", "", "&nbsp;");        
        $return.= getSpan("", "", "", "CROP PRODUCT");
        $return.= getSpan("span_data", "span_data_id_fccropproduct", "", "&nbsp;");        
    }

    $return.= "</pre>";

    return $return;
}

function getHTMLFormularioCapas($fad_personalid_search, $parcel_search, $bearer, $mostrar_ab, $mostrar_rp, $mostrar_fc, $refparcel_search="", $num_apu_code="") {

    $href = "../out/out.main.farmer.php?fad_personalid_search=".$fad_personalid_search."&token=".$bearer."&parcel_search=".$parcel_search;
    if ($refparcel_search != "")
        $href.= "&refparcel_search=".$refparcel_search;
    if ($num_apu_code != "")
        $href.= "&num_apu_code=".$num_apu_code;
    $return = "<table class=\"table_summary_preview_layers\">";
    $return.= "<tr>";
    $return.= "<th>AB</th>";
    $return.= "<td><a href=\"".$href."&mostrar_ab=true#mapid\" >View Agricultural Block</a></td>";
    $return.= "<td style=\"background: #FF0000;width:15px;\">&nbsp;</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<th>RP</th>";
    $return.= "<td><a href=\"".$href."&mostrar_rp=true#mapid\" >View Reference Plot</a></td>";
    $return.= "<td style=\"background: #FF00FF;width:15px;\">&nbsp;</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<th>CP</th>";
    $return.= "<td><a href=\"".$href."&mostrar_fc=true#mapid\" >View Crop Parcel</a></td>";
    $return.= "<td style=\"background: #07A84C;width:15px;\">&nbsp;</td>";
    $return.= "</tr>";
    $return.= "</table>";

    return $return;
}


function getHTMLMainFarmerDetailedSummary($arr_polygons, $fad_personalid_search, $bearer, $parcel_search) {

    global $arrCatalogLandCover;

    $total_surface = '0';
    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            if (($ap["reference_plots"] != false) && (count($ap["reference_plots"]) > 0)) {
                foreach ($ap["reference_plots"] as $aprp) {
                    $total_surface += $aprp["rpgeometry"]["surface"];
                }
            }

        }
    }

    $estadisticas = extraerEstadisticas($arr_polygons);

    $return = "<table class=\"table_contenedor_summary_preview\">";
    $return.= "<tr><td>";

    $return.= "<table class=\"table_summary_preview\">";
    $return.= "<tr>";
    $return.= "<th>Farmer</th>";
    $return.= "<th>".$fad_personalid_search."</th>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Total surface</td>";
    $return.= "<td>".$total_surface." ha</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Number of parcels</td>";
    $return.= "<td>".$estadisticas["parcels_num_parcels"]."</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Average parcel surface</td>";
    $return.= "<td>".redondear_dos_decimal($total_surface / $estadisticas["parcels_num_parcels"])." ha</td>";
    $return.= "</tr>";
    $return.= "</table>";

    $return.= "</td><td>";

    $return.= "<table class=\"table_summary_preview_crops\">";
    $return.= "<tr>";
    $return.= "<th colspan=2>Crops</th>";
    $return.= "</tr>";


    //print_r($estadisticas["arr_crops"]);
    //echo "<br />";
    if (($estadisticas["arr_crops"] != false) && (count($estadisticas["arr_crops"]) > 0)) {
        $keys_crops = array_keys($estadisticas["arr_crops"]);

        if (($keys_crops != false) && (count($keys_crops) > 0)) {
            foreach ($keys_crops as $kc) {
                if (!is_numeric($kc)) {

                    $porcentaje = redondear_dos_decimal(($estadisticas["arr_crops"][$kc]["surface"] * 100) / $total_surface);

                    $return.= "<tr>";
                    $return.= "<td>".$kc."</td>";
                    $return.= "<td>".$estadisticas["arr_crops"][$kc]["surface"]." ha (".$porcentaje." %)</td>";
                    $return.= "</tr>";
                }
            }
        }
    }

    $return.= "</table>";

    $return.= "</td><td>";

    $return.= "<table class=\"table_summary_preview_activities\">";
    $return.= "<tr>";
    $return.= "<th colspan=2>Activities</th>";
    $return.= "</tr>";

    $porcentaje_phyto = redondear_dos_decimal(($estadisticas["total_has_phyto"] * 100) / $total_surface);

    $return.= "<tr>";
    $return.= "<td>Phytosanitary</td>";
    $return.= "<td>".$estadisticas["total_has_phyto"]." ha (".$porcentaje_phyto." %)</td>";
    $return.= "</tr>";

    $porcentaje_irr = redondear_dos_decimal(($estadisticas["total_has_irr"] * 100) / $total_surface);

    $return.= "<tr>";
    $return.= "<td>Irrigation</td>";
    $return.= "<td>".$estadisticas["total_has_irr"]." ha (".$porcentaje_irr." %)</td>";
    $return.= "</tr>";

    $porcentaje_fer = redondear_dos_decimal(($estadisticas["total_has_fer"] * 100) / $total_surface);

    $return.= "<tr>";
    $return.= "<td>Fertilizer</td>";
    $return.= "<td>".$estadisticas["total_has_fer"]." ha (".$porcentaje_fer." %)</td>";
    $return.= "</tr>";
    $return.= "</table>";

    $return.= "</td></tr>";
    $return.= "</table>";


    $return.= "<br /><br />";

    $return.= "<table class=\"table_summary\">";
    $return.= "<tr>";
    //$return.= "<th>Farmer</th>";
    $return.= "<th>Production Unit</th>";
    $return.= "<th>Land cover</th>";
    $return.= "<th>AB Surface</th>";
    $return.= "<th>AB</th>";
    $return.= "<th>Reference Plot</th>";
    $return.= "<th>RP</th>";
    $return.= "<th>RP Surface</th>";
    $return.= "</tr>";

    $i = 0;
    $coordenadas_iniciales = "";
    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            
            $class = "table_summary_odd";
            if (($i % 2) == 1)
                $class = "table_summary_even";

                $rowspan = "1";
                if (count($ap["reference_plots"]) > 1) {
                    $rowspan = count($ap["reference_plots"]);
                }
    
                if (($ap["reference_plots"] != false) && (count($ap["reference_plots"]) > 0)) {
                    $j = 0;
                    foreach ($ap["reference_plots"] as $aprp) {
                        //print_r($ap);die;
                        $return.= "<tr class=\"".$class."\">";
                        //$return.= "<td>".$fad_personalid_search."</td>";
                        if ($j == 0) {
                            $return.= "<td rowspan=".$rowspan.">".$ap["apu_code"]."</td>";
                            $return.= "<td rowspan=".$rowspan.">".$arrCatalogLandCover[$ap["soil_type"]]."</td>";
                            $return.= "<td rowspan=".$rowspan.">".$ap["surface"]." ha</td>";
                            $return.= "<td rowspan=".$rowspan."><a href=\"../out/out.main.farmer.php?fad_personalid_search=".$fad_personalid_search."&token=".$bearer."&mostrar_ab=true&parcel_search=".$ap["apu_code"]."&num_apu_code=".$ap["num_apu_code"]."#mapid\"><img src=\"../assets/images/maps_icon.png\" class=\"icon\" /></a></td>";
                        }
                        //$return.= "<td>&nbsp;</td>";
                        //$return.= "<td>".$aprp["key"]."</td>";
                        //$return.= "<td>".$aprp["reference_parcel"]."</td>";
                        $return.= "<td>".$aprp["key"]." - ".$aprp["reference_parcel"]."</td>";
                        $return.= "<td><a href=\"../out/out.main.farmer.php?fad_personalid_search=".$fad_personalid_search."&token=".$bearer."&mostrar_rp=true&parcel_search=".$ap["apu_code"]."&refparcel_search=".$aprp["reference_parcel"]."&num_apu_code=".$ap["num_apu_code"]."#mapid\"><img src=\"../assets/images/maps_icon.png\" class=\"icon\" /></a></td>";
                        $return.= "<td>".$aprp["rpgeometry"]["surface"]." ha</td>";

                        $return.= "</tr>";
    
                        if ($parcel_search == $ap["apu_code"]) {
                            $coordenadas_iniciales = $aprp["rpgeometry"]["latini"].",".$aprp["rpgeometry"]["lngini"];
                        }
                        if ($coordenadas_iniciales == "") {
                            $coordenadas_iniciales = $aprp["rpgeometry"]["latini"].",".$aprp["rpgeometry"]["lngini"];
                        }
                        $j++;
                    }
                }

            $i++;
        }
    }

    $return.= "</table>";

    echo $return;

    return $coordenadas_iniciales;
}

function getHTMLMainFarmerQuerySummary($arr_polygons, $fad_personalid_search, $bearer, $parcel_search, $arr_data) {

    global $settings, $arrCatalogLandCover;

    $return = "<div class=\"span_contenedor_querys\">";

    $return.= "<div class=\"span_table_querys_total\">";
    $return.= "<br />";
    
    $return.= "<h4 class=\"class_titulo_querys\">Total</h4>";
    $return.= "<br />";

    $return.= "<table class=\"table_summary_preview_querys_global\">";
    //$return.= "<tr>";
    //$return.= "<th colspan=2>Total</th>";
    //$return.= "</tr>";
    /*$return.= "<tr>";
    $return.= "<td>Total surface (ha)</td>";
    $return.= "<th class=\"data\">".number_format($arr_data["total"]["surfaceAB"], 2)." ha</th>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Agricultural block surface (ha)</td>";
    $return.= "<td class=\"data\">".number_format($arr_data["total"]["surfaceAB"], 2)." ha</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Reference plot surface (ha)</td>";
    $return.= "<td class=\"data\">".number_format($arr_data["total"]["surfaceRP"], 2)." ha</td>";
    $return.= "</tr>";*/
    $return.= "<tr>";
    $return.= "<td>Crop parcel surface (ha)</td>";
    $return.= "<th class=\"data\">".number_format($arr_data["total"]["surfaceCP"], 2)." ha</th>";
    $return.= "</tr>";
    $return.= "</table>";
    
    $return.= "<br />";

    // land covers
    $return.= "<table class=\"table_summary_preview_querys_global\">";
    $return.= "<tr>";
    $return.= "<th>Land cover type</th>";
    $return.= "<th>Surface (ha)</th>";
    $return.= "</tr>";
    
    $i = 0;
    $keysArrCatalogLandCover = array_keys($arrCatalogLandCover);
    if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
        foreach ($arrCatalogLandCover as $aclc) {
            $return.= "<tr>";
            $return.= "<td>".$aclc."</td>";
            $return.= "<td class=\"data\">".number_format($arr_data["total"]["landcover"][$keysArrCatalogLandCover[$i]], 2)." ha</td>";
            $return.= "</tr>";
            $i++;
        }
    }
    
    $return.= "</table>";

    $return.= "<div class=\"div_content_landcover\">";
    $return.= "<br /><canvas id=\"chart_total_landcover\" class=\"span_piechart_landcover\"></canvas>";

    if ($settings->_mostrar_legend_barras) {
        $return.= "<div class=\"div_legend\">";
        $i = 0;
        if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
            foreach ($arrCatalogLandCover as $aclc) {
                if ($arr_data["total"]["landcover"][$keysArrCatalogLandCover[$i]] != 0) {
                    $return.= "<span class=\"span_piechart_legend_color\" style=\"background-color:".$settings->_landcovercolors[$i]."\">&nbsp;</span>";
                    $return.= "<span class=\"span_piechart_legend_name\">".$aclc."</span><br />";
                }
                    
                $i++;
            }
        }
        $return.= "</div>";
    }
    $return.= "</div>";

    // irrigation
    $return.= "<table class=\"table_summary_preview_querys_global\">";
    $return.= "<tr>";
    $return.= "<th>Irrigation</th>";
    $return.= "<th>Surface (ha)</th>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Irrigated land</td>";
    $return.= "<td class=\"data\">".number_format($arr_data["total"]["irrigation"][1], 2)." ha</td>";
    $return.= "</tr>";
    $return.= "<tr>";
    $return.= "<td>Non-irrigated land</td>";
    $return.= "<td class=\"data\">".number_format($arr_data["total"]["irrigation"][0], 2)." ha</td>";
    $return.= "</tr>";
    $return.= "</table>";

    $return.= "<br /><canvas id=\"chart_total_irrigation\" class=\"span_piechart\"></canvas>";

    if ($settings->_mostrar_legend_pie) {
        $return.= "<div class=\"div_legend\">";
        $i = 1;
        if (($settings->_arrCatalogIrrigation != false) && (count($settings->_arrCatalogIrrigation) > 0)) {
            foreach ($settings->_arrCatalogIrrigation as $aci) {
                //echo "<br />comparo: 0 con ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                if ($arr_data["total"]["irrigation"][$settings->_arrCatalogIrrigation[$i]] != 0) {
                    //$return.= "<br />".$aclc." - ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                    $return.= "<span class=\"span_piechart_legend_color\" style=\"background-color:".$settings->_irrigationcolors[$i]."\">&nbsp;</span>";
                    $return.= "<span class=\"span_piechart_legend_name\">".$settings->_arrCatalogIrrigationLegend[$i]."</span><br />";
                }
                    
                $i--;
            }
        }
        $return.= "</div>"; // div_legend
    }

    
    $return.= "</div>";


    if ($fad_personalid_search != "") {
        $return.= "<div class=\"span_table_querys_farmer\">";
        $return.= "<br />";
        
        $return.= "<h4 class=\"class_titulo_querys\">Farmer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$fad_personalid_search."</h4>";
        $return.= "<br />";

        $return.= "<table class=\"table_summary_preview_querys_global\">";
        //$return.= "<tr>";
        //$return.= "<th colspan=2>Farmer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$fad_personalid_search."</th>";
        //$return.= "</tr>";
        /*$return.= "<tr>";
        $return.= "<td>Total surface (ha)</dh>";
        $return.= "<th class=\"data\">".number_format($arr_data["farmer"]["surfaceAB"], 2)." ha</th>";
        $return.= "</tr>";
        $return.= "<tr>";
        $return.= "<td>Agricultural block surface (ha)</td>";
        $return.= "<td class=\"data\">".number_format($arr_data["farmer"]["surfaceAB"], 2)." ha</td>";
        $return.= "</tr>";
        $return.= "<tr>";
        $return.= "<td>Reference plot surface (ha)</td>";
        $return.= "<td class=\"data\">".number_format($arr_data["farmer"]["surfaceRP"], 2)." ha</td>";
        $return.= "</tr>";*/
        $return.= "<tr>";
        $return.= "<td>Crop parcel surface (ha)</td>";
        $return.= "<th class=\"data\">".number_format($arr_data["farmer"]["surfaceFC"], 2)." ha</th>";
        $return.= "</tr>";
        $return.= "</table>";
        
        $return.= "<br />";

        // land covers
        $return.= "<table class=\"table_summary_preview_querys_global\">";
        $return.= "<tr>";
        $return.= "<th>Land cover type</th>";
        $return.= "<th>Surface (ha)</th>";
        $return.= "</tr>";
        
        $i = 0;
        $keysArrCatalogLandCover = array_keys($arrCatalogLandCover);
        if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
            foreach ($arrCatalogLandCover as $aclc) {
                $return.= "<tr>";
                $return.= "<td>".$aclc."</td>";
                $return.= "<td class=\"data\">".number_format($arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]], 2)." ha</td>";
                $return.= "</tr>";
                $i++;
            }
        }
        
        $return.= "</table>";

        $return.= "<div class=\"div_content_landcover\">";
        $return.= "<br /><canvas id=\"chart_farmer_landcover\" class=\"span_piechart_landcover\"></canvas>";

        if ($settings->_mostrar_legend_barras) {
            $return.= "<div class=\"div_legend\">";
            $i = 0;
            if (($arrCatalogLandCover != false) && (count($arrCatalogLandCover) > 0)) {
                foreach ($arrCatalogLandCover as $aclc) {
                    //echo "<br />comparo: 0 con ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                    if ($arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]] != 0) {
                        //$return.= "<br />".$aclc." - ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                        $return.= "<span class=\"span_piechart_legend_color\" style=\"background-color:".$settings->_landcovercolors[$i]."\">&nbsp;</span>";
                        $return.= "<span class=\"span_piechart_legend_name\">".$aclc."</span><br />";
                    }
                        
                    $i++;
                }
            }
            $return.= "</div>";
        }
        $return.= "</div>";

        // irrigation
        $return.= "<table class=\"table_summary_preview_querys_global\">";
        $return.= "<tr>";
        $return.= "<th>Irrigation</th>";
        $return.= "<th>Surface (ha)</th>";
        $return.= "</tr>";
        $return.= "<tr>";
        $return.= "<td>Irrigated land</td>";
        $return.= "<td class=\"data\">".number_format($arr_data["farmer"]["irrigation"][1], 2)." ha</td>";
        $return.= "</tr>";
        $return.= "<tr>";
        $return.= "<td>Non-irrigated land</td>";
        $return.= "<td class=\"data\">".number_format($arr_data["farmer"]["irrigation"][0], 2)." ha</td>";
        $return.= "</tr>";
        $return.= "</table>";
        
        //$return.= "<br /><canvas id=\"chart_farmer_irrigation\" class=\"span_piechart\"></canvas>";
        //$return.= "</div>";

        $return.= "<div class=\"div_content_irrigation\">";
        $return.= "<br /><canvas id=\"chart_farmer_irrigation\" class=\"span_piechart_irrigation\"></canvas>";
        
        if ($settings->_mostrar_legend_pie) {
            $return.= "<div class=\"div_legend\">";
            $i = 1;
            if (($settings->_arrCatalogIrrigation != false) && (count($settings->_arrCatalogIrrigation) > 0)) {
                foreach ($settings->_arrCatalogIrrigation as $aci) {
                    //echo "<br />comparo: 0 con ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                    if ($arr_data["farmer"]["irrigation"][$settings->_arrCatalogIrrigation[$i]] != 0) {
                        //$return.= "<br />".$aclc." - ".$arr_data["farmer"]["landcover"][$keysArrCatalogLandCover[$i]];
                        $return.= "<span class=\"span_piechart_legend_color\" style=\"background-color:".$settings->_irrigationcolors[$i]."\">&nbsp;</span>";
                        $return.= "<span class=\"span_piechart_legend_name\">".$settings->_arrCatalogIrrigationLegend[$i]."</span><br />";
                    }
                        
                    $i--;
                }
            }
            $return.= "</div>"; // div_legend
        }
        $return.= "</div>"; // div_content_irrigation

        $return.= "</div>";

        $return.= "<br />";
        
    }
    $return.= "</div>";

    $return.= "<br /><br />";



    ?><script language="javascript">
    $( document ).ready(function() {

        var barColorsLandCover = [];
        <?php
        if (count($settings->_landcovercolors) > 0) {
            foreach ($settings->_landcovercolors as $slcc) {
                ?>
                barColorsLandCover.push("<?=$slcc?>");
                <?php
            }
        }
        ?>

        var barColorsIrrigation = [];
        <?php
        if (count($settings->_irrigationcolors) > 0) {
            foreach ($settings->_irrigationcolors as $sic) {
                ?>
                barColorsIrrigation.push("<?=$sic?>");
                <?php
            }
        }
        ?>        


        // farmer land cover
        var xvaluesfarmerlandcover = [];
        var xvaluesfarmerlandcoverlegend = [];
        var xvaluesfarmerbarcolor = [];
        <?php
            $xvaluesfarmerlandcover = array();
            if (($arr_data["farmer"]["landcover"] != false) && (count($arr_data["farmer"]["landcover"]) > 0)) {
                $i = 1;
                foreach ($arr_data["farmer"]["landcover"] as $aflc) {

                    if ($aflc > 0) {
                        ?>
                        xvaluesfarmerlandcover.push(<?=$aflc?>);
                        xvaluesfarmerlandcoverlegend.push("<?=$arrCatalogLandCover[$i]?>");
                        xvaluesfarmerbarcolor.push(barColorsLandCover[<?=($i-1)?>]);
                        <?php
                    }
                    $i++;
                }
        } ?>

        // total land cover
        var xvaluestotallandcover = [];
        var xvaluestotallandcoverlegend = [];
        var xvaluestotalbarcolor = [];
        <?php 
            $xvaluestotallandcover = array();
            //print_r($arr_data["total"]["landcover"]);
            if (($arr_data["total"]["landcover"] != false) && (count($arr_data["total"]["landcover"]) > 0)) {
                $i = 1;
                $j = 0;
                foreach ($arr_data["total"]["landcover"] as $atlc) {

                    if ($atlc > 0) {
                        ?>
                        xvaluestotallandcover[<?=$j?>] = <?=$atlc?>;
                        xvaluestotallandcoverlegend[<?=$j?>] = "<?=$arrCatalogLandCover[$i]?>";
                        //xvaluestotalbarcolor.push(barColorsLandCover[<?=($i-1)?>]);
                        xvaluestotalbarcolor[<?=$j?>] = barColorsLandCover[<?=($i-1)?>];
                        <?php
                        $j++;
                    }
                    $i++;
                }
        } ?>

        // farmer irrigation
        var xvaluesfarmerirrigation = [];
        var xvaluesfarmerirrigationlegend = [];
        //var totalhasxvaluesfarmerirrigation = 0;
        xvaluesfarmerirrigationlegend[0] = "Non-irrigation";
        xvaluesfarmerirrigationlegend[1] = "Irrigation";
        <?php 
            if (($arr_data["farmer"]["irrigation"] != false) && (count($arr_data["farmer"]["irrigation"]) > 0)) {
                $i = 1;
                foreach ($arr_data["farmer"]["irrigation"] as $afi) {
                    ?>
                    xvaluesfarmerirrigation[<?=($i-1)?>] = <?=$afi?>;
                    <?php
                    $i++;
                }
        } ?>

        // total irrigation
        var xvaluestotalirrigation = [];
        var xvaluestotalirrigationlegend = [];
        //var totalhasxvaluestotalirrigation = 0;
        xvaluestotalirrigationlegend[0] = "Non-irrigation";
        xvaluestotalirrigationlegend[1] = "Irrigation";
        <?php 
            $totalhasxvaluestotalirrigation = 0;
            if (($arr_data["total"]["irrigation"] != false) && (count($arr_data["total"]["irrigation"]) > 0)) {
                $i = 1;
                foreach ($arr_data["total"]["irrigation"] as $ati) {
                    ?>
                    xvaluestotalirrigation[<?=($i-1)?>] = <?=$ati?>;
                    <?php
                    //$totalhasxvaluestotalirrigation = $totalhasxvaluestotalirrigation + $ati;
                    $i++;
                }
        }

        ?>

        


        //drawChartJS('chart_farmer_landcover', 'Farmer Land Cover Type', xvaluesfarmerlandcoverlegend, xvaluesfarmerlandcover, barColorsLandCover, false);
        //drawChartJS('chart_total_landcover', 'Total Land Cover Type', xvaluestotallandcoverlegend, xvaluestotallandcover, barColorsLandCover, false);
        drawChartJS('chart_farmer_irrigation', 'Farmer Irrigation', xvaluesfarmerirrigationlegend, xvaluesfarmerirrigation, barColorsIrrigation, false);
        drawChartJS('chart_total_irrigation', 'Total Irrigation', xvaluestotalirrigationlegend, xvaluestotalirrigation, barColorsIrrigation, false);        
        
        //alert(xvaluesfarmerlandcover);
        //alert(xvaluesfarmerlandcoverlegend);
        //alert(xvaluesfarmerbarcolor);
        
        //drawChartBarJS('chart_farmer_landcover', 'Farmer Land Cover', xvaluesfarmerlandcoverlegend, xvaluesfarmerlandcover, barColorsLandCover, false);        
        drawChartBarNewJS('chart_farmer_landcover', 'Farmer Land Cover', xvaluesfarmerlandcoverlegend, xvaluesfarmerlandcover, xvaluesfarmerbarcolor, false);
        drawChartBarNewJS('chart_total_landcover', 'Total Land Cover Type', xvaluestotallandcoverlegend, xvaluestotallandcover, xvaluestotalbarcolor, false);

        /*
        // chart.js example
        var xValues = ["Italy", "France", "Spain", "USA", "Argentina"];
        var yValues = [55, 49, 44, 24, 15];
        var barColors = [
                "#b91d47",
                "#00aba9",
                "#2b5797",
                "#e8c3b9",
                "#1e7145"
            ];
        drawChartJS("myChart", "World Wide Wine Production 2018", xValues, yValues, barColors);
        */

    });

    </script>
    
    <?php

    $coordenadas_iniciales = "";

    $return.= "</table>";

    echo $return;

    return $coordenadas_iniciales;
}


function getHTMLTooltip($apu_code, $key, $surface) {

    $return = "<div style=\"border-bottom: 0px solid #000\">";
    $return.= "<div class=\"div_fila_ab\"><span class=\"tootltip_fc_code\">Code</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$apu_code."</span></div>";
    $return.= "<div class=\"div_fila_ab\"><span class=\"tootltip_fc_code\">Key</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$key."</span></div>";
    $return.= "<div class=\"div_fila_ab\"><span class=\"tootltip_fc_code\">Surface</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$surface."</span></div>";
    $return.= "</div>";    

    return $return;
}

function getHTMLTooltipRP($apu_code, $reference_parcel, $key, $surface) {

    $return = "<div style=\"border-bottom: 0px solid #000\">";
    $return.= "<div class=\"div_fila_rp\"><span class=\"tootltip_fc_code\">Code</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$apu_code."</span></div>";
    $return.= "<div class=\"div_fila_rp\"><span class=\"tootltip_fc_code\">RefParcel</span></div>";
    $return.= "<div class=\"div_fila_rp\"><span>".$reference_parcel."</span></div>";
    $return.= "<div class=\"div_fila_rp\"><span class=\"tootltip_fc_code\">Key</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$key."</span></div>";
    $return.= "<div class=\"div_fila_rp\"><span class=\"tootltip_fc_code\">Surface</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$surface."</span></div>";
    $return.= "</div>";

    return $return;
}

function getHTMLTooltipFC($apu_code, $reference_parcel, $rpkey, $fckey, $surface, $cropproduct) {

    $return = "<div style=\"border-bottom: 0px solid #000\">";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">Code</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$apu_code."</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">RefParcel</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span>".$reference_parcel."</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">RP Key</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$rpkey."</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">FC Key</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$fckey."</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">Surface</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$surface."</span></div>";
    $return.= "<div class=\"div_fila_fc\"><span class=\"tootltip_fc_code\">Crop Product</span><span class=\"separator\">&nbsp;</span><span class=\"tootltip_fc_value\">".$cropproduct."</span></div>";
    $return.= "</div>";

    return $return;
}

function getHTMLPopoverPhytosanitary($arr_phytosanitary) {

    global $arrCatalogPhytosanitary, $arrCatalogPhytosanitaryEquipment, $arrCatalogUnitType, $settings;

    $arr_phytosanitary_ordenado = ordenarArray("start_date", $arr_phytosanitary);

    $i = 0;
    $return = "";
    if (($arr_phytosanitary_ordenado != false) && (count($arr_phytosanitary_ordenado) > 0)) {
        $return.= "<br /><br /><b>Phytosanitaries</b>";
        $return.= "<table>";
        $return.= "<tr><th>Start date</th><th>End date</th><th>Phytosanitary</th><th>Dose</th><th>Unit</th><th>Equipment</th></tr>";
        foreach ($arr_phytosanitary_ordenado as $data) {
            $expstartdate = explode(" ", $data["start_date"]);
            $expenddate = explode(" ", $data["end_date"]);
            if ($i < $settings->_maxtablesorangesize) {
                $return.= "<tr border=0><td width=70 border=0>".$expstartdate[0]."</td><td width=70>".$expenddate[0]."</td><td width=200>".$arrCatalogPhytosanitary[$data["id"]]."</td><td width=40>".$data["dose"]."</td><td width=120>".$arrCatalogUnitType[$data["unit"]]."</td><td width=250>".$arrCatalogPhytosanitaryEquipment[$data["equipment"]]."</td></tr>";
            }
            $i++;
        }
        $return.= "</table>";
    }

    //$return = "<span><span>texto en div</span></span>";

    return $return;
}

function getHTMLPopoverIrrigation($arr_irrigation) {

    global $arrCatalogIrrigationType, $arrCatalogWaterOrigin, $arrCatalogUnitType, $settings;

    $arr_irrigation_ordenado = ordenarArray("date", $arr_irrigation);

    $i = 0;
    $return = "";
    if (($arr_irrigation_ordenado != false) && (count($arr_irrigation_ordenado) > 0)) {
        $return.= "<br /><br /><b>Irrigation</b>";
        $return.= "<table>";
        $return.= "<tr><th>Date</th><th>Type</th><th>WO</th><th>Vol</th><th>Unit</th></tr>";
        foreach ($arr_irrigation_ordenado as $data) {
            //echo "//**//**".$data["unit"];
            //print_r($data);die;
            $expdate = explode(" ", $data["date"]);
            //$return.= "<tr><td width=100>".$expdate[0]."</td><td width=150>".$arrCatalogIrrigationType[$data["type"]]."</td><td width=80>".$arrCatalogWaterOrigin[$data["water_origin"]]."</td><td width=60>".$data["volume"]."</td><td width=40>".$arrCatalogUnitType[$data["unit"]]."</td></tr>";
            if ($i < $settings->_maxtablesorangesize) {
                $return.= "<tr><td width=100>".$expdate[0]."</td><td width=150>".$arrCatalogIrrigationType[$data["type"]]."</td><td width=80>".$arrCatalogWaterOrigin[$data["water_origin"]]."</td><td width=60>".$data["volume"]."</td><td width=40>".$arrCatalogUnitType[$data["unit"]]."</td></tr>";
            }
            $i++;
        }
        $return.= "</table>";
    }

    return $return;
}

function getHTMLPopoverFertilizer($arr_fertilizer) {

    global $arrCatalogUnitType, $arrCatalogFertilizationMethods, $arrCatalogFertilizationTypes, $arrCatalogFertilizer, $arrCatalogFertilizerTypes, $settings;

    //echo "** popoverfertilizer 0: ".print_r($arr_fertilizer, true)."**";
    //echo "** arrCatalogFertilizer 0: ".print_r($arrCatalogFertilizer, true)."**";
    $arr_fertilizer_ordenado = ordenarArray("start_date", $arr_fertilizer);

    $i = 0;
    $return = "";
    if (($arr_fertilizer_ordenado != false) && (count($arr_fertilizer_ordenado) > 0)) {
        $return.= "<br /><br /><b>Fertilizer</b>";
        $return.= "<table>";
        $return.= "<tr><th>Start Date</th><th>End Date</th><th>Fertilizer</th><th>Type</th><th>Fer. Type</th><th>Fer. Method</th><th>Dose</th><th>Unit</th></tr>";
        foreach ($arr_fertilizer_ordenado as $data) {
            //echo "//**//**".$data["unit"];
            //print_r($data);die;
            $expstartdate = explode(" ", $data["start_date"]);
            $expenddate = explode(" ", $data["end_date"]);
            //$return.= "<tr><td width=100>".$expdate[0]."</td><td width=150>".$arrCatalogIrrigationType[$data["type"]]."</td><td width=80>".$arrCatalogWaterOrigin[$data["water_origin"]]."</td><td width=60>".$data["volume"]."</td><td width=40>".$arrCatalogUnitType[$data["unit"]]."</td></tr>";
            if ($i < $settings->_maxtablesorangesize) {
                /*$return.= "*****";
                $return.= "*".$expstartdate[0];
                $return.= "*".$expenddate[0];
                $return.= "*".print_r($arrCatalogFertilizer, true)."AA";
                $return.= "*".$arrCatalogFertilizer[$data["id"]];
                $return.= "*".$arrCatalogFertilizerTypes[$data["type"]];
                $return.= "*".$arrCatalogFertilizationTypes[$data["fertilization_type"]];
                $return.= "*".$arrCatalogFertilizationMethods[$data["fertilization_method"]];
                $return.= "*".$data["dose"];
                $return.= "*".$arrCatalogUnitType[$data["unit"]];
                $return.= "*****";*/
                $return.= "<tr><td width=100>".$expstartdate[0]."</td><td width=100>".$expenddate[0]."</td><td width=150>".$arrCatalogFertilizer[$data["id"]]."</td><td width=80>".$arrCatalogFertilizerTypes[$data["type"]]."</td><td width=80>".$arrCatalogFertilizationTypes[$data["fertilization_type"]]."</td><td width=80>".$arrCatalogFertilizationMethods[$data["fertilization_method"]]."</td><td width=80>".$data["dose"]."</td><td width=40>".$arrCatalogUnitType[$data["unit"]]."</td></tr>";
            }
            $i++;
        }
        $return.= "</table>";
    }
    //echo "** popoverfertilizer fin: ".$return."**";

    return $return;
}


function getColorAB($get, $reference_parcel, $mostrar_ab, $mostrar_rp, $mostrar_fc, $landcover) {

    global $settings;
    
    $return = $settings->_color_ab_relleno;
    //echo "<b>comparo: ".$get." con ".$reference_parcel."</b>";
    if (($get != "") && ($get == $reference_parcel))
        $return = $settings->_color_ab_relleno_seleccionado;
    else {
        if (($mostrar_ab) && (!$mostrar_rp) && (!$mostrar_fc)) {
            $return = $settings->_landcovercolors[($landcover-1)];
            //echo "***".$settings->_landcovercolors[$landcover]."***";
        }
    }
    return $return;
}

//function getColorRP($get, $reference_parcel) {
function getColorRP($get, $reference_parcel, $get_parcel_search, $get_num_apu_code, $apu_code, $num_apu_code) {

    global $settings;
    
    if ($apu_code == "ES000000003032") {
        /*echo "<br />get: ".$get;
        echo "<br />reference_parcel: ".$reference_parcel;
        echo "<br />get_parcel_search: ".$get_parcel_search;
        echo "<br />get_num_apu_code: ".$get_num_apu_code;
        echo "<br />apu_code: ".$apu_code;
        echo "<br />num_apu_code: ".$num_apu_code;*/
    }

    $return = $settings->_color_rp_relleno;
    //echo "<b>comparo: ".$get." con ".$reference_parcel."</b>";
    // si selecciono RP y es el que voy a mostrar
    if (($get != "") && ($get == $reference_parcel)) {
        $return = $settings->_color_rp_relleno_seleccionado;
    }
    if ($get == "") {
        if ($get_parcel_search != "") {
            if (($get == $reference_parcel) || ($get_parcel_search == $apu_code) && ($get_num_apu_code == $num_apu_code))
                $return = $settings->_color_rp_relleno_seleccionado;
        }
    }
    return $return;
}

function getOpacityRP($get, $reference_parcel) {

    global $settings;
    
    $return = $settings->_opacidad_rp_relleno;
    //echo "<b>comparo: ".$get." con ".$reference_parcel."</b>";
    if (($get != "") && ($get == $reference_parcel))
        $return = $settings->_opacidad_rp_relleno_seleccionado;
    return $return;
}

function getColorFC($get, $reference_parcel) {

    global $settings;
    
    $return = $settings->_color_fc_relleno;
    //echo "<b>comparo: ".$get." con ".$reference_parcel."</b>";die;
    if (($get != "") && ($get == $reference_parcel))
        $return = $settings->_color_fc_relleno_seleccionado;
    return $return;
}

function getOpacityFC($get, $reference_parcel) {

    global $settings;
    
    $return = $settings->_opacidad_fc_relleno;
    //echo "<b>comparo: ".$get." con ".$reference_parcel."</b>";
    if (($get != "") && ($get == $reference_parcel))
        $return = $settings->_opacidad_fc_relleno_seleccionado;
    return $return;
}

function getLandCoversFromPolygons($arr_polygons) {

    global $arrCatalogLandCover;

    $i = 0;
    $arr_landcovers = array();
    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            if (!in_array($arrCatalogLandCover[$ap["soil_type"]], $arr_landcovers)) {
                $arr_landcovers[$i] = $arrCatalogLandCover[$ap["soil_type"]];
                $i++;
            }
        }
    }

    return $arr_landcovers;
}

function extraerEstadisticas($arr_polygons) {

    //print_r($arr_polygons);
    $return = array();
    // número de parcelas
    $num_parcels = 0;
    // superficie total
    $total_surface = 0;
    // array de cultivos del farmer
    $arr_crops = array();
    // array de superficies de cultivos del farmer
    $arr_crops_surface = array();
    // numero de parcelas que aplicaron fitos
    $num_parcels_phyto = 0;
    $total_has_phyto = 0;
    // numero de parcelas que aplicaron riego
    $num_parcels_irr = 0;
    $total_has_irr = 0;
    // numero de parcelas que aplicaron fertilizantes
    $num_parcels_fer = 0;
    $total_has_fer = 0;

    if (($arr_polygons != false) && (count($arr_polygons) > 0)) {
        foreach ($arr_polygons as $ap) {
            if (($ap["reference_plots"] != false) && (count($ap["reference_plots"]) > 0)) {
                $j = 0;
                foreach ($ap["reference_plots"] as $aprp) {
                    //echo "<br /><hr /><br />";
                    //print_r($aprp);
                    if (($aprp["field_crops_stats"] != false) && (count($aprp["field_crops_stats"]) > 0)) {
                        foreach ($aprp["field_crops_stats"] as $aprpfcst) {
                            $total_surface = $total_surface + $aprpfcst["fcgeometry"]["surface"];
                            $num_parcels++;

                            //echo "<br />cultivo: ".$aprpfcst["crop_product"];
                            //echo "<br />surface: ".$aprpfcst["fcgeometry"]["surface"];
                            //echo "<br />numparcel: ".$num_parcels;

                            $cultivo = $aprpfcst["crop_product"];
                            if (in_array($cultivo, $arr_crops)) {
                                //echo "<br />".$cultivo." : ".$aprpfcst["fcgeometry"]["surface"];
                                $arr_crops[$aprpfcst["crop_product"]]["surface"] = $arr_crops[$aprpfcst["crop_product"]]["surface"] + $aprpfcst["fcgeometry"]["surface"];
                                $arr_crops[$aprpfcst["crop_product"]]["numparcels"] = $arr_crops[$aprpfcst["crop_product"]]["numparcels"] + 1;
                            }
                            else {
                                //$arr_crops[$aprpfcst["crop_product"]]["surface"] = $aprpfcst["fcgeometry"]["surface"];                            
                                array_push($arr_crops, $cultivo);
                                $arr_crops[$aprpfcst["crop_product"]]["surface"] = $aprpfcst["fcgeometry"]["surface"];
                                $arr_crops[$aprpfcst["crop_product"]]["numparcels"] = 1;
                            }

                            // si se aplicó riego
                            if (($aprpfcst["crop_irrigation"] != false) && (count($aprpfcst["crop_irrigation"]) > 0)) {
                                $num_parcels_irr++;
                                $total_has_irr = $total_has_irr + $aprpfcst["fcgeometry"]["surface"];
                            }
                            // si se aplicarion fitos
                            if (($aprpfcst["phytosanitary"] != false) && (count($aprpfcst["phytosanitary"]) > 0)) {
                                $num_parcels_phyto++;
                                $total_has_phyto = $total_has_phyto + $aprpfcst["fcgeometry"]["surface"];
                            }
                            // si se aplicó fertilizante
                            if (($aprpfcst["fertilizer"] != false) && (count($aprpfcst["fertilizer"]) > 0)) {
                                $num_parcels_fer++;
                                $total_has_fer = $total_has_fer + $aprpfcst["fcgeometry"]["surface"];
                            }
                        }
                    }
                
                }
            }

        }
    }

    $return["parcels_total_surface"] = $total_surface;
    $return["parcels_num_parcels"] = $num_parcels;
    //$return["parcels_average_surface"] = redondear_dos_decimal($total_surface / $num_parcels);
    $return["arr_crops"] = $arr_crops;

    $return["num_parcels_phyto"] = $num_parcels_phyto;
    $return["num_parcels_irr"] = $num_parcels_irr;
    $return["num_parcels_fer"]  = $num_parcels_fer;
    $return["total_has_phyto"] = $total_has_phyto;
    $return["total_has_irr"] = $total_has_irr;
    $return["total_has_fer"] = $total_has_fer;
    

    //print_r($return);

    return $return;
}

?>