




function drawChartBarNewJS(spanid, charttitle, xValues, yValues, barColors, legend_display) {

    var densityData = {
        //data: [5427, 5243, 5514, 3933, 1326, 687, 1271, 1638],
        data: yValues,
        //backgroundColor: ['#000000', '#FF00FF', '#FF0000', '#0000FF', '#00FF00', '#FFFF00', '#00FFFF', '#AAFF00']
        backgroundColor: barColors
    };
    var ctx = $("#" + spanid).get(0).getContext("2d");
    var barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            //labels: ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"],
            labels: xValues,
            datasets: [densityData],
            backgroundColor: barColors
        },
        options: {
            title: {
                display: true,
                text: charttitle
            },
            legend: { display: legend_display },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        display: false
                    },
                    gridLines: {
                        display: true,
                        drawOnChartArea: false,
                        color: '#DFAD6B'
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        display: true,
                        fontColor: '#DFAD6B'
                    },
                    gridLines: {
                        display: true,
                        drawOnChartArea: false,
                        color: '#DFAD6B'
                    }
                }]                
            }
        }
    });   


    /*new Chart(spanid, {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: charttitle
            },
            legend: {
                display: legend_display
            },
            tooltips: {
                enabled: true
            },
            mantainAspectRatio: true
        }
    });*/
        
}

function drawChartJS(spanid, charttitle, xValues, yValues, barColors, legend_display) {

    new Chart(spanid, {
        type: "pie",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            title: {
                display: true,
                text: charttitle
            },
            legend: {
                display: legend_display
            },
            tooltips: {
                enabled: true
            },
            mantainAspectRatio: true
        }
    });
}




