<?php

// op.login.php

include_once("../inc/inc.utils.php");
require_once('../vendor/autoload.php');

session_start();

$username = "";
if ((isset($_POST["username"])) && (strlen($_POST["username"]) > 0)) {
    $username = sanitizeString($_POST["username"]);
}

$password = "";
if ((isset($_POST["password"])) && (strlen($_POST["password"]) > 0)) {
    $password = sanitizeString($_POST["password"]);
}

if (($username != "") && ($password != "")) {

    $arr = array();
    $arr["username"] = $username;
    $arr["password"] = $password;
    //$data = new \Swagger\Client\Model\LoginRequest($arr); // \Swagger\Client\Model\ActMatsMessage | data
    
    
    $apiInstance = new Swagger\Client\Api\AuthControllerApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
        new GuzzleHttp\Client()
    );
    $login_request = new \Swagger\Client\Model\LoginRequest($arr); // \Swagger\Client\Model\LoginRequest | loginRequest
    
    try {
        $result = $apiInstance->authenticateUserUsingPOST($login_request);
        //print_r($result);
        $bearer = $result["accessToken"];
        setcookie("cookie_token", $bearer, 0);
        $_SESSION["token"] = $bearer;
        $sesion = $bearer;
        header("Location: ../out/out.menu.php?token=".$bearer);
    } catch (Exception $e) {
        header("Location: ../out/out.login.php?error_login=true");
        //echo 'Exception when calling AuthControllerApi->authenticateUserUsingPOST: ', $e->getMessage(), PHP_EOL;
    }
    

}