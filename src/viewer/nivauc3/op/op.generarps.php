<?php


$arr_cropparcels = array();

// 00225772G
$arr_cropparcels[0] = "2e8cee30-c674-45e4-9950-49d59d251fc7";
$arr_cropparcels[1] = "cc7e3f57-0047-46b2-970d-b89f1d4b4b4b";
$arr_cropparcels[2] = "6b3e12e5-8009-46ac-9d00-23970891749e";
$arr_cropparcels[3] = "60f9a2ab-e969-47d9-96d4-7e750b6d0bc4";
$arr_cropparcels[4] = "ae59e50c-7836-4a16-8000-82540ef48f7a";

// A14010136
$arr_cropparcels[5] = "f6b62674-fe07-488b-b290-bd6d704d4306";
$arr_cropparcels[6] = "155fb3d4-217e-42d3-bf52-59c8fb421ef0";
$arr_cropparcels[7] = "dfd142a0-24b9-4b88-bdee-89cd519adff7";
$arr_cropparcels[8] = "ace92508-2b83-4a55-b8ff-3e18f2fbe003";
$arr_cropparcels[9] = "74365490-d8cb-47ca-86ba-4cab2f0ffbe0";
$arr_cropparcels[10] = "1d4d5010-819b-4c56-8ac2-8224b4730591";

// B11843307
$arr_cropparcels[11] = "f40d7250-c1c9-4fbc-aee3-47c73f1e530c";
$arr_cropparcels[12] = "f3a0ff86-4ae2-44e1-a011-d13734ef584e";
$arr_cropparcels[13] = "4b0f23a0-99c4-444e-8028-9e5bfffa51f8";
$arr_cropparcels[14] = "a084cdf8-782e-4456-9818-73065c592705";
$arr_cropparcels[15] = "8e2a6816-c879-422c-a5c7-18316e1daa41";
$arr_cropparcels[16] = "585306f9-e192-4c77-b192-6cf68b857443";
$arr_cropparcels[17] = "39f6b218-0c10-40a3-9719-0129dbc143aa";
$arr_cropparcels[18] = "9768f055-479d-4f5f-9316-9eb572253980";
$arr_cropparcels[19] = "977d0fd0-38d7-4a36-b3e9-7160364460a0";
$arr_cropparcels[20] = "7a455f68-5eb8-4c85-b3cc-e533ceb784f5";
$arr_cropparcels[21] = "9e3f3521-2dbd-4d08-8f9a-00c36c51ff77";

// B14512602
$arr_cropparcels[22] = "1a69ca88-3607-4687-ac60-ce744c1dd358";
$arr_cropparcels[23] = "6da9527d-e0cb-41df-b2fc-20ecc2341eb8";
$arr_cropparcels[24] = "569ee7a5-0c4c-488e-8828-f7833da3b361";
$arr_cropparcels[25] = "1a24c442-a8fe-493e-99da-48816403e8fb";
$arr_cropparcels[26] = "33889eea-85a9-4236-966c-c682a6612c30";
$arr_cropparcels[27] = "51d6a832-4ebf-4afc-acef-76869610f86d";
$arr_cropparcels[28] = "8b91ff8f-25c6-483d-ac8c-1f0602badd44";
$arr_cropparcels[29] = "1cb210b8-c021-4755-abb2-3793bb06ec1d";
$arr_cropparcels[30] = "afa91a8c-b75d-457f-a610-b352b262510b";
$arr_cropparcels[31] = "056a77ab-cc21-434f-98d7-05f2a4ca4dea";
$arr_cropparcels[32] = "caf939a1-e3e4-40e7-9d37-e3f0456e0158";
$arr_cropparcels[33] = "90ff21a5-0678-4355-a539-e2142d22424e";
$arr_cropparcels[34] = "e9033da6-a491-4fac-b6ec-89a8bee19d96";
$arr_cropparcels[35] = "6ae2c172-5855-4e48-be31-6a4699916441";
$arr_cropparcels[36] = "6a71339a-f51c-4088-a940-43781a3b0063";
$arr_cropparcels[37] = "9502b0c0-12cf-4ae6-aced-9fb7b51101a3";
$arr_cropparcels[38] = "edf20f30-6a8e-4209-8314-98d1a687560d";
$arr_cropparcels[39] = "e307e746-aafd-4082-9be0-9bb7d69dd567";
$arr_cropparcels[40] = "b5358ede-4280-41b4-a25c-42b9a80aa3a8";
$arr_cropparcels[41] = "f6d37873-c38c-47d2-a7f0-2f0a4ea99afb";
$arr_cropparcels[42] = "8cd11072-333b-47fa-b01a-6f97bc9e5aa8";
$arr_cropparcels[43] = "e5119bcf-f7d5-49e7-bfd4-406c91643360";
$arr_cropparcels[44] = "f73d4cec-3e64-43cb-862b-94fa90300eba";
$arr_cropparcels[45] = "7db9bfae-f201-4a64-84d0-0da3a2aa64d9";
$arr_cropparcels[46] = "edbad7b0-ace8-4ce1-ae93-e1c9797f2a4e";
$arr_cropparcels[47] = "37ae7535-672f-4a50-9b2b-dac31e4cf4b6";
$arr_cropparcels[48] = "47659df6-b647-4f99-9838-ba64444e3f16";

// F41155607
$arr_cropparcels[49] = "26ad73f7-1b93-4806-b871-ecf01224b773";
$arr_cropparcels[50] = "654f35bd-fcf0-46df-917e-49849476a100";
$arr_cropparcels[51] = "1bb387ab-e8c4-4aae-8a6d-06f1d7e7bf30";
$arr_cropparcels[52] = "e4dce5a5-2363-4382-9198-bda97e3ac8d8";
$arr_cropparcels[53] = "c31a675d-c397-400e-8e7d-df4d3e48c41f";
$arr_cropparcels[54] = "fe182539-34cf-473c-a665-2bf3147e72bb";
$arr_cropparcels[55] = "afa1cf53-7106-4986-892e-2680496c722a";
$arr_cropparcels[56] = "c5378fae-2cbb-4d62-b412-40375286ebe8";
$arr_cropparcels[57] = "69bd3be2-4f2b-4588-9490-9890bd01be01";
$arr_cropparcels[58] = "c32308bf-672c-429d-9d05-fbf297460452";
$arr_cropparcels[59] = "733d73a9-3832-4b5d-8f52-6ea8ef934924";
$arr_cropparcels[60] = "756d0235-c97d-41f6-b0c1-b829f99b227a";
$arr_cropparcels[61] = "87bfd338-3d59-49ab-a4cd-5acc423a9ee0";
$arr_cropparcels[62] = "7be0a4f7-2cba-47a6-a099-e403f14be510";
$arr_cropparcels[63] = "cd36eacd-4b9e-4644-877f-79a99ddac775";
$arr_cropparcels[64] = "cdfb212f-d092-4d67-8b55-7ca1ef09019f";

// 14639873M
$arr_cropparcels[65] = "a604056a-716c-4cb6-b6c7-7cb18160262d";
$arr_cropparcels[66] = "e6fa3385-318c-4f3f-bb1a-081dfe5c0f2b";
$arr_cropparcels[67] = "5034ebba-b17d-4426-9258-91681acac4e7";
$arr_cropparcels[68] = "faec68fb-3254-422f-afc0-a0210c1b6c09";
$arr_cropparcels[69] = "70754414-2305-4a61-83d4-7e7ace651a45";
$arr_cropparcels[70] = "3dbf5b31-4191-4e0f-a263-aaca5ca652a9";

// J90422882
$arr_cropparcels[71] = "128da221-500b-414f-92c5-8681a1687b00";
$arr_cropparcels[72] = "f6d022ac-72fb-4960-91f5-df619bd54f25";

// J91335463
$arr_cropparcels[73] = "be3a60d0-e936-467f-a77f-292c17d2baef";




echo "<h4>separar total surface</h4>";
echo "<h4>poner coma en miles</h4>";
echo "<h4>poner porcentajes en pie charts</h4>";


echo "<h5>buscar colores para land covers</h5>";
echo "<h5>out.main.farmer.php nivel AB poner colores de los landcovers en rellenos con diferentes tipologías</h5>";
echo "<h5>out.main.querys.php probar gráfico de barras para land covers</h5>";
echo "<h5>out.main.querys.php colores irrigation: verde y marrón</h5>";
echo "<h5>out.main.querys.php colores land cover: granates y marrón</h5>";
echo "<h5>out.main.querys.php en las tablas sacar sólo total surface por crop parcel</h5>";



$i = 0;
while ($i < count($arr_cropparcels)) {

    echo "<br />-- parcela: ".$arr_cropparcels[$i];

    // irrigation

    //insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('25e0f458-13a0-4328-a581-00f6854ce437', 1, 1, 25, 1, '2022-01-31');
    //insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('25e0f458-13a0-4328-a581-00f6854ce437', 1, 1, 24, 1, '2021-12-31');
    //insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('25e0f458-13a0-4328-a581-00f6854ce437', 1, 1, 25, 1, '2021-11-31');

    // cambiar a 1 a la semana irrigation

    echo "<br />-- insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('".$arr_cropparcels[$i]."', 1, 1, 25, 1, '2022-01-31');";
    echo "<br />-- insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('".$arr_cropparcels[$i]."', 1, 1, 24, 1, '2022-02-28');";
    echo "<br />-- insert into sc_niva.rp_dcropirrigation (rcp_id, irr_id, wao_id, cri_volume, uni_id, cri_irrigationdate) values ('".$arr_cropparcels[$i]."', 1, 1, 25, 1, '2022-03-31');";


    // phytosanitary

    //insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('334496f2-8f15-4dfe-b90e-1cfeba536c79', '12612', 'ES', 0.31, 1, 1, '22-01-31', '22-02-01', '22-12-31');
    //insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('334496f2-8f15-4dfe-b90e-1cfeba536c79', '12677', 'ES', 0.32, 1, 1, '22-01-31', '22-02-01', '22-12-31');
    //insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('334496f2-8f15-4dfe-b90e-1cfeba536c79', '12741', 'ES', 0.33, 1, 1, '22-01-31', '22-02-01', '22-12-31');

    // cambiar a 1 al día phytos

    echo "<br />insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('".$arr_cropparcels[$i]."', '12612', 'ES', 0.31, 1, 1, '2022-01-31', '2022-02-01', '2022-12-31');";
    echo "<br />insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('".$arr_cropparcels[$i]."', '12677', 'ES', 0.32, 1, 1, '2022-01-31', '2022-02-01', '2022-12-31');";
    echo "<br />insert into sc_niva.rp_dcropphytosanit (rcp_id, phy_id, cou_id, cph_dose, uni_id, phe_id, cph_startdate, cph_enddate, cph_expirydate) values ('".$arr_cropparcels[$i]."', '12741', 'ES', 0.33, 1, 1, '2022-01-31', '2022-02-01', '2022-12-31');";


    $i++;
}

?>