<?php

// out.main.farmer.php

require_once('../vendor/autoload.php');
include_once("../inc/inc.settings.php");
include_once("../inc/inc.classui.php");
include_once("../inc/inc.fileutils.php");
include_once("../inc/inc.utils.php");
include_once("../inc/inc.coordinates.php");
include_once("../inc/inc.authentication.php");
include_once("../inc/inc.apicatalogs.php");


global $bearer, $arrCatalogPhytosanitaryEquipment, $arrCatalogUnitType, $arrCatalogCropProduct;


/*echo "logeuado: ";
print_r($_COOKIE);
echo "<br />";
//print_r($_SESSION);
echo "<br/>token (".$bearer.")";*/

$trazas_polygons = false;

$fad_personalid_search = "";
if ((isset($_GET["fad_personalid_search"])) && (strlen($_GET["fad_personalid_search"]) > 0)) {
    $fad_personalid_search = sanitizeString($_GET["fad_personalid_search"]);
}

$parcel_search = "";
if ((isset($_GET["parcel_search"])) && (strlen($_GET["parcel_search"]) > 0)) {
    $parcel_search = sanitizeString($_GET["parcel_search"]);
}

$num_apu_code = "";
if ((isset($_GET["num_apu_code"])) && (strlen($_GET["num_apu_code"]) > 0)) {
    $num_apu_code = sanitizeString($_GET["num_apu_code"]);
}

$refparcel_search = "";
if ((isset($_GET["refparcel_search"])) && (strlen($_GET["refparcel_search"]) > 0)) {
    $refparcel_search = sanitizeString($_GET["refparcel_search"]);
}

$mostrar_ab = false;
$mostrar_rp = false;
$mostrar_fc = false;
if ((isset($_GET["mostrar_ab"])) && (sanitizeString($_GET["mostrar_ab"]) == "true")) {
    $mostrar_ab = true;
}

if ((isset($_GET["mostrar_rp"])) && (sanitizeString($_GET["mostrar_rp"]) == "true")) {
    $mostrar_ab = false;
    $mostrar_rp = true;
}

if ((isset($_GET["mostrar_fc"])) && (sanitizeString($_GET["mostrar_fc"]) == "true")) {
    $mostrar_ab = false;
    $mostrar_rp = false;
    $mostrar_fc = true;
}

$token = "";
if ((isset($_GET["token"])) && (strlen($_GET["token"]) > 0)) {
    $token = sanitizeString($_GET["token"]);
}

htmlStartPage($token);
//print_r($arr_farmers);



echo getFormStart("form_farmer", "form_farmer_id", "../out/out.main.farmer.php", "GET", "", "");

echo getEncabezadoStart(0, false);
echo getCeldaDatos(0, "Search by FARMER Personal ID");
echo getCeldaDatos(0, getInputTag("text", "", "fad_personalid_search", "fad_personalid_search_id", "", ""));
echo getEncabezadoEnd();

echo getEncabezadoStart(0, false);
echo getCeldaDatos(0, getInputTag("submit", "", "", "", "Search", "Search"));
echo getEncabezadoEnd();

echo getInputTag("hidden", "", "token", "", $bearer, "");

echo getFormEnd();


if (isset($_GET["fad_personalid_search"])) {
    //echo "pun_code_a_buscar: ".$_GET["pun_code_search"];

    // --------------------------------------------------------------------------------------------
    // inicio AgriculturalProducerPartiesApi $apiInstance->getByCodeUsingGET

    /*
    //$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyY2FtcGFuYSIsImlhdCI6MTYzMjk5NTkyOCwiZXhwIjoxNjMzMDAxOTI4fQ.i5ESsiaV3YAP7xdYBBOBnCi4PexOZWvovQZmUM6KQYB_3kVALBtWSXC8X_KCD9LHZj35QLkKYVuCcCijyOy0Sw');
    $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', "Bearer ".$bearer."");

    $apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
        new GuzzleHttp\Client(),
        $config
    );

    try {
        $result = $apiInstance->getByCountryUsingGET("ES");
        print_r($result);
    } catch (Exception $e) {
        //echo 'Exception when calling QueryCatalogsApi->getCountriesUsingGET: ', $e->getMessage(), PHP_EOL;
        header("Location: ../out/out.login.php");
    }*/


    //print_r($arrCatalogUnitType);die;

    $apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
        new GuzzleHttp\Client(),
        $config
    );

    $country = "ES"; // string | country

    if (sanitizeString($_GET["fad_personalid_search"]) != "") {
        try {
            // ej ES000000002353 paises bajos
            // ej ES000000003490 andalucía
            // ej ES000000003144 andalucía

            /*
            3144
            -- select * from pr_dproductionunit where pun_id = 'c8efd25e-5722-41c0-8dde-011af89e98dc';
            -- select * from ab_dagrblock where abl_id = '47f975bb-9680-4633-a5e2-ca1f86aec0de';
            -- SELECT rfp_id, rfp_key, abl_id, rfp_refparcel, rfp_datefrom, rfp_dateto FROM sc_niva.rp_drefplot where rfp_id = '158aef9e-0f66-40c8-a901-efb29fb9e920';
            */

            $result = $apiInstance->getByPersonalIdCountryUsingGET($country, sanitizeString($_GET["fad_personalid_search"]));
            //print_r($result["agricultural_production_unit"]);
            //print_r($result);
            $i = 0;
            $j = 0;
            $arr_polygons = array();
            $arr_farmer_units = array();
            if (($result != false) && (count($result["agricultural_production_unit"]) > 0)) {
                if (($result["agricultural_production_unit"] != false) && (count($result["agricultural_production_unit"]) > 0)) {
                    foreach ($result["agricultural_production_unit"] as $res) {
                        $arr_farmer_units[$i]["code"] = $res["code"];
                        $arr_farmer_units[$i]["country"] = $res["country"];
                        $arr_farmer_units[$i]["crop_plot"] = $res["crop_plot"];

                        //$arr_farmer_units[$i]["crop_plot"]["coordinates"] = $res["crop_plot"]["coordinates"];
                        
                        $i++;
                    }

                    //echo "<br /><hr />";
                    //print_r($arr_farmer_units[0]["crop_plot"]);
                    //echo "<br /><hr />";
                    //print_r($arr_farmer_units[1]["crop_plot"]);
                    //echo "<br /><hr />";
                    //echo "count arr_farmer_units: ".count($arr_farmer_units);
                    //echo "<br /><hr />";

                    $coords_string = "";

                    if (($arr_farmer_units != false) && (count($arr_farmer_units) > 0)) {
                        foreach ($arr_farmer_units as $afu) {
                            if (count($afu["crop_plot"]) > 0) {
                                foreach ($afu["crop_plot"] as $cp) {
                                    //echo "<br /><hr />";
                                    //echo "<br />end: ".$cp["end"]->date;
                                    //print_r($cp);
                                    //echo "<br /><b>key</b>: ".$cp["key"];
                                    //echo "<br /><b>surface</b>: ".$cp["geometry"][0]["surface"];
                                    //echo "<br /><b>type</b>: ".$cp["type"];
                                    //echo "<br /><b>geo</b>: ".print_r($cp["geometry"], true);
                                    //echo "<br /><b>coo</b>: ".print_r($cp["geometry"][0]["coordinates"], true);
                                    //echo "<br /><b>coo[0]</b>: ".print_r($cp["geometry"][0]["coordinates"][0], true);
                                    //echo "<br /><b>coo cp[geometry] count</b>: ".count($cp["geometry"]);
                                    //echo "<br /><b>coo cp[geometry][0] count</b>: ".count($cp["geometry"][0]);
                                    //echo "<br /><b>coo cp[geometry][0][coordinates] count</b>: ".count($cp["geometry"][0]["coordinates"]);
                    
                                    $surface = 0;
                                    $coords_string = "";
                                    $coord_ini = array();
                                    if (count($cp["geometry"]) > 0) {
                                        foreach ($cp["geometry"] as $geo) {
                                            //echo "<br />GEO: ".print_r($geo["coordinates"], true);
                                            $surface = $geo["surface"];


                                            foreach ($geo["coordinates"] as $geoitems) {
                                                //echo "<br /><b>GEO ITEMS</b>: ".print_r($geoitems, true);
                                                foreach ($geoitems as $coordspolygons) {
                                                    //echo "<br /><b>COORDS ".$afu["code"]."</b>: ".print_r($coordspolygons, true);
                                                    
                                                    if ($coords_string == "") {
                                                        $coord_ini[0] = $coordspolygons[0];
                                                        $coord_ini[1] = $coordspolygons[1];
                                                    }
                                                    if ($coords_string != "") {
                                                        $coords_string.= ",";
                                                    }
                                                    if (count(explode("(((", $coordspolygons[1])) > 1) {
                                                        $coords_string.= "[[".str_replace("(((", "", $coordspolygons[1]).",".$coordspolygons[2]."]";
                                                    }
                                                    else {
                                                        $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                                                    }
                                                    $coords_string = parseCoordinates($coords_string);
                                                    
                                                    //echo "<br /><b>COORDS</b>: ".print_r($coordspolygons, true);
                                                    /*if ($coords_string == "") {
                                                        $coord_ini[0] = $coordspolygons[0];
                                                        $coord_ini[1] = $coordspolygons[1];
                                                    }
                                                    if ($coords_string != "") {
                                                        $coords_string.= ",";
                                                    }
                                                    $coords_string.= "[".$coordspolygons[0].",".$coordspolygons[1]."]";
                                                    $coords_string = parseCoordinates($coords_string);*/
                                                }
                                            }
                                        }
                                    }
                                    $arr_polygons[$j]["coords"] = "".$coords_string."";
                                    $arr_polygons[$j]["type"] = "Polygon";
                                    if (count(explode("]],[[", $coords_string)) > 1)
                                        $arr_polygons[$j]["type"] = "MultiPolygon";
                                    $arr_polygons[$j]["surface"] = $surface;
                                    $arr_polygons[$j]["key"] = $cp["key"];
                                    $arr_polygons[$j]["apu_code"] = $afu["code"];
                                    $arr_polygons[$j]["num_apu_code"] = $j."_".$afu["code"];
                                    $arr_polygons[$j]["country"] = $afu["country"];
                                    $arr_polygons[$j]["soil_type"] = $cp["soil_type"][0]["type"];

                                    $reference_plots = parseaReferencePlots($cp["reference_plot"], $arr_polygons[$j]["apu_code"], $arr_polygons[$j]["num_apu_code"], $mostrar_rp, $mostrar_fc);
                                    //echo "<br /><br />RP: <br />";
                                    //print_r($reference_plots);
                                    $arr_polygons[$j]["reference_plots"] = $reference_plots;
                                    $j++;
                                } // foreach ($afu["crop_plot"] as $cp) {
                            }
                            //echo "<b>coords_string (".$afu["code"]."): [".$coords_string."]</b>";
                        } // foreach ($arr_farmer_units as $afu) {
                    }

                    //print_r($arr_polygons);die;

                    echo "<br />";
                    $coordenadas_iniciales = getHTMLMainFarmerDetailedSummary($arr_polygons, $fad_personalid_search, $bearer, $parcel_search);
                    //echo "***".$coordenadas_iniciales."***";
                    echo "<br />";
                    echo getHTMLEncabezadoMapa($fad_personalid_search, $parcel_search, $bearer, $mostrar_ab, $mostrar_rp, $mostrar_fc, $refparcel_search, $num_apu_code);
                    //echo "<br />";
                    //echo "<pre id=\"contenedor_coordenadas\"><br /></pre>";
                    //echo getHTMLContenedorCoordenadas();



                    echo "<br /><div class=\"center\"><div id=\"mapid\"></div></div>";

                    ?>
                    <script>
                    //mapboxgl.accessToken = 'pk.eyJ1IjoidmlwZXJpb3JpYnVzIiwiYSI6ImNrOXp3ZHFuMjBpd2gzbG52YWo2Y2JrZnEifQ.p9d6loHUABC0wvgIzSGOww';
                    mapboxgl.accessToken = '<?=$settings->_mapbox_accesstoken?>';
                    var map = new mapboxgl.Map({
                        container: 'mapid',
                        style: 'mapbox://styles/mapbox/satellite-v9',
                        //center: [<?=$coord_ini[0]?>, <?=$coord_ini[1]?>],
                        center: [<?=$coordenadas_iniciales?>],
                        zoom: 14
                    });
                    
                    // Add fullscreen control to the map.
                    map.addControl(new mapboxgl.FullscreenControl());
                    // Add zoom and rotation controls to the map.
                    map.addControl(new mapboxgl.NavigationControl());

                    let hoveredStateId = null;
                    map.on('load', function () {
                        <?php
                        $countapucodeprefix = 0;
                        $countsubfix = 0;
                        if (count($arr_polygons) > 0) {
                            foreach ($arr_polygons as $ap) {

                                if ($trazas_polygons) {
                                    // escribir trazas de los polígonos a nivel AB
                                    escribir_trazas($ap["coords"], "../logs/farmer_".sanitizeString($_GET["fad_personalid_search"])."_ab_".$ap["num_apu_code"]."_time_".mktime().".txt");
                                }

                                $apu_code_prefix = $countapucodeprefix."_".$ap["num_apu_code"];

                                ?>
                                coords = [[<?=$ap["coords"]?>]];  
                                
                                    
                                map.addSource('source_layer_<?=$apu_code_prefix?>', {
                                    'type': 'geojson',
                                    'data': {
                                        'type': 'FeatureCollection',
                                        'features': [
                                            {
                                                'type': 'Feature',
                                                'properties': {
                                                    //'description': 'Code: <?=$ap["apu_code"]?><br>Key: <?=$ap["key"]?><br>Surface <?=$ap["surface"]?> ha'
                                                    'description': '<?=getHTMLTooltip($ap["apu_code"], $ap["key"], $ap["surface"])?>'
                                                },
                                                'geometry': {
                                                    'type': '<?=$ap["type"]?>',
                                                    
                                                    //'coordinates': [[[[-6.01193523702405,36.9639809552435],[-6.01193120726731,36.963974985937],[-6.01068286255209,36.9621253623426],[-6.01051539675803,36.961877231666],[-6.01047800610188,36.9618129407554],[-6.00881940676726,36.962608862716],[-6.00807034172092,36.96296831815],[-6.00804711575998,36.9629267526184],[-6.00801543645922,36.9629556837691],[-6.00770990413377,36.9633794186753],[-6.00765760162268,36.9635830916514],[-6.00761554744304,36.9637468517934],[-6.0076028437976,36.9642368542739],[-6.00759865637163,36.9643983712126],[-6.00770857107229,36.9648044973522],[-6.00772597549634,36.9648688056585],[-6.00784855626885,36.9652968586862],[-6.00779364088889,36.9654790498967],[-6.00773547745842,36.9656720159067],[-6.0077092494995,36.9658833422452],[-6.00770199719923,36.9659417733317],[-6.0077751734319,36.9661662828626],[-6.00789961874605,36.9663949674826],[-6.00869905025015,36.9656729953516],[-6.00873273983789,36.9656448186096],[-6.00884599064908,36.9655047048865],[-6.00888357190671,36.965465827939],[-6.00888577949407,36.9654635435227],[-6.00894296765948,36.9654260949068],[-6.00898517608815,36.9654031708075],[-6.00909420147557,36.965343956789],[-6.00916619054689,36.9653110544531],[-6.00934617796974,36.9652372607462],[-6.00943514034117,36.9652034194584],[-6.00947877235308,36.9651868217507],[-6.00949792608131,36.9651781159998],[-6.00956452056631,36.9651478463317],[-6.00957669654763,36.9651423119484],[-6.00957878046846,36.9651413642869],[-6.00965137190978,36.9651024702771],[-6.00966488983959,36.9650952266922],[-6.00966820663641,36.9650934493753],[-6.00968185434977,36.9650861367155],[-6.00978533454676,36.9650429635936],[-6.00990962221049,36.9649911092901],[-6.01000664603578,36.9649528594079],[-6.01001375175159,36.964950058712],[-6.01011864522006,36.9649087057147],[-6.01020802574854,36.964869963873],[-6.01027415074415,36.9648413015786],[-6.01040712621615,36.9647753484495],[-6.01053975229062,36.9647095680421],[-6.01059150465456,36.9646836465179],[-6.01064495613099,36.9646568735633],[-6.01071365163321,36.9646224670548],[-6.01076795307496,36.9645895395807],[-6.01079649445739,36.9645722331537],[-6.01086488337846,36.9645307643014],[-6.01109413709978,36.9643802134425],[-6.01116436658006,36.9643317466119],[-6.01124480537864,36.9642762344036],[-6.011330438531,36.9642195765507],[-6.0113685936494,36.9642000615547],[-6.01137622621993,36.9641961579741],[-6.01139307863961,36.9641875391768],[-6.01152983606165,36.9641359929406],[-6.01161386542503,36.964114629376],[-6.01162649650617,36.964110762886],[-6.01167451099198,36.9640960643897],[-6.01169377936259,36.9640901659159],[-6.0118075976835,36.9640422240106],[-6.01185976644255,36.9640202499118],[-6.01193523702405,36.9639809552435]],[[-6.00868880989695,36.9647891996677],[-6.00868821547267,36.9648119414265],[-6.00856882890868,36.9649549481507],[-6.00852541948952,36.9651009705085],[-6.00846272446138,36.9652183988821],[-6.00844183498762,36.9652874781389],[-6.00843981505159,36.965310466004],[-6.00843621368615,36.9653514376096],[-6.00843289229067,36.9653892248308],[-6.00827319549826,36.9655512142843],[-6.00822880286183,36.9655962434786],[-6.00822462226414,36.9655854695899],[-6.00820006032665,36.9655221808979],[-6.00821502011751,36.9652460653509],[-6.00822358332442,36.9651949269514],[-6.00823037205117,36.9651543908555],[-6.0082623003114,36.9649637254024],[-6.00838872335735,36.9647235315363],[-6.00840911491478,36.9646139820195],[-6.00851014788125,36.9643564107741],[-6.00851075281146,36.9646285263322],[-6.00851993353557,36.9646717959902],[-6.0085270846828,36.9647055032647],[-6.00853147531554,36.9647101199966],[-6.0085367610311,36.9647156800096],[-6.00856639458804,36.9647468464045],[-6.00863565316618,36.9647642302331],[-6.00868880989695,36.9647891996677]],[[-6.00853319341597,36.9637151457232],[-6.00849520542384,36.9638341516271],[-6.00838923823205,36.9639377613958],[-6.00828903669438,36.9639697611895],[-6.0082206708286,36.9639915938819],[-6.00820816755139,36.9639941474696],[-6.00810481719614,36.9640152551407],[-6.00809727212004,36.9640131432423],[-6.00808622760014,36.9640100522715],[-6.00803101402309,36.9639945980743],[-6.00799680747362,36.9639704805618],[-6.00797673012245,36.9639563260062],[-6.00797154666383,36.9638816659823],[-6.00798258670954,36.9637912758703],[-6.00802269719342,36.963736561278],[-6.0080593185694,36.9636866064554],[-6.00811371461044,36.9636493660154],[-6.00821787033665,36.9635780600679],[-6.00830626809324,36.9635415784202],[-6.00846062987343,36.9636403913775],[-6.00853319341597,36.9637151457232]]]]
                                                    //'coordinates': [[[-6.1265946147371,36.9382406144685],[-6.12581105787296,36.9403290926441],[-6.12758695502307,36.940767283012],[-6.12837038147088,36.9386993871114],[-6.12705486420378,36.9383544778309],[-6.12695256620707,36.9383291683461],[-6.1265946147371,36.9382406144685]]]
                                                    //'coordinates': [[[-5.25291805664084,37.6718762116236],[-5.2523302034369,37.6723297441499],[-5.25232711130184,37.6723321291286],[-5.2529208371502,37.6728156148606],[-5.25312601904053,37.6732872367495],[-5.25349326617395,37.674083340793],[-5.25349786122054,37.6740717595321],[-5.2534987735174,37.6740694612513],[-5.25350280818038,37.6740592938119],[-5.25348730744307,37.6740214511461],[-5.25339244011014,37.6737898446955],[-5.25342138960103,37.673774822255],[-5.25347728453528,37.6737247837298],[-5.25351041154264,37.6737553135561],[-5.25349457780474,37.6737652163172],[-5.25359552381692,37.673850256826],[-5.25359813378061,37.6738701541538],[-5.25364314480715,37.673898018208],[-5.25366478220824,37.6738832427345],[-5.25376458099885,37.6739635180516],[-5.25370062445662,37.674006231188],[-5.25364910457113,37.6740453262742],[-5.25354702936471,37.6739495817734],[-5.25353634075635,37.6739395561112],[-5.2535032346889,37.6739507884484],[-5.25353374877287,37.6740175228372],[-5.25360485840773,37.6741463786587],[-5.25364838833846,37.674196741497],[-5.25376412052274,37.6741344295302],[-5.25391239540893,37.6740369942603],[-5.25405707656938,37.673959660354],[-5.25436729518211,37.6738001423331],[-5.25472451992249,37.6736208027543],[-5.25497262366623,37.6735120819594],[-5.25313015094688,37.6720478856811],[-5.25293980791963,37.6718944499968],[-5.25291805664084,37.6718762116236]]]

                                                    'coordinates': coords
                                                }
                                            }
                                        ]
                                    }
                                });

                                <?php
                                if (($mostrar_ab) && (!$mostrar_rp) && (!$mostrar_fc)) {
                                ?>
                                    // Load an image to use as the pattern from an external URL.
                                    /*map.loadImage(
                                        //'https://docs.mapbox.com/mapbox-gl-js/assets/colorado_flag.png',
                                        '../assets/images/ablandcover/fondo_ab_lc_<?=($ap["soil_type"])?>.png',
                                        (err, image) => {
                                            // Throw an error if something goes wrong.
                                            if (err) throw err;
                                            
                                            // Add the image to the map style.
                                            map.addImage('pattern_<?=$ap["apu_code"]?>', image);
                                            
                                            // Create a new layer and style it using `fill-pattern`.
                                            map.addLayer({
                                                'id': 'pattern-layer_source_layer_<?=$ap["apu_code"]?>',
                                                'type': 'fill',
                                                'source': 'source_layer_<?=$ap["apu_code"]?>',
                                                'paint': {
                                                    'fill-pattern': 'pattern_<?=$ap["apu_code"]?>'
                                                }
                                            });
                                        }
                                    );*/


                                    map.addLayer({
                                        'id': 'fill_layer_<?=$apu_code_prefix?>',
                                        'type': 'fill',
                                        'source': 'source_layer_<?=$apu_code_prefix?>',
                                        'layout': {
                                            // Make the layer visible by default.
                                            'visibility': 'visible'
                                        },
                                        'paint': {
                                            'fill-color': '<?=getColorAB($num_apu_code, $ap["num_apu_code"], $mostrar_ab, $mostrar_rp, $mostrar_fc, $ap["soil_type"])?>',
                                            'fill-opacity': [
                                                'case',
                                                ['boolean', ['feature-state', 'hover'], false],
                                                <?=$settings->_opacidad_ab_relleno_hover?>,
                                                1
                                            ]

                                        },
                                        'filter': ['==', '$type', 'Polygon']
                                    });

                                <?php
                                }
                                else {
                                ?>
                                    map.addLayer({
                                        'id': 'fill_layer_<?=$apu_code_prefix?>',
                                        'type': 'fill',
                                        'source': 'source_layer_<?=$apu_code_prefix?>',
                                        'layout': {
                                            // Make the layer visible by default.
                                            'visibility': 'visible'
                                        },
                                        'paint': {
                                            'fill-color': '<?=getColorAB($num_apu_code, $ap["num_apu_code"], $mostrar_ab, $mostrar_rp, $mostrar_fc, $ap["soil_type"])?>',
                                            'fill-opacity': [
                                                'case',
                                                ['boolean', ['feature-state', 'hover'], false],
                                                <?=$settings->_opacidad_ab_relleno_hover?>,
                                                <?=$settings->_opacidad_ab_relleno?>
                                            ]

                                        },
                                        'filter': ['==', '$type', 'Polygon']
                                    });

                                <?php
                                }
                                ?>

                                map.addLayer({
                                    'id': 'border_layer_<?=$apu_code_prefix?>',
                                    'type': 'line',
                                    'source': 'source_layer_<?=$apu_code_prefix?>',
                                    'layout': {
                                        'line-join': 'round',
                                        'line-cap': 'round'
                                    },
                                    'paint': {
                                        'line-color': '<?=$settings->_color_ab_borde?>',
                                        'line-width': <?=$settings->_grosor_ab_borde?>
                                    }
                                });                                

                                // Create a popup, but don't add it to the map yet.
                                popup = new mapboxgl.Popup({
                                    closeButton: false,
                                    closeOnClick: false
                                });

                                map.on('mousemove', function (e) {

                                    // coordenadas del raton
                                    //document.getElementById('coordenadas').innerHTML = JSON.stringify(e.lngLat);
                                    //document.getElementById('coordenadas').innerHTML = '<?=$ap["apu_code"]?>';

                                    
                                });

                                map.on('mousemove', 'fill_layer_<?=$apu_code_prefix?>', (e) => {

                                    //document.getElementById('contenedor_coordenadas').innerHTML = '<?=$ap["apu_code"]?>';

                                    document.getElementById('span_data_id_code').innerHTML = '<?=$ap["apu_code"]?>';
                                    document.getElementById('span_data_id_key').innerHTML = '<?=$ap["key"]?>';
                                    document.getElementById('span_data_id_surface').innerHTML = '<?=$ap["surface"]?> ha';                                    

                                    /*if (e.features.length > 0) {
                                        
                                        alert(JSON.stringify(e.features, null));
                                        if (hoveredStateId !== null) {
                                            map.setFeatureState(
                                                { source: 'fill_layer_<?=$ap["apu_code"]?>', id: hoveredStateId },
                                                { hover: false }
                                            );
                                        }
                                        hoveredStateId = 'fill_layer_<?=$ap["apu_code"]?>';
                                        //alert("p1 " + e.features[0].id);
                                        map.setFeatureState(
                                            { source: 'fill_layer_<?=$ap["apu_code"]?>', id: hoveredStateId },
                                            { hover: true }
                                        );
                                        //alert("p2");
                                    }*/
                                    // Change the cursor style as a UI indicator.
                                    map.getCanvas().style.cursor = 'pointer';
                                    
                                    // Copy coordinates array.
                                    const coordinates = e.features[0].geometry.coordinates.slice();
                                    const description = e.features[0].properties.description;
                                    //const description = "Descripcion";
                                    
                                    // Ensure that if the map is zoomed out such that multiple
                                    // copies of the feature are visible, the popup appears
                                    // over the copy being pointed to.
                                    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                                    }

                                    var coordinates_north = '-6.0276854038238525,36.98175737946314';
                                    //alert(coordinates[0][0]);

                                    // Populate the popup and set its coordinates
                                    // based on the feature found.
                                    popup.setLngLat(coordinates[0][3]).setHTML(description).addTo(map);
                                    //popup.setLngLat(coordinates_north).setHTML(description).addTo(map);
                                    //alert('llego');
                                    
                                    
                                });
                                
                                map.on('mouseleave', function (e) {

                                    // coordenadas del raton
                                    document.getElementById('coordenadas').innerHTML = '';

                                });

                                map.on('mouseleave', 'fill_layer_<?=$apu_code_prefix?>', () => {

                                    map.getCanvas().style.cursor = '';
                                    popup.remove();
                                });

                                <?php

                                // --------------------------------------------------------------------------------
                                // inicio RP
                                $reference_plots = $ap["reference_plots"];
                                //print_r($reference_plots);die;

                                if (count($reference_plots) > 0) {
                                    foreach ($reference_plots as $rp) {


                                        $rp_subfix = $rp["reference_parcel"]."-".$countsubfix;

                                        ?>
                                        coords = [[<?=$rp["rpgeometry"]["coords"]?>]];    
                                        
                                        map.addSource('source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>', {
                                            'type': 'geojson',
                                            'data': {
                                                'type': 'FeatureCollection',
                                                'features': [
                                                    {
                                                        'type': 'Feature',
                                                        'properties': {
                                                            //'description': 'Code: <?=$rp["reference_parcel"]?><br>Key: <?=$rp["key"]?><br>Surface <?=$rp["rpgeometry"]["surface"]?> ha'
                                                            'description': '<?=getHTMLTooltipRP($ap["apu_code"], $rp["reference_parcel"], $rp["key"], $rp["rpgeometry"]["surface"])?>'
                                                        },
                                                        'geometry': {
                                                            'type': '<?=$rp["rpgeometry"]["type"]?>',
                                                            
                                                            //'coordinates': [[[[-6.01193523702405,36.9639809552435],[-6.01193120726731,36.963974985937],[-6.01068286255209,36.9621253623426],[-6.01051539675803,36.961877231666],[-6.01047800610188,36.9618129407554],[-6.00881940676726,36.962608862716],[-6.00807034172092,36.96296831815],[-6.00804711575998,36.9629267526184],[-6.00801543645922,36.9629556837691],[-6.00770990413377,36.9633794186753],[-6.00765760162268,36.9635830916514],[-6.00761554744304,36.9637468517934],[-6.0076028437976,36.9642368542739],[-6.00759865637163,36.9643983712126],[-6.00770857107229,36.9648044973522],[-6.00772597549634,36.9648688056585],[-6.00784855626885,36.9652968586862],[-6.00779364088889,36.9654790498967],[-6.00773547745842,36.9656720159067],[-6.0077092494995,36.9658833422452],[-6.00770199719923,36.9659417733317],[-6.0077751734319,36.9661662828626],[-6.00789961874605,36.9663949674826],[-6.00869905025015,36.9656729953516],[-6.00873273983789,36.9656448186096],[-6.00884599064908,36.9655047048865],[-6.00888357190671,36.965465827939],[-6.00888577949407,36.9654635435227],[-6.00894296765948,36.9654260949068],[-6.00898517608815,36.9654031708075],[-6.00909420147557,36.965343956789],[-6.00916619054689,36.9653110544531],[-6.00934617796974,36.9652372607462],[-6.00943514034117,36.9652034194584],[-6.00947877235308,36.9651868217507],[-6.00949792608131,36.9651781159998],[-6.00956452056631,36.9651478463317],[-6.00957669654763,36.9651423119484],[-6.00957878046846,36.9651413642869],[-6.00965137190978,36.9651024702771],[-6.00966488983959,36.9650952266922],[-6.00966820663641,36.9650934493753],[-6.00968185434977,36.9650861367155],[-6.00978533454676,36.9650429635936],[-6.00990962221049,36.9649911092901],[-6.01000664603578,36.9649528594079],[-6.01001375175159,36.964950058712],[-6.01011864522006,36.9649087057147],[-6.01020802574854,36.964869963873],[-6.01027415074415,36.9648413015786],[-6.01040712621615,36.9647753484495],[-6.01053975229062,36.9647095680421],[-6.01059150465456,36.9646836465179],[-6.01064495613099,36.9646568735633],[-6.01071365163321,36.9646224670548],[-6.01076795307496,36.9645895395807],[-6.01079649445739,36.9645722331537],[-6.01086488337846,36.9645307643014],[-6.01109413709978,36.9643802134425],[-6.01116436658006,36.9643317466119],[-6.01124480537864,36.9642762344036],[-6.011330438531,36.9642195765507],[-6.0113685936494,36.9642000615547],[-6.01137622621993,36.9641961579741],[-6.01139307863961,36.9641875391768],[-6.01152983606165,36.9641359929406],[-6.01161386542503,36.964114629376],[-6.01162649650617,36.964110762886],[-6.01167451099198,36.9640960643897],[-6.01169377936259,36.9640901659159],[-6.0118075976835,36.9640422240106],[-6.01185976644255,36.9640202499118],[-6.01193523702405,36.9639809552435]],[[-6.00868880989695,36.9647891996677],[-6.00868821547267,36.9648119414265],[-6.00856882890868,36.9649549481507],[-6.00852541948952,36.9651009705085],[-6.00846272446138,36.9652183988821],[-6.00844183498762,36.9652874781389],[-6.00843981505159,36.965310466004],[-6.00843621368615,36.9653514376096],[-6.00843289229067,36.9653892248308],[-6.00827319549826,36.9655512142843],[-6.00822880286183,36.9655962434786],[-6.00822462226414,36.9655854695899],[-6.00820006032665,36.9655221808979],[-6.00821502011751,36.9652460653509],[-6.00822358332442,36.9651949269514],[-6.00823037205117,36.9651543908555],[-6.0082623003114,36.9649637254024],[-6.00838872335735,36.9647235315363],[-6.00840911491478,36.9646139820195],[-6.00851014788125,36.9643564107741],[-6.00851075281146,36.9646285263322],[-6.00851993353557,36.9646717959902],[-6.0085270846828,36.9647055032647],[-6.00853147531554,36.9647101199966],[-6.0085367610311,36.9647156800096],[-6.00856639458804,36.9647468464045],[-6.00863565316618,36.9647642302331],[-6.00868880989695,36.9647891996677]],[[-6.00853319341597,36.9637151457232],[-6.00849520542384,36.9638341516271],[-6.00838923823205,36.9639377613958],[-6.00828903669438,36.9639697611895],[-6.0082206708286,36.9639915938819],[-6.00820816755139,36.9639941474696],[-6.00810481719614,36.9640152551407],[-6.00809727212004,36.9640131432423],[-6.00808622760014,36.9640100522715],[-6.00803101402309,36.9639945980743],[-6.00799680747362,36.9639704805618],[-6.00797673012245,36.9639563260062],[-6.00797154666383,36.9638816659823],[-6.00798258670954,36.9637912758703],[-6.00802269719342,36.963736561278],[-6.0080593185694,36.9636866064554],[-6.00811371461044,36.9636493660154],[-6.00821787033665,36.9635780600679],[-6.00830626809324,36.9635415784202],[-6.00846062987343,36.9636403913775],[-6.00853319341597,36.9637151457232]]]]
                                                            //'coordinates': [[[-6.1265946147371,36.9382406144685],[-6.12581105787296,36.9403290926441],[-6.12758695502307,36.940767283012],[-6.12837038147088,36.9386993871114],[-6.12705486420378,36.9383544778309],[-6.12695256620707,36.9383291683461],[-6.1265946147371,36.9382406144685]]]
                                                            //'coordinates': [[[-5.25291805664084,37.6718762116236],[-5.2523302034369,37.6723297441499],[-5.25232711130184,37.6723321291286],[-5.2529208371502,37.6728156148606],[-5.25312601904053,37.6732872367495],[-5.25349326617395,37.674083340793],[-5.25349786122054,37.6740717595321],[-5.2534987735174,37.6740694612513],[-5.25350280818038,37.6740592938119],[-5.25348730744307,37.6740214511461],[-5.25339244011014,37.6737898446955],[-5.25342138960103,37.673774822255],[-5.25347728453528,37.6737247837298],[-5.25351041154264,37.6737553135561],[-5.25349457780474,37.6737652163172],[-5.25359552381692,37.673850256826],[-5.25359813378061,37.6738701541538],[-5.25364314480715,37.673898018208],[-5.25366478220824,37.6738832427345],[-5.25376458099885,37.6739635180516],[-5.25370062445662,37.674006231188],[-5.25364910457113,37.6740453262742],[-5.25354702936471,37.6739495817734],[-5.25353634075635,37.6739395561112],[-5.2535032346889,37.6739507884484],[-5.25353374877287,37.6740175228372],[-5.25360485840773,37.6741463786587],[-5.25364838833846,37.674196741497],[-5.25376412052274,37.6741344295302],[-5.25391239540893,37.6740369942603],[-5.25405707656938,37.673959660354],[-5.25436729518211,37.6738001423331],[-5.25472451992249,37.6736208027543],[-5.25497262366623,37.6735120819594],[-5.25313015094688,37.6720478856811],[-5.25293980791963,37.6718944499968],[-5.25291805664084,37.6718762116236]]]
        
                                                            'coordinates': coords
                                                        }
                                                    }
                                                ]
                                            }
                                        });
        
                                        map.addLayer({
                                            'id': 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>',
                                            'type': 'fill',
                                            'source': 'source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>',
                                            'layout': {
                                                // Make the layer visible by default.
                                                'visibility': 'visible'
                                            },
                                            'paint': {
                                                'fill-color': '<?=getColorRP($countapucodeprefix."_".$refparcel_search, $countapucodeprefix."_".$rp["reference_parcel"], $parcel_search, $num_apu_code, $rp["apu_code"], $rp["num_apu_code"])?>',
                                                'fill-opacity': <?=getOpacityRP($countapucodeprefix."_".$refparcel_search, $countapucodeprefix."_".$rp["reference_parcel"])?>
                                            },
                                            'filter': ['==', '$type', 'Polygon']
                                        });
                                        map.addLayer({
                                            'id': 'border_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>',
                                            'type': 'line',
                                            'source': 'source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>',
                                            'layout': {
                                                'line-join': 'round',
                                                'line-cap': 'round'
                                            },
                                            'paint': {
                                                'line-color': '<?=$settings->_color_rp_borde?>',
                                                'line-width': <?=$settings->_grosor_rp_borde?>
                                            }
                                        });
        
                                        // Create a popup, but don't add it to the map yet.
                                        popup = new mapboxgl.Popup({
                                            closeButton: false,
                                            closeOnClick: false
                                        });
                                        popup.setMaxWidth("200px");
                                        
                                        map.on('mousemove', 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>', (e) => {

                                            //document.getElementById('coordenadas').innerHTML = '<?=$rp["reference_parcel"]?>';
                                            document.getElementById('span_data_id_code').innerHTML = '<?=$ap["apu_code"]?>';
                                            document.getElementById('span_data_id_key').innerHTML = '<?=$ap["key"]?>';
                                            document.getElementById('span_data_id_surface').innerHTML = '<?=$ap["surface"]?> ha';                                  
                                            document.getElementById('span_data_id_refparcel').innerHTML = '<?=$rp["reference_parcel"]?>';

                                            // Change the cursor style as a UI indicator.
                                            map.getCanvas().style.cursor = 'pointer';
                                            
                                            // Copy coordinates array.
                                            const coordinates = e.features[0].geometry.coordinates.slice();
                                            const description = e.features[0].properties.description;
                                            //const description = "Descripcion";
                                            
                                            // Ensure that if the map is zoomed out such that multiple
                                            // copies of the feature are visible, the popup appears
                                            // over the copy being pointed to.
                                            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                                            }
        
                                            //alert(coordinates[0][0]);
        
        
                                            // Populate the popup and set its coordinates
                                            // based on the feature found.
                                            popup.setLngLat(coordinates[0][3]).setHTML(description).addTo(map);
                                            //alert('llego');
                                            
                                        });
                                        
                                        map.on('mouseleave', 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>', () => {
                                            map.getCanvas().style.cursor = '';
                                            popup.remove();
                                        });
        
                                        <?php
        
                                        // --------------------------------------------------------------------------------
                                        // inicio FC
                                        $field_crops = $rp["field_crops"];
                                        /*if ($rp["reference_parcel"] == "ES000000004110") { 
                                            echo "****";
                                            print_r($field_crops);//die;
                                            echo "****";
                                        }*/
                                        if (count($field_crops) > 0) {
                                            foreach ($field_crops as $fc) {

                                                //echo "<br />####";
                                                //print_r($fc["crop_irrigation"]);
                                                //echo "<br />####";

                                                if (isset($fc["type"])) {                                                
                                                    if ($fc["type"] == "MultiPolygon") {
                                                    ?>
                                                        coords = [<?=$fc["fcgeometry"]["coords"]?>];    
                                                    <?php
                                                    }
                                                    else {
                                                    ?>
                                                        coords = [[<?=$fc["fcgeometry"]["coords"]?>]];    
                                                    <?php
                                                    }
                                                    if ($fc["type"] == "") {
                                                        $fc["type"] = "Polygon";
                                                    }
                                                    if ($fc["type"] == "MultiPolygon") {
                                                        $fc["type"] = "Polygon";
                                                    }
                                                }

                                                //echo "****";
                                                //$fc["type"];//die;
                                                //echo "****";
                                                //echo "<br />****FC ".$fc["key"]." TYPE ".$fc["type"]."****";
                                                ?>
                                                
                                                map.addSource('source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>', {
                                                    'type': 'geojson',
                                                    'data': {
                                                        'type': 'FeatureCollection',
                                                        'features': [
                                                            {
                                                                'type': 'Feature',
                                                                'properties': {
                                                                    //'description': 'Code: <?=$rp["reference_parcel"]?><br>Key: <?=$fc["key"]?><br>Surface <?=$fc["fcgeometry"]["surface"]?> ha'
                                                                    'description': '<?=getHTMLTooltipFC($ap["apu_code"], $rp["reference_parcel"], $rp["key"], $fc["key"], $fc["fcgeometry"]["surface"], $fc["crop_product"])?>'
                                                                },
                                                                'geometry': {
                                                                    'type': '<?=$fc["type"]?>',
                                                                    
                                                                    //'coordinates': [[[[-6.01193523702405,36.9639809552435],[-6.01193120726731,36.963974985937],[-6.01068286255209,36.9621253623426],[-6.01051539675803,36.961877231666],[-6.01047800610188,36.9618129407554],[-6.00881940676726,36.962608862716],[-6.00807034172092,36.96296831815],[-6.00804711575998,36.9629267526184],[-6.00801543645922,36.9629556837691],[-6.00770990413377,36.9633794186753],[-6.00765760162268,36.9635830916514],[-6.00761554744304,36.9637468517934],[-6.0076028437976,36.9642368542739],[-6.00759865637163,36.9643983712126],[-6.00770857107229,36.9648044973522],[-6.00772597549634,36.9648688056585],[-6.00784855626885,36.9652968586862],[-6.00779364088889,36.9654790498967],[-6.00773547745842,36.9656720159067],[-6.0077092494995,36.9658833422452],[-6.00770199719923,36.9659417733317],[-6.0077751734319,36.9661662828626],[-6.00789961874605,36.9663949674826],[-6.00869905025015,36.9656729953516],[-6.00873273983789,36.9656448186096],[-6.00884599064908,36.9655047048865],[-6.00888357190671,36.965465827939],[-6.00888577949407,36.9654635435227],[-6.00894296765948,36.9654260949068],[-6.00898517608815,36.9654031708075],[-6.00909420147557,36.965343956789],[-6.00916619054689,36.9653110544531],[-6.00934617796974,36.9652372607462],[-6.00943514034117,36.9652034194584],[-6.00947877235308,36.9651868217507],[-6.00949792608131,36.9651781159998],[-6.00956452056631,36.9651478463317],[-6.00957669654763,36.9651423119484],[-6.00957878046846,36.9651413642869],[-6.00965137190978,36.9651024702771],[-6.00966488983959,36.9650952266922],[-6.00966820663641,36.9650934493753],[-6.00968185434977,36.9650861367155],[-6.00978533454676,36.9650429635936],[-6.00990962221049,36.9649911092901],[-6.01000664603578,36.9649528594079],[-6.01001375175159,36.964950058712],[-6.01011864522006,36.9649087057147],[-6.01020802574854,36.964869963873],[-6.01027415074415,36.9648413015786],[-6.01040712621615,36.9647753484495],[-6.01053975229062,36.9647095680421],[-6.01059150465456,36.9646836465179],[-6.01064495613099,36.9646568735633],[-6.01071365163321,36.9646224670548],[-6.01076795307496,36.9645895395807],[-6.01079649445739,36.9645722331537],[-6.01086488337846,36.9645307643014],[-6.01109413709978,36.9643802134425],[-6.01116436658006,36.9643317466119],[-6.01124480537864,36.9642762344036],[-6.011330438531,36.9642195765507],[-6.0113685936494,36.9642000615547],[-6.01137622621993,36.9641961579741],[-6.01139307863961,36.9641875391768],[-6.01152983606165,36.9641359929406],[-6.01161386542503,36.964114629376],[-6.01162649650617,36.964110762886],[-6.01167451099198,36.9640960643897],[-6.01169377936259,36.9640901659159],[-6.0118075976835,36.9640422240106],[-6.01185976644255,36.9640202499118],[-6.01193523702405,36.9639809552435]],[[-6.00868880989695,36.9647891996677],[-6.00868821547267,36.9648119414265],[-6.00856882890868,36.9649549481507],[-6.00852541948952,36.9651009705085],[-6.00846272446138,36.9652183988821],[-6.00844183498762,36.9652874781389],[-6.00843981505159,36.965310466004],[-6.00843621368615,36.9653514376096],[-6.00843289229067,36.9653892248308],[-6.00827319549826,36.9655512142843],[-6.00822880286183,36.9655962434786],[-6.00822462226414,36.9655854695899],[-6.00820006032665,36.9655221808979],[-6.00821502011751,36.9652460653509],[-6.00822358332442,36.9651949269514],[-6.00823037205117,36.9651543908555],[-6.0082623003114,36.9649637254024],[-6.00838872335735,36.9647235315363],[-6.00840911491478,36.9646139820195],[-6.00851014788125,36.9643564107741],[-6.00851075281146,36.9646285263322],[-6.00851993353557,36.9646717959902],[-6.0085270846828,36.9647055032647],[-6.00853147531554,36.9647101199966],[-6.0085367610311,36.9647156800096],[-6.00856639458804,36.9647468464045],[-6.00863565316618,36.9647642302331],[-6.00868880989695,36.9647891996677]],[[-6.00853319341597,36.9637151457232],[-6.00849520542384,36.9638341516271],[-6.00838923823205,36.9639377613958],[-6.00828903669438,36.9639697611895],[-6.0082206708286,36.9639915938819],[-6.00820816755139,36.9639941474696],[-6.00810481719614,36.9640152551407],[-6.00809727212004,36.9640131432423],[-6.00808622760014,36.9640100522715],[-6.00803101402309,36.9639945980743],[-6.00799680747362,36.9639704805618],[-6.00797673012245,36.9639563260062],[-6.00797154666383,36.9638816659823],[-6.00798258670954,36.9637912758703],[-6.00802269719342,36.963736561278],[-6.0080593185694,36.9636866064554],[-6.00811371461044,36.9636493660154],[-6.00821787033665,36.9635780600679],[-6.00830626809324,36.9635415784202],[-6.00846062987343,36.9636403913775],[-6.00853319341597,36.9637151457232]]]]
                                                                    //'coordinates': [[[-6.1265946147371,36.9382406144685],[-6.12581105787296,36.9403290926441],[-6.12758695502307,36.940767283012],[-6.12837038147088,36.9386993871114],[-6.12705486420378,36.9383544778309],[-6.12695256620707,36.9383291683461],[-6.1265946147371,36.9382406144685]]]
                                                                    //'coordinates': [[[-5.25291805664084,37.6718762116236],[-5.2523302034369,37.6723297441499],[-5.25232711130184,37.6723321291286],[-5.2529208371502,37.6728156148606],[-5.25312601904053,37.6732872367495],[-5.25349326617395,37.674083340793],[-5.25349786122054,37.6740717595321],[-5.2534987735174,37.6740694612513],[-5.25350280818038,37.6740592938119],[-5.25348730744307,37.6740214511461],[-5.25339244011014,37.6737898446955],[-5.25342138960103,37.673774822255],[-5.25347728453528,37.6737247837298],[-5.25351041154264,37.6737553135561],[-5.25349457780474,37.6737652163172],[-5.25359552381692,37.673850256826],[-5.25359813378061,37.6738701541538],[-5.25364314480715,37.673898018208],[-5.25366478220824,37.6738832427345],[-5.25376458099885,37.6739635180516],[-5.25370062445662,37.674006231188],[-5.25364910457113,37.6740453262742],[-5.25354702936471,37.6739495817734],[-5.25353634075635,37.6739395561112],[-5.2535032346889,37.6739507884484],[-5.25353374877287,37.6740175228372],[-5.25360485840773,37.6741463786587],[-5.25364838833846,37.674196741497],[-5.25376412052274,37.6741344295302],[-5.25391239540893,37.6740369942603],[-5.25405707656938,37.673959660354],[-5.25436729518211,37.6738001423331],[-5.25472451992249,37.6736208027543],[-5.25497262366623,37.6735120819594],[-5.25313015094688,37.6720478856811],[-5.25293980791963,37.6718944499968],[-5.25291805664084,37.6718762116236]]]
                
                                                                    'coordinates': coords
                                                                }
                                                            }
                                                        ]
                                                    }
                                                });
                
                                                map.addLayer({
                                                    'id': 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>',
                                                    'type': 'fill',
                                                    'source': 'source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>',
                                                    'layout': {
                                                        // Make the layer visible by default.
                                                        'visibility': 'visible'
                                                    },
                                                    'paint': {
                                                        'fill-color': '<?=getColorFC($parcel_search, $ap["apu_code"])?>',
                                                        'fill-opacity': <?=getOpacityFC($parcel_search, $ap["apu_code"])?>
                                                    },
                                                    'filter': ['==', '$type', 'Polygon']
                                                });
                                                map.addLayer({
                                                    'id': 'border_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>',
                                                    'type': 'line',
                                                    'source': 'source_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>',
                                                    'layout': {
                                                        'line-join': 'round',
                                                        'line-cap': 'round'
                                                    },
                                                    'paint': {
                                                        'line-color': '<?=$settings->_color_fc_borde?>',
                                                        'line-width': <?=$settings->_grosor_fc_borde?>
                                                    }
                                                });                                
                
                                                // Create a popup, but don't add it to the map yet.
                                                popup = new mapboxgl.Popup({
                                                    closeButton: false,
                                                    closeOnClick: false
                                                });
                                                
                                                map.on('mousemove', 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>', (e) => {

                                                    //document.getElementById('coordenadas').innerHTML = '<?=$rp["reference_parcel"]?> - <?=$fc["key"]?>';
                                                    document.getElementById('span_data_id_code').innerHTML = '<?=$ap["apu_code"]?>';
                                                    document.getElementById('span_data_id_key').innerHTML = '<?=$ap["key"]?>';
                                                    document.getElementById('span_data_id_surface').innerHTML = '<?=$ap["surface"]?> ha';                                  
                                                    document.getElementById('span_data_id_refparcel').innerHTML = '<?=$rp["reference_parcel"]?>';
                                                    document.getElementById('span_data_id_fckey').innerHTML = '<?=$fc["key"]?>';
                                                    document.getElementById('span_data_id_fcsurface').innerHTML = '<?=$fc["fcgeometry"]["surface"]?> ha';
                                                    document.getElementById('span_data_id_fccropproduct').innerHTML = '(<?=$fc["crop_product"]?>) <?=$arrCatalogCropProduct[$fc["crop_product"]]?>';

                                                    <?php
                                                        //echo "**fitos**";
                                                        //print_r($fc["phytosanitary"]);die;
                                                        $html_phytosanitary = getHTMLPopoverPhytosanitary($fc["phytosanitary"]);
                                                        //echo "**".$html_phytosanitary;
                                                        $html_irrigation = getHTMLPopoverIrrigation($fc["crop_irrigation"]);
                                                        //echo "**".$html_irrigation;
                                                        $html_fertilizer = getHTMLPopoverFertilizer($fc["fertilizer"]);
                                                        //$html_fertilizer = "";
                                                        //echo "**".$html_fertilizer;die;
                                                    ?>

                                                    //document.getElementById('id_btn_irrigation').setAttribute('data-content', "<?=$html_irrigation?>");
                                                    document.getElementById('id_toast_phytosanitaries_body').innerHTML = "<?=$html_phytosanitary?>";
                                                    document.getElementById('id_toast_irrigation_body').innerHTML = "<?=$html_irrigation?>";
                                                    document.getElementById('id_toast_fertilizer_body').innerHTML = "<?=$html_fertilizer?>";

                                                    document.getElementById('id_div_phytosanitaries_body').innerHTML = "<?=$html_phytosanitary?>";
                                                    document.getElementById('id_div_irrigation_body').innerHTML = "<?=$html_irrigation?>";
                                                    document.getElementById('id_div_fertilizer_body').innerHTML = "<?=$html_fertilizer?>";

                                                    // Change the cursor style as a UI indicator.
                                                    map.getCanvas().style.cursor = 'pointer';
                                                    
                                                    // Copy coordinates array.
                                                    const coordinates = e.features[0].geometry.coordinates.slice();
                                                    const description = e.features[0].properties.description;
                                                    //const description = "Descripcion";
                                                    
                                                    // Ensure that if the map is zoomed out such that multiple
                                                    // copies of the feature are visible, the popup appears
                                                    // over the copy being pointed to.
                                                    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                                                        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
                                                    }
                
                                                    //alert(coordinates[0][0]);
                
                
                                                    // Populate the popup and set its coordinates
                                                    // based on the feature found.
                                                    popup.setLngLat(coordinates[0][3]).setHTML(description).addTo(map);
                                                    //alert('llego');
                                                    
                                                });
                                                
                                                map.on('mouseleave', 'fill_layer_<?=$apu_code_prefix?>_rp_<?=$rp_subfix?>_fc_<?=$fc["key"]?>', () => {
                                                    map.getCanvas().style.cursor = '';
                                                    popup.remove();
                                                });
                
                                                <?php
                
                                            }
                                        }
                                        //print_r($field_crops);

                                        // fin FC
                                        // --------------------------------------------------------------------------------

                                        $countsubfix++;

                                    }
                                }
                                // fin RP
                                // --------------------------------------------------------------------------------

                                $countapucodeprefix++;

                            } // foreach ($arr_polygons as $ap) { recorro poligonos AB
                        }
                    ?>

                    });




                </script>
                    <?php
                }
            }
            else {
                echo "<br /><h4>Farmer not found: ".sanitizeString($_GET["fad_personalid_search"])."</h4>";
            }

        } catch (Exception $e) {
            header("Location: ../out/out.login.php");
        }
        // fin AgriculturalProducerPartiesApi $apiInstance->getByCodeUsingGET
        // --------------------------------------------------------------------------------------------
    }
} // if (isset($_GET["pun_code_search"]))


htmlEndPage();

?>