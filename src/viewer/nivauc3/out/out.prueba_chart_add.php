<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
</head><!--from   w  ww  .d e mo 2s  .com-->
<body>
    <canvas id="barOrgaoAno" height="200"></canvas>
<br/>
<button id="button">
    Click me!
</button>
    <script type='text/javascript'>
var data = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [{
        label: "Compras",
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        data: [65, 59, 80, 81, 56, 55, 40]
    }]
};
var ctx = $("#barOrgaoAno").get(0).getContext("2d");
var myBarChart = new Chart(ctx, {
    type: "bar",
    data: data,
});
$('button#button').click(function() {
    var newDataset = {
        label: "Vendas",
        backgroundColor: 'rgba(99, 255, 132, 0.2)',
        borderColor: 'rgba(99, 255, 132, 1)',
        borderWidth: 1,
        data: [10, 20, 30, 40, 50, 60]
    }
    data.datasets.push(newDataset);
    myBarChart.update();
});
  </script>
</body>
</html>