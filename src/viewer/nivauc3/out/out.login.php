<?php
include_once("../inc/inc.utils.php");

$error = "";
if ((isset($_GET["error_login"]) && (sanitizeString($_GET["error_login"]) != ""))) {
    $error = sanitizeString($_GET["error_login"]);
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>NIVA UC3</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!--
Ramayana CSS Template
https://templatemo.com/tm-529-ramayana
-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="../assets/css/fontawesome.css">
    <!--<link rel="stylesheet" href="../assets/css/templatemo-style.css">-->
    <link rel="stylesheet" href="../assets/css/owl.css">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="../assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="../assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="../assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">    

    <style>
        /*body {font-family: Arial, Helvetica, sans-serif;}*/
        form {border: 0px solid #f1f1f1;}
        
        input[type=text], input[type=password] {
          width: 100%;
          padding: 12px 20px;
          margin: 8px 0;
          /*display: inline-block;*/
          border: 1px solid #ccc;
          box-sizing: border-box;
        }
        
        button {
          /*background-color: #4CAF50;*/
          background-color: #07A84C;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }
        
        button:hover {
          opacity: 0.8;
        }
        
        .cancelbtn {
          width: auto;
          padding: 10px 18px;
          background-color: #f44336;
        }
        
        .imgcontainer {
          text-align: center;
          margin: 24px 0 12px 0;
        }
        
        img.avatar {
          width: 30%;
          border-radius: 50%;
        }
        
        .container {
          padding: 16px;
        }
        
        span.psw {
          float: right;
          padding-top: 16px;
        }
        
        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
          span.psw {
             display: block;
             float: none;
          }
          .cancelbtn {
             width: 100%;
          }
        }

    #header {
      width:100%; /* Establecemos que el header abarque el 100% del documento */
      overflow:hidden; /* Eliminamos errores de float */
      background:#07A84C;
      margin-bottom:20px;
      border-bottom: 0px solid #000;
    }

    .wrapper {
      width:90%; /* Establecemos que el ancho sera del 90% */
      max-width:1000px; /* Aqui le decimos que el ancho máximo sera de 1000px */
      margin:auto; /* Centramos los elementos */
      overflow:hidden; /* Eliminamos errores de float */
    }

    #header .logo {
      color:#ffffff;
      font-size:50px;
      line-height:80px;
      float:left;
    }

    #header nav {
      float:right;
      line-height:150px;
    }

    #header nav a {
      display:inline-block;
      color:#fff;
      text-decoration:none;
      padding:10px 20px;
      line-height:normal;
      font-size:20px;
      font-weight:bold;
      -webkit-transition:all 500ms ease;
      -o-transition:all 500ms ease;
      transition:all 500ms ease;
    }

    #header nav a:hover {
      background:#f56f3a;
      border-radius:50px;
    }

    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        /*opacity: .7;*/
        background-color: #212934;
        color: white;
        text-align: center;
    }
    .footer_div {
        width: 100%;
        text-align: left;
    }
    .footer_logo {
        width: 30px;
    }
    .login_failed {
        font-style: italic;
        color: #FA0000;
    }

    </style>

  </head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

      <!-- Main -->
        <div id="main">
          <div class="inner">

            <!-- Header -->
            <header id="header">
              <div class="logo">
                <img src="../assets/images/EU_logo.png" style="width:180px;margin-right:50px;" />
                NIVA UC3
              </div>
            </header>

            <section class="simple-post">
                <div class="container-fluid">
                  <div class="row">
                    <h2>&nbsp;</h2>

                    <div class="container">
                      <div class="imgcontainer">
                          <img src="../assets/images/logo.png" alt="Avatar" class="avatar">
                      </div>
                    </div>
                    
                    <div class="container">
                        <form action="../op/op.login.php" method="post">
                            
                            <label for="uname"><b>Please enter username and password</b></label>
                            <?php
                            if ($error != "") {
                            ?>
                                <label for="uname" class="login_failed"><b>Login failed</b></label>
                            <?php
                            }
                            ?>
                            <input type="text" placeholder="Username" name="username" required >
                            <input type="password" placeholder="Password" name="password" required >
                        
                            <button type="submit">Enter</button>
                        </form>
                        <label>
                        <!--<input type="checkbox" checked="checked" name="remember"> Remember me-->
                        </label>
                    </div>
                    
                    <!--<div class="container" style="background-color:#f1f1f1">
                        <button type="button" class="cancelbtn">Cancelar</button>
                        <span class="psw">Forgot <a href="#">password?</a></span>
                    </div>-->
                  </div>
                </div>
            </section>

          </div>
        </div>


    </div>

    <br />
    <br />
    <br />
    <div class="footer">
      <!--<br /><div class="footer_div"><img src="../assets/images/logo_black.png" class="footer_logo" /></div>-->
      <br />
      <p>&copy; 2022 NIVA | All rights reserved</p>
    </div>       
  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="../assets/js/browser.min.js"></script>
    <script src="../assets/js/breakpoints.min.js"></script>
    <script src="../assets/js/transition.js"></script>
    <script src="../assets/js/owl-carousel.js"></script>
    <script src="../assets/js/custom.js"></script>

  </body>

</html>
