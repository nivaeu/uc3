<?php

echo "<h4>chart.js</h4>";


?>
<!DOCTYPE html>
<html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<body>

<canvas id="myChart" style="width:100%;max-width:600px"></canvas>

<script>
/*aDatasets1 = [65,59,80,81,56,55,40, 5];  
aDatasets2 = [20,30,40,50,60,20,25, 5];
aDatasets3 = [30,20,25,65,90,34,20, 5];*/
aDatasets1 = [65];  
aDatasets2 = [20];
aDatasets3 = [30];
aDataset = [ [30], [20], [40] ];
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange", "Black", "White"],
        labels: [""],
        
        datasets: [ {
            label: 'Result',
            fill: false,
            data: aDataset[0],
            backgroundColor: '#8B0000',

        },
        
        {
            label: 'Attendance',
            fill: false,
            data: aDataset[1],
            backgroundColor: '#B8860B',
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
            ],
            borderWidth: 0
        },
        {
            label: ['score'],
            data: aDataset[2],
            fill: true,
            backgroundColor:  '#DC143C',
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
                'rgba(255,99,132,1)',
            ],
            borderWidth: 0
        }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        title: {
            display: true,
            text: 'Nishi IT Institute'
        },
        responsive: true,
        
        tooltips: {
            callbacks: {
                /*labelColor: function(tooltipItem, chart) {
                    return {
                        borderColor: 'rgb(255, 0, 20)',
                        backgroundColor: 'rgb(255,20, 0)'
                    }
                }*/
            }
        },
        plugins: {
            legend: {
                display: false
            }
        }
        /*,
        legend: {
            labels: {
                // This more specific font property overrides the global property
                fontColor: 'red',
               
            }
        }*/
    }
});
</script>

</body>
</html>
