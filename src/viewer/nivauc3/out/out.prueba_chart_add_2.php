<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.min.js"></script>
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
</head><!--from   w  ww  .d e mo 2s  .com-->
<body>
    <canvas id="barOrgaoAno" height="200"></canvas>
<br/>
<button id="button">
    Click me!
</button>
<script type='text/javascript'>
    /*var data = {
        labels: ["Arable", "Permanent", "Other"],
        datasets: [{
            label: "Compras",
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            data: [0, 10, 20]
        }]
    };
    var ctx = $("#barOrgaoAno").get(0).getContext("2d");
    var myBarChart = new Chart(ctx, {
        type: "bar",
        data: data,
    });*/


    var densityData = {
        label: 'Density of Planets (kg/m3)',
        data: [5427, 5243, 5514, 3933, 1326, 687, 1271, 1638]
    };
    var ctx = $("#barOrgaoAno").get(0).getContext("2d");
    var barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"],
            datasets: [densityData]
        }
    });    


</script>
</body>
</html>