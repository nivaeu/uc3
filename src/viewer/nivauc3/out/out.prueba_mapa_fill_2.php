<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Add a pattern to a polygon</title>
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<link href="https://api.mapbox.com/mapbox-gl-js/v2.7.0/mapbox-gl.css" rel="stylesheet">
<script src="https://api.mapbox.com/mapbox-gl-js/v2.7.0/mapbox-gl.js"></script>
<style>
body { margin: 0; padding: 0; }
#map { position: absolute; top: 0; bottom: 0; width: 100%; }
</style>
</head>
<body>
<div id="map"></div>
 
<script>
	// TO MAKE THE MAP APPEAR YOU MUST
	// ADD YOUR ACCESS TOKEN FROM
	// https://account.mapbox.com
	mapboxgl.accessToken = 'pk.eyJ1IjoibWFsLXdvb2QiLCJhIjoiY2oyZ2t2em50MDAyMzJ3cnltMDFhb2NzdiJ9.X-D4Wvo5E5QxeP7K_I3O8w';
const map = new mapboxgl.Map({
    container: 'map', // container ID
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [-105.819711, 39.147614], // starting position
    zoom: 5 // starting zoom
});
 
map.on('load', () => {
    // Add the GeoJSON data.
    map.addSource('source', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'Polygon',
                'coordinates': [
                    [
                        [-108.977199, 40.975108],
                        [-102.105019, 40.995138],
                        [-102.078486, 37.017605],
                        [-109.083333, 37.017605],
                        [-108.977199, 40.975108]
                    ]
                ]
            }
        }
    });
    
    // Load an image to use as the pattern from an external URL.
    map.loadImage(
        //'https://docs.mapbox.com/mapbox-gl-js/assets/colorado_flag.png',
        '../assets/images/ablandcover/fondo_ab_lc_8.png',
        (err, image) => {
            // Throw an error if something goes wrong.
            if (err) throw err;
            
            // Add the image to the map style.
            map.addImage('pattern', image);
            
            // Create a new layer and style it using `fill-pattern`.
            map.addLayer({
                'id': 'pattern-layer',
                'type': 'fill',
                'source': 'source',
                'paint': {
                    'fill-pattern': 'pattern'
                }
            });
        }
    );
});
</script>
 
</body>
</html>