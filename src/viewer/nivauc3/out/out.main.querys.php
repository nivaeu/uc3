<?php

// out.main.querys.php

require_once('../vendor/autoload.php');
include_once("../inc/inc.settings.php");
include_once("../inc/inc.classui.php");
include_once("../inc/inc.utils.php");
include_once("../inc/inc.coordinates.php");
include_once("../inc/inc.authentication.php");
include_once("../inc/inc.apicatalogs.php");


global $bearer, $arrCatalogPhytosanitaryEquipment, $arrCatalogUnitType, $arrCatalogCropProduct, $arrCatalogLandCover;


$fad_personalid_search = "";
if ((isset($_GET["fad_personalid_search"])) && (strlen($_GET["fad_personalid_search"]) > 0)) {
    $fad_personalid_search = sanitizeString($_GET["fad_personalid_search"]);
}

$parcel_search = "";
if ((isset($_GET["parcel_search"])) && (strlen($_GET["parcel_search"]) > 0)) {
    $parcel_search = sanitizeString($_GET["parcel_search"]);
}

$refparcel_search = "";
if ((isset($_GET["refparcel_search"])) && (strlen($_GET["refparcel_search"]) > 0)) {
    $refparcel_search = sanitizeString($_GET["refparcel_search"]);
}
$token = "";
if ((isset($_GET["token"])) && (strlen($_GET["token"]) > 0)) {
    $token = sanitizeString($_GET["token"]);
}


htmlStartPage($token);

echo getFormStart("form_farmer", "form_farmer_id", "../out/out.main.querys.php", "GET", "", "");

echo getEncabezadoStart(0, false);
echo getCeldaDatos(0, "Search by FARMER Personal ID");
echo getCeldaDatos(0, getInputTag("text", "", "fad_personalid_search", "fad_personalid_search_id", "", ""));
echo getEncabezadoEnd();

echo getEncabezadoStart(0, false);
echo getCeldaDatos(0, getInputTag("submit", "", "", "", "Search", "Search"));
echo getEncabezadoEnd();

echo getInputTag("hidden", "", "token", "", $bearer, "");

echo getFormEnd();


if (isset($_GET["fad_personalid_search"])) {
    //echo "pun_code_a_buscar: ".$_GET["pun_code_search"];

    // --------------------------------------------------------------------------------------------
    // inicio AgriculturalProducerPartiesApi $apiInstance->getByCodeUsingGET

    $apiInstance = new Swagger\Client\Api\AgriculturalProducerPartiesApi(
        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
        // This is optional, `GuzzleHttp\Client` will be used as default.
        new GuzzleHttp\Client(),
        $config
    );

    $country = "ES"; // string | country

    if (sanitizeString($_GET["fad_personalid_search"]) != "") {
        try {
            // ej ES000000002353 paises bajos
            // ej ES000000003490 andalucía
            // ej ES000000003144 andalucía

            /*
            3144
            -- select * from pr_dproductionunit where pun_id = 'c8efd25e-5722-41c0-8dde-011af89e98dc';
            -- select * from ab_dagrblock where abl_id = '47f975bb-9680-4633-a5e2-ca1f86aec0de';
            -- SELECT rfp_id, rfp_key, abl_id, rfp_refparcel, rfp_datefrom, rfp_dateto FROM sc_niva.rp_drefplot where rfp_id = '158aef9e-0f66-40c8-a901-efb29fb9e920';
            */

            $result = $apiInstance->getByPersonalIdCountryUsingGET($country, sanitizeString($_GET["fad_personalid_search"]));
            //print_r($result["agricultural_production_unit"]);
            //print_r($result);
            $i = 0;
            $j = 0;
            $arr_polygons = array();
            $arr_farmer_units = array();
            if (($result != false) && (count($result["agricultural_production_unit"]) > 0)) {
                if (($result["agricultural_production_unit"] != false) && (count($result["agricultural_production_unit"]) > 0)) {
                    foreach ($result["agricultural_production_unit"] as $res) {
                        $arr_farmer_units[$i]["code"] = $res["code"];
                        $arr_farmer_units[$i]["country"] = $res["country"];
                        $arr_farmer_units[$i]["crop_plot"] = $res["crop_plot"];

                        //$arr_farmer_units[$i]["crop_plot"]["coordinates"] = $res["crop_plot"]["coordinates"];
                        
                        $i++;
                    }
                    $coords_string = "";


                    // sacamos los datos de los servicios
                    $apiInstance = new Swagger\Client\Api\QuerySurfacesApi(
                        // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
                        // This is optional, `GuzzleHttp\Client` will be used as default.
                        new GuzzleHttp\Client(),
                        $config
                    );
                    $resultFarmerAB = $apiInstance->getSurfaceABByFarmerUsingGET($fad_personalid_search);
                    $resultFarmerRP = $apiInstance->getSurfaceRPByFarmerUsingGET($fad_personalid_search);
                    $resultFarmerFC = $apiInstance->getSurfaceFCByFarmerUsingGET($fad_personalid_search);

                    $keysArrCatalogLandCover = array_keys($arrCatalogLandCover);
                    $arr_data_farmer_landcover = array();
                    if (($keysArrCatalogLandCover != false) && (count($keysArrCatalogLandCover) > 0)) {
                        foreach  ($keysArrCatalogLandCover as $kaclc) {
                            $resultFarmerLandCover = $apiInstance->getSurfaceABByFarmerLandCoverUsingGET($fad_personalid_search, $kaclc);
                            //echo "<br />".$kaclc." : ".$resultFarmerLandCover;
                            $arr_data_farmer_landcover[$kaclc] = $resultFarmerLandCover;
                        }
                    }

                    $arr_data_farmer_irrigation = array();
                    $arr_data_farmer_irrigation[0] = $apiInstance->getSurfaceABByFarmerIrrigationUsingGET($fad_personalid_search, 0);
                    $arr_data_farmer_irrigation[1] = $apiInstance->getSurfaceABByFarmerIrrigationUsingGET($fad_personalid_search, 1);


                    $resultTotalAB = $apiInstance->getSurfaceABUsingGET();
                    $resultTotalRP = $apiInstance->getSurfaceRPUsingGET();
                    $resultTotalCP = $apiInstance->getSurfaceCPUsingGET();

                    $arr_data_total_landcover = array();
                    if (($keysArrCatalogLandCover != false) && (count($keysArrCatalogLandCover) > 0)) {
                        foreach  ($keysArrCatalogLandCover as $kaclc) {
                            $resultTotalLandCover = $apiInstance->getSurfaceABByLandCoverUsingGET($kaclc);
                            //echo "<br />".$kaclc." : ".$resultFarmerLandCover;
                            $arr_data_total_landcover[$kaclc] = $resultTotalLandCover;
                        }
                    }

                    $arr_data_total_irrigation = array();
                    $arr_data_total_irrigation[0] = $apiInstance->getSurfaceByIrrigationUsingGET(0);
                    $arr_data_total_irrigation[1] = $apiInstance->getSurfaceByIrrigationUsingGET(1);


                    $arr_data = array();
                    $arr_data["farmer"]["personalID"] = $fad_personalid_search;
                    $arr_data["farmer"]["surfaceAB"] = $resultFarmerAB;
                    $arr_data["farmer"]["surfaceRP"] = $resultFarmerRP;
                    $arr_data["farmer"]["surfaceFC"] = $resultFarmerFC;
                    $arr_data["farmer"]["landcover"] = $arr_data_farmer_landcover;
                    $arr_data["farmer"]["irrigation"] = $arr_data_farmer_irrigation;

                    $arr_data["total"]["surfaceAB"] = $resultTotalAB;
                    $arr_data["total"]["surfaceRP"] = $resultTotalRP;
                    $arr_data["total"]["surfaceCP"] = $resultTotalCP;
                    $arr_data["total"]["landcover"] = $arr_data_total_landcover;
                    $arr_data["total"]["irrigation"] = $arr_data_total_irrigation;



                    echo "<br />";
                    $coordenadas_iniciales = getHTMLMainFarmerQuerySummary($arr_polygons, $fad_personalid_search, $bearer, $parcel_search, $arr_data);
                    echo "<br />";

                }
            }
            else {
                echo "<br /><h4>Farmer not found: ".sanitizeString($_GET["fad_personalid_search"])."</h4>";
            }

        } catch (Exception $e) {
            header("Location: ../out/out.login.php");
        }
        // fin AgriculturalProducerPartiesApi $apiInstance->getByCodeUsingGET
        // --------------------------------------------------------------------------------------------
    }
} // if (isset($_GET["pun_code_search"]))


htmlEndPage();

?>