<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: BearerToken
//$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbmRhbHVjaWEiLCJpYXQiOjE2MzE2MDk3OTMsImV4cCI6MTYzMTYxNTc5M30.Chgf9T1wvOrtTQgamI1wBmxxxEmB7QpTv5kRPNxDdtjwo4oqEVsRnd7KR40jtRwY0Ab5jAKN-hpGOjQQgRc7LQ');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$arr = array();
$arr["username"] = "rcampana";
$arr["password"] = "rcampana";
//$data = new \Swagger\Client\Model\LoginRequest($arr); // \Swagger\Client\Model\ActMatsMessage | data


$apiInstance = new Swagger\Client\Api\AuthControllerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login_request = new \Swagger\Client\Model\LoginRequest($arr); // \Swagger\Client\Model\LoginRequest | loginRequest

try {
    $result = $apiInstance->authenticateUserUsingPOST($login_request);
    print_r($result);
    $bearer = $result["accessToken"];
} catch (Exception $e) {
    echo 'Exception when calling AuthControllerApi->authenticateUserUsingPOST: ', $e->getMessage(), PHP_EOL;
}

echo "<br /><hr /><br />";
echo $bearer;
echo "<br /><hr /><br />";

//$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyY2FtcGFuYSIsImlhdCI6MTYzMjk5NTkyOCwiZXhwIjoxNjMzMDAxOTI4fQ.i5ESsiaV3YAP7xdYBBOBnCi4PexOZWvovQZmUM6KQYB_3kVALBtWSXC8X_KCD9LHZj35QLkKYVuCcCijyOy0Sw');
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', "Bearer ".$bearer."");

$apiInstance = new Swagger\Client\Api\QueryCatalogsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCountriesUsingGET();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling QueryCatalogsApi->getCountriesUsingGET: ', $e->getMessage(), PHP_EOL;
}

?>