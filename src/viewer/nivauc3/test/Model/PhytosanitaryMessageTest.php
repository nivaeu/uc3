<?php
/**
 * PhytosanitaryMessageTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * eu.niva4cap.farmregistry
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * PhytosanitaryMessageTest Class Doc Comment
 *
 * @category    Class
 * @description PhytosanitaryMessage
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class PhytosanitaryMessageTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "PhytosanitaryMessage"
     */
    public function testPhytosanitaryMessage()
    {
    }

    /**
     * Test attribute "phytosanitary"
     */
    public function testPropertyPhytosanitary()
    {
    }

    /**
     * Test attribute "co_pphytosanitary"
     */
    public function testPropertyCoPphytosanitary()
    {
    }

    /**
     * Test attribute "cophytosanitary"
     */
    public function testPropertyCophytosanitary()
    {
    }
}
