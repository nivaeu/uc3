<?php
/**
 * AgriculturalProducerPartiesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * eu.niva4cap.farmregistry
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.21
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * AgriculturalProducerPartiesApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AgriculturalProducerPartiesApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createUsingPOST5
     *
     * Inserts data into DB.
     *
     */
    public function testCreateUsingPOST5()
    {
    }

    /**
     * Test case for getAgriculturalProductionUnitByCodeUsingGET
     *
     * Returns an Agricultural Production Unit by its code of an Agricultural Producer Party by its key.
     *
     */
    public function testGetAgriculturalProductionUnitByCodeUsingGET()
    {
    }

    /**
     * Test case for getAgriculturalProductionUnitsByCodeUsingGET
     *
     * Returns all Agricultural Production Units of an Agricultural Producer Party by its code.
     *
     */
    public function testGetAgriculturalProductionUnitsByCodeUsingGET()
    {
    }

    /**
     * Test case for getByCodeUsingGET
     *
     * Returns an Agricultural Producer Party by its Farm Registry Code.
     *
     */
    public function testGetByCodeUsingGET()
    {
    }

    /**
     * Test case for getByCountryUsingGET
     *
     * Returns an Agricultural Producer Party by its Country and Personal ID.
     *
     */
    public function testGetByCountryUsingGET()
    {
    }

    /**
     * Test case for getByPersonalIdCountryUsingGET
     *
     * Returns an Agricultural Producer Party by its Country and Personal ID.
     *
     */
    public function testGetByPersonalIdCountryUsingGET()
    {
    }

    /**
     * Test case for getDetailsUsingGET
     *
     * Returns all Agricultural Producer Parties main details.
     *
     */
    public function testGetDetailsUsingGET()
    {
    }

    /**
     * Test case for getUsingGET
     *
     * Returns all Agricultural Producer Parties codes.
     *
     */
    public function testGetUsingGET()
    {
    }
}
