/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.security.rest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.security.beans.ERole;
import eu.niva4cap.farmregistry.security.beans.Role;
import eu.niva4cap.farmregistry.security.beans.User;
import eu.niva4cap.farmregistry.security.jwt.JwtUtils;
import eu.niva4cap.farmregistry.security.repositories.IRole;
import eu.niva4cap.farmregistry.security.repositories.IUser;
import eu.niva4cap.farmregistry.security.requests.LoginRequest;
import eu.niva4cap.farmregistry.security.requests.SignupRequest;
import eu.niva4cap.farmregistry.security.responses.JwtResponse;
import eu.niva4cap.farmregistry.security.responses.MessageResponse;
import eu.niva4cap.farmregistry.security.services.UserDetailsImpl;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_AUTH_CONTROLLER, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*")
public class AuthController {
    @Autowired
    protected AuthenticationManager authenticationManager;

    @Autowired
    protected IUser iUser;

    @Autowired
    protected IRole iRole;

    @Autowired
    protected PasswordEncoder encoder;

    @Autowired
    protected JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody final LoginRequest loginRequest) {
        final Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String jwt = jwtUtils.generateJwtToken(authentication);

        final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        final List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(),
                userDetails.getUsername(), userDetails.getCountry(), roles));
    }

    
    @PostMapping("/register")
	@ApiOperation(value = "Only available with Admin role", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> registerUser(@Valid @RequestBody final SignupRequest signUpRequest) {

    	if (iUser.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        
        
        final User user = new User(signUpRequest.getUsername(),
                encoder.encode(signUpRequest.getPassword()), signUpRequest.getCountry());

        final Set<Role> roles = new HashSet<>();

        for (String role : signUpRequest.getRole()) {
            role = role.trim().toUpperCase();

            final Optional<Role> oRole = iRole.findByName(role);

            if (!oRole.isPresent() || !Arrays.asList(ERole.values()).toString().contains(role)) {
                return ResponseEntity.badRequest()
                        .body(new MessageResponse("Error: No valid role!"));
            }

            roles.add(oRole.get());
        }

        user.setRoles(roles);
        iUser.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}