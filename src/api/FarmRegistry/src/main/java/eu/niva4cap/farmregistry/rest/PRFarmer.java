/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class PRFarmer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_PR_FARMER, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Farmers")
@ApiIgnore
public class PRFarmer
{
	
	/** The i farmer. */
	@Autowired
	private IPrFarmer iFarmer;

	/**
	 * Obtiene el.
	 *
	 * @return the response entity
	 */
	@GetMapping
	@ApiOperation(value = "Returns all Farmers", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrFarmer>> get()
	{
		try
		{
			return ResponseEntity.ok(iFarmer.findAll());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	/**
	 * Gets the by code.
	 *
	 * @param code the code
	 * @return the by code
	 */
	@GetMapping(value="{code}")
	@ApiOperation(value = "Returns a Farmer by farmer code", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<PrFarmer> getByCode(@PathVariable("code") String code)
	{
		try
		{
			Optional<PrFarmer> beanDB = iFarmer.findByCode(code);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	/**
	 * Gets the by country.
	 *
	 * @param country the country
	 * @return the by country
	 */
	@GetMapping(value="country/{country}")
	@ApiOperation(value = "Returns all Farmers by country code", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Simple.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrFarmer>> getByCountry(@PathVariable("country") String country)
	{
		try
		{
			Optional<List<PrFarmer>> beanDB = iFarmer.findByCountry(country);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}
	
	/**
	 * Gets the by country and personalID.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @return the by country and personalID
	 */
	@GetMapping(value="country/{country}/personalID/{personalID}")
	@ApiOperation(value = "Returns all Farmers by country code and personalID", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<Optional<PrFarmer>> getByCountryAndPersonalID(@PathVariable("country") String country, @PathVariable("personalID") String personalID)
	{
		try
		{
			Optional<PrFarmer> beanDB = iFarmer.findByCountryAndPersonalID(country, personalID);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB);
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}	
}