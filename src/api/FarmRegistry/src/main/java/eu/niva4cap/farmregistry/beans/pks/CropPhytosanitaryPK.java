/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * The Class CropPhytosanitaryPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class CropPhytosanitaryPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7199857432749919231L;

    /** The id crop parcel. */
    @Column(name = "RCP_id")
    private UUID idCropParcel;

    /** The id phytosanitary. */
    @Column(name = "PHY_id")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ID is null")
    private String idPhytosanitary;

    /** The id country. */
    @Column(name = "COU_id")
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.Country is null or empty")
    private String idCountry;

    /** The start date. */
    @Column(name = "CPH_startdate")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.StartDate is null")
    private Date startDate;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CropPhytosanitaryPK other = (CropPhytosanitaryPK) obj;
        if (idCountry == null) {
            if (other.idCountry != null) {
                return false;
            }
        } else if (!idCountry.equals(other.idCountry)) {
            return false;
        }
        if (idCropParcel == null) {
            if (other.idCropParcel != null) {
                return false;
            }
        } else if (!idCropParcel.equals(other.idCropParcel)) {
            return false;
        }
        if (idPhytosanitary == null) {
            if (other.idPhytosanitary != null) {
                return false;
            }
        } else if (!idPhytosanitary.equals(other.idPhytosanitary)) {
            return false;
        }
        if (startDate == null) {
            if (other.startDate != null) {
                return false;
            }
        } else if (!startDate.equals(other.startDate)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the id country.
     *
     * @return the id country
     */
    public String getIdCountry() {
        return idCountry;
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    public UUID getIdCropParcel() {
        return idCropParcel;
    }

    /**
     * Gets the id phytosanitary.
     *
     * @return the id phytosanitary
     */
    public String getIdPhytosanitary() {
        return idPhytosanitary;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate == null ? null : (Date) startDate.clone();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idCountry == null) ? 0 : idCountry.hashCode());
        result = prime * result + ((idCropParcel == null) ? 0 : idCropParcel.hashCode());
        result = prime * result + ((idPhytosanitary == null) ? 0 : idPhytosanitary.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    /**
     * Sets the id country.
     *
     * @param idCountry
     *            the new id country
     */
    public void setIdCountry(final String idCountry) {
        this.idCountry = idCountry;
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel
     *            the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        this.idCropParcel = idCropParcel;
    }

    /**
     * Sets the id phytosanitary.
     *
     * @param idPhytosanitary
     *            the new id phytosanitary
     */
    public void setIdPhytosanitary(final String idPhytosanitary) {
        this.idPhytosanitary = idPhytosanitary;
    }

    /**
     * Sets the start date.
     *
     * @param startDate
     *            the new start date
     */
    public void setStartDate(final Date startDate) {
        this.startDate = (Date) startDate.clone();
    }
}