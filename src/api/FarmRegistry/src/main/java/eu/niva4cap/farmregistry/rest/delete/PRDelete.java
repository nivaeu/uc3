/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.delete;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.repositories.IAbAgriculturalBlock;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.repositories.IPrProductionUnit;
import eu.niva4cap.farmregistry.repositories.IRpReferencePlot;
import eu.niva4cap.farmregistry.rest.ecrop.AgriculturalProducerParty;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class PRDelete.
 *
 * @author irodri11
 * @version 1.0.0,  28-jun-2021
 * @since JDK 1.6
 */
@RestController
@RequestMapping(value = ConstantesRest.REST_DELETE, produces = {ConstantesRest.REST_PRODUCES})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET , RequestMethod.DELETE})
@Api(tags = "Delete entities")
public class PRDelete {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AgriculturalProducerParty.class);

    /** The i farmer. */
    @Autowired
    private IPrFarmer iFarmer;
    
    /** The i production unit. */
    @Autowired
    private IPrProductionUnit iProductionUnit;
    
    /** The i agricultural block. */
    @Autowired
    private IAbAgriculturalBlock iAgriculturalBlock;
    
    /** The i reference plot. */
    @Autowired
    private IRpReferencePlot iReferencePlot;
        
    
    /**
     * Delete.
     *
     * @param code the code
     * @return the response entity
     */
    @DeleteMapping(value = "code/{code}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its code", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("code") final String code) {
        final Response response = new Response();

        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCode(code);

            if (!farmer.isPresent()) {
                response.addError("Agricultural Producer Party not found: " + code);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
                iFarmer.delete(farmer.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    
    /**
     * Delete.
     *
     * @param country the country
     * @param personalID the personalID
     * @return the response entity
     */
    @DeleteMapping(value = "country/{country}/personalID/{personalID}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its country and personalID", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("country") final String country, @PathVariable("personalID") final String personalID) {
        final Response response = new Response();

        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCountryAndPersonalID(country, personalID);

            if (!farmer.isPresent()) {
                response.addError("Agricultural Producer Party not found: " + country + " - " + personalID);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
                iFarmer.delete(farmer.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }    
	
    /**
     * Delete.
     *
     * @param country the country
     * @param personalID the personalID
     * @param apuKey the apuKey
     * @return the response entity
     */
    @DeleteMapping(value = "country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its country, personalID and APUKey", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("country") final String country, @PathVariable("personalID") final String personalID, @PathVariable("apuKey") final String apuKey) {
        final Response response = new Response();

        try {
        	
        	final Optional<PrProductionUnit> produnit = iProductionUnit.findByCountryAndPersonalIDAPUKey(country, personalID, apuKey);
        	
            if (!produnit.isPresent()) {
                response.addError("Agricultural Production Unit not found: " + country + " - " + personalID + " - " + produnit);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
            	iProductionUnit.delete(produnit.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Delete.
     *
     * @param country the country
     * @param personalID the personalID
     * @param code the code
     * @return the response entity
     */
    @DeleteMapping(value = "country/{country}/personalID/{personalID}/code/{code}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its country, personalID and far_code", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> deleteByCode(@PathVariable("country") final String country, @PathVariable("personalID") final String personalID, @PathVariable("code") final String code) {
        final Response response = new Response();

        try {
        	
        	final Optional<PrProductionUnit> produnit = iProductionUnit.findByCode(code);
        	
            if (!produnit.isPresent()) {
                response.addError("Agricultural Production Unit not found: " + country + " - " + personalID + " - " + produnit);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
            	iProductionUnit.delete(produnit.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    
    
    /**
     * Delete.
     *
     * @param country the country
     * @param personalID the personalID
     * @param apuKey the apuKey
     * @param agrBlock the agrBlock
     * @return the response entity
     */
    @DeleteMapping(value = "country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey}/agriculturalBlock/{agrBlock}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("country") final String country, @PathVariable("personalID") final String personalID, @PathVariable("apuKey") final String apuKey, @PathVariable("agrBlock") final String agrBlock) {
        final Response response = new Response();

        try {
        	
        	final List<AbAgriculturalBlock> agrblock = iAgriculturalBlock.findByCountryAndPersonalID(country, personalID, apuKey, agrBlock);
        	
            if ((agrblock.size() > 0) && (agrblock.get(0) != null)) {
                response.addError("Agricultural Block not found: " + country + " - " + personalID + " - " + apuKey + " - " + agrBlock);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
            	iAgriculturalBlock.delete(agrblock.get(0));

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
    
    /**
     * Delete.
     *
     * @param country the country
     * @param personalID the personalID
     * @param apuKey the apuKey
     * @param agrBlock the agrBlock
     * @param refPlot
     * @return the response entity
     */
    @DeleteMapping(value = "country/{country}/personalID/{personalID}/agriculturalProductionUnits/{apuKey}/agriculturalBlock/{agrBlock}/referencePlot/{refPlot}")
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its country, personalID, APUKey and agrBlock", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("country") final String country, @PathVariable("personalID") final String personalID, @PathVariable("apuKey") final String apuKey, @PathVariable("agrBlock") final String agrBlock, @PathVariable("refPlot") final String refPlot) {
        final Response response = new Response();

        try {
        	
        	final Optional<List<RpReferencePlot>> refplot = iReferencePlot.findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(country, personalID, apuKey, agrBlock, refPlot);
        	
            if (!refplot.isPresent()) {
                response.addError("Reference Plot not found: " + country + " - " + personalID + " - " + apuKey + " - " + agrBlock + " - " + refPlot);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
            	iReferencePlot.delete(refplot.get().get(0));

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    


}
