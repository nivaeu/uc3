/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportDocument;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.beans.validators.Validator;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.repositories.IPrProductionUnit;
import eu.niva4cap.farmregistry.repositories.ecrop.ICropReportDocument;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class ECrop.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_LOAD_ECROP, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
@Api(tags = "Agricultural Producer Parties")
public class ECrop {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ECrop.class);

    /** The i crop report document. */
    @Autowired
    private ICropReportDocument iCropReportDocument;

    /** The i farmer. */
    @Autowired
    private IPrFarmer iFarmer;

    /** The i production unit. */
    @Autowired
    private IPrProductionUnit iProductionUnit;

    /** The validator. */
    @Autowired
    private Validator validator;

    /**
     * Crea el.
     *
     * @param data the data
     * @return the response entity
     */
    @PostMapping
    @ApiOperation(value = "Inserts data into DB", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('WRITE')")
    @Transactional(propagation = Propagation.NEVER)
    public ResponseEntity<Response> create(@RequestBody @Valid final CropReportMessage data) {
        final Response response = new Response();

        try {
        	        	        	
            final CropReportDocument cropReportDocument = data.getCropReportDocument();
            iCropReportDocument.save(cropReportDocument);

            for (final AgriculturalProducerParty agriculturalProducerParty : data
                    .getAgriculturalProducerParties()) {
                PrFarmer prFarmer = new PrFarmer();

                if (agriculturalProducerParty.getCountry() != null
                        && agriculturalProducerParty.getPersonalID() != null) {
                    final Optional<PrFarmer> farmer = iFarmer.findByCountryAndPersonalID(
                            agriculturalProducerParty.getCountry(),
                            agriculturalProducerParty.getPersonalID());

                    if (farmer.isPresent()) {
                        prFarmer = farmer.get();
                    }
                }
                
                prFarmer.parseAgriculturalProducerParty(agriculturalProducerParty);

                if (validator.validate(prFarmer)) {
                    if (prFarmer.getCode() == null) {
                        iFarmer.setCode(prFarmer);
                    }

                    for (final PrProductionUnit prProductionUnit : prFarmer.getProductionUnits()) {
                        if (prProductionUnit.getCode() == null) {
                            iProductionUnit.setCode(prProductionUnit);
                        }
                    }
                    System.out.println("inserto prfarmer: " + prFarmer.getCode());
                    iFarmer.save(prFarmer);
                }

                response.addErrors(validator.getErrors());
            }

            if (response.hasErrors()) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}