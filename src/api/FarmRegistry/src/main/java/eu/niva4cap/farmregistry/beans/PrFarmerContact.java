/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.StructuredAddress;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrFarmerContact.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HFARMERCONTACT")
@Table(name = "PR_DFARMERCONTACT")
@JsonPropertyOrder({ "id_country", "city", "street", "number", "postcode", "additional", "email1", "email2", "phone1", "phone2" })
public class PrFarmerContact
{
	
	/** The id. */
	@Id
	@Column(name = "FAC_id")
	@GeneratedValue(generator = "UUID")
	private UUID id;

	/** The id country. */
	@Column(name = "COU_id")
	@JsonView(JsonViews.Detailed.class)
	@NotEmpty(message = "StructuredAddress.Country is null or empty")
	private String idCountry;

	/** The city. */
	@Column(name = "FAC_city")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.CityName is too long")
	private String city;

	/** The street. */
	@Column(name = "FAC_street")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.StreetName is too long")
	private String street;

	/** The number. */
	@Column(name = "FAC_number")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.BuildingNumber is too long")
	private String number;

	/** The postcode. */
	@Column(name = "FAC_postcode")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Postcode is too long")
	private String postcode;

	/** The additional. */
	@Column(name = "FAC_additional")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Additional is too long")
	private String additional;

	/** The email 1. */
	@Column(name = "FAC_email1")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Email1 is too long")
	private String email1;

	/** The email 2. */
	@Column(name = "FAC_email2")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Email2 is too long")
	private String email2;

	/** The phone 1. */
	@Column(name = "FAC_phone1")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Phone1 is too long")
	private String phone1;

	/** The phone 2. */
	@Column(name = "FAC_phone2")
	@JsonView(JsonViews.Detailed.class)
	@Size(max = 255, message = "StructuredAddress.Phone2 is too long")
	private String phone2;

	/** The farmer. */
	@OneToOne(mappedBy = "farmerContact")
	private PrFarmer farmer;

	/** The production unit. */
	@OneToOne(mappedBy = "farmerContact")
	private PrProductionUnit productionUnit;

	/**
	 * Parsea el structured address.
	 *
	 * @param structuredAddress the structured address
	 */
	public void parseStructuredAddress(StructuredAddress structuredAddress) {
		if (structuredAddress.getCountry()!=null) {
			setIdCountry(structuredAddress.getCountry());
		}

		if (structuredAddress.getCityName()!=null) {
			setCity(structuredAddress.getCityName());
		}

		if (structuredAddress.getStreetName()!=null) {
			setStreet(structuredAddress.getStreetName());
		}

		if (structuredAddress.getBuildingNumber()!=null) {
			setNumber(structuredAddress.getBuildingNumber());
		}

		if (structuredAddress.getPostcode()!=null) {
			setPostcode(structuredAddress.getPostcode());
		}

		if (structuredAddress.getAdditional()!=null) {
			setAdditional(structuredAddress.getAdditional());
		}

		if (structuredAddress.getEmail1()!=null) {
			setEmail1(structuredAddress.getEmail1());
		}

		if (structuredAddress.getEmail2()!=null) {
			setEmail2(structuredAddress.getEmail2());
		}

		if (structuredAddress.getPhone1()!=null) {
			setPhone1(structuredAddress.getPhone1());
		}

		if (structuredAddress.getPhone2()!=null) {
			setPhone2(structuredAddress.getPhone2());
		}
	}

	/**
	 * Gets the id country.
	 *
	 * @return the id country
	 */
	public String getIdCountry() {
		return idCountry;
	}

	/**
	 * Sets the id country.
	 *
	 * @param idCountry the new id country
	 */
	@JsonProperty(value = "id_country")
	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	@JsonProperty(value = "city")
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	@JsonProperty(value = "street")
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	@JsonProperty(value = "number")
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * Gets the postcode.
	 *
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * Sets the postcode.
	 *
	 * @param postcode the new postcode
	 */
	@JsonProperty(value = "postcode")
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * Gets the additional.
	 *
	 * @return the additional
	 */
	public String getAdditional() {
		return additional;
	}

	/**
	 * Sets the additional.
	 *
	 * @param additional the new additional
	 */
	@JsonProperty(value = "additional")
	public void setAdditional(String additional) {
		this.additional = additional;
	}

	/**
	 * Gets the email 1.
	 *
	 * @return the email 1
	 */
	public String getEmail1() {
		return email1;
	}

	/**
	 * Sets the email 1.
	 *
	 * @param email1 the new email 1
	 */
	@JsonProperty(value = "email1")
	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	/**
	 * Gets the email 2.
	 *
	 * @return the email 2
	 */
	public String getEmail2() {
		return email2;
	}

	/**
	 * Sets the email 2.
	 *
	 * @param email2 the new email 2
	 */
	@JsonProperty(value = "email2")
	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	/**
	 * Gets the phone 1.
	 *
	 * @return the phone 1
	 */
	public String getPhone1() {
		return phone1;
	}

	/**
	 * Sets the phone 1.
	 *
	 * @param phone1 the new phone 1
	 */
	@JsonProperty(value = "phone1")
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	/**
	 * Gets the phone 2.
	 *
	 * @return the phone 2
	 */
	public String getPhone2() {
		return phone2;
	}

	/**
	 * Sets the phone 2.
	 *
	 * @param phone2 the new phone 2
	 */
	@JsonProperty(value = "phone2")
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	/**
	 * Gets the farmer.
	 *
	 * @return the farmer
	 */
	public PrFarmer getFarmer() {
		return farmer;
	}

	/**
	 * Sets the farmer.
	 *
	 * @param farmer the new farmer
	 */
	public void setFarmer(PrFarmer farmer) {
		this.farmer = farmer;
	}

	/**
	 * Gets the production unit.
	 *
	 * @return the production unit
	 */
	public PrProductionUnit getProductionUnit() {
		return productionUnit;
	}

	/**
	 * Sets the production unit.
	 *
	 * @param productionUnit the new production unit
	 */
	public void setProductionUnit(PrProductionUnit productionUnit) {
		this.productionUnit = productionUnit;
	}
}