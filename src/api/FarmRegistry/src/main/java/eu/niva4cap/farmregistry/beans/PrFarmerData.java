/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrFarmerData.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HFARMERDATA")
@Table(name = "PR_DFARMERDATA")
@JsonPropertyOrder({"id_farmer_type", "name", "personal_id"})
public class PrFarmerData {

    /** The id farmer. */
    @Id
    @Column(name = "FAR_id")
    private UUID idFarmer;

    /** The id farmer type. */
    @Column(name = "FAT_id")
    @JsonView(JsonViews.Detailed.class)
    private Integer idFarmerType;

    /** The personal id. */
    @Column(name = "FAD_personalid")
    @JsonView(JsonViews.Detailed.class)
    @NotEmpty(message = "AgriculturalProducerParty.PersonalID is null or empty")
    @Size(max = 255, message = "AgriculturalProducerParty.PersonalID is too long")
    private String personalId;

    /** The name. */
    @Column(name = "FAD_name")
    @JsonView(JsonViews.Detailed.class)
    @Size(max = 255, message = "AgriculturalProducerParty.Name is too long")
    private String name;

    /** The farmer. */
    @MapsId
    @OneToOne
    @JoinColumn(name = "FAR_id")
    private PrFarmer farmer;

    /**
     * Gets the farmer.
     *
     * @return the farmer
     */
    public PrFarmer getFarmer() {
        return farmer;
    }

    /**
     * Gets the id farmer.
     *
     * @return the id farmer
     */
    @JsonIgnore
    public UUID getIdFarmer() {
        return idFarmer;
    }

    /**
     * Gets the id farmer type.
     *
     * @return the id farmer type
     */
    public Integer getIdFarmerType() {
        return idFarmerType;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the personal id.
     *
     * @return the personal id
     */
    public String getPersonalId() {
        return personalId;
    }

    /**
     * Parsea el agricultural producer party.
     *
     * @param agriculturalProducerParty
     *            the agricultural producer party
     */
    public void parseAgriculturalProducerParty(
            final AgriculturalProducerParty agriculturalProducerParty) {
        if (agriculturalProducerParty.getClassification() != null) {
            setIdFarmerType(agriculturalProducerParty.getClassification());
        }

        if (agriculturalProducerParty.getPersonalID() != null) {
            setPersonalId(agriculturalProducerParty.getPersonalID());
        }

        if (agriculturalProducerParty.getName() != null) {
            setName(agriculturalProducerParty.getName());
        }
    }

    /**
     * Sets the farmer.
     *
     * @param farmer
     *            the new farmer
     */
    public void setFarmer(final PrFarmer farmer) {
        this.farmer = farmer;
    }

    /**
     * Sets the id farmer.
     *
     * @param idFarmer
     *            the new id farmer
     */
    public void setIdFarmer(final UUID idFarmer) {
        this.idFarmer = idFarmer;
    }

    /**
     * Sets the id farmer type.
     *
     * @param idFarmerType
     *            the new id farmer type
     */
    @JsonProperty(value = "id_farmer_type")
    public void setIdFarmerType(final Integer idFarmerType) {
        this.idFarmerType = idFarmerType;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    @JsonProperty(value = "name")
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the personal id.
     *
     * @param personalId
     *            the new personal id
     */
    @JsonProperty(value = "personal_id")
    public void setPersonalId(final String personalId) {
        this.personalId = personalId;
    }
}