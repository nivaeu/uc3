/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class CropIrrigationPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class CropIrrigationPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7104054111187182744L;

    /** The id crop parcel. */
    @Column(name = "RCP_id")
    private UUID idCropParcel;

    /** The irrigation date. */
    @Column(name = "CRI_irrigationdate")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.Date is null")
    private Date irrigationDate;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CropIrrigationPK other = (CropIrrigationPK) obj;
        if (idCropParcel == null) {
            if (other.idCropParcel != null) {
                return false;
            }
        } else if (!idCropParcel.equals(other.idCropParcel)) {
            return false;
        }
        if (irrigationDate == null) {
            if (other.irrigationDate != null) {
                return false;
            }
        } else if (!irrigationDate.equals(other.irrigationDate)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    public UUID getIdCropParcel() {
        return idCropParcel;
    }

    /**
     * Gets the irrigation date.
     *
     * @return the irrigation date
     */
    public Date getIrrigationDate() {
        return irrigationDate == null ? null : (Date) irrigationDate.clone();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idCropParcel == null) ? 0 : idCropParcel.hashCode());
        result = prime * result + ((irrigationDate == null) ? 0 : irrigationDate.hashCode());
        return result;
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel
     *            the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        this.idCropParcel = idCropParcel;
    }

    /**
     * Sets the irrigation date.
     *
     * @param irrigationDate
     *            the new irrigation date
     */
    public void setIrrigationDate(final Date irrigationDate) {
        this.irrigationDate = (Date) irrigationDate.clone();
    }
}