/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.SoilType;
import eu.niva4cap.farmregistry.beans.pks.LandCoverPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class AbLandCover.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "AB_HLANDCOVER")
@Table(name = "AB_DLANDCOVER")
@JsonPropertyOrder({"id_land_cover", "date_from", "date_to"})
public class AbLandCover {

    /** The land cover PK. */
    @Valid
    protected @EmbeddedId LandCoverPK landCoverPK = new LandCoverPK();

    /** The id agricultural block. */
    @Column(name = "ABL_id", insertable = false, updatable = false)
    private UUID idAgriculturalBlock;

    /** The id land cover. */
    @Column(name = "LAN_id", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private String idLandCover;

    /** The date from. */
    @Column(name = "ALC_datefrom", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "ALC_dateto")
    @JsonView(JsonViews.Detailed.class)
    private Date dateTo;

    /** The agricultural block. */
    @MapsId(value = "idAgriculturalBlock")
    @ManyToOne
    @JoinColumn(name = "ABL_id")
    private AbAgriculturalBlock agriculturalBlock;

    /**
     * Gets the agricultural block.
     *
     * @return the agricultural block
     */
    @JsonIgnore
    public AbAgriculturalBlock getAgriculturalBlock() {
        return agriculturalBlock;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return landCoverPK.getDateFrom();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the id agricultural block.
     *
     * @return the id agricultural block
     */
    @JsonIgnore
    public UUID getIdAgriculturalBlock() {
        return landCoverPK.getIdAgriculturalBlock();
    }

    /**
     * Gets the id land cover.
     *
     * @return the id land cover
     */
    public Integer getIdLandCover() {
        return landCoverPK.getIdLandCover();
    }

    /**
     * Parsea el land cover.
     *
     * @param landCover
     *            the land cover
     */
    public void parseLandCover(final SoilType landCover) {
        setIdLandCover(landCover.getType());
        setDateFrom(landCover.getDateFrom());
        setDateTo(landCover.getDateTo());
    }

    /**
     * Sets the agricultural block.
     *
     * @param agriculturalBlock
     *            the new agricultural block
     */
    public void setAgriculturalBlock(final AbAgriculturalBlock agriculturalBlock) {
        this.agriculturalBlock = agriculturalBlock;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        landCoverPK.setDateFrom(dateFrom);
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the id agricultural block.
     *
     * @param idAgriculturalBlock
     *            the new id agricultural block
     */
    public void setIdAgriculturalBlock(final UUID idAgriculturalBlock) {
        landCoverPK.setIdAgriculturalBlock(idAgriculturalBlock);
    }

    /**
     * Sets the id land cover.
     *
     * @param idLandCover
     *            the new id land cover
     */
    @JsonProperty(value = "id_land_cover")
    public void setIdLandCover(final Integer idLandCover) {
        landCoverPK.setIdLandCover(idLandCover);
    }
}