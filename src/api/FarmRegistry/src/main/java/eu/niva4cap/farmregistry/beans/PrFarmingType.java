/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.FarmingType;
import eu.niva4cap.farmregistry.beans.pks.FarmingTypePK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrFarmingType.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HFARMINGTYPE")
@Table(name = "PR_DFARMINGTYPE")
@JsonPropertyOrder({"id_farming_type", "date_from", "date_to"})
public class PrFarmingType {

    /** The farming type PK. */
    @Valid
    @EmbeddedId
    protected FarmingTypePK farmingTypePK = new FarmingTypePK();

    /** The id production unit. */
    @Column(name = "PUN_id", insertable = false, updatable = false)
    private UUID idProductionUnit;

    /** The id farming type. */
    @Column(name = "FRT_id", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private String idFarmingType;

    /** The date from. */
    @Column(name = "PFT_datefrom", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "PFT_dateto")
    @JsonView(JsonViews.Detailed.class)
    private Date dateTo;

    /** The production unit. */
    @MapsId(value = "idProductionUnit")
    @ManyToOne
    @JoinColumn(name = "PUN_id")
    private PrProductionUnit productionUnit;

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return farmingTypePK.getDateFrom();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the id farming type.
     *
     * @return the id farming type
     */
    public String getIdFarmingType() {
        return farmingTypePK.getIdFarmingType();
    }

    /**
     * Gets the id production unit.
     *
     * @return the id production unit
     */
    @JsonIgnore
    public UUID getIdProductionUnit() {
        return farmingTypePK.getIdProductionUnit();
    }

    /**
     * Gets the production unit.
     *
     * @return the production unit
     */
    public PrProductionUnit getProductionUnit() {
        return productionUnit;
    }

    /**
     * Parsea el farming type.
     *
     * @param farmingType
     *            the farming type
     */
    public void parseFarmingType(final FarmingType farmingType) {
        setIdFarmingType(farmingType.getType());
        setDateFrom(farmingType.getDateFrom());
        setDateTo(farmingType.getDateTo());
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        farmingTypePK.setDateFrom(dateFrom);
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the id farming type.
     *
     * @param idFarmingType
     *            the new id farming type
     */
    @JsonProperty(value = "id_farming_type")
    public void setIdFarmingType(final String idFarmingType) {
        farmingTypePK.setIdFarmingType(idFarmingType);
    }

    /**
     * Sets the id production unit.
     *
     * @param idProductionUnit
     *            the new id production unit
     */
    public void setIdProductionUnit(final UUID idProductionUnit) {
        farmingTypePK.setIdProductionUnit(idProductionUnit);
    }

    /**
     * Sets the production unit.
     *
     * @param productionUnit
     *            the new production unit
     */
    public void setProductionUnit(final PrProductionUnit productionUnit) {
        this.productionUnit = productionUnit;
    }
}