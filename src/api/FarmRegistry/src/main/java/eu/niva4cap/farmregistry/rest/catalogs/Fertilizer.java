/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.catalogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizer;
import eu.niva4cap.farmregistry.beans.ecrop.FertilizerMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.beans.validators.Validator;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizer;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class Fertilizer.
 *
 * @author irodri11
 * @version 1.0.0,  13-may-2021
 * @since JDK 1.6
 */

@RestController
@RequestMapping(value = ConstantesRest.REST_FERTILIZER, produces = { ConstantesRest.REST_PRODUCES })
@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
@Api(tags = "Fertilizers")
public class Fertilizer {

	
	@Autowired
	private Validator validator;

	@Autowired
	private ICoFertilizer iFertilizer;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FertilizerMessage.class);
	
	
	@PostMapping
	@ApiOperation(value = "Inserts data into DB", authorizations = { @Authorization(value="BearerToken") })
	@PreAuthorize("hasRole('WRITE')")
	@Transactional(propagation = Propagation.NEVER)
	public ResponseEntity<Response> create(@RequestBody final FertilizerMessage data) {
		final Response response = new Response();

		try {

			for (final CoFertilizer cofertilizer : data.getCoPfertilizer()) {
				CoFertilizer fertilizer = new CoFertilizer();

				if (cofertilizer.getId() != null && cofertilizer.getName() != null) {
					fertilizer.setId(cofertilizer.getId());
					fertilizer.setName(cofertilizer.getName());
					fertilizer.setIdFertilizerType(cofertilizer.getIdFertilizerType());
					fertilizer.setExpiryDate(cofertilizer.getExpiryDate());

				}
				
                if (validator.validate(fertilizer)) {
                    iFertilizer.save(fertilizer);
                }				
								
    			response.addErrors(validator.getErrors());

			}


			if (response.hasErrors()) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return ResponseEntity.badRequest().body(response);
			}

			else {
				response.setStatus(HttpStatus.OK.value());
				return ResponseEntity.ok().body(response);
			}
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());

			response.addError("Internal error in WebService");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}
	
	

	
}
