/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class CropReportMessage.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
public class CropReportMessage
{
	
	/** The crop report document. */
	@JsonView(JsonViews.FullDetailed.class)
	@Valid
	@NotNull(message = "CropReportDocument is null")
	private CropReportDocument cropReportDocument;

	/** The agricultural producer parties. */
	@JsonView(JsonViews.FullDetailed.class)
	@NotEmpty(message = "AgriculturalProducerParty is null or empty")
	private List<AgriculturalProducerParty> agriculturalProducerParties;

	/**
	 * Gets the crop report document.
	 *
	 * @return the crop report document
	 */
	public CropReportDocument getCropReportDocument() {
		return cropReportDocument;
	}

	/**
	 * Sets the crop report document.
	 *
	 * @param cropReportDocument the new crop report document
	 */
	@JsonProperty(value = "CropReportDocument")
	public void setCropReportDocument(CropReportDocument cropReportDocument) {
		this.cropReportDocument = cropReportDocument;
	}

	/**
	 * Gets the agricultural producer parties.
	 *
	 * @return the agricultural producer parties
	 */
	public List<AgriculturalProducerParty> getAgriculturalProducerParties() {
		return agriculturalProducerParties;
	}

	/**
	 * Sets the agricultural producer parties.
	 *
	 * @param agriculturalProducerParties the new agricultural producer parties
	 */
	@JsonProperty(value = "AgriculturalProducerParty")
	public void setAgriculturalProducerParties(List<AgriculturalProducerParty> agriculturalProducerParties) {
		this.agriculturalProducerParties = agriculturalProducerParties;
	}
}