/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.repositories.IAbAgriculturalBlock;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class CropPlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_CROP_PLOT, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
@Api(tags = "Crop Plots")
public class CropPlot {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CropPlot.class);

    /** The i agricultural block. */
    @Autowired
    private IAbAgriculturalBlock iAgriculturalBlock;

    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping
	@RequestMapping(method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Crop Plots code list", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot>> getCodeList() {
    	System.out.println("--- inicio get");
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot> cropPlots = new ArrayList<>();

            final List<AbAgriculturalBlock> abAgriculturalBlocks = iAgriculturalBlock.findAll();

            for (final AbAgriculturalBlock abAgriculturalBlock : abAgriculturalBlocks) {
                final eu.niva4cap.farmregistry.beans.ecrop.CropPlot cropPlot = new eu.niva4cap.farmregistry.beans.ecrop.CropPlot();
                cropPlots.add(cropPlot);

                cropPlot.parseAgriculturalBlockCodeList(abAgriculturalBlock);
            }

            return ResponseEntity.ok(cropPlots);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
    
    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping(value = "details")
	@RequestMapping(value = "details", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Crop Plots", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot>> get() {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot> cropPlots = new ArrayList<>();

            final List<AbAgriculturalBlock> abAgriculturalBlocks = iAgriculturalBlock.findAll();

            for (final AbAgriculturalBlock abAgriculturalBlock : abAgriculturalBlocks) {
                final eu.niva4cap.farmregistry.beans.ecrop.CropPlot cropPlot = new eu.niva4cap.farmregistry.beans.ecrop.CropPlot();
                cropPlots.add(cropPlot);

                cropPlot.parseAgriculturalBlock(abAgriculturalBlock);
            }

            return ResponseEntity.ok(cropPlots);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
        
    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping(value = "country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey}")
	@RequestMapping(value = "country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey}", 
	method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Crop Plots by farmer personalID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot>> getCropPlotsByFarmer(
    		@PathVariable("country") final String code,
    		@PathVariable("personalID") final String personalID,
            @PathVariable("AgriculturalProductionUnitKey") final String productionUnitKey
    		) {
    	
    	
    	
    	System.out.println("--- inicio NewCropPlot.getCropPlotsByFarmer (" + code + " - " + personalID + ")");
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot> cropPlots = new ArrayList<>();

            List<AbAgriculturalBlock> abAgriculturalBlocks = iAgriculturalBlock.findByCountryAndPersonalID(code, personalID, productionUnitKey);

			for (final AbAgriculturalBlock abAgriculturalBlock : abAgriculturalBlocks) {
                final eu.niva4cap.farmregistry.beans.ecrop.CropPlot cropPlot = new eu.niva4cap.farmregistry.beans.ecrop.CropPlot();
                
                
                cropPlot.parseAgriculturalBlock(abAgriculturalBlock);
                cropPlots.add(cropPlot);

            }

            return ResponseEntity.ok(cropPlots);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
    
    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping(value = "country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey}/key/{key}")
	@RequestMapping(value = "country/{country}/personalID/{personalID}/AgriculturalProductionUnitKey/{AgriculturalProductionUnitKey}/key/{key}", 
	method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})

    @ApiOperation(value = "Returns all Crop Plots by farmer personalID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot>> getCropPlotsByFarmerKey(
    		@PathVariable("country") final String code,
    		@PathVariable("personalID") final String personalID,
            @PathVariable("AgriculturalProductionUnitKey") final String productionUnitKey,
            @PathVariable("key") final String key
    		) {
    	
    	System.out.println("--- inicio CropPlot.getCropPlotsByFarmerKey (" + code + " - " + personalID + " - " + productionUnitKey + " - " + key + ")");
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.CropPlot> cropPlots = new ArrayList<>();

            List<AbAgriculturalBlock> abAgriculturalBlocks = iAgriculturalBlock.findByCountryAndPersonalID(code, personalID, productionUnitKey, key);

			for (final AbAgriculturalBlock abAgriculturalBlock : abAgriculturalBlocks) {
                final eu.niva4cap.farmregistry.beans.ecrop.CropPlot cropPlot = new eu.niva4cap.farmregistry.beans.ecrop.CropPlot();

                cropPlot.parseAgriculturalBlock(abAgriculturalBlock);
                cropPlots.add(cropPlot);
            }

            return ResponseEntity.ok(cropPlots);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
    
}