/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropPhytosanitary;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CropPhytosanitary.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"ID", "Country", "Dose", "Unit", "Equipment", "StartDate", "EndDate"})
public class CropPhytosanitary {
    
    /** The id. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "0")
    private String id;
    
    /** The country. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "ES")
    private String country;
    
    /** The dose. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "55.21")
    private BigDecimal dose;
    
    /** The unit. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer unit;
    
    /** The equipment. */
    @JsonView(JsonViews.Detailed.class)  
    @ApiModelProperty(example = "1")
    private Integer equipment;
    
    /** The start date. */
    @JsonView(JsonViews.Detailed.class)    
    @ApiModelProperty(example = "2020-01-01")
    private Date startDate;
    
    /** The end date. */
    @JsonView(JsonViews.Detailed.class)   
    @ApiModelProperty(example = "2021-03-12")
    private Date endDate;

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the dose.
     *
     * @return the dose
     */
    public BigDecimal getDose() {
        return dose;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the equipment.
     *
     * @return the equipment
     */
    public Integer getEquipment() {
        return equipment;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate == null ? null : (Date) startDate.clone();
    }

    /**
     * Gets the unit.
     *
     * @return the unit
     */
    public Integer getUnit() {
        return unit;
    }

    /**
     * Parsea el crop phytosanitary.
     *
     * @param cropPhytosanitary the crop phytosanitary
     */
    public void parseCropPhytosanitary(final RpCropPhytosanitary cropPhytosanitary) {
        setId(cropPhytosanitary.getIdPhytosanitary());
        setCountry(cropPhytosanitary.getIdCountry());
        setDose(cropPhytosanitary.getDose());
        setUnit(cropPhytosanitary.getIdUnitType());
        setEquipment(cropPhytosanitary.getIdPhytosanitaryEquipment());
        setStartDate(cropPhytosanitary.getStartDate());
        setEndDate(cropPhytosanitary.getEndDate());
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    @JsonProperty(value = "Country")
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * Sets the dose.
     *
     * @param dose the new dose
     */
    @JsonProperty(value = "Dose")
    public void setDose(final BigDecimal dose) {
        this.dose = dose;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    @JsonProperty(value = "EndDate")
    public void setEndDate(final Date endDate) {
        this.endDate = endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Sets the equipment.
     *
     * @param equipment the new equipment
     */
    @JsonProperty(value = "Equipment")
    public void setEquipment(final Integer equipment) {
        this.equipment = equipment;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @JsonProperty(value = "ID")
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    @JsonProperty(value = "StartDate")
    public void setStartDate(final Date startDate) {
        this.startDate = startDate == null ? null : (Date) startDate.clone();
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    @JsonProperty(value = "Unit")
    public void setUnit(final Integer unit) {
        this.unit = unit;
    }
}