/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.PrFarmingType;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.PrProductionWorkunit;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class AgriculturalProductionUnit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Code", "Key", "Country", "DateFrom", "DateTo", "FarmingType", "Workunit",
        "StructuredAddress", "CropPlot"})
public class AgriculturalProductionUnit {
    
    /** The code. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "ES000000003589")
    private String code;
    
    /** The key. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "08105974")
    private String key;
    
    /** The country. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "ES")
    private String country;
    
    /** The date from. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date dateFrom;
    
    /** The date to. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2021-03-24")
    private Date dateTo;
    
    /** The farming types. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<FarmingType> farmingTypes;
    
    /** The workunits. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<Workunit> workunits;
    
    /** The structured address. */
    @JsonView(JsonViews.FullDetailed.class)
    private StructuredAddress structuredAddress;
    
    /** The crop plots. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<CropPlot> cropPlots;

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the crop plots.
     *
     * @return the crop plots
     */
    public List<CropPlot> getCropPlots() {
        return cropPlots;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the farming types.
     *
     * @return the farming types
     */
    public List<FarmingType> getFarmingTypes() {
        return farmingTypes;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the structured address.
     *
     * @return the structured address
     */
    public StructuredAddress getStructuredAddress() {
        return structuredAddress;
    }

    /**
     * Gets the workunits.
     *
     * @return the workunits
     */
    public List<Workunit> getWorkunits() {
        return workunits;
    }
    
    /**
     * Parsea el production unit.
     *
     * @param productionUnit the production unit
     */
    public void parseProductionUnit(final PrProductionUnit productionUnit) {
        setCode(productionUnit.getCode());
        setKey(productionUnit.getKey());
        setCountry(productionUnit.getIdCountry());
        setDateFrom(productionUnit.getDateFrom());
        setDateTo(productionUnit.getDateTo());

        if (productionUnit.getFarmingTypes() != null) {
            setFarmingTypes(new ArrayList<FarmingType>());

            for (final PrFarmingType prFarmingType : productionUnit.getFarmingTypes()) {
                final FarmingType farmingType = new FarmingType();
                getFarmingTypes().add(farmingType);

                farmingType.parseFarmingType(prFarmingType);
            }
        }

        if (productionUnit.getProductionWorkunits() != null) {
            setWorkunits(new ArrayList<Workunit>());

            for (final PrProductionWorkunit prProductionWorkunit : productionUnit
                    .getProductionWorkunits()) {
                final Workunit workunit = new Workunit();
                getWorkunits().add(workunit);

                workunit.parseProductionWorkunit(prProductionWorkunit);
            }
        }

        if (productionUnit.getAgriculturalBlocks() != null) {
            setCropPlots(new ArrayList<CropPlot>());

            for (final AbAgriculturalBlock abAgriculturalBlock : productionUnit
                    .getAgriculturalBlocks()) {
                final CropPlot cropPlot = new CropPlot();
                getCropPlots().add(cropPlot);

                cropPlot.parseAgriculturalBlock(abAgriculturalBlock);
            }
        }
    }
    
    /**
     * Parsea el production unit.
     *
     * @param productionUnit the production unit
     */
    public void parseProductionUnitCodeList(final PrProductionUnit productionUnit) {
        setCode(productionUnit.getCode());
        setKey(productionUnit.getKey());
        setCountry(productionUnit.getIdCountry());
        setDateFrom(productionUnit.getDateFrom());
        setDateTo(productionUnit.getDateTo());

    }
    

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    @JsonProperty(value = "Code")
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    @JsonProperty(value = "Country")
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * Sets the crop plots.
     *
     * @param cropPlots the new crop plots
     */
    @JsonProperty(value = "CropPlot")
    public void setCropPlots(final List<CropPlot> cropPlots) {
        this.cropPlots = cropPlots;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "DateFrom")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "DateTo")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the farming types.
     *
     * @param farmingTypes the new farming types
     */
    @JsonProperty(value = "FarmingType")
    public void setFarmingTypes(final List<FarmingType> farmingTypes) {
        this.farmingTypes = farmingTypes;
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    @JsonProperty(value = "Key")
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the structured address.
     *
     * @param structuredAddress the new structured address
     */
    @JsonProperty(value = "StructuredAddress")
    public void setStructuredAddress(final StructuredAddress structuredAddress) {
        this.structuredAddress = structuredAddress;
    }

    /**
     * Sets the workunits.
     *
     * @param workunits the new workunits
     */
    @JsonProperty(value = "Workunit")
    public void setWorkunits(final List<Workunit> workunits) {
        this.workunits = workunits;
    }
}