/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.PrFarmer;

/**
 * The Interface IPrFarmer.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public interface IPrFarmer extends JpaRepository<PrFarmer, UUID> {

    /**
     * Find by code.
     *
     * @param code
     *            the code
     * @return the optional
     */
    @Query(value = "SELECT * FROM PR_DFARMER WHERE FAR_code = :code", nativeQuery = true)
    Optional<PrFarmer> findByCode(@Param("code") String code);

    /**
     * Find by country.
     *
     * @param country
     *            the country
     * @return the optional
     */
    @Query(value = "SELECT * FROM PR_DFARMER WHERE COU_id = :country", nativeQuery = true)
    Optional<List<PrFarmer>> findByCountry(@Param("country") String country);

    /**
     * Find by country and personal ID.
     *
     * @param country
     *            the country
     * @param personalID
     *            the personal ID
     * @return the optional
     */
    @Query(value = "SELECT * FROM PR_DFARMER f, PR_DFARMERDATA fd WHERE f.COU_id = :country and fd.FAR_id = f.FAR_id and fd.FAD_personalid = :personalID",
           nativeQuery = true)
    Optional<PrFarmer> findByCountryAndPersonalID(@Param("country") String country,
            @Param("personalID") String personalID);

    
    /**
     * Calculate total AB surface.
     *
     * @return the total AB surface
     */
    @Query(value = "SELECT SUM(SUPERFICIE.AGE_SURFACE) FROM (SELECT AGE_GEOMETRY, AGE_SURFACE" + 
    		"	FROM AB_DGEOMETRY GROUP BY AGE_GEOMETRY, AGE_SURFACE) SUPERFICIE;",
           nativeQuery = true)
    double surfaceAB();


    /**
     * Calculate total RP surface.
     *
     * @return the total RP surface
     */
    @Query(value = "SELECT SUM(SUPERFICIE.RGE_SURFACE) FROM (SELECT RGE_GEOMETRY, RGE_SURFACE" + 
    		"	FROM RP_DGEOMETRY GROUP BY RGE_GEOMETRY, RGE_SURFACE) SUPERFICIE;",
           nativeQuery = true)
    double surfaceRP();


    /**
     * Calculate total CP surface.
     *
     * @return the total CP surface
     */
    @Query(value = "SELECT SUM(SUPERFICIE.CPG_SURFACE) FROM (SELECT CPG_GEOMETRY, CPG_SURFACE" + 
    		"	FROM RP_DCROPPARCELGEOM GROUP BY CPG_GEOMETRY, CPG_SURFACE) SUPERFICIE;",
           nativeQuery = true)
    double surfaceCP();

       
    
    /**
     * Calculate total AB surface by personal ID.
     *
     * @param personalID
     *            the personal ID
     * @return the total AB surface
     */
    @Query(value = "SELECT SUM(AGE_SURFACE) FROM AB_DGEOMETRY INNER JOIN AB_DAGRBLOCK ON AB_DGEOMETRY.ABL_ID = AB_DAGRBLOCK.ABL_ID WHERE PUN_ID IN (SELECT PUN_ID FROM PR_DPRODUCTIONUNIT PU INNER JOIN PR_DFARMERDATA PFD ON PU.FAR_ID = PFD.FAR_ID WHERE FAD_PERSONALID = :personalID)",
           nativeQuery = true)
    double surfaceABByPersonalID(@Param("personalID") String personalID);

    /**
     * Calculate total RP surface by personal ID.
     *
     * @param personalID
     *            the personal ID
     * @return the total RP surface
     */
    @Query(value = "SELECT SUM(AGR.RGE_SURFACE) " + 
    		"FROM (SELECT *" + 
    		"	  FROM RP_DGEOMETRY" + 
    		"	  		INNER JOIN RP_DREFPLOT" + 
    		"	  		ON RP_DGEOMETRY.RFP_ID = RP_DREFPLOT.RFP_ID" + 
    		"	  		INNER JOIN AB_DAGRBLOCK" + 
    		"	 		ON RP_DREFPLOT.ABL_ID = AB_DAGRBLOCK.ABL_ID" + 
    		"	  WHERE PUN_ID IN (SELECT PUN_ID" + 
    		"		FROM PR_DPRODUCTIONUNIT PU" + 
    		"		 	INNER JOIN PR_DFARMERDATA PFD ON PU.FAR_ID = PFD.FAR_ID" + 
    		"		WHERE FAD_PERSONALID = :personalID)) AGR;",
           nativeQuery = true)
    double surfaceRPByPersonalID(@Param("personalID") String personalID);
    

    /**
     * Calculate total CP surface by personal ID.
     *
     * @param personalID
     *            the personal ID
     * @return the total CP surface
     */
    @Query(value = "SELECT SUM(AGR.CPG_SURFACE) " + 
    		"  FROM (SELECT * " + 
    		"	  FROM RP_DCROPPARCELGEOM " + 
    		"	  		INNER JOIN RP_DCROPPARCEL " + 
    		"	  		ON RP_DCROPPARCELGEOM.RCP_ID = RP_DCROPPARCEL.RCP_ID " + 
    		"	  		INNER JOIN RP_DREFPLOT " + 
    		"	  		ON RP_DCROPPARCEL.RFP_ID = RP_DREFPLOT.RFP_ID " + 
    		"	  		INNER JOIN AB_DAGRBLOCK " + 
    		"	 		ON RP_DREFPLOT.ABL_ID = AB_DAGRBLOCK.ABL_ID " + 
    		"	  WHERE PUN_ID IN (SELECT PUN_ID " + 
    		"		FROM PR_DPRODUCTIONUNIT PU " + 
    		"		 	INNER JOIN PR_DFARMERDATA PFD ON PU.FAR_ID = PFD.FAR_ID " + 
    		"		WHERE FAD_PERSONALID = :personalID)) AGR;",
           nativeQuery = true)
    double surfaceCPByPersonalID(@Param("personalID") String personalID);
    
    
    /**
     * Calculate total CP surface by personal ID and irrigation.
     *
     * @param personalID
     *            the personal ID
     * @param irrigation
     * 			  the irrigation
     * @return the total CP surface
     */
    @Query(value = "SELECT SUM(AGR.CPG_SURFACE) " + 
    		"  FROM (SELECT * " + 
    		"	  FROM RP_DCROPPARCELGEOM " + 
    		"	  		INNER JOIN RP_DCROPPARCEL " + 
    		"	  		ON RP_DCROPPARCELGEOM.RCP_ID = RP_DCROPPARCEL.RCP_ID " + 
    		"	  		INNER JOIN RP_DREFPLOT " + 
    		"	  		ON RP_DCROPPARCEL.RFP_ID = RP_DREFPLOT.RFP_ID " + 
    		"	  		INNER JOIN AB_DAGRBLOCK " + 
    		"	 		ON RP_DREFPLOT.ABL_ID = AB_DAGRBLOCK.ABL_ID " + 
    		"	  WHERE RCP_IRRIGATION = :irrigation AND PUN_ID IN (SELECT PUN_ID " + 
    		"		FROM PR_DPRODUCTIONUNIT PU " + 
    		"		 	INNER JOIN PR_DFARMERDATA PFD ON PU.FAR_ID = PFD.FAR_ID " + 
    		"		WHERE FAD_PERSONALID = :personalID)) AGR;",
           nativeQuery = true)
    double surfaceCPByPersonalIDIrrigation(@Param("personalID") String personalID, 
    		@Param("irrigation") boolean irrigation);
    
    /**
     * Calculate total AB surface by personal ID and landcover.
     *
     * @param personalID
     *            the personal ID
     * @param landCover
     * 			  the land cover
     * @return the total AB surface
     */
    @Query(value = "SELECT CASE WHEN SUM(AGR.AGE_SURFACE) IS NULL THEN 0 ELSE SUM(AGR.AGE_SURFACE) END " + 
    		" FROM (SELECT * " + 
    		"	  FROM AB_DGEOMETRY " + 
    		"	 		INNER JOIN AB_DAGRBLOCK" + 
    		"	 		ON AB_DGEOMETRY.ABL_ID = AB_DAGRBLOCK.ABL_ID" + 
    		"	  		INNER JOIN AB_DLANDCOVER" + 
    		"	  		ON AB_DLANDCOVER.ABL_ID = AB_DAGRBLOCK.ABL_ID" + 
    		"	  		INNER JOIN CO_PLANDCOVER" + 
    		"	  		ON AB_DLANDCOVER.LAN_ID = CO_PLANDCOVER.LAN_ID" + 
    		"	  WHERE" + 
    		"	  		AB_DLANDCOVER.LAN_ID = :landCover AND" + 
    		"	  		PUN_ID IN (SELECT PUN_ID" + 
    		"		FROM PR_DPRODUCTIONUNIT PU" + 
    		"		 	INNER JOIN PR_DFARMERDATA PFD ON PU.FAR_ID = PFD.FAR_ID" + 
    		"		WHERE FAD_PERSONALID = :personalID)) AGR;",
           nativeQuery = true)
    double surfaceABByPersonalIDLandCover(@Param("personalID") String personalID,
    		@Param("landCover") int landCover);
    
    /**
     * Calculate total AB surface by landcover.
     *
     * @param landCover
     * 			  the land cover
     * @return the total AB surface
     */
    @Query(value = "SELECT CASE WHEN SUM(AGR.AGE_SURFACE) IS NULL THEN 0 ELSE SUM(AGR.AGE_SURFACE) END " +  
    		" FROM (SELECT * " + 
    		"	  FROM AB_DGEOMETRY " + 
    		"	 		INNER JOIN AB_DAGRBLOCK" + 
    		"	 		ON AB_DGEOMETRY.ABL_ID = AB_DAGRBLOCK.ABL_ID" + 
    		"	  		INNER JOIN AB_DLANDCOVER" + 
    		"	  		ON AB_DLANDCOVER.ABL_ID = AB_DAGRBLOCK.ABL_ID" + 
    		"	  		INNER JOIN CO_PLANDCOVER" + 
    		"	  		on AB_DLANDCOVER.LAN_ID = CO_PLANDCOVER.LAN_ID" + 
    		"	  WHERE " + 
    		"	  		AB_DLANDCOVER.LAN_ID = :landCover ) AGR;",
           nativeQuery = true)
    double surfaceABByLandCover(@Param("landCover") int landCover);    
    
    /**
     * Calculate total surface by irrigation.
     *
     * @param irrigation
     * 			  the irrigation
     * @return the total surface
     */
    @Query(value = "SELECT CASE WHEN SUM(AGR.CPG_SURFACE) IS NULL THEN 0 ELSE SUM(AGR.CPG_SURFACE) END " +  
    		" FROM (SELECT * " + 
    		"	  FROM RP_DCROPPARCEL " + 
    		"	 		INNER JOIN RP_DCROPPARCELGEOM" + 
    		"	 		ON RP_DCROPPARCEL.RCP_ID = RP_DCROPPARCELGEOM.RCP_ID" + 
    		"	  WHERE " + 
    		"	  		RP_DCROPPARCEL.RCP_IRRIGATION = :irrigation ) AGR;",
           nativeQuery = true)
    double surfaceByIrrigation(@Param("irrigation") boolean irrigation);    
    
    
    
    /**
     * Next AT.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_AT')", nativeQuery = true)
    long nextAT();

    /**
     * Next BE.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_BE')", nativeQuery = true)
    long nextBE();

    /**
     * Next BG.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_BG')", nativeQuery = true)
    long nextBG();

    /**
     * Next CH.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_CH')", nativeQuery = true)
    long nextCH();

    /**
     * Next CY.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_CY')", nativeQuery = true)
    long nextCY();

    /**
     * Next CZ.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_CZ')", nativeQuery = true)
    long nextCZ();

    /**
     * Next DE.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_DE')", nativeQuery = true)
    long nextDE();

    /**
     * Next DK.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_DK')", nativeQuery = true)
    long nextDK();

    /**
     * Next EE.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_EE')", nativeQuery = true)
    long nextEE();

    /**
     * Next EL.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_EL')", nativeQuery = true)
    long nextEL();

    /**
     * Next ES.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_ES')", nativeQuery = true)
    long nextES();

    /**
     * Next FI.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_FI')", nativeQuery = true)
    long nextFI();

    /**
     * Next FR.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_FR')", nativeQuery = true)
    long nextFR();

    /**
     * Next HR.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_HR')", nativeQuery = true)
    long nextHR();

    /**
     * Next HU.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_HU')", nativeQuery = true)
    long nextHU();

    /**
     * Next IE.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_IE')", nativeQuery = true)
    long nextIE();

    /**
     * Next IL.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_IL')", nativeQuery = true)
    long nextIL();

    /**
     * Next IT.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_IT')", nativeQuery = true)
    long nextIT();

    /**
     * Next LT.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_LT')", nativeQuery = true)
    long nextLT();

    /**
     * Next LU.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_LU')", nativeQuery = true)
    long nextLU();

    /**
     * Next LV.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_LV')", nativeQuery = true)
    long nextLV();

    /**
     * Next MT.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_MT')", nativeQuery = true)
    long nextMT();

    /**
     * Next NL.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_NL')", nativeQuery = true)
    long nextNL();

    /**
     * Next NO.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_NO')", nativeQuery = true)
    long nextNO();

    /**
     * Next PL.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_PL')", nativeQuery = true)
    long nextPL();

    /**
     * Next PT.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_PT')", nativeQuery = true)
    long nextPT();

    /**
     * Next RO.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_RO')", nativeQuery = true)
    long nextRO();

    /**
     * Next SE.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_SE')", nativeQuery = true)
    long nextSE();

    /**
     * Next SI.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_SI')", nativeQuery = true)
    long nextSI();

    /**
     * Next SK.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_SK')", nativeQuery = true)
    long nextSK();

    /**
     * Next UK.
     *
     * @return the long
     */
    @Query(value = "SELECT NEXTVAL ('SEQ_PR_DFARMER_UK')", nativeQuery = true)
    long nextUK();

    /**
     * Asigna el code.
     *
     * @param <S>
     *            the generic type
     * @param farmer
     *            the farmer
     * @return the s
     */
    default <S extends PrFarmer> S setCode(final S farmer) {
        try {
            if ("AT".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextAT()));
            } else if ("BE".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextBE()));
            } else if ("BG".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextBG()));
            } else if ("CH".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextCH()));
            } else if ("CY".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextCY()));
            } else if ("CZ".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextCZ()));
            } else if ("DE".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextDE()));
            } else if ("DK".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextDK()));
            } else if ("EE".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextEE()));
            } else if ("EL".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextEL()));
            } else if ("ES".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextES()));
            } else if ("FI".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextFI()));
            } else if ("FR".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextFR()));
            } else if ("HR".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextHR()));
            } else if ("HU".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextHU()));
            } else if ("IE".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextIE()));
            } else if ("IL".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextIL()));
            } else if ("IT".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextIT()));
            } else if ("LT".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextLT()));
            } else if ("LU".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextLU()));
            } else if ("LV".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextLV()));
            } else if ("MT".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextMT()));
            } else if ("NL".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextNL()));
            } else if ("NO".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextNO()));
            } else if ("PL".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextPL()));
            } else if ("PT".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextPT()));
            } else if ("RO".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextRO()));
            } else if ("SE".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextSE()));
            } else if ("SI".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextSI()));
            } else if ("SK".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextSK()));
            } else if ("UK".equals((farmer.getIdCountry()))) {
                farmer.setCode(farmer.getIdCountry() + String.format("%012d", nextUK()));
            }

            return farmer;
        }

        catch (final Exception e) {
            return null;
        }
    }
}