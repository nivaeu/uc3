/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoActSus.
 *
 * @author irodri11
 * @version 1.0.0,  27-ago-2021
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PACTMATS")
@JsonPropertyOrder({"id", "phytosanitary", "country", "substance", "percentage", "quantity", "unit"})
public class CoActMats {
    
    /** The id. */
    @Id
    @Column(name = "ACM_ID")
    @ApiModelProperty(example = "1")
    @JsonView(JsonViews.Simple.class)
    private Integer id;

    /** The phytosanitary id. */
    @Column(name = "PHY_ID")
    @ApiModelProperty(example = "11179")
    @JsonView(JsonViews.Simple.class)
    private String phy_id;

    /** The country. */
    @Column(name = "COU_ID")
    @ApiModelProperty(example = "ES")
    @JsonView(JsonViews.Simple.class)
    private String cou_id;
    
    /** The substance id. */
    @Column(name = "ACS_ID")
    @ApiModelProperty(example = "1")
    @JsonView(JsonViews.Simple.class)
    private Integer acs_id;
    
    /** The percentage of active substance. */
    @Column(name = "ACM_PERCSACS")
    @ApiModelProperty(example = "80")
    @JsonView(JsonViews.Simple.class)
    private float acm_percsacs;
    
    /** The quantity of active substance. */
    @Column(name = "ACM_QUANACS")
    @ApiModelProperty(example = "10")
    @JsonView(JsonViews.Simple.class)
    private float acm_quanacs;
        
    
    /** The unit id. */
    @Column(name = "UNI_ID")
    @ApiModelProperty(example = "5")
    @JsonView(JsonViews.Simple.class)
    private Integer uni_id;
        
    
    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public Integer getId() {
        return id;
    }

	/**
     * Gets the phytosanitary id.
     *
     * @return the phytosanitary id
     */
    @JsonProperty(value = "phytosanitary")
    public String getPhy_id() {
        return phy_id;
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    @JsonProperty(value = "country")
    public String getIdCountry() {
		return cou_id;
	}

    /**
     * Gets the substance id.
     *
     * @return the substance id
     */
    @JsonProperty(value = "substance")
	public Integer getAcs_id() {
		return acs_id;
	}
	
    /**
     * Gets the percentage of substance.
     *
     * @return the percentage of substance
     */
    @JsonProperty(value = "percentage")
	public float getAcm_percsacs() {
		return acm_percsacs;
	}

    /**
     * Gets the quantity of substance.
     *
     * @return the quantity of substance
     */
    @JsonProperty(value = "quantity")
	public float getAcm_quanacs() {
		return acm_quanacs;
	}
	
    /**
     * Gets the unit id.
     *
     * @return the unit id
     */
    @JsonProperty(value = "unit")
	public Integer getUni_id() {
		return uni_id;
	}

	/**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Sets the phytosanitary id.
     *
     * @param name the new phytosanitary id
     */
	public void setPhy_id(String phy_id) {
		this.phy_id = phy_id;
	}
    
    /**
     * Sets the country id.
     *
     * @param name the new country id
     */
	public void setIdCountry(String cou_id) {
		this.cou_id = cou_id;
	}
	
    /**
     * Sets the substance id.
     *
     * @param name the new substance id
     */
	public void setAcs_id(Integer acs_id) {
		this.acs_id = acs_id;
	}
	
    /**
     * Sets the percentage of substance.
     *
     * @param name the new percentage of substance
     */
	public void setAcm_percsacs(float acm_percsacs) {
		this.acm_percsacs = acm_percsacs;
	}	
	
    /**
     * Sets the quantity of substance.
     *
     * @param name the new quantity of substance
     */
	public void setAcm_quanacs(float acm_quanacs) {
		this.acm_quanacs = acm_quanacs;
	}	
	
    /**
     * Sets the unit id.
     *
     * @param name the new unit id
     */
	public void setUni_id(Integer uni_id) {
		this.uni_id = uni_id;
	}
	
}