/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.util.List;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.niva4cap.farmregistry.beans.catalogs.CoActSus;
import eu.niva4cap.farmregistry.utils.JsonViews;

@JsonInclude(Include.NON_NULL)

public class ActSusMessage {
	
	@JsonView(JsonViews.FullDetailed.class)
	@NotEmpty(message = "ActSus is null or empty")
	private List<CoActSus> coActSus;


	public List<CoActSus> getCoActSus(){
		return coActSus;
	}
	
	@JsonProperty(value = "CoActSus")
	public void setCoActSus(List<CoActSus> coActSus) {
		this.coActSus = coActSus;
	
	}
}
