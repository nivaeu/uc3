/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories.catalogs;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety;

/**
 * The Interface ICoCropVariety.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface ICoCropVariety extends JpaRepository<CoCropVariety, String>
{
	
	/**
	 * Find by id and product and country.
	 *
	 * @param id the id
	 * @param product the product
	 * @param country the country
	 * @return the optional
	 */
	@Query(value="SELECT * FROM CO_PCROPVARIETY WHERE CRV_id = :id AND CRP_id = :product AND COU_id = :country", nativeQuery=true) Optional<CoCropVariety> findByIdAndProductAndCountry(@Param("id") String id, @Param("product") String product, @Param("country") String country);
}