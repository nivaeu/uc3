/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrFarmerContact;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class StructuredAddress.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "Country", "CityName", "StreetName", "BuildingNumber", "Postcode", "Additional", "Email1", "Email2", "Phone1", "Phone2" })
public class StructuredAddress
{
	
	/** The country. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "ES")
	private String country;
	
	/** The city name. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "Cuenca")
	private String cityName;
	
	/** The street name. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "LE FOINTI, 6")
	private String streetName;
	
	/** The building number. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "53")
	private String buildingNumber;
	
	/** The postcode. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "16611")
	private String postcode;
	
	/** The additional. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "")
	private String additional;
	
	/** The email 1. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "test@prueba.es")
	private String email1;
	
	/** The email 2. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "test2@prueba.es")
	private String email2;
	
	/** The phone 1. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "845830687")
	private String phone1;
	
	/** The phone 2. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "329485485")
	private String phone2;

	/**
	 * Parsea el farmer contact.
	 *
	 * @param farmerContact the farmer contact
	 */
	public void parseFarmerContact(PrFarmerContact farmerContact) {
		setCountry(farmerContact.getIdCountry());
		setCityName(farmerContact.getCity());
		setStreetName(farmerContact.getStreet());
		setBuildingNumber(farmerContact.getNumber());
		setPostcode(farmerContact.getPostcode());
		setAdditional(farmerContact.getAdditional());
		setEmail1(farmerContact.getEmail1());
		setEmail2(farmerContact.getEmail2());
		setPhone1(farmerContact.getPhone1());
		setPhone2(farmerContact.getPhone2());
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	@JsonProperty(value = "Country")
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the city name.
	 *
	 * @return the city name
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * Sets the city name.
	 *
	 * @param cityName the new city name
	 */
	@JsonProperty(value = "CityName")
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * Gets the street name.
	 *
	 * @return the street name
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the street name.
	 *
	 * @param streetName the new street name
	 */
	@JsonProperty(value = "StreetName")
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the building number.
	 *
	 * @return the building number
	 */
	public String getBuildingNumber() {
		return buildingNumber;
	}

	/**
	 * Sets the building number.
	 *
	 * @param buildingNumber the new building number
	 */
	@JsonProperty(value = "BuildingNumber")
	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	/**
	 * Gets the postcode.
	 *
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * Sets the postcode.
	 *
	 * @param postcode the new postcode
	 */
	@JsonProperty(value = "Postcode")
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * Gets the additional.
	 *
	 * @return the additional
	 */
	public String getAdditional() {
		return additional;
	}

	/**
	 * Sets the additional.
	 *
	 * @param additional the new additional
	 */
	@JsonProperty(value = "Additional")
	public void setAdditional(String additional) {
		this.additional = additional;
	}

	/**
	 * Gets the email 1.
	 *
	 * @return the email 1
	 */
	public String getEmail1() {
		return email1;
	}

	/**
	 * Sets the email 1.
	 *
	 * @param email1 the new email 1
	 */
	@JsonProperty(value = "Email1")
	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	/**
	 * Gets the email 2.
	 *
	 * @return the email 2
	 */
	public String getEmail2() {
		return email2;
	}

	/**
	 * Sets the email 2.
	 *
	 * @param email2 the new email 2
	 */
	@JsonProperty(value = "Email2")
	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	/**
	 * Gets the phone 1.
	 *
	 * @return the phone 1
	 */
	public String getPhone1() {
		return phone1;
	}

	/**
	 * Sets the phone 1.
	 *
	 * @param phone1 the new phone 1
	 */
	@JsonProperty(value = "Phone1")
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	/**
	 * Gets the phone 2.
	 *
	 * @return the phone 2
	 */
	public String getPhone2() {
		return phone2;
	}

	/**
	 * Sets the phone 2.
	 *
	 * @param phone2 the new phone 2
	 */
	@JsonProperty(value = "Phone2")
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
}