/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.catalogs.CoActMats;
import eu.niva4cap.farmregistry.beans.catalogs.CoActSus;
import eu.niva4cap.farmregistry.beans.catalogs.CoCountry;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProduct;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProductGroup;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety;
import eu.niva4cap.farmregistry.beans.catalogs.CoCultivationDetail;
import eu.niva4cap.farmregistry.beans.catalogs.CoCultivationSystem;
import eu.niva4cap.farmregistry.beans.catalogs.CoFarmerType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFarmingType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizationMethod;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizationType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizer;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizerType;
import eu.niva4cap.farmregistry.beans.catalogs.CoIrrigationType;
import eu.niva4cap.farmregistry.beans.catalogs.CoLaborType;
import eu.niva4cap.farmregistry.beans.catalogs.CoLandCover;
import eu.niva4cap.farmregistry.beans.catalogs.CoLandTenure;
import eu.niva4cap.farmregistry.beans.catalogs.CoOrganicFarming;
import eu.niva4cap.farmregistry.beans.catalogs.CoPhytosanitary;
import eu.niva4cap.farmregistry.beans.catalogs.CoPhytosanitaryEquipment;
import eu.niva4cap.farmregistry.beans.catalogs.CoSeedType;
import eu.niva4cap.farmregistry.beans.catalogs.CoUnitType;
import eu.niva4cap.farmregistry.beans.catalogs.CoWaterOrigin;
import eu.niva4cap.farmregistry.beans.catalogs.CoWorkunitType;
import eu.niva4cap.farmregistry.beans.ecrop.CropPlot;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportDocument;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.repositories.IAbAgriculturalBlock;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActMats;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActSus;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCountry;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProduct;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProductGroup;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropVariety;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationDetail;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationSystem;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmingType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationMethod;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizer;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoIrrigationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLaborType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandCover;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandTenure;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoOrganicFarming;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitary;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitaryEquipment;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoSeedType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoUnitType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWaterOrigin;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWorkunitType;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class QuerySurfaces. 
 *
 * @author irodri11
 * @version 1.0.0, 09-mar-2022
 * @since JDK 1.6
 */
@RestController
@RequestMapping(value = ConstantesRest.REST_QUERY_QUERY_SURFACES)
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@Api(tags = "Query Surfaces")
public class  QuerySurfaces {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CropPlot.class);


	/** The i PrFarmer. */
	@Autowired
	private IPrFarmer iPrFarmer;



	/*
	 * Total surface by farmer at agricultural block level
	 * @return the surface
	 */
	@GetMapping(value = "agrblock")
	@RequestMapping(value = "agrblock", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total AB surface",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceAB() {
		try {
			double result = iPrFarmer.surfaceAB();

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}

	/*
	 * Total surface by farmer at reference plot level
	 * @return the surface
	 */
	@GetMapping(value = "refplot")
	@RequestMapping(value = "refplot", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total RP surface",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceRP() {
		try {
			double result = iPrFarmer.surfaceRP();

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}

	/*
	 * Total surface by farmer at crop parcel level
	 * @return the surface
	 */
	@GetMapping(value = "cropparcel")
	@RequestMapping(value = "cropparcel", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total CP surface",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceCP() {
		try {
			double result = iPrFarmer.surfaceCP();

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}
	
	
	/*
	 * Total surface by farmer at agricultural block level
	 * @return the surface
	 */
	@GetMapping(value = "farmer/{farmer}/agrblock")
	@RequestMapping(value = "farmer/{farmer}/agrblock", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total AB surface by farmer",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceABByFarmer(
			@PathVariable("farmer") final String farmer) {
		try {
			double result = iPrFarmer.surfaceABByPersonalID(farmer);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}
	
	/*
	 * Total surface by farmer at reference plot level
	 * @return the surface
	 */
	@GetMapping(value = "farmer/{farmer}/refplot")
	@RequestMapping(value = "farmer/{farmer}/refplot", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total RP surface by farmer",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceRPByFarmer(
			@PathVariable("farmer") final String farmer) {
		try {
			double result = iPrFarmer.surfaceRPByPersonalID(farmer);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}	
	
	/*
	 * Total surface by farmer at crop plot level
	 * @return the surface
	 */
	@GetMapping(value = "farmer/{farmer}/cropparcel")
	@RequestMapping(value = "farmer/{farmer}/cropparcel", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total CP surface by farmer",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceFCByFarmer(
			@PathVariable("farmer") final String farmer) {
		try {
			double result = iPrFarmer.surfaceCPByPersonalID(farmer);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}	
	
	
	/*
	 * Total surface by farmer and land cover
	 * @return the surface
	 */
	@GetMapping(value = "farmer/{farmer}/irrigation/{irrigation}")
	@RequestMapping(value = "farmer/{farmer}/irrigation/{irrigation}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total AB surface by farmer and irrigation",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceABByFarmerIrrigation(
			@PathVariable("farmer") final String farmer,
			@PathVariable("irrigation") final int irrigation) {
		try {
			double result = 0;
			boolean p = false;
			if (irrigation == 1) p = true;
			result = iPrFarmer.surfaceCPByPersonalIDIrrigation(farmer, p);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}		
	
	/*
	 * Total surface by farmer and land cover
	 * @return the surface
	 */
	@GetMapping(value = "farmer/{farmer}/landCover/{landCover}")
	@RequestMapping(value = "farmer/{farmer}/landCover/{landCover}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total AB surface by farmer and land cover",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceABByFarmerLandCover(
			@PathVariable("farmer") final String farmer,
			@PathVariable("landCover") final int landCover) {
		try {
			double result = iPrFarmer.surfaceABByPersonalIDLandCover(farmer, landCover);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}	
	
	
	/*
	 * Total surface by land cover
	 * @return the surface
	 */
	@GetMapping(value = "landCover/{landCover}")
	@RequestMapping(value = "landCover/{landCover}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total AB surface by farmer and land cover",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceABByLandCover(
			@PathVariable("landCover") final int landCover) {
		try {
			double result = iPrFarmer.surfaceABByLandCover(landCover);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}	

	/*
	 * Total surface by irrigation (0,1)
	 * @return the surface
	 */
	@GetMapping(value = "irrigation/{irrigation}")
	@RequestMapping(value = "irrigation/{irrigation}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns total surface by irrigation",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public double getSurfaceByIrrigation(
			@PathVariable("irrigation") final int irrigation) {
		try {
			double result = 0;
			boolean p = false;
			if (irrigation == 1) p = true;
			result = iPrFarmer.surfaceByIrrigation(p);

			return result;
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return 0;
		}
	}	
	
}
