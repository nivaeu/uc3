/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.Workunit;
import eu.niva4cap.farmregistry.beans.pks.ProductionWorkunitPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrProductionWorkunit.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HPRODWORKUNIT")
@Table(name = "PR_DPRODWORKUNIT")
@JsonPropertyOrder({"id_workunit_type", "awu", "date_from", "date_to"})
public class PrProductionWorkunit {

    /** The production workunit PK. */
    @Valid
    protected @EmbeddedId ProductionWorkunitPK productionWorkunitPK = new ProductionWorkunitPK();

    /** The id production unit. */
    @Column(name = "PUN_id", insertable = false, updatable = false)
    private UUID idProductionUnit;

    /** The id workunit type. */
    @Column(name = "WUT_id", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Integer idWorkunitType;

    /** The date from. */
    @Column(name = "PUW_datefrom", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "PUW_dateto")
    @JsonView(JsonViews.Detailed.class)
    private Date dateTo;

    /** The awu. */
    @Column(name = "PUW_awu")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 6, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.Workunit.AWU is too long")
    private BigDecimal awu;

    /** The production unit. */
    @MapsId(value = "idProductionUnit")
    @ManyToOne
    @JoinColumn(name = "PUN_id")
    private PrProductionUnit productionUnit;

    /**
     * Gets the awu.
     *
     * @return the awu
     */
    public BigDecimal getAwu() {
        return awu;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return productionWorkunitPK.getDateFrom();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the id production unit.
     *
     * @return the id production unit
     */
    @JsonIgnore
    public UUID getIdProductionUnit() {
        return productionWorkunitPK.getIdProductionUnit();
    }

    /**
     * Gets the id workunit type.
     *
     * @return the id workunit type
     */
    public Integer getIdWorkunitType() {
        return productionWorkunitPK.getIdWorkunitType();
    }

    /**
     * Gets the production unit.
     *
     * @return the production unit
     */
    public PrProductionUnit getProductionUnit() {
        return productionUnit;
    }

    /**
     * Parsea el workunit.
     *
     * @param workunit
     *            the workunit
     */
    public void parseWorkunit(final Workunit workunit) {
        setIdWorkunitType(workunit.getType());
        setAwu(workunit.getAwu());
        setDateFrom(workunit.getDateFrom());
        setDateTo(workunit.getDateTo());
    }

    /**
     * Sets the awu.
     *
     * @param awu
     *            the new awu
     */
    @JsonProperty(value = "awu")
    public void setAwu(final BigDecimal awu) {
        this.awu = awu;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        productionWorkunitPK.setDateFrom(dateFrom);
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the id production unit.
     *
     * @param idProductionUnit
     *            the new id production unit
     */
    public void setIdProductionUnit(final UUID idProductionUnit) {
        productionWorkunitPK.setIdProductionUnit(idProductionUnit);
    }

    /**
     * Sets the id workunit type.
     *
     * @param idWorkunitType
     *            the new id workunit type
     */
    @JsonProperty(value = "id_workunit_type")
    public void setIdWorkunitType(final Integer idWorkunitType) {
        productionWorkunitPK.setIdWorkunitType(idWorkunitType);
    }

    /**
     * Sets the production unit.
     *
     * @param productionUnit
     *            the new production unit
     */
    public void setProductionUnit(final PrProductionUnit productionUnit) {
        this.productionUnit = productionUnit;
    }
}