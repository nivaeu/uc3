/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class CropReportDocument.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "ECR_DREPORTDOC")
@JsonInclude(Include.NON_NULL)
public class CropReportDocument {

    /** The id. */
    @Id
    @Column(name = "ECR_id")
    @GeneratedValue(generator = "sequence")
    @SequenceGenerator(name = "sequence", sequenceName = "SEQ_ECR_DREPORTDOC", allocationSize = 1)
    @JsonView(JsonViews.FullDetailed.class)
    private Integer id;

    /** The report count. */
    @Column(name = "ECR_reportcount")
    @JsonView(JsonViews.Detailed.class)
    private Integer reportCount;

    /** The description. */
    @Column(name = "ECR_description")
    @JsonView(JsonViews.Simple.class)
    private String description;

    /** The issue. */
    @Column(name = "ECR_issue")
    @JsonView(JsonViews.Simple.class)
    @UpdateTimestamp
    private Timestamp issue;

    /** The type. */
    @Column(name = "ECR_type")
    @JsonView(JsonViews.Detailed.class)
    private Integer type;

    /** The copy. */
    @Column(name = "ECR_copy")
    @JsonView(JsonViews.FullDetailed.class)
    private Boolean copy;

    /** The control requirement. */
    @Column(name = "ECR_ctrlrequirement")
    @JsonView(JsonViews.FullDetailed.class)
    private Boolean controlRequirement;

    /** The purpose. */
    @Column(name = "ECR_purpose")
    @JsonView(JsonViews.Detailed.class)
    private Integer purpose;

    /** The line count. */
    @Column(name = "ECR_linecount")
    @JsonView(JsonViews.Detailed.class)
    private Integer lineCount;

    /** The information. */
    @Column(name = "ECR_information")
    @JsonView(JsonViews.Simple.class)
    private String information;

    /** The status. */
    @Column(name = "ECR_status")
    @JsonView(JsonViews.Detailed.class)
    private Integer status;

    /** The sequence. */
    @Column(name = "ECR_sequence")
    @JsonView(JsonViews.Detailed.class)
    private Integer sequence;

    /**
     * Gets the control requirement.
     *
     * @return the control requirement
     */
    public Boolean getControlRequirement() {
        return controlRequirement;
    }

    /**
     * Gets the copy.
     *
     * @return the copy
     */
    public Boolean getCopy() {
        return copy;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the information.
     *
     * @return the information
     */
    public String getInformation() {
        return information;
    }

    /**
     * Gets the issue.
     *
     * @return the issue
     */
    public Timestamp getIssue() {
        return issue;
    }

    /**
     * Gets the line count.
     *
     * @return the line count
     */
    public Integer getLineCount() {
        return lineCount;
    }

    /**
     * Gets the purpose.
     *
     * @return the purpose
     */
    public Integer getPurpose() {
        return purpose;
    }

    /**
     * Gets the report count.
     *
     * @return the report count
     */
    public Integer getReportCount() {
        return reportCount;
    }

    /**
     * Gets the sequence.
     *
     * @return the sequence
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * Sets the control requirement.
     *
     * @param controlRequirement
     *            the new control requirement
     */
    @JsonProperty(value = "ControlRequirement")
    public void setControlRequirement(final Boolean controlRequirement) {
        this.controlRequirement = controlRequirement;
    }

    /**
     * Sets the copy.
     *
     * @param copy
     *            the new copy
     */
    @JsonProperty(value = "Copy")
    public void setCopy(final Boolean copy) {
        this.copy = copy;
    }

    /**
     * Sets the description.
     *
     * @param description
     *            the new description
     */
    @JsonProperty(value = "Description")
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    @JsonProperty(value = "ID")
    @JsonIgnore
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Sets the information.
     *
     * @param information
     *            the new information
     */
    @JsonProperty(value = "Information")
    public void setInformation(final String information) {
        this.information = information;
    }

    /**
     * Sets the issue.
     *
     * @param issue
     *            the new issue
     */
    @JsonProperty(value = "Issue")
    public void setIssue(final Timestamp issue) {
        this.issue = issue;
    }

    /**
     * Sets the line count.
     *
     * @param lineCount
     *            the new line count
     */
    @JsonProperty(value = "LineCount")
    public void setLineCount(final Integer lineCount) {
        this.lineCount = lineCount;
    }

    /**
     * Sets the purpose.
     *
     * @param purpose
     *            the new purpose
     */
    @JsonProperty(value = "Purpose")
    public void setPurpose(final Integer purpose) {
        this.purpose = purpose;
    }

    /**
     * Sets the report count.
     *
     * @param reportCount
     *            the new report count
     */
    @JsonProperty(value = "ReportCount")
    public void setReportCount(final Integer reportCount) {
        this.reportCount = reportCount;
    }

    /**
     * Sets the sequence.
     *
     * @param sequence
     *            the new sequence
     */
    @JsonProperty(value = "Sequence")
    public void setSequence(final Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the new status
     */
    @JsonProperty(value = "Status")
    public void setStatus(final Integer status) {
        this.status = status;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    @JsonProperty(value = "Type")
    public void setType(final Integer type) {
        this.type = type;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    @NotEmpty(message = "CropReportDocument is null or empty")
    public String toString() {
        return super.toString();
    }
}