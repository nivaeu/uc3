/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.pks.ReferencePlotGeometryPK;
import eu.niva4cap.farmregistry.utils.Coordinates;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpGeometry.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HGEOMETRY")
@Table(name = "RP_DGEOMETRY")
@JsonPropertyOrder({"feature", "surface", "date_from", "date_to"})
public class RpGeometry {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RpGeometry.class);

    /** The reference plot geometry PK. */
    @Valid
    @EmbeddedId
    protected ReferencePlotGeometryPK referencePlotGeometryPK = new ReferencePlotGeometryPK();

    /** The id reference plot. */
    @Column(name = "RFP_ID", insertable = false, updatable = false)
    private UUID idReferencePlot;

    /** The geometry. */
    @Column(name = "RGE_GEOMETRY")
    @JsonView(JsonViews.Detailed.class)
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.Geometry.Coordinates is null")
    private Geometry geometry;

    /** The surface. */
    @Column(name = "RGE_SURFACE")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 10, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.Geometry.Surface is too long")
    private BigDecimal surface;

    /** The date from. */
    @Column(name = "RGE_DATEFROM", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "RGE_DATETO")
    @JsonView(JsonViews.Detailed.class)
    private Date dateTo;

    /** The reference plot. */
    @MapsId(value = "idReferencePlot")
    @ManyToOne
    @JoinColumn(name = "RFP_ID")
    private RpReferencePlot referencePlot;

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return referencePlotGeometryPK.getDateFrom();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the geometry.
     *
     * @return the geometry
     */
    public String getGeometry() {
        return geometry.toString();
    }

    /**
     * Gets the id reference plot.
     *
     * @return the id reference plot
     */
    @JsonIgnore
    public UUID getIdReferencePlot() {
        return referencePlotGeometryPK.getIdReferencePlot();
    }

    /**
     * Gets the reference plot.
     *
     * @return the reference plot
     */
    @JsonIgnore
    public RpReferencePlot getReferencePlot() {
        return referencePlot;
    }

    /**
     * Gets the surface.
     *
     * @return the surface
     */
    public BigDecimal getSurface() {
        return surface;
    }

    /**
     * Parsea el geometry.
     *
     * @param geometry the geometry
     */
    public void parseGeometry(final eu.niva4cap.farmregistry.beans.ecrop.Geometry geometry) {
        setGeometry(geometry.getCoordinates());
        setSurface(geometry.getSurface());
        setDateFrom(geometry.getDateFrom());
        setDateTo(geometry.getDateTo());
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        referencePlotGeometryPK.setDateFrom(dateFrom);
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometry.
     *
     * @param geometry the new geometry
     */
    @JsonProperty(value = "feature")
    public void setGeometry(final String[][][] geometry) {
        try {
            this.geometry = new WKTReader().read(Coordinates.geoJSON2WKT(geometry));
            this.geometry.setSRID(4326);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Sets the id reference plot.
     *
     * @param idReferencePlot the new id reference plot
     */
    public void setIdReferencePlot(final UUID idReferencePlot) {
        referencePlotGeometryPK.setIdReferencePlot(idReferencePlot);
    }

    /**
     * Sets the reference plot.
     *
     * @param referencePlot the new reference plot
     */
    public void setReferencePlot(final RpReferencePlot referencePlot) {
        this.referencePlot = referencePlot;
    }

    /**
     * Sets the surface.
     *
     * @param surface the new surface
     */
    @JsonProperty(value = "surface")
    public void setSurface(final BigDecimal surface) {
        this.surface = surface;
    }
}