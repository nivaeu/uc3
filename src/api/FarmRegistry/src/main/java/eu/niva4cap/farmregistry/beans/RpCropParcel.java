/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.ActivityDetail;
import eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer;
import eu.niva4cap.farmregistry.beans.ecrop.CropIrrigation;
import eu.niva4cap.farmregistry.beans.ecrop.CropPhytosanitary;
import eu.niva4cap.farmregistry.beans.ecrop.FieldCrop;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropParcel.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPPARCEL")
@Table(name = "RP_DCROPPARCEL")
@JsonPropertyOrder({"key", "irrigation", "id_crop_product", "id_crop_variety", "landscape_features",
        "id_land_tenure", "date_from", "date_to", "geometry", "crop_activity",
        "crop_activity_detail", "crop_irrigation", "crop_phytosanitary", "crop_fertilizer"})
public class RpCropParcel {

    /** The id. */
    @Id
    @Column(name = "RCP_ID")
    @GeneratedValue(generator = "UUID")
    private UUID id;

    /** The key. */
    @Column(name = "RCP_KEY")
    @JsonView(JsonViews.Simple.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Key is null or empty")
    @Size(max = 50,
          message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Key is too long")
    private String key;

    /** The id reference plot. */
    @Column(name = "RFP_ID", insertable = false, updatable = false)
    private UUID idReferencePlot;

    /** The irrigation. */
    @Column(name = "RCP_IRRIGATION")
    @JsonView(JsonViews.FullDetailed.class)
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Irrigation is null")
    private Boolean irrigation;

    /** The id crop product. */
    @Column(name = "CRP_ID")
    @JsonView(JsonViews.FullDetailed.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropProduct is null or empty")
    private String idCropProduct;

    /** The id crop variety. */
    @Column(name = "CRV_ID")
    @JsonView(JsonViews.FullDetailed.class)
    private String idCropVariety;

    /** The landscape features. */
    @Column(name = "RCP_landscfeatures")
    @JsonView(JsonViews.FullDetailed.class)
    private Boolean landscapeFeatures;

    /** The id land tenure. */
    @Column(name = "LTE_ID")
    @JsonView(JsonViews.FullDetailed.class)
    private Integer idLandTenure;

    /** The date from. */
    @Column(name = "RCP_DATEFROM")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "RCP_DATETO")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateTo;

    /** The reference plot. */
    @ManyToOne
    @JoinColumn(name = "RFP_ID")
    private RpReferencePlot referencePlot;

    /** The geometries. */
    @OneToMany(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "geometry")
    private List<RpCropParcelGeometry> geometries = new ArrayList<>();

    /** The crop activity. */
    @OneToOne(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_activity")
    private RpCropActivity cropActivity;

    /** The crop activity details. */
    @OneToMany(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_activity_detail")
    private List<RpCropActivityDetail> cropActivityDetails = new ArrayList<>();

    /** The crop irrigations. */
    @OneToMany(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_irrigation")
    private List<RpCropIrrigation> cropIrrigations = new ArrayList<>();

    /** The crop phytosanitaries. */
    @OneToMany(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_phytosanitary")
    private List<RpCropPhytosanitary> cropPhytosanitaries = new ArrayList<>();

    /** The crop fertilizers. */
    @OneToMany(mappedBy = "cropParcel", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_fertilizer")
    private List<RpCropFertilizer> cropFertilizers = new ArrayList<>();

    /**
     * Gets the activity detail.
     *
     * @param type
     *            the type
     * @param date
     *            the date
     * @return the activity detail
     */
    public RpCropActivityDetail getActivityDetail(final Integer type, final Date date) {
        for (final RpCropActivityDetail activityDetail : getCropActivityDetails()) {
            if (activityDetail.getIdLaborType() == type
                    && activityDetail.getStartDate().toString().equals(date.toString())) {
                return activityDetail;
            }
        }

        return null;
    }

    /**
     * Gets the crop activity.
     *
     * @return the crop activity
     */
    public RpCropActivity getCropActivity() {
        return cropActivity;
    }

    /**
     * Gets the crop activity details.
     *
     * @return the crop activity details
     */
    public List<RpCropActivityDetail> getCropActivityDetails() {
        return cropActivityDetails;
    }

    /**
     * Gets the crop fertilizers.
     *
     * @return the crop fertilizers
     */
    public List<RpCropFertilizer> getCropFertilizers() {
        return cropFertilizers;
    }

    /**
     * Gets the crop irrigations.
     *
     * @return the crop irrigations
     */
    public List<RpCropIrrigation> getCropIrrigations() {
        return cropIrrigations;
    }

    /**
     * Gets the crop phytosanitaries.
     *
     * @return the crop phytosanitaries
     */
    public List<RpCropPhytosanitary> getCropPhytosanitaries() {
        return cropPhytosanitaries;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the fertilizer.
     *
     * @param id
     *            the id
     * @param date
     *            the date
     * @return the fertilizer
     */
    public RpCropFertilizer getFertilizer(final Integer id, final Date date) {
        for (final RpCropFertilizer fertilizer : getCropFertilizers()) {
            if (fertilizer.getIdFertilizer() == id
                    && fertilizer.getStartDate().toString().equals(date.toString())) {
                return fertilizer;
            }
        }

        return null;
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<RpCropParcelGeometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the geometry.
     *
     * @param date
     *            the date
     * @return the geometry
     */
    public RpCropParcelGeometry getGeometry(final Date date) {
        for (final RpCropParcelGeometry geometry : getGeometries()) {
            if (geometry.getDateFrom().toString().equals(date.toString())) {
                return geometry;
            }
        }

        return null;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonIgnore
    public UUID getId() {
        return id;
    }

    /**
     * Gets the id crop product.
     *
     * @return the id crop product
     */
    public String getIdCropProduct() {
        return idCropProduct;
    }

    /**
     * Gets the id crop variety.
     *
     * @return the id crop variety
     */
    public String getIdCropVariety() {
        return idCropVariety;
    }

    /**
     * Gets the id land tenure.
     *
     * @return the id land tenure
     */
    public Integer getIdLandTenure() {
        return idLandTenure;
    }

    /**
     * Gets the id reference plot.
     *
     * @return the id reference plot
     */
    @JsonIgnore
    public UUID getIdReferencePlot() {
        return referencePlot.getId();
    }

    /**
     * Gets the irrigation.
     *
     * @return the irrigation
     */
    public Boolean getIrrigation() {
        return irrigation;
    }

    /**
     * Gets the irrigation.
     *
     * @param type
     *            the type
     * @param date
     *            the date
     * @return the irrigation
     */
    public RpCropIrrigation getIrrigation(final Integer type, final Date date) {
        for (final RpCropIrrigation irr : getCropIrrigations()) {
            if (irr.getIdIrrigationType() == type
                    && irr.getIrrigationDate().toString().equals(date.toString())) {
                return irr;
            }
        }

        return null;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the landscape features.
     *
     * @return the landscape features
     */
    public Boolean getLandscapeFeatures() {
        return landscapeFeatures;
    }

    /**
     * Gets the phytosanitary.
     *
     * @param id
     *            the id
     * @param date
     *            the date
     * @param country
     *            the country
     * @return the phytosanitary
     */
    public RpCropPhytosanitary getPhytosanitary(final String id, final Date date,
            final String country) {
        for (final RpCropPhytosanitary phytosanitary : getCropPhytosanitaries()) {
            if (phytosanitary.getIdPhytosanitary().equals(id)
                    && phytosanitary.getStartDate().toString().equals(date.toString())
                    && phytosanitary.getIdCountry().equals(country)) {
                return phytosanitary;
            }
        }

        return null;
    }

    /**
     * Gets the reference plot.
     *
     * @return the reference plot
     */
    @JsonIgnore
    public RpReferencePlot getReferencePlot() {
        return referencePlot;
    }

    /**
     * Parsea el field crop.
     *
     * @param fieldCrop
     *            the field crop
     */
    public void parseFieldCrop(final FieldCrop fieldCrop) {
        setKey(fieldCrop.getKey());

        if (fieldCrop.getIrrigation() != null) {
            setIrrigation(fieldCrop.getIrrigation());
        }

        if (fieldCrop.getCropProduct() != null) {
            setIdCropProduct(fieldCrop.getCropProduct());
        }

        if (fieldCrop.getCropVariety() != null) {
            setIdCropVariety(fieldCrop.getCropVariety());
        }

        if (fieldCrop.getLandscapeFeatures() != null) {
            setLandscapeFeatures(fieldCrop.getLandscapeFeatures());
        }

        if (fieldCrop.getLandTenure() != null) {
            setIdLandTenure(fieldCrop.getLandTenure());
        }

        if (fieldCrop.getDateFrom() != null) {
            setDateFrom(fieldCrop.getDateFrom());
        }

        if (fieldCrop.getDateTo() != null) {
            setDateTo(fieldCrop.getDateTo());
        }

        if (fieldCrop.getGeometries() != null) {
            for (final eu.niva4cap.farmregistry.beans.ecrop.Geometry geometry : fieldCrop
                    .getGeometries()) {
                RpCropParcelGeometry rpCropParcelGeometry = new RpCropParcelGeometry();

                final RpCropParcelGeometry wanted = getGeometry(geometry.getDateFrom());

                if (wanted != null) {
                    rpCropParcelGeometry = wanted;
                }

                else {
                    getGeometries().add(rpCropParcelGeometry);

                    rpCropParcelGeometry.setCropParcel(this);
                }

                rpCropParcelGeometry.parseGeometry(geometry);
            }
        }

        if (fieldCrop.getActivity() != null) {
            if (getCropActivity() == null) {
                setCropActivity(new RpCropActivity());
            }

            final RpCropActivity rpCropActivity = getCropActivity();
            rpCropActivity.setCropParcel(this);

            rpCropActivity.parseActivity(fieldCrop.getActivity());
        }

        if (fieldCrop.getActivityDetails() != null) {
            for (final ActivityDetail activityDetail : fieldCrop.getActivityDetails()) {
                RpCropActivityDetail rpCropActivityDetail = new RpCropActivityDetail();

                final RpCropActivityDetail wanted = getActivityDetail(activityDetail.getLaborType(),
                        activityDetail.getStartDate());

                if (wanted != null) {
                    rpCropActivityDetail = wanted;
                }

                else {
                    getCropActivityDetails().add(rpCropActivityDetail);

                    rpCropActivityDetail.setCropParcel(this);
                }

                rpCropActivityDetail.parseActivityDetail(activityDetail);
            }
        }

        if (fieldCrop.getCropIrrigations() != null) {
            for (final CropIrrigation cropIrrigation : fieldCrop.getCropIrrigations()) {
                RpCropIrrigation rpCropIrrigation = new RpCropIrrigation();

                final RpCropIrrigation wanted = getIrrigation(cropIrrigation.getType(),
                        cropIrrigation.getDate());

                if (wanted != null) {
                    rpCropIrrigation = wanted;
                }

                else {
                    getCropIrrigations().add(rpCropIrrigation);

                    rpCropIrrigation.setCropParcel(this);
                }

                rpCropIrrigation.parseCropIrrigation(cropIrrigation);
            }
        }

        if (fieldCrop.getCropPhytosanitaries() != null) {
            for (final CropPhytosanitary cropPhytosanitary : fieldCrop.getCropPhytosanitaries()) {
                RpCropPhytosanitary rpCropPhytosanitary = new RpCropPhytosanitary();

                final RpCropPhytosanitary wanted = getPhytosanitary(cropPhytosanitary.getId(),
                        cropPhytosanitary.getStartDate(), cropPhytosanitary.getCountry());

                if (wanted != null) {
                    rpCropPhytosanitary = wanted;
                }

                else {
                    getCropPhytosanitaries().add(rpCropPhytosanitary);

                    rpCropPhytosanitary.setCropParcel(this);
                }

                rpCropPhytosanitary.parsePhytosanitary(cropPhytosanitary);
            }
        }

        if (fieldCrop.getCropFertilizers() != null) {
            for (final CropFertilizer cropFertilizer : fieldCrop.getCropFertilizers()) {
                RpCropFertilizer rpCropFertilizer = new RpCropFertilizer();

                final RpCropFertilizer wanted = getFertilizer(cropFertilizer.getId(),
                        cropFertilizer.getStartDate());

                if (wanted != null) {
                    rpCropFertilizer = wanted;
                }

                else {
                    getCropFertilizers().add(rpCropFertilizer);

                    rpCropFertilizer.setCropParcel(this);
                }

                rpCropFertilizer.parseFertilizer(cropFertilizer);
            }
        }
    }

    /**
     * Sets the crop activity.
     *
     * @param cropActivity
     *            the new crop activity
     */
    public void setCropActivity(final RpCropActivity cropActivity) {
        this.cropActivity = cropActivity;
    }

    /**
     * Sets the crop activity details.
     *
     * @param cropActivityDetails
     *            the new crop activity details
     */
    public void setCropActivityDetails(final List<RpCropActivityDetail> cropActivityDetails) {
        this.cropActivityDetails = cropActivityDetails;
    }

    /**
     * Sets the crop fertilizers.
     *
     * @param cropFertilizers
     *            the new crop fertilizers
     */
    public void setCropFertilizers(final List<RpCropFertilizer> cropFertilizers) {
        this.cropFertilizers = cropFertilizers;
    }

    /**
     * Sets the crop irrigations.
     *
     * @param cropIrrigations
     *            the new crop irrigations
     */
    public void setCropIrrigations(final List<RpCropIrrigation> cropIrrigations) {
        this.cropIrrigations = cropIrrigations;
    }

    /**
     * Sets the crop phytosanitaries.
     *
     * @param cropPhytosanitaries
     *            the new crop phytosanitaries
     */
    public void setCropPhytosanitaries(final List<RpCropPhytosanitary> cropPhytosanitaries) {
        this.cropPhytosanitaries = cropPhytosanitaries;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometries.
     *
     * @param geometries
     *            the new geometries
     */
    public void setGeometries(final List<RpCropParcelGeometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Sets the id crop product.
     *
     * @param idCropProduct
     *            the new id crop product
     */
    @JsonProperty(value = "id_crop_product")
    public void setIdCropProduct(final String idCropProduct) {
        this.idCropProduct = idCropProduct;
    }

    /**
     * Sets the id crop variety.
     *
     * @param idCropVariety
     *            the new id crop variety
     */
    @JsonProperty(value = "id_crop_variety")
    public void setIdCropVariety(final String idCropVariety) {
        this.idCropVariety = idCropVariety;
    }

    /**
     * Sets the id land tenure.
     *
     * @param idLandTenure
     *            the new id land tenure
     */
    @JsonProperty(value = "id_land_tenure")
    public void setIdLandTenure(final Integer idLandTenure) {
        this.idLandTenure = idLandTenure;
    }

    /**
     * Sets the id reference plot.
     *
     * @param idReferencePlot
     *            the new id reference plot
     */
    public void setIdReferencePlot(final UUID idReferencePlot) {
        referencePlot.setId(idReferencePlot);
    }

    /**
     * Sets the irrigation.
     *
     * @param irrigation
     *            the new irrigation
     */
    @JsonProperty(value = "irrigation")
    public void setIrrigation(final Boolean irrigation) {
        this.irrigation = irrigation;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the new key
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the landscape features.
     *
     * @param landscapeFeatures
     *            the new landscape features
     */
    @JsonProperty(value = "landscape_features")
    public void setLandscapeFeatures(final Boolean landscapeFeatures) {
        this.landscapeFeatures = landscapeFeatures;
    }

    /**
     * Sets the reference plot.
     *
     * @param referencePlot
     *            the new reference plot
     */
    public void setReferencePlot(final RpReferencePlot referencePlot) {
        this.referencePlot = referencePlot;
    }
}