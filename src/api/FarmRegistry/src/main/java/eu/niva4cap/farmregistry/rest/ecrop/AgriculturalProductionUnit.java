/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.repositories.IPrProductionUnit;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class AgriculturalProductionUnit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping(value= ConstantesRest.REST_QUERY_AGRICULTURAL_PRODUCTION_UNIT)
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
@Api(tags = "Production Units")
public class AgriculturalProductionUnit {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AgriculturalProductionUnit.class);

    /** The i production unit. */
    @Autowired
    private IPrProductionUnit iProductionUnit;

    /**
     * Delete.
     *
     * @param code the code
     * @return the response entity
     */
    @DeleteMapping(value = "{code}")
	@RequestMapping(value = "{code}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Deletes an Agricultural Production Unit by its code",  authorizations = { @Authorization(value="BearerToken")})
    @PreAuthorize("hasRole('WRITE')")
    public ResponseEntity<Response> delete(@PathVariable("code") final String code) {
        final Response response = new Response();

        try {
            final Optional<PrProductionUnit> productionUnit = iProductionUnit.findByCode(code);

            if (!productionUnit.isPresent()) {
                response.addError("Agricultural Production Unit not found: " + code);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
                iProductionUnit.delete(productionUnit.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Obtiene el.
     *
     * @param params the params
     * @return the response entity
     */
    @GetMapping
	@RequestMapping(method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Agricultural Production Units", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.Detailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit>>
            getCodeList(@RequestParam final Map<String, String> params) {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit> agriculturalProductionUnits = new ArrayList<>();

            final PrProductionUnit productionUnit = new PrProductionUnit();
            productionUnit.setFarmer(new PrFarmer());

            if (params.get("Country") != null) {
                productionUnit.setIdCountry(params.get("Country"));
            }

            final List<PrProductionUnit> prProductionUnits = iProductionUnit
                    .findAll(Example.of(productionUnit));

            for (final PrProductionUnit prPoductionUnit : prProductionUnits) {
                final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit agriculturalProductionUnit = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit();
                agriculturalProductionUnits.add(agriculturalProductionUnit);

                agriculturalProductionUnit.parseProductionUnitCodeList(prPoductionUnit);
            }

            return ResponseEntity.ok(agriculturalProductionUnits);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    /**
     * Obtiene el.
     *
     * @param params the params
     * @return the response entity
     */
    @GetMapping(value = "details")
	@RequestMapping(value = "details", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Agricultural Production Units", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit>>
            get(@RequestParam final Map<String, String> params) {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit> agriculturalProductionUnits = new ArrayList<>();

            final PrProductionUnit productionUnit = new PrProductionUnit();
            productionUnit.setFarmer(new PrFarmer());

            if (params.get("Country") != null) {
                productionUnit.setIdCountry(params.get("Country"));
            }

            final List<PrProductionUnit> prProductionUnits = iProductionUnit
                    .findAll(Example.of(productionUnit));

            for (final PrProductionUnit prPoductionUnit : prProductionUnits) {
                final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit agriculturalProductionUnit = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit();
                agriculturalProductionUnits.add(agriculturalProductionUnit);

                agriculturalProductionUnit.parseProductionUnit(prPoductionUnit);
            }

            return ResponseEntity.ok(agriculturalProductionUnits);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }    
    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     */
    @GetMapping(value = "code/{code}")
	@RequestMapping(value = "code/{code}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Agricultural Production Unit by its code", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit>
            getByCode(@PathVariable("code") final String code) {
        try {
            final Optional<PrProductionUnit> productionUnit = iProductionUnit.findByCode(code);

            if (!productionUnit.isPresent()) {
                return ResponseEntity.ok(null);
            }

            final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit agriculturalProductionUnit = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit();

            agriculturalProductionUnit.parseProductionUnit(productionUnit.get());

            return ResponseEntity.ok(agriculturalProductionUnit);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    
    
    
	/**
	 * Gets the by country and personalID.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @return the by country and personalID
	 */
	@GetMapping(value="country/{country}/personalID/{personalID}")
	@ApiOperation(value = "Returns all Production Units by country code and personalID", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrProductionUnit>> getByCountryAndPersonalID(@PathVariable("country") String country, @PathVariable("personalID") String personalID)
	{
		try
		{
			Optional<List<PrProductionUnit>> beanDB = iProductionUnit.findByCountryAndPersonalID(country, personalID);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	/**
	 * Gets the by country, personalID and interval.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @param datefrom the date from
	 * @param dateto the date to
	 * @return the by country and personalID
	 */
	@GetMapping(value="country/{country}/personalID/{personalID}/dateFrom/{dateFrom}/dateTo/{dateTo}")
	@ApiOperation(value = "Returns all Production Units by country code, personalID, dateFrom and dateTo", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrProductionUnit>> getByCountryAndPersonalIDAndInterval(@PathVariable("country") String country, @PathVariable("personalID") String personalID, @PathVariable("dateFrom") Date dateFrom, @PathVariable("dateto") Date dateTo)
	{
		try
		{
			Optional<List<PrProductionUnit>> beanDB = iProductionUnit.findByCountryAndPersonalIDAndInterval(country, personalID, dateFrom, dateTo);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}	   	
    
}