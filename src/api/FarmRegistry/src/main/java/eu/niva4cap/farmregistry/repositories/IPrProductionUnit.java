/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.PrProductionUnit;

/**
 * The Interface IPrProductionUnit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IPrProductionUnit extends JpaRepository<PrProductionUnit, UUID>
{
	
	/**
	 * Next AT.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_AT')", nativeQuery=true) long nextAT();
	
	/**
	 * Next BE.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_BE')", nativeQuery=true) long nextBE();
	
	/**
	 * Next BG.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_BG')", nativeQuery=true) long nextBG();
	
	/**
	 * Next CH.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_CH')", nativeQuery=true) long nextCH();
	
	/**
	 * Next CY.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_CY')", nativeQuery=true) long nextCY();
	
	/**
	 * Next CZ.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_CZ')", nativeQuery=true) long nextCZ();
	
	/**
	 * Next DE.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_DE')", nativeQuery=true) long nextDE();
	
	/**
	 * Next DK.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_DK')", nativeQuery=true) long nextDK();
	
	/**
	 * Next EE.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_EE')", nativeQuery=true) long nextEE();
	
	/**
	 * Next EL.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_EL')", nativeQuery=true) long nextEL();
	
	/**
	 * Next ES.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_ES')", nativeQuery=true) long nextES();
	
	/**
	 * Next FI.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_FI')", nativeQuery=true) long nextFI();
	
	/**
	 * Next FR.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_FR')", nativeQuery=true) long nextFR();
	
	/**
	 * Next HR.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_HR')", nativeQuery=true) long nextHR();
	
	/**
	 * Next HU.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_HU')", nativeQuery=true) long nextHU();
	
	/**
	 * Next IE.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_IE')", nativeQuery=true) long nextIE();
	
	/**
	 * Next IL.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_IL')", nativeQuery=true) long nextIL();
	
	/**
	 * Next IT.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_IT')", nativeQuery=true) long nextIT();
	
	/**
	 * Next LT.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_LT')", nativeQuery=true) long nextLT();
	
	/**
	 * Next LU.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_LU')", nativeQuery=true) long nextLU();
	
	/**
	 * Next LV.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_LV')", nativeQuery=true) long nextLV();
	
	/**
	 * Next MT.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_MT')", nativeQuery=true) long nextMT();
	
	/**
	 * Next NL.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_NL')", nativeQuery=true) long nextNL();
	
	/**
	 * Next NO.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_NO')", nativeQuery=true) long nextNO();
	
	/**
	 * Next PL.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_PL')", nativeQuery=true) long nextPL();
	
	/**
	 * Next PT.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_PT')", nativeQuery=true) long nextPT();
	
	/**
	 * Next RO.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_RO')", nativeQuery=true) long nextRO();
	
	/**
	 * Next SE.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_SE')", nativeQuery=true) long nextSE();
	
	/**
	 * Next SI.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_SI')", nativeQuery=true) long nextSI();
	
	/**
	 * Next SK.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_SK')", nativeQuery=true) long nextSK();
	
	/**
	 * Next UK.
	 *
	 * @return the long
	 */
	@Query(value="SELECT NEXTVAL ('SEQ_PR_DPRODUCTIONUNIT_UK')", nativeQuery=true) long nextUK();

	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the optional
	 */
	@Query(value="SELECT * FROM PR_DPRODUCTIONUNIT WHERE PUN_code = :code", nativeQuery=true) Optional<PrProductionUnit> findByCode(@Param("code") String code);
	
	/**
	 * Find by country.
	 *
	 * @param country the country
	 * @return the optional
	 */
	@Query(value="SELECT * FROM PR_DPRODUCTIONUNIT WHERE COU_id = :country", nativeQuery=true) Optional<List<PrProductionUnit>> findByCountry(@Param("country") String country);
	
	/**
	 * Find by country and personalID.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @return the optional
	 */
	@Query(value="SELECT pu.* \r\n" + 
			"FROM PR_DPRODUCTIONUNIT pu INNER JOIN PR_DFARMER fa ON pu.far_id = fa.far_id\r\n" + 
			"	INNER JOIN PR_DFARMERDATA fd ON fa.far_id = fd.far_id\r\n" + 
			"WHERE pu.COU_id = :country AND fd.fad_personalid = :personalID", nativeQuery=true) Optional<List<PrProductionUnit>> findByCountryAndPersonalID(@Param("country") String country, @Param("personalID") String personalID);

	
	/**
	 * Find by country, personalID and apuKey.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @param apuKey the apuKey
	 * @return the optional
	 */
	@Query(value="SELECT pu.* \r\n" + 
			"FROM PR_DPRODUCTIONUNIT pu INNER JOIN PR_DFARMER fa ON pu.far_id = fa.far_id\r\n" + 
			"	INNER JOIN PR_DFARMERDATA fd ON fa.far_id = fd.far_id\r\n" + 
			"WHERE pu.COU_id = :country AND fd.fad_personalid = :personalID"
			+ "	AND pu.pun_key = :apuKey", nativeQuery=true) Optional<PrProductionUnit> findByCountryAndPersonalIDAPUKey(@Param("country") String country, @Param("personalID") String personalID, @Param("apuKey") String apuKey);
		
	
	
	/**
	 * Find by country, personalID and interval.
	 *
	 * @param country the country
	 * @param personalID the personalID
	 * @param dateFrom the date from
	 * @param dateTo the date to
	 * @return the optional
	 */
	@Query(value="SELECT pu.* \r\n" + 
			"FROM PR_DPRODUCTIONUNIT pu INNER JOIN PR_DFARMER fa ON pu.far_id = fa.far_id\r\n" + 
			"	INNER JOIN PR_DFARMERDATA fd ON fa.far_id = fd.far_id\r\n" + 
			"WHERE pu.COU_id = :country AND fd.fad_personalid = :personalID"
			+ "	AND (pu.PUN_DATEFROM > :dateFrom OR pu.PUN_DATETO < :dateTo)", nativeQuery=true) Optional<List<PrProductionUnit>> findByCountryAndPersonalIDAndInterval(@Param("country") String country, @Param("personalID") String personalID, @Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
		
	

	/**
	 * Asigna el code.
	 *
	 * @param <S> the generic type
	 * @param productionUnit the production unit
	 * @return the s
	 */
	default <S extends PrProductionUnit> S setCode(S productionUnit)
	{
		try
		{
			if ("AT".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextAT()));
			} else if ("BE".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextBE()));
			} else if ("BG".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextBG()));
			} else if ("CH".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextCH()));
			} else if ("CY".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextCY()));
			} else if ("CZ".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextCZ()));
			} else if ("DE".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextDE()));
			} else if ("DK".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextDK()));
			} else if ("EE".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextEE()));
			} else if ("EL".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextEL()));
			} else if ("ES".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextES()));
			} else if ("FI".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextFI()));
			} else if ("FR".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextFR()));
			} else if ("HR".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextHR()));
			} else if ("HU".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextHU()));
			} else if ("IE".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextIE()));
			} else if ("IL".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextIL()));
			} else if ("IT".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextIT()));
			} else if ("LT".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextLT()));
			} else if ("LU".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextLU()));
			} else if ("LV".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextLV()));
			} else if ("MT".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextMT()));
			} else if ("NL".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextNL()));
			} else if ("NO".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextNO()));
			} else if ("PL".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextPL()));
			} else if ("PT".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextPT()));
			} else if ("RO".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextRO()));
			} else if ("SE".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextSE()));
			} else if ("SI".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextSI()));
			} else if ("SK".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextSK()));
			} else if ("UK".equals((productionUnit.getIdCountry()))) {
				productionUnit.setCode(productionUnit.getIdCountry() + String.format("%012d", nextUK()));
			}

			return productionUnit;
		}

		catch (Exception e)
		{
			return null;
		}
	}
}