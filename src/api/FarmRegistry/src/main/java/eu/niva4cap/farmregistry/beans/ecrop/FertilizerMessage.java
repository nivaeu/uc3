/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizer;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class FertilizerMessage.
 *
 * @author irodri11
 * @version 1.0.0,  12-may-2021
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
public class FertilizerMessage {

	@JsonView(JsonViews.FullDetailed.class)
	@NotEmpty(message = "Fertilizer is null or empty")
	private List<CoFertilizer> cofertilizer;


	public List<CoFertilizer> getCoPfertilizer(){
		return cofertilizer;
	}
	
	@JsonProperty(value = "Fertilizer")
	public void setCoFertilizer(List<CoFertilizer> cofertilizer) {
		this.cofertilizer = cofertilizer;
	}
	
}


