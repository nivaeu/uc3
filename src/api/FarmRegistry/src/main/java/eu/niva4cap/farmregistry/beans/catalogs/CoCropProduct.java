/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoCropProduct.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PCROPPRODUCT")
@JsonPropertyOrder({"id", "name", "botanical_name", "id_group", "id_landcover", "expiry_date"})
public class CoCropProduct {

    /** The id. */
    @Id
    @Column(name = "CRP_ID")
    @ApiModelProperty(example = "TRZAX")
    @JsonView(JsonViews.Simple.class)
    private String id;

    /** The name. */
    @Column(name = "CRP_NAME")
    @ApiModelProperty(example = "Bread wheat, soft wheat or common wheat")
    @JsonView(JsonViews.Simple.class)
    private String name;

    /** The botanical name. */
    @Column(name = "CRP_BOTANICALNAME")
    @ApiModelProperty(example = "Triticum aestivum")
    @JsonView(JsonViews.Simple.class)
    private String botanicalName;

    /** The id group. */
    @Column(name = "CPG_ID")
    @ApiModelProperty(example = "null")
    @JsonView(JsonViews.Simple.class)
    private Integer idGroup;

    /** The id land cover. */
    @Column(name = "LAN_ID")
    @ApiModelProperty(example = "null")
    @JsonView(JsonViews.Simple.class)
    private Integer idLandCover;
    
    /** The expiry date. */
    @Column(name = "CRP_EXPIRYDATE")
    @ApiModelProperty(example = "2025-12-31")
    @JsonView(JsonViews.Detailed.class)
    private Date expiryDate;

    /**
     * Gets the botanical name.
     *
     * @return the botanical name
     */
    @JsonProperty(value = "botanical_name")
    public String getBotanicalName() {
        return botanicalName;
    }

    /**
     * Gets the expiry date.
     *
     * @return the expiry date
     */
    @JsonProperty(value = "expiry_date")
    public Date getExpiryDate() {
        return expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
     * Gets the id group.
     *
     * @return the id group
     */
    @JsonProperty(value = "id_group")
    public Integer getIdGroup() {
        return idGroup;
    }
    
    /**
     * Gets the id land cover.
     *
     * @return the id land cover
     */
    @JsonProperty(value = "id_landcover")
    public Integer getIdLandCover() {
        return idLandCover;
    }
    

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
     * Sets the botanical name.
     *
     * @param botanicalName
     *            the new botanical name
     */
    public void setBotanicalName(final String botanicalName) {
        this.botanicalName = botanicalName;
    }

    /**
     * Sets the expiry date.
     *
     * @param expiryDate
     *            the new expiry date
     */
    public void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Sets the id group.
     *
     * @param idGroup
     *            the new id group
     */
    public void setIdGroup(final Integer idGroup) {
        this.idGroup = idGroup;
    }
    
    /**
     * Sets the id land cover.
     *
     * @param idLandCover
     *            the new id land cover
     */
    public void setIdLandCover(final Integer idLandCover) {
        this.idLandCover = idLandCover;
    }    

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(final String name) {
        this.name = name;
    }
}