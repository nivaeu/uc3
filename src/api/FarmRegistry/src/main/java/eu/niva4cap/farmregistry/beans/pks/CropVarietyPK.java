/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;

import javax.persistence.Column;

/**
 * The Class CropVarietyPK.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public class CropVarietyPK implements Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2979158885346162824L;

	/** The id variety. */
	@Column(name = "CRV_id")
	private String idVariety;

	/** The id product. */
	@Column(name = "CRP_id")
	private String idProduct;

	/** The id country. */
	@Column(name = "COU_id")
	private String idCountry;

	/**
	 * Gets the id variety.
	 *
	 * @return the id variety
	 */
	public String getIdVariety() {
		return idVariety;
	}

	/**
	 * Sets the id variety.
	 *
	 * @param idVariety the new id variety
	 */
	public void setIdVariety(String idVariety) {
		this.idVariety = idVariety;
	}

	/**
	 * Gets the id product.
	 *
	 * @return the id product
	 */
	public String getIdProduct() {
		return idProduct;
	}

	/**
	 * Sets the id product.
	 *
	 * @param idProduct the new id product
	 */
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	/**
	 * Gets the id country.
	 *
	 * @return the id country
	 */
	public String getIdCountry() {
		return idCountry;
	}

	/**
	 * Sets the id country.
	 *
	 * @param idCountry the new id country
	 */
	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCountry == null) ? 0 : idCountry.hashCode());
		result = prime * result + ((idProduct == null) ? 0 : idProduct.hashCode());
		result = prime * result + ((idVariety == null) ? 0 : idVariety.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CropVarietyPK other = (CropVarietyPK) obj;
		if (idCountry == null) {
			if (other.idCountry != null)
				return false;
		} else if (!idCountry.equals(other.idCountry))
			return false;
		if (idProduct == null) {
			if (other.idProduct != null)
				return false;
		} else if (!idProduct.equals(other.idProduct))
			return false;
		if (idVariety == null) {
			if (other.idVariety != null)
				return false;
		} else if (!idVariety.equals(other.idVariety))
			return false;
		return true;
	}
}