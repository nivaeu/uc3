/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class CropParcelGeometryPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class CropParcelGeometryPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2726940557081309472L;

    /** The id crop parcel. */
    @Column(name = "RCP_id")
    private UUID idCropParcel;

    /** The date from. */
    @Column(name = "CPG_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Geometry.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CropParcelGeometryPK other = (CropParcelGeometryPK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idCropParcel == null) {
            if (other.idCropParcel != null) {
                return false;
            }
        } else if (!idCropParcel.equals(other.idCropParcel)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    public UUID getIdCropParcel() {
        return idCropParcel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result + ((idCropParcel == null) ? 0 : idCropParcel.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel
     *            the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        this.idCropParcel = idCropParcel;
    }
}