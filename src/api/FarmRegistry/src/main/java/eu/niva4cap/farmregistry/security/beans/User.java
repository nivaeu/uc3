/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.security.beans;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "SEC_DUSER", uniqueConstraints = {@UniqueConstraint(columnNames = "USR_username")})
public class User {
    @Id
    @Column(name = "USR_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REV_ENVERS")
    @SequenceGenerator(name = "SEQ_REV_ENVERS", sequenceName = "SEQ_REV_ENVERS", allocationSize = 1)
    private Integer id;

    @Column(name = "USR_USERNAME")
    private String username;

    @Column(name = "USR_PASSWORD")
    private String password;

    @Column(name = "COU_ID")
    private String country;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "SEC_DUSER_ROL", joinColumns = @JoinColumn(name = "USR_ID"),
               inverseJoinColumns = @JoinColumn(name = "ROL_ID"))
    private Set<Role> roles = new HashSet<>();

    public User() {
    }

    public User(final String username, final String password, final String country) {
        this.username = username;
        this.password = password;
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public Integer getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public String getUsername() {
        return username;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }

    public void setUsername(final String username) {
        this.username = username;
    }
}