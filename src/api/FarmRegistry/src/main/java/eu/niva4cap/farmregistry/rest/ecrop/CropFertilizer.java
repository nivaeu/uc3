/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropFertilizer;
import eu.niva4cap.farmregistry.repositories.IRpCropFertilizer;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class CropFertilizer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_CROP_FERTILIZER, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
@Api(tags = "Crop Fertilizers")
public class CropFertilizer {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CropFertilizer.class);

    /** The i crop fertilizer. */
    @Autowired
    private IRpCropFertilizer iCropFertilizer;

    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping
	@RequestMapping( method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Crop Fertilizers", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.Detailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer>> get() {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer> fertilizers = new ArrayList<>();

            final List<RpCropFertilizer> rpCropFertilizers = iCropFertilizer.findAll();

            for (final RpCropFertilizer rpCropFertilizer : rpCropFertilizers) {
                final eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer cropFertilizer = new eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer();
                fertilizers.add(cropFertilizer);

                cropFertilizer.parseCropFertilizer(rpCropFertilizer);
            }

            return ResponseEntity.ok(fertilizers);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
}