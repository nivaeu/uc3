/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;

/**
 * The Interface IRpCropParcel.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IRpCropParcel extends JpaRepository<RpCropParcel, UUID>
{
	
	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DCROPPARCEL cp, RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE cp.RFP_ID = dp.RFP_ID AND dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country",
	           nativeQuery = true)
	
	    Optional<List<RpCropParcel>> findByCountryAndPersonalID(@Param("country") String country,
	            @Param("personalID") String personalID);	

	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DCROPPARCEL cp, RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE cp.RFP_ID = dp.RFP_ID AND dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :agriculturalProductionUnitKey",
	           nativeQuery = true)
	
	    Optional<List<RpCropParcel>> findByCountryAndPersonalIDAndAPUKey(@Param("country") String country,
	            @Param("personalID") String personalID, @Param("agriculturalProductionUnitKey") String agriculturalProductionUnitKey);	
	

	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DCROPPARCEL cp, RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE cp.RFP_ID = dp.RFP_ID AND dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :agriculturalProductionUnitKey and ab.ABL_KEY = :agriculturalBlock",
	           nativeQuery = true)
	
	    Optional<List<RpCropParcel>> findByCountryAndPersonalIDAPUKeyAgrBlock(@Param("country") String country,
	            @Param("personalID") String personalID, @Param("agriculturalProductionUnitKey") String agriculturalProductionUnitKey,
	            @Param("agriculturalBlock") String agriculturalBlock);	

	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DCROPPARCEL cp, RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE cp.RFP_ID = dp.RFP_ID AND dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :agriculturalProductionUnitKey and ab.ABL_KEY = :agriculturalBlock and dp.RFP_KEY = :referencePlot",
	           nativeQuery = true)

    Optional<List<RpCropParcel>> findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(@Param("country") String country,
            @Param("personalID") String personalID, @Param("agriculturalProductionUnitKey") String agriculturalProductionUnitKey,
            @Param("agriculturalBlock") String agriculturalBlock, @Param("referencePlot") String referencePlot);	
	
	
}