/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.PrFarmerData;

/**
 * The Interface IPrfarmerData.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IPrfarmerData extends JpaRepository<PrFarmerData, UUID>
{
	
	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the optional
	 */
	@Query(value="SELECT fd.* FROM PR_DFARMERDATA fd INNER JOIN PR_DFARMER f ON fd.FAR_id = f.FAR_id AND f.FAR_code = :code", nativeQuery=true) Optional<PrFarmerData> findByCode(@Param("code") String code);
}