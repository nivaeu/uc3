/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.repositories.IPrProductionUnit;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class PRProductionUnit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_PR_PRODUCTION_UNIT, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Production Units")
@ApiIgnore
public class PRProductionUnit
{
	
	/** The i production unit. */
	@Autowired
	private IPrProductionUnit iProductionUnit;

	/**
	 * Obtiene el.
	 *
	 * @return the response entity
	 */
	@GetMapping
	@ApiOperation(value = "Returns all Production Units", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrProductionUnit>> get()
	{
		try
		{
			return ResponseEntity.ok(iProductionUnit.findAll());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	/**
	 * Gets the by code.
	 *
	 * @param code the code
	 * @return the by code
	 */
	@GetMapping(value="{code}")
	@ApiOperation(value = "Returns a Production Unit by production unit code", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<PrProductionUnit> getByCode(@PathVariable("code") String code)
	{
		try
		{
			Optional<PrProductionUnit> beanDB = iProductionUnit.findByCode(code);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	/**
	 * Gets the by country.
	 *
	 * @param country the country
	 * @return the by country
	 */
	@GetMapping(value="country/{country}")
	@ApiOperation(value = "Returns all Production Units by country code", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Simple.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrProductionUnit>> getByCountry(@PathVariable("country") String country)
	{
		try
		{
			Optional<List<PrProductionUnit>> beanDB = iProductionUnit.findByCountry(country);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}
	

}