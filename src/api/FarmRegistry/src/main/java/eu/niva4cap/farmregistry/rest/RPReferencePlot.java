/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.repositories.IRpReferencePlot;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class RPReferencePlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_REFERENCE_PLOT)
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Reference Plots")
@ApiIgnore
public class RPReferencePlot
{
	
	/** The i reference plot. */
	@Autowired
	private IRpReferencePlot iReferencePlot;

	/**
	 * Obtiene el.
	 *
	 * @return the response entity
	 */
	@GetMapping
	@ApiOperation(value = "Returns all Reference Plots" , authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpReferencePlot>> get()
	{
		try
		{
			return ResponseEntity.ok(iReferencePlot.findAll());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}
	@GetMapping(value = "/country/{country}/personalID/{personalID}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Reference Plots by its Country and Personal ID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpReferencePlot>> getByCountryAndPersonalID(@PathVariable("country") String country, @PathVariable("personalID") String personalID) {
        try {
            Optional<List<RpReferencePlot>> referencePlot = iReferencePlot.findByCountryAndPersonalID(country, personalID);

        	if (!referencePlot.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(referencePlot.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }	
	
	@GetMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Reference Plots by its Country, Personal ID and Key", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpReferencePlot>> getByCountryAndPersonalIDAndKey(@PathVariable("country") String country, 
			@PathVariable("personalID") String personalID,
			@PathVariable("agriculturalProductionUnitKey") String key) {
        try {
            Optional<List<RpReferencePlot>> referencePlot = iReferencePlot.findByCountryAndPersonalIDAndKey(country, personalID, key);

        	if (!referencePlot.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(referencePlot.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    } 
	
	@GetMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/AgriculturalBlock/{AgriculturalBlock}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/AgriculturalBlock/{AgriculturalBlock}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Reference Plots by its Country, Personal ID, agriculturalProductionUnitKey and AgriculturalBlock ", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpReferencePlot>> getByCountryAndPersonalID(@PathVariable("country") String country, 
			@PathVariable("personalID") String personalID,
			@PathVariable("agriculturalProductionUnitKey") String key, 
			@PathVariable("AgriculturalBlock") String AgriculturalBlock)
		{
		try {
            Optional<List<RpReferencePlot>> referencePlot = iReferencePlot.findByCountryAndPersonalIDAndKeyAndBlock(country, personalID, key, AgriculturalBlock );

        	if (!referencePlot.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(referencePlot.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }
}