/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty;
import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrFarmer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HFARMER")
@Table(name = "PR_DFARMER",
       uniqueConstraints = {@UniqueConstraint(columnNames = "FAR_code")})
@JsonPropertyOrder({"code", "id_country", "farmer_data", "farmer_contact", "production_unit"})
public class PrFarmer {
    
    /** The id. */
    @Id
    @Column(name = "FAR_id")
    @GeneratedValue(generator = "UUID")
    private UUID id;

    /** The code. */
    @Column(name = "FAR_code")
    @JsonView(JsonViews.Simple.class)
    private String code;

    /** The id country. */
    @Column(name = "COU_id")
    @JsonView(JsonViews.Detailed.class)
    @NotEmpty(message = "AgriculturalProducerParty.Country is null or empty")
    private String idCountry;

    /** The farmer data. */
    @OneToOne(mappedBy = "farmer", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "farmer_data")
    private PrFarmerData farmerData;

    /** The farmer contact. */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FAC_id")
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "farmer_contact")
    private PrFarmerContact farmerContact;

    /** The production units. */
    @OneToMany(mappedBy = "farmer", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "production_unit")
    private List<PrProductionUnit> productionUnits = new ArrayList<>();

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the farmer contact.
     *
     * @return the farmer contact
     */
    public PrFarmerContact getFarmerContact() {
        return farmerContact;
    }

    /**
     * Gets the farmer data.
     *
     * @return the farmer data
     */
    public PrFarmerData getFarmerData() {
        return farmerData;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonIgnore
    public UUID getId() {
        return id;
    }

    /**
     * Gets the id country.
     *
     * @return the id country
     */
    public String getIdCountry() {
        return idCountry;
    }

    /**
     * Gets the production unit by key.
     *
     * @param key the key
     * @return the production unit by key
     */
    public PrProductionUnit getProductionUnitByKey(final String key) {
        for (final PrProductionUnit productionUnit : getProductionUnits()) {
            if (productionUnit.getKey() != null && productionUnit.getKey().equals(key)) {
                return productionUnit;
            }
        }

        return null;
    }

    /**
     * Gets the production units.
     *
     * @return the production units
     */
    public List<PrProductionUnit> getProductionUnits() {
        return productionUnits;
    }

    /**
     * Parsea el agricultural producer party.
     *
     * @param agriculturalProducerParty the agricultural producer party
     */
    public void parseAgriculturalProducerParty(
            final AgriculturalProducerParty agriculturalProducerParty) {
        if (getIdCountry() == null) {
            setIdCountry(agriculturalProducerParty.getCountry());
        }

        if (getFarmerData() == null) {
            setFarmerData(new PrFarmerData());
        }

        final PrFarmerData prFarmerData = getFarmerData();
        prFarmerData.setFarmer(this);

        prFarmerData.parseAgriculturalProducerParty(agriculturalProducerParty);

        if (agriculturalProducerParty.getStructuredAddress() != null) {
            if (getFarmerContact() == null) {
                setFarmerContact(new PrFarmerContact());
            }

            final PrFarmerContact prFarmerContact = getFarmerContact();
            prFarmerContact.setFarmer(this);

            prFarmerContact
                    .parseStructuredAddress(agriculturalProducerParty.getStructuredAddress());
        }

        if (agriculturalProducerParty.getAgriculturalProductionUnits() != null) {
            for (final AgriculturalProductionUnit agriculturalProductionUnit : agriculturalProducerParty
                    .getAgriculturalProductionUnits()) {
                PrProductionUnit prProductionUnit = new PrProductionUnit();

                final PrProductionUnit wanted = getProductionUnitByKey(
                        agriculturalProductionUnit.getKey());

                if (wanted != null) {
                    prProductionUnit = wanted;
                }

                else {
                    getProductionUnits().add(prProductionUnit);

                    prProductionUnit.setFarmer(this);
                }

                prProductionUnit.parseAgriculturalProductionUnit(agriculturalProductionUnit);
            }
        }
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    @JsonProperty(value = "code")
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the farmer contact.
     *
     * @param farmerContact the new farmer contact
     */
    public void setFarmerContact(final PrFarmerContact farmerContact) {
        this.farmerContact = farmerContact;
    }

    /**
     * Sets the farmer data.
     *
     * @param farmerData the new farmer data
     */
    public void setFarmerData(final PrFarmerData farmerData) {
        this.farmerData = farmerData;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Sets the id country.
     *
     * @param idCountry the new id country
     */
    @JsonProperty(value = "id_country")
    public void setIdCountry(final String idCountry) {
        this.idCountry = idCountry;
    }

    /**
     * Sets the production units.
     *
     * @param productionUnits the new production units
     */
    public void setProductionUnits(final List<PrProductionUnit> productionUnits) {
        this.productionUnits = productionUnits;
    }
}