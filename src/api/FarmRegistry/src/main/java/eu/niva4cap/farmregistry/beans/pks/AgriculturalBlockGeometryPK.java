/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class AgriculturalBlockGeometryPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class AgriculturalBlockGeometryPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2069190211219014770L;

    /** The id agricultural block. */
    @Column(name = "ABL_id")
    private UUID idAgriculturalBlock;

    /** The date from. */
    @Column(name = "AGE_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.Geometry.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgriculturalBlockGeometryPK other = (AgriculturalBlockGeometryPK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idAgriculturalBlock == null) {
            if (other.idAgriculturalBlock != null) {
                return false;
            }
        } else if (!idAgriculturalBlock.equals(other.idAgriculturalBlock)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id agricultural block.
     *
     * @return the id agricultural block
     */
    public UUID getIdAgriculturalBlock() {
        return idAgriculturalBlock;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result
                + ((idAgriculturalBlock == null) ? 0 : idAgriculturalBlock.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id agricultural block.
     *
     * @param idAgriculturalBlock
     *            the new id agricultural block
     */
    public void setIdAgriculturalBlock(final UUID idAgriculturalBlock) {
        this.idAgriculturalBlock = idAgriculturalBlock;
    }
}