/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit;
import eu.niva4cap.farmregistry.beans.ecrop.CropPlot;
import eu.niva4cap.farmregistry.beans.ecrop.FarmingType;
import eu.niva4cap.farmregistry.beans.ecrop.Workunit;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class PrProductionUnit.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "PR_HPRODUCTIONUNIT")
@Table(name = "PR_DPRODUCTIONUNIT",
       uniqueConstraints = {@UniqueConstraint(columnNames = "PUN_code")})
@JsonPropertyOrder({"code", "key", "id_country", "date_from", "date_to", "farmer_contact",
        "farming_type", "production_workunit", "agricultural_block"})
public class PrProductionUnit {

    /** The id. */
    @Id
    @Column(name = "PUN_id")
    @GeneratedValue(generator = "UUID")
    private UUID id;

    /** The code. */
    @Column(name = "PUN_code")
    @JsonView(JsonViews.Simple.class)
    private String code;

    /** The key. */
    @Column(name = "PUN_key")
    @JsonView(JsonViews.Simple.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.Key is null or empty")
    @Size(max = 50,
          message = "AgriculturalProducerParty.AgriculturalProductionUnit.Key is too long")
    private String key;

    /** The id country. */
    @Column(name = "COU_id")
    @JsonView(JsonViews.Detailed.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.Country is null or empty")
    private String idCountry;

    /** The id farmer. */
    @Column(name = "FAR_id", insertable = false, updatable = false)
    private UUID idFarmer;

    /** The date from. */
    @Column(name = "PUN_datefrom")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "PUN_dateto")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateTo;

    /** The farmer. */
    @ManyToOne
    @JoinColumn(name = "FAR_id")
    private PrFarmer farmer;

    /** The farming types. */
    @OneToMany(mappedBy = "productionUnit", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "farming_type")
    private List<PrFarmingType> farmingTypes = new ArrayList<>();

    /** The production workunits. */
    @OneToMany(mappedBy = "productionUnit", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "production_workunit")
    private List<PrProductionWorkunit> productionWorkunits = new ArrayList<>();

    /** The farmer contact. */
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FAC_id")
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "farmer_contact")
    private PrFarmerContact farmerContact;

    /** The agricultural blocks. */
    @OneToMany(mappedBy = "productionUnit", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "agricultural_block")
    private List<AbAgriculturalBlock> agriculturalBlocks = new ArrayList<>();

    /**
     * Gets the agricultural block by key.
     *
     * @param key
     *            the key
     * @return the agricultural block by key
     */
    public AbAgriculturalBlock getAgriculturalBlockByKey(final String key) {
        for (final AbAgriculturalBlock agriculturalBlock : getAgriculturalBlocks()) {
            if (agriculturalBlock.getKey().equals(key)) {
                return agriculturalBlock;
            }
        }

        return null;
    }

    /**
     * Gets the agricultural blocks.
     *
     * @return the agricultural blocks
     */
    public List<AbAgriculturalBlock> getAgriculturalBlocks() {
        return agriculturalBlocks;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the farmer.
     *
     * @return the farmer
     */
    public PrFarmer getFarmer() {
        return farmer;
    }

    /**
     * Gets the farmer contact.
     *
     * @return the farmer contact
     */
    public PrFarmerContact getFarmerContact() {
        return farmerContact;
    }

    /**
     * Gets the farming type.
     *
     * @param type
     *            the type
     * @param date
     *            the date
     * @return the farming type
     */
    public PrFarmingType getFarmingType(final String type, final Date date) {
        for (final PrFarmingType farmingType : getFarmingTypes()) {
            if (farmingType.getIdFarmingType().equals(type)
                    && farmingType.getDateFrom().toString().equals(date.toString())) {
                return farmingType;
            }
        }

        return null;
    }

    /**
     * Gets the farming types.
     *
     * @return the farming types
     */
    public List<PrFarmingType> getFarmingTypes() {
        return farmingTypes;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonIgnore
    public UUID getId() {
        return id;
    }

    /**
     * Gets the id country.
     *
     * @return the id country
     */
    public String getIdCountry() {
        return idCountry;
    }

    /**
     * Gets the id farmer.
     *
     * @return the id farmer
     */
    @JsonIgnore
    public UUID getIdFarmer() {
        return farmer.getId();
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the production workunits.
     *
     * @return the production workunits
     */
    public List<PrProductionWorkunit> getProductionWorkunits() {
        return productionWorkunits;
    }

    /**
     * Gets the workunit.
     *
     * @param type
     *            the type
     * @param date
     *            the date
     * @return the workunit
     */
    public PrProductionWorkunit getWorkunit(final Integer type, final Date date) {
        for (final PrProductionWorkunit productionWorkunit : getProductionWorkunits()) {
            if (productionWorkunit.getIdWorkunitType() == type
                    && productionWorkunit.getDateFrom().toString().equals(date.toString())) {
                return productionWorkunit;
            }
        }

        return null;
    }

    /**
     * Parsea el agricultural production unit.
     *
     * @param agriculturalProductionUnit
     *            the agricultural production unit
     */
    public void parseAgriculturalProductionUnit(
            final AgriculturalProductionUnit agriculturalProductionUnit) {
        setKey(agriculturalProductionUnit.getKey());

        if (getIdCountry() == null) {
            setIdCountry(agriculturalProductionUnit.getCountry());
        }

        if (agriculturalProductionUnit.getDateFrom() != null) {
            setDateFrom(agriculturalProductionUnit.getDateFrom());
        }

        if (agriculturalProductionUnit.getDateTo() != null) {
            setDateTo(agriculturalProductionUnit.getDateTo());
        }

        if (agriculturalProductionUnit.getStructuredAddress() != null) {
            if (getFarmerContact() == null) {
                setFarmerContact(new PrFarmerContact());
            }

            final PrFarmerContact prFarmerContact = getFarmerContact();
            prFarmerContact.setProductionUnit(this);

            prFarmerContact
                    .parseStructuredAddress(agriculturalProductionUnit.getStructuredAddress());
        }

        if (agriculturalProductionUnit.getFarmingTypes() != null) {
            for (final FarmingType farmingType : agriculturalProductionUnit.getFarmingTypes()) {
                PrFarmingType prFarmingType = new PrFarmingType();

                final PrFarmingType wanted = getFarmingType(farmingType.getType(),
                        farmingType.getDateFrom());

                if (wanted != null) {
                    prFarmingType = wanted;
                }

                else {
                    getFarmingTypes().add(prFarmingType);

                    prFarmingType.setProductionUnit(this);
                }

                prFarmingType.parseFarmingType(farmingType);
            }
        }

        if (agriculturalProductionUnit.getWorkunits() != null) {
            for (final Workunit workunit : agriculturalProductionUnit.getWorkunits()) {
                PrProductionWorkunit prProductionWorkunit = new PrProductionWorkunit();

                final PrProductionWorkunit wanted = getWorkunit(workunit.getType(),
                        workunit.getDateFrom());

                if (wanted != null) {
                    prProductionWorkunit = wanted;
                }

                else {
                    getProductionWorkunits().add(prProductionWorkunit);

                    prProductionWorkunit.setProductionUnit(this);
                }

                prProductionWorkunit.parseWorkunit(workunit);
            }
        }

        if (agriculturalProductionUnit.getCropPlots() != null) {
            for (final CropPlot cropPlot : agriculturalProductionUnit.getCropPlots()) {
                AbAgriculturalBlock abAgriculturalBlock = new AbAgriculturalBlock();

                final AbAgriculturalBlock wanted = getAgriculturalBlockByKey(cropPlot.getKey());

                if (wanted != null) {
                    abAgriculturalBlock = wanted;
                }

                else {
                    getAgriculturalBlocks().add(abAgriculturalBlock);

                    abAgriculturalBlock.setProductionUnit(this);
                }

                abAgriculturalBlock.parseCropPlot(cropPlot);
            }
        }
    }

    /**
     * Sets the agricultural blocks.
     *
     * @param agriculturalBlocks
     *            the new agricultural blocks
     */
    public void setAgriculturalBlocks(final List<AbAgriculturalBlock> agriculturalBlocks) {
        this.agriculturalBlocks = agriculturalBlocks;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the farmer.
     *
     * @param farmer
     *            the new farmer
     */
    public void setFarmer(final PrFarmer farmer) {
        this.farmer = farmer;
    }

    /**
     * Sets the farmer contact.
     *
     * @param farmerContact
     *            the new farmer contact
     */
    public void setFarmerContact(final PrFarmerContact farmerContact) {
        this.farmerContact = farmerContact;
    }

    /**
     * Sets the farming types.
     *
     * @param farmingTypes
     *            the new farming types
     */
    public void setFarmingTypes(final List<PrFarmingType> farmingTypes) {
        this.farmingTypes = farmingTypes;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Sets the id country.
     *
     * @param idCountry
     *            the new id country
     */
    @JsonProperty(value = "id_country")
    public void setIdCountry(final String idCountry) {
        this.idCountry = idCountry;
    }

    /**
     * Sets the id farmer.
     *
     * @param idFarmer
     *            the new id farmer
     */
    public void setIdFarmer(final UUID idFarmer) {
        farmer.setId(idFarmer);
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the new key
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the production workunits.
     *
     * @param productionWorkunits
     *            the new production workunits
     */
    public void setProductionWorkunits(final List<PrProductionWorkunit> productionWorkunits) {
        this.productionWorkunits = productionWorkunits;
    }
}