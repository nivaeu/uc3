/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropFertilizer;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CropFertilizer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"ID", "Dose", "Unit", "Type", "FertilizationType", "FertilizationMethod",
        "StartDate", "EndDate"})
public class CropFertilizer {
    
    /** The id. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "5")
    private Integer id;
    
    /** The fertilizer type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2")
    private Integer fertilizerType;
    
    /** The fertilization type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer fertilizationType;
    
    /** The fertilization method. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer fertilizationMethod;
    
    /** The dose. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "25.52")
    private BigDecimal dose;
    
    /** The unit. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer unit;
    
    /** The start date. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date startDate;
    
    /** The end date. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2021-03-12")
    private Date endDate;

    /**
     * Gets the dose.
     *
     * @return the dose
     */
    public BigDecimal getDose() {
        return dose;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the fertilization method.
     *
     * @return the fertilization method
     */
    public Integer getFertilizationMethod() {
        return fertilizationMethod;
    }

    /**
     * Gets the fertilization type.
     *
     * @return the fertilization type
     */
    public Integer getFertilizationType() {
        return fertilizationType;
    }

    /**
     * Gets the fertilizer type.
     *
     * @return the fertilizer type
     */
    public Integer getFertilizerType() {
        return fertilizerType;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate == null ? null : (Date) startDate.clone();
    }

    /**
     * Gets the unit.
     *
     * @return the unit
     */
    public Integer getUnit() {
        return unit;
    }

    /**
     * Parsea el crop fertilizer.
     *
     * @param cropFertilizer the crop fertilizer
     */
    public void parseCropFertilizer(final RpCropFertilizer cropFertilizer) {
        setId(cropFertilizer.getIdFertilizer());
        setFertilizerType(cropFertilizer.getIdFertilizerType());
        setFertilizationType(cropFertilizer.getIdFertilizationType());
        setFertilizationMethod(cropFertilizer.getIdFertilizationMethod());
        setDose(cropFertilizer.getDose());
        setUnit(cropFertilizer.getIdUnitType());
        setStartDate(cropFertilizer.getStartDate());
        setEndDate(cropFertilizer.getEndDate());
    }

    /**
     * Sets the dose.
     *
     * @param dose the new dose
     */
    @JsonProperty(value = "Dose")
    public void setDose(final BigDecimal dose) {
        this.dose = dose;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    @JsonProperty(value = "EndDate")
    public void setEndDate(final Date endDate) {
        this.endDate = endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Sets the fertilization method.
     *
     * @param fertilizationMethod the new fertilization method
     */
    @JsonProperty(value = "FertilizationMethod")
    public void setFertilizationMethod(final Integer fertilizationMethod) {
        this.fertilizationMethod = fertilizationMethod;
    }

    /**
     * Sets the fertilization type.
     *
     * @param fertilizationType the new fertilization type
     */
    @JsonProperty(value = "FertilizationType")
    public void setFertilizationType(final Integer fertilizationType) {
        this.fertilizationType = fertilizationType;
    }

    /**
     * Sets the fertilizer type.
     *
     * @param fertilizerType the new fertilizer type
     */
    @JsonProperty(value = "Type")
    public void setFertilizerType(final Integer fertilizerType) {
        this.fertilizerType = fertilizerType;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    @JsonProperty(value = "ID")
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    @JsonProperty(value = "StartDate")
    public void setStartDate(final Date startDate) {
        this.startDate = startDate == null ? null : (Date) startDate.clone();
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    @JsonProperty(value = "Unit")
    public void setUnit(final Integer unit) {
        this.unit = unit;
    }
}