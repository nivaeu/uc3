/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.repositories.IRpCropParcel;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class FieldCrop.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_FIELD_CROP, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
@Api(tags = "Fields Crops")
public class FieldCrop {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldCrop.class);

    /** The i crop parcel. */
    @Autowired
    private IRpCropParcel iCropParcel;

    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    
    @GetMapping
    @ApiOperation(value = "Returns all Field Crops key", authorizations = { @Authorization(value="BearerToken") })
	@RequestMapping(method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @JsonView(JsonViews.Detailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.FieldCrop>> getCodeList() {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.FieldCrop> fieldCrops = new ArrayList<>();

            final List<RpCropParcel> rpCropParcels = iCropParcel.findAll();

            for (final RpCropParcel rpCropParcel : rpCropParcels) {
                final eu.niva4cap.farmregistry.beans.ecrop.FieldCrop fieldCrop = new eu.niva4cap.farmregistry.beans.ecrop.FieldCrop();
                fieldCrops.add(fieldCrop);

                fieldCrop.parseCropParcelCodeList(rpCropParcel);
            }

            return ResponseEntity.ok(fieldCrops);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
    
    /**
     * Obtiene el.
     *
     * @return the response entity
     */
    @GetMapping(value = "details")
	@RequestMapping(value = "details", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Field Crops", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.FieldCrop>> get() {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.FieldCrop> fieldCrops = new ArrayList<>();

            final List<RpCropParcel> rpCropParcels = iCropParcel.findAll();

            for (final RpCropParcel rpCropParcel : rpCropParcels) {
                final eu.niva4cap.farmregistry.beans.ecrop.FieldCrop fieldCrop = new eu.niva4cap.farmregistry.beans.ecrop.FieldCrop();
                fieldCrops.add(fieldCrop);

                fieldCrop.parseCropParcel(rpCropParcel);
            }

            return ResponseEntity.ok(fieldCrops);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.ok(null);
        }
    }
}