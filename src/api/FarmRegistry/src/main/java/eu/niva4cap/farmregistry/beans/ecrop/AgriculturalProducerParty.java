/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class AgriculturalProducerParty.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Code", "Country", "Classification", "Name", "PersonalID", "StructuredAddress",
        "AgriculturalProductionUnit"})
public class AgriculturalProducerParty {
    
    /** The code. */
    @JsonView(JsonViews.Simple.class)
    @ApiModelProperty(example = "ES000000003725")
    private String code;
    
    /** The country. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "ES")
    private String country;
    
    /** The name. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "TERENO LOYA ILKOLLES")
    private String name;
    
    /** The classification. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "1")
    private Integer classification;
    
    /** The personal ID. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "04531713T")
    private String personalID;
    
    /** The structured address. */
    @JsonView(JsonViews.FullDetailed.class)
    private StructuredAddress structuredAddress;
    
    /** The agricultural production units. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<AgriculturalProductionUnit> agriculturalProductionUnits;

    /**
     * Gets the agricultural production units.
     *
     * @return the agricultural production units
     */
    @JsonView(JsonViews.FullDetailed.class)
    public List<AgriculturalProductionUnit> getAgriculturalProductionUnits() {
        return agriculturalProductionUnits;
    }

    /**
     * Gets the classification.
     *
     * @return the classification
     */
    public Integer getClassification() {
        return classification;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the personal ID.
     *
     * @return the personal ID
     */
    public String getPersonalID() {
        return personalID;
    }

    /**
     * Gets the structured address.
     *
     * @return the structured address
     */
    public StructuredAddress getStructuredAddress() {
        return structuredAddress;
    }

    /**
     * Parsea el farmer.
     *
     * @param farmer the farmer
     */
    public void parseFarmer(final PrFarmer farmer) {
        setCode(farmer.getCode());
        setCountry(farmer.getIdCountry());
        setName(farmer.getFarmerData().getName());
        setClassification(farmer.getFarmerData().getIdFarmerType());
        setPersonalID(farmer.getFarmerData().getPersonalId());
        
        if (farmer.getFarmerContact() != null) {
            final StructuredAddress sa = new StructuredAddress();
            setStructuredAddress(sa);

            sa.parseFarmerContact(farmer.getFarmerContact());
        }
        
        if (farmer.getProductionUnits() != null) {
            setAgriculturalProductionUnits(new ArrayList<AgriculturalProductionUnit>());

            for (final PrProductionUnit prProductionUnit : farmer.getProductionUnits()) {
                final AgriculturalProductionUnit agriculturalProductionUnit = new AgriculturalProductionUnit();
                getAgriculturalProductionUnits().add(agriculturalProductionUnit);

                agriculturalProductionUnit.parseProductionUnit(prProductionUnit);
            }
        }
        
    }
    
    /**
     * Parsea el farmer.
     *
     * @param farmer the farmer
     */
    public void parseFarmerCodeList(final PrFarmer farmer) {
        setCode(farmer.getCode());
        setCountry(farmer.getIdCountry());
        setName(farmer.getFarmerData().getName());
        setClassification(farmer.getFarmerData().getIdFarmerType());
        setPersonalID(farmer.getFarmerData().getPersonalId());
        
        if (farmer.getFarmerContact() != null) {
            final StructuredAddress sa = new StructuredAddress();
            setStructuredAddress(sa);

            sa.parseFarmerContact(farmer.getFarmerContact());
        }
    }

    /**
     * Sets the agricultural production units.
     *
     * @param agriculturalProductionUnits the new agricultural production units
     */
    @JsonProperty(value = "AgriculturalProductionUnit")
    public void setAgriculturalProductionUnits(
            final List<AgriculturalProductionUnit> agriculturalProductionUnits) {
        this.agriculturalProductionUnits = agriculturalProductionUnits;
    }

    /**
     * Sets the classification.
     *
     * @param classification the new classification
     */
    @JsonProperty(value = "Classification")
    public void setClassification(final Integer classification) {
        this.classification = classification;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    @JsonProperty(value = "Code")
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * Sets the country.
     *
     * @param country the new country
     */
    @JsonProperty(value = "Country")
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    @JsonProperty(value = "Name")
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the personal ID.
     *
     * @param personalID the new personal ID
     */
    @JsonProperty(value = "PersonalID")
    public void setPersonalID(final String personalID) {
        this.personalID = personalID;
    }

    /**
     * Sets the structured address.
     *
     * @param structuredAddress the new structured address
     */
    @JsonProperty(value = "StructuredAddress")
    public void setStructuredAddress(final StructuredAddress structuredAddress) {
        this.structuredAddress = structuredAddress;
    }
}