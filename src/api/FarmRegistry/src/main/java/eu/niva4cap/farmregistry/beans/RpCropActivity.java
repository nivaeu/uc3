/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.Activity;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropActivity.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPACTIVITY")
@Table(name = "RP_DCROPACTIVITY")
@JsonPropertyOrder({ "agricultural_activity", "id_cultivation_system", "id_cultivation_detail", "id_seed_type", "id_organic_farming" })
public class RpCropActivity
{
	
	/** The id crop parcel. */
	@Id
	@Column(name = "RCP_ID")
	private UUID idCropParcel;

	/** The agricultural activity. */
	@Column(name = "AGA_AGRIACTIVITY")
	@JsonView(JsonViews.Detailed.class)
	private Boolean agriculturalActivity;

	/** The id cultivation system. */
	@Column(name = "CUS_ID")
	@JsonView(JsonViews.Detailed.class)
	private Integer idCultivationSystem;

	/** The id cultivation detail. */
	@Column(name = "CUD_ID")
	@JsonView(JsonViews.Detailed.class)
	private Integer idCultivationDetail;

	/** The id seed type. */
	@Column(name = "SEE_ID")
	@JsonView(JsonViews.Detailed.class)
	private Integer idSeedType;

	/** The id organic farming. */
	@Column(name = "ORF_ID")
	@JsonView(JsonViews.Detailed.class)
	private Integer idOrganicFarming;

	/** The crop parcel. */
	@MapsId
	@OneToOne
	@JoinColumn(name = "RCP_ID")
	private RpCropParcel cropParcel;

	/**
	 * Parsea el activity.
	 *
	 * @param activity the activity
	 */
	public void parseActivity(Activity activity) {
		setAgriculturalActivity(activity.getAgriculturalActivity());
		setIdCultivationSystem(activity.getCultivationSystem());
		setIdCultivationDetail(activity.getCultivationDetail());
		setIdSeedType(activity.getSeedType());
		setIdOrganicFarming(activity.getOrganicFarming());
	}

	/**
	 * Gets the id crop parcel.
	 *
	 * @return the id crop parcel
	 */
	@JsonIgnore
	public UUID getIdCropParcel() {
		return idCropParcel;
	}

	/**
	 * Sets the id crop parcel.
	 *
	 * @param idCropParcel the new id crop parcel
	 */
	public void setIdCropParcel(UUID idCropParcel) {
		this.idCropParcel = idCropParcel;
	}

	/**
	 * Gets the agricultural activity.
	 *
	 * @return the agricultural activity
	 */
	public Boolean getAgriculturalActivity() {
		return agriculturalActivity;
	}

	/**
	 * Sets the agricultural activity.
	 *
	 * @param agriculturalActivity the new agricultural activity
	 */
	@JsonProperty(value = "agricultural_activity")
	public void setAgriculturalActivity(Boolean agriculturalActivity) {
		this.agriculturalActivity = agriculturalActivity;
	}

	/**
	 * Gets the id cultivation system.
	 *
	 * @return the id cultivation system
	 */
	public Integer getIdCultivationSystem() {
		return idCultivationSystem;
	}

	/**
	 * Sets the id cultivation system.
	 *
	 * @param idCultivationSystem the new id cultivation system
	 */
	@JsonProperty(value = "id_cultivation_system")
	public void setIdCultivationSystem(Integer idCultivationSystem) {
		this.idCultivationSystem = idCultivationSystem;
	}

	/**
	 * Gets the id cultivation detail.
	 *
	 * @return the id cultivation detail
	 */
	public Integer getIdCultivationDetail() {
		return idCultivationDetail;
	}

	/**
	 * Sets the id cultivation detail.
	 *
	 * @param idCultivationDetail the new id cultivation detail
	 */
	@JsonProperty(value = "id_cultivation_detail")
	public void setIdCultivationDetail(Integer idCultivationDetail) {
		this.idCultivationDetail = idCultivationDetail;
	}

	/**
	 * Gets the id seed type.
	 *
	 * @return the id seed type
	 */
	public Integer getIdSeedType() {
		return idSeedType;
	}

	/**
	 * Sets the id seed type.
	 *
	 * @param idSeedType the new id seed type
	 */
	@JsonProperty(value = "id_seed_type")
	public void setIdSeedType(Integer idSeedType) {
		this.idSeedType = idSeedType;
	}

	/**
	 * Gets the id organic farming.
	 *
	 * @return the id organic farming
	 */
	public Integer getIdOrganicFarming() {
		return idOrganicFarming;
	}

	/**
	 * Sets the id organic farming.
	 *
	 * @param idOrganicFarming the new id organic farming
	 */
	@JsonProperty(value = "id_organic_farming")
	public void setIdOrganicFarming(Integer idOrganicFarming) {
		this.idOrganicFarming = idOrganicFarming;
	}

	/**
	 * Gets the crop parcel.
	 *
	 * @return the crop parcel
	 */
	@JsonIgnore
	public RpCropParcel getCropParcel() {
		return cropParcel;
	}

	/**
	 * Sets the crop parcel.
	 *
	 * @param cropParcel the new crop parcel
	 */
	public void setCropParcel(RpCropParcel cropParcel) {
		this.cropParcel = cropParcel;
	}
}