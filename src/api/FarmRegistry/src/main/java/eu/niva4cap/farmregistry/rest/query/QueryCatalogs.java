/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.catalogs.CoActMats;
import eu.niva4cap.farmregistry.beans.catalogs.CoActSus;
import eu.niva4cap.farmregistry.beans.catalogs.CoCountry;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProduct;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProductGroup;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety;
import eu.niva4cap.farmregistry.beans.catalogs.CoCultivationDetail;
import eu.niva4cap.farmregistry.beans.catalogs.CoCultivationSystem;
import eu.niva4cap.farmregistry.beans.catalogs.CoFarmerType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFarmingType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizationMethod;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizationType;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizer;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizerType;
import eu.niva4cap.farmregistry.beans.catalogs.CoIrrigationType;
import eu.niva4cap.farmregistry.beans.catalogs.CoLaborType;
import eu.niva4cap.farmregistry.beans.catalogs.CoLandCover;
import eu.niva4cap.farmregistry.beans.catalogs.CoLandTenure;
import eu.niva4cap.farmregistry.beans.catalogs.CoOrganicFarming;
import eu.niva4cap.farmregistry.beans.catalogs.CoPhytosanitary;
import eu.niva4cap.farmregistry.beans.catalogs.CoPhytosanitaryEquipment;
import eu.niva4cap.farmregistry.beans.catalogs.CoSeedType;
import eu.niva4cap.farmregistry.beans.catalogs.CoUnitType;
import eu.niva4cap.farmregistry.beans.catalogs.CoWaterOrigin;
import eu.niva4cap.farmregistry.beans.catalogs.CoWorkunitType;
import eu.niva4cap.farmregistry.beans.ecrop.CropPlot;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportDocument;
import eu.niva4cap.farmregistry.beans.ecrop.CropReportMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.repositories.IAbAgriculturalBlock;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActMats;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActSus;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCountry;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProduct;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProductGroup;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropVariety;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationDetail;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationSystem;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmingType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationMethod;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizer;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoIrrigationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLaborType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandCover;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandTenure;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoOrganicFarming;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitary;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitaryEquipment;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoSeedType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoUnitType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWaterOrigin;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWorkunitType;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

/**
 * The Class QueryCatalogs. 
 *
 * @author irodri11
 * @version 1.0.0, 18-mar-2021
 * @since JDK 1.6
 */
@RestController
@RequestMapping(value = ConstantesRest.REST_QUERY_QUERY_CATALOGS)
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
@Api(tags = "Query Catalogs")
public class QueryCatalogs {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CropPlot.class);

	/** The i ActMats. */
	@Autowired
	private ICoActMats iCoActMats;

	/** The i ActSus. */
	@Autowired
	private ICoActSus iCoActSus;

	/** The i country. */
	@Autowired
	private ICoCountry iCountry;

	/** The i coCropProduct. */
	@Autowired
	private ICoCropProduct iCoCropProduct;

	/** The i coCropProductGroup. */
	@Autowired
	private ICoCropProductGroup iCoCropProductGroup;
	
	/** The i coCropVariety. */
	@Autowired
	private ICoCropVariety iCoCropVariety;

	/** The i coCultivationDetail. */
	@Autowired
	private ICoCultivationDetail iCoCultivationDetail;
	
	/** The i coCultivationSystem. */
	@Autowired
	private ICoCultivationSystem iCoCultivationSystem;

	/** The i coFarmerType. */
	@Autowired
	private ICoFarmerType iCoFarmerType;
	
	/** The i coFarmingType. */
	@Autowired
	private ICoFarmingType iCoFarmingType;
	
	/** The i coFertilizationMethod. */
	@Autowired
	private ICoFertilizationMethod iCoFertilizationMethod;		
	
	/** The i coFertilizationType. */
	@Autowired
	private ICoFertilizationType iCoFertilizationType;		

	/** The i coFertilizers. */
	@Autowired
	private ICoFertilizer iCoFertilizers;		

	/** The i coFertilizerType. */
	@Autowired
	private ICoFertilizerType iCoFertilizerType;		

	/** The i coIrrigationType. */
	@Autowired
	private ICoIrrigationType iCoIrrigationType;		

	/** The i coLaborType. */
	@Autowired
	private ICoLaborType iCoLaborType;		

	/** The i coLandCover. */
	@Autowired
	private ICoLandCover iCoLandCover;		

	/** The i coLandTenure. */
	@Autowired
	private ICoLandTenure iCoLandTenure;		

	/** The i coOrganicFarming. */
	@Autowired
	private ICoOrganicFarming iCoOrganicFarming;		

	/** The i coPhytosanitary. */
	@Autowired
	private ICoPhytosanitary iCoPhytosanitary;		

	/** The i coPhytosanitaryEquipment. */
	@Autowired
	private ICoPhytosanitaryEquipment iCoPhytosanitaryEquipment;		
	
	/** The i coSeedType. */
	@Autowired
	private ICoSeedType iCoSeedType;		

	/** The i coUnitType. */
	@Autowired
	private ICoUnitType iCoUnitType;		

	/** The i coWaterOrigin. */
	@Autowired
	private ICoWaterOrigin iCoWaterOrigin;			

	/** The i coWorkunitType. */
	@Autowired
	private ICoWorkunitType iCoWorkunitType;			
	

	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "activeMaterials")
	@RequestMapping(value = "activeMaterials", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Active Materials",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<eu.niva4cap.farmregistry.beans.catalogs.CoActMats>> getActMats() {
		try {
			final List<CoActMats> coActMats = iCoActMats.findAll();

			return ResponseEntity.ok(coActMats);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}


	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "activeSubstances")
	@RequestMapping(value = "activeSubstances", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Active Substances",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<eu.niva4cap.farmregistry.beans.catalogs.CoActSus>> getActSubs() {
		try {
			final List<CoActSus> coActSus = iCoActSus.findAll();

			return ResponseEntity.ok(coActSus);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}

	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "countries")
	@RequestMapping(value = "countries", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Countries",  authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<eu.niva4cap.farmregistry.beans.catalogs.CoCountry>> getCountries() {
		try {
			final List<CoCountry> coCountries = iCountry.findAll();

			return ResponseEntity.ok(coCountries);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}
	

	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "cropProducts")
	@RequestMapping(value = "cropProducts", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Crop Products", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<eu.niva4cap.farmregistry.beans.catalogs.CoCropProduct>> getCropProducts() {
		try {
			final List<CoCropProduct> coCropProducts = iCoCropProduct.findAll();

			return ResponseEntity.ok(coCropProducts);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "cropProductGroups")
	@RequestMapping(value = "cropProductGroups", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Crop Product Groups", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoCropProductGroup>> getCropProductGroups() {
		try {
			final List<CoCropProductGroup> coCropProducts = iCoCropProductGroup.findAll();

			return ResponseEntity.ok(coCropProducts);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		

	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "cropVarieties")
	@RequestMapping(value = "cropVarieties", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Crop Varieties", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety>> getCropVarieties() {
		try {
			final List<CoCropVariety> coCropVarieties = iCoCropVariety.findAll();

			return ResponseEntity.ok(coCropVarieties);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "cultivationDetails")
	@RequestMapping(value = "cultivationDetails", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Cultivation Details", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoCultivationDetail>> getCultivationDetails() {
		try {
			final List<CoCultivationDetail> coCultivationdetail = iCoCultivationDetail.findAll();

			return ResponseEntity.ok(coCultivationdetail);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "cultivationSystems")
	@RequestMapping(value = "cultivationSystems", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Cultivation Systems", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoCultivationSystem>> getCultivationSystems() {
		try {
			final List<CoCultivationSystem> coCultivationsystem = iCoCultivationSystem.findAll();

			return ResponseEntity.ok(coCultivationsystem);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "farmerTypes")
	@RequestMapping(value = "farmerTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all FarmerTypes", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFarmerType>> getFarmerTypes() {
		try {
			final List<CoFarmerType> coFarmerType = iCoFarmerType.findAll();

			return ResponseEntity.ok(coFarmerType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "farmingTypes")
	@RequestMapping(value = "farmingTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all FarmingTypes", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFarmingType>> getFarmingTypes() {
		try {
			final List<CoFarmingType> coFarmingType = iCoFarmingType.findAll();

			return ResponseEntity.ok(coFarmingType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "fertilizationMethods")
	@RequestMapping(value = "fertilizationMethods", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Fertilization Methods", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFertilizationMethod>> getFertilizationMethods() {
		try {
			final List<CoFertilizationMethod> coFertilizationMethod = iCoFertilizationMethod.findAll();

			return ResponseEntity.ok(coFertilizationMethod);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "fertilizationTypes")
	@RequestMapping(value = "fertilizationTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Fertilization Types", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFertilizationType>> getFertilizationTypes() {
		try {
			final List<CoFertilizationType> coFertilizationType = iCoFertilizationType.findAll();

			return ResponseEntity.ok(coFertilizationType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "fertilizers")
	@RequestMapping(value = "fertilizers", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Fertilizers", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFertilizer>> getFertilizers() {
		try {
			final List<CoFertilizer> coFertilizer = iCoFertilizers.findAll();

			return ResponseEntity.ok(coFertilizer);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "fertilizerTypes")
	@RequestMapping(value = "fertilizerTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Fertilizer Type", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoFertilizerType>> getFertilizerTypes() {
		try {
			final List<CoFertilizerType> coFertilizerType = iCoFertilizerType.findAll();

			return ResponseEntity.ok(coFertilizerType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "irrigationTypes")
	@RequestMapping(value = "irrigationTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Irrigation Type", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoIrrigationType>> getIrrigationTypes() {
		try {
			final List<CoIrrigationType> coIrrigationType = iCoIrrigationType.findAll();

			return ResponseEntity.ok(coIrrigationType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "laborTypes")
	@RequestMapping(value = "laborTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Labor Type", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoLaborType>> getLaborTypes() {
		try {
			final List<CoLaborType> coLaborType = iCoLaborType.findAll();

			return ResponseEntity.ok(coLaborType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
		
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "landCovers")
	@RequestMapping(value = "landCovers", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Land Covers", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoLandCover>> getLandCovers() {
		try {
			final List<CoLandCover> coLandCover = iCoLandCover.findAll();

			return ResponseEntity.ok(coLandCover);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
		
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "landTenures")
	@RequestMapping(value = "landTenures", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Land Tenures", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoLandTenure>> getLandTenures() {
		try {
			final List<CoLandTenure> coLandTenure = iCoLandTenure.findAll();

			return ResponseEntity.ok(coLandTenure);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "organicFarmings")
	@RequestMapping(value = "organicFarmings", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Organic Farmings", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoOrganicFarming>> getOrganicFarmings() {
		try {
			final List<CoOrganicFarming> coOrganicFarming = iCoOrganicFarming.findAll();

			return ResponseEntity.ok(coOrganicFarming);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}			

	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "phytosanitaries")
	@RequestMapping(value = "phytosanitaries", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Phytosanitaries", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoPhytosanitary>> getPhytosanitaries() {
		try {
			final List<CoPhytosanitary> coPhytosanitary = iCoPhytosanitary.findAll();

			return ResponseEntity.ok(coPhytosanitary);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}			

	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "phytosanitaryEquipments")
	@RequestMapping(value = "phytosanitaryEquipments", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Phytosanitary Equipments", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoPhytosanitaryEquipment>> getPhytosanitaryEquipment() {
		try {
			final List<CoPhytosanitaryEquipment> coPhytosanitaryEquipment = iCoPhytosanitaryEquipment.findAll();

			return ResponseEntity.ok(coPhytosanitaryEquipment);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}			

	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "seedTypes")
	@RequestMapping(value = "seedTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Seed Types", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoSeedType>> getSeedTypes() {
		try {
			final List<CoSeedType> coSeedType = iCoSeedType.findAll();

			return ResponseEntity.ok(coSeedType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}			
	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "unitTypes")
	@RequestMapping(value = "unitTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Unit Types", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoUnitType>> getUnitTypes() {
		try {
			final List<CoUnitType> coUnitType = iCoUnitType.findAll();

			return ResponseEntity.ok(coUnitType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}			
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "waterOrigins")
	@RequestMapping(value = "waterOrigins", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Water Origins", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoWaterOrigin>> getWaterOrigins() {
		try {
			final List<CoWaterOrigin> coWaterOrigin = iCoWaterOrigin.findAll();

			return ResponseEntity.ok(coWaterOrigin);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}	
	
	/*
	 *
	 * @return the response entity
	 */
	@GetMapping(value = "workunitTypes")
	@RequestMapping(value = "workunitTypes", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
	@ApiOperation(value = "Returns all Workunit Types", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<CoWorkunitType>> getWorkunitTypes() {
		try {
			final List<CoWorkunitType> coWorkunitType = iCoWorkunitType.findAll();

			return ResponseEntity.ok(coWorkunitType);
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.ok(null);
		}
	}		
	

}
