/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.CropIrrigation;
import eu.niva4cap.farmregistry.beans.pks.CropIrrigationPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropIrrigation.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPIRRIGATION")
@Table(name = "RP_DCROPIRRIGATION")
@JsonPropertyOrder({"id_irrigation_type", "id_water_origin", "volume", "id_unit_type",
        "irrigation_date"})
public class RpCropIrrigation {
    
    /** The crop irrigation PK. */
    @Valid
    protected @EmbeddedId CropIrrigationPK cropIrrigationPK = new CropIrrigationPK();

    /** The id crop parcel. */
    @Column(name = "RCP_ID", insertable = false, updatable = false)
    private UUID idCropParcel;

    /** The id irrigation type. */
    @Column(name = "IRR_ID")
    @JsonView(JsonViews.Detailed.class)
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.Type is null")
    private Integer idIrrigationType;

    /** The id water origin. */
    @Column(name = "WAO_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idWaterOrigin;

    /** The volume. */
    @Column(name = "CRI_VOLUME")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 6, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.Volume is too long")
    private BigDecimal volume;

    /** The id unit type. */
    @Column(name = "UNI_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idUnitType;

    /** The irrigation date. */
    @Column(name = "CRI_IRRIGATIONDATE", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date irrigationDate;

    /** The crop parcel. */
    @MapsId(value = "idCropParcel")
    @ManyToOne
    @JoinColumn(name = "RCP_ID")
    private RpCropParcel cropParcel;

    /**
     * Gets the crop parcel.
     *
     * @return the crop parcel
     */
    @JsonIgnore
    public RpCropParcel getCropParcel() {
        return cropParcel;
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    @JsonIgnore
    public UUID getIdCropParcel() {
        return cropIrrigationPK.getIdCropParcel();
    }

    /**
     * Gets the id irrigation type.
     *
     * @return the id irrigation type
     */
    public Integer getIdIrrigationType() {
        return idIrrigationType;
    }

    /**
     * Gets the id unit type.
     *
     * @return the id unit type
     */
    public Integer getIdUnitType() {
        return idUnitType;
    }

    /**
     * Gets the id water origin.
     *
     * @return the id water origin
     */
    public Integer getIdWaterOrigin() {
        return idWaterOrigin;
    }

    /**
     * Gets the irrigation date.
     *
     * @return the irrigation date
     */
    public Date getIrrigationDate() {
        return cropIrrigationPK.getIrrigationDate();
    }

    /**
     * Gets the volume.
     *
     * @return the volume
     */
    public BigDecimal getVolume() {
        return volume;
    }

    /**
     * Parsea el crop irrigation.
     *
     * @param cropIrrigation the crop irrigation
     */
    public void parseCropIrrigation(final CropIrrigation cropIrrigation) {
        setIdIrrigationType(cropIrrigation.getType());
        setIdWaterOrigin(cropIrrigation.getWaterOrigin());
        setVolume(cropIrrigation.getVolume());
        setIdUnitType(cropIrrigation.getUnit());
        setIrrigationDate(cropIrrigation.getDate());
    }

    /**
     * Sets the crop parcel.
     *
     * @param cropParcel the new crop parcel
     */
    public void setCropParcel(final RpCropParcel cropParcel) {
        this.cropParcel = cropParcel;
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        cropIrrigationPK.setIdCropParcel(idCropParcel);
    }

    /**
     * Sets the id irrigation type.
     *
     * @param idIrrigationType the new id irrigation type
     */
    @JsonProperty(value = "id_irrigation_type")
    public void setIdIrrigationType(final Integer idIrrigationType) {
        this.idIrrigationType = idIrrigationType;
    }

    /**
     * Sets the id unit type.
     *
     * @param idUnitType the new id unit type
     */
    @JsonProperty(value = "id_unit_type")
    public void setIdUnitType(final Integer idUnitType) {
        this.idUnitType = idUnitType;
    }

    /**
     * Sets the id water origin.
     *
     * @param idWaterOrigin the new id water origin
     */
    @JsonProperty(value = "id_water_origin")
    public void setIdWaterOrigin(final Integer idWaterOrigin) {
        this.idWaterOrigin = idWaterOrigin;
    }

    /**
     * Sets the irrigation date.
     *
     * @param irrigationDate the new irrigation date
     */
    @JsonProperty(value = "irrigation_date")
    public void setIrrigationDate(final Date irrigationDate) {
        cropIrrigationPK.setIrrigationDate(irrigationDate);
    }

    /**
     * Sets the volume.
     *
     * @param volume the new volume
     */
    @JsonProperty(value = "volume")
    public void setVolume(final BigDecimal volume) {
        this.volume = volume;
    }
}