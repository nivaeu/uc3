/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class CropFertilizerPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class CropFertilizerPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3142496344176965094L;

    /** The id crop parcel. */
    @Column(name = "RCP_id")
    private UUID idCropParcel;

    /** The id fertilizer type. */
    @Column(name = "FET_id")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.Type is null")
    private Integer idFertilizerType;

    /** The start date. */
    @Column(name = "CFE_startdate")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.StartDate is null")
    private Date startDate;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CropFertilizerPK other = (CropFertilizerPK) obj;
        if (idCropParcel == null) {
            if (other.idCropParcel != null) {
                return false;
            }
        } else if (!idCropParcel.equals(other.idCropParcel)) {
            return false;
        }
        if (idFertilizerType == null) {
            if (other.idFertilizerType != null) {
                return false;
            }
        } else if (!idFertilizerType.equals(other.idFertilizerType)) {
            return false;
        }
        if (startDate == null) {
            if (other.startDate != null) {
                return false;
            }
        } else if (!startDate.equals(other.startDate)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    public UUID getIdCropParcel() {
        return idCropParcel;
    }

    /**
     * Gets the id fertilizer type.
     *
     * @return the id fertilizer type
     */
    public Integer getIdFertilizerType() {
        return idFertilizerType;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate == null ? null : (Date) startDate.clone();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((idCropParcel == null) ? 0 : idCropParcel.hashCode());
        result = prime * result + ((idFertilizerType == null) ? 0 : idFertilizerType.hashCode());
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        return result;
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel
     *            the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        this.idCropParcel = idCropParcel;
    }

    /**
     * Sets the id fertilizer type.
     *
     * @param idFertilizerType
     *            the new id fertilizer type
     */
    public void setIdFertilizerType(final Integer idFertilizerType) {
        this.idFertilizerType = idFertilizerType;
    }

    /**
     * Sets the start date.
     *
     * @param startDate
     *            the new start date
     */
    public void setStartDate(final Date startDate) {
        this.startDate = (Date) startDate.clone();
    }
}