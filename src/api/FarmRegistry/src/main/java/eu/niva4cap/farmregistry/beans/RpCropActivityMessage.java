/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class Activity.
 *
 * @author irodri11
 * @version 1.0.0, 10-jun-2021
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "AgriculturalProducerPartyCountry", "AgriculturalProducerPartyPersonalID",
		"AgriculturalProductionUnitCode", "AgriculturalProductionUnitKey", "AgriculturalBlockKey", "ReferenceParcel",
		"ReferenceCropParcelKey", "AgriculturalActivity", "CultivationSystem", "CultivationDetail", "SeedType",
		"OrganicFarming", "LaborType", "StartDate", "EndDate" })
public class RpCropActivityMessage {

	/** The agricultural producer party country. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "ES")
	private String country;

	/** The agricultural producer party personalID. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "01010101A")
	private String personalID;

	/** The agricultural production unit code. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "ES000000000001")
	private String productionUnitCode;

	/** The agricultural production unit key. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "08080808")
	private String productionUnitKey;

	/** The agricultural block key. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "0000001")
	private String agriculturalBlockKey;

	/** The reference parcel. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "15218A049302610000IS")
	private String referenceParcel;

	/** The reference crop parcel key. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "FC_123330")
	private String referenceCropParcelKey;

	/** The agricultural activity. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "true")
	private Boolean agriculturalActivity;

	/** The cultivation system. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "2")
	private Integer cultivationSystem;

	/** The cultivation detail. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "5")
	private Integer cultivationDetail;

	/** The seed type. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "6")
	private Integer seedType;

	/** The organic farming. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "1")
	private Integer organicFarming;

	/** The labor type. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "1")
	private Integer laborType;

	/** The start date. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "2020-01-01")
	private Date startDate;

	/** The end date. */
	@JsonView(JsonViews.Detailed.class)
	@ApiModelProperty(example = "2021-01-01")
	private Date endDate;

	/**
	 * Parsea el activity message.
	 *
	 * @param cropActivityMessage
	 *            the crop activity message
	 */
	public void parseActivityMessage(RpCropActivityMessage cropActivityMessage) {
		setCountry(cropActivityMessage.getCountry());
		setPersonalID(cropActivityMessage.getPersonalID());
		setProductionUnitCode(cropActivityMessage.getProductionUnitCode());
		setProductionUnitKey(cropActivityMessage.getProductionUnitKey());
		setAgriculturalBlockKey(cropActivityMessage.getAgriculturalBlockKey());
		setReferenceParcel(cropActivityMessage.getReferenceParcel());
		setReferenceCropParcelKey(cropActivityMessage.getReferenceCropParcelKey());
		setAgriculturalActivity(cropActivityMessage.getAgriculturalActivity());
		setCultivationSystem(cropActivityMessage.getCultivationSystem());
		setCultivationDetail(cropActivityMessage.getCultivationDetail());
		setSeedType(cropActivityMessage.getSeedType());
		setOrganicFarming(cropActivityMessage.getOrganicFarming());
		setLaborType(cropActivityMessage.getLaborType());
		setStartDate(cropActivityMessage.getStartDate());
		setEndDate(cropActivityMessage.getEndDate());
	}

	
	
	/**
	 * Gets the country.
	 *
	 * @return the country
	 */	
	public String getCountry() {
		return country;
	}



	/**
	 * Sets the country.
	 *
	 * @param country
	 *            the new country
	 */
	@JsonProperty(value = "AgriculturalProducerPartyCountry")
	public void setCountry(String country) {
		this.country = country;
	}



	/**
	 * Gets the personalID.
	 *
	 * @return the personalID
	 */
	public String getPersonalID() {
		return personalID;
	}



	/**
	 * Sets the personalID.
	 *
	 * @param personalID
	 *            the new personalID
	 */
	@JsonProperty(value = "AgriculturalProducerPartyPersonalID")
	public void setPersonalID(String personalID) {
		this.personalID = personalID;
	}



	/**
	 * Gets the production unit code.
	 *
	 * @return the production unit code
	 */
	public String getProductionUnitCode() {
		return productionUnitCode;
	}



	/**
	 * Sets the productionUnitCode.
	 *
	 * @param productionUnitCode
	 *            the new productionUnitCode
	 */
	@JsonProperty(value = "AgriculturalProductionUnitCode")
	public void setProductionUnitCode(String productionUnitCode) {
		this.productionUnitCode = productionUnitCode;
	}



	/**
	 * Gets the production unit key.
	 *
	 * @return the production unit key
	 */
	public String getProductionUnitKey() {
		return productionUnitKey;
	}



	/**
	 * Sets the productionUnitKey.
	 *
	 * @param productionUnitKey
	 *            the new productionUnitKey
	 */
	@JsonProperty(value = "AgriculturalProductionUnitKey")
	public void setProductionUnitKey(String productionUnitKey) {
		this.productionUnitKey = productionUnitKey;
	}



	/**
	 * Gets the agricultural block key.
	 *
	 * @return the agricultural block key
	 */
	public String getAgriculturalBlockKey() {
		return agriculturalBlockKey;
	}



	/**
	 * Sets the agriculturalBlockKey.
	 *
	 * @param agriculturalBlockKey
	 *            the new agriculturalBlockKey
	 */
	@JsonProperty(value = "AgriculturalBlockKey")
	public void setAgriculturalBlockKey(String agriculturalBlockKey) {
		this.agriculturalBlockKey = agriculturalBlockKey;
	}



	/**
	 * Gets the reference parcel.
	 *
	 * @return the reference parcel
	 */
	public String getReferenceParcel() {
		return referenceParcel;
	}



	/**
	 * Sets the referenceParcel.
	 *
	 * @param referenceParcel
	 *            the new referenceParcel
	 */
	@JsonProperty(value = "ReferenceParcel")
	public void setReferenceParcel(String referenceParcel) {
		this.referenceParcel = referenceParcel;
	}



	/**
	 * Gets the reference crop parcel key.
	 *
	 * @return the reference crop parcel key
	 */
	public String getReferenceCropParcelKey() {
		return referenceCropParcelKey;
	}



	/**
	 * Sets the referenceCropParcelKey.
	 *
	 * @param referenceCropParcelKey
	 *            the new referenceCropParcelKey
	 */
	@JsonProperty(value = "ReferenceCropParcelKey")
	public void setReferenceCropParcelKey(String referenceCropParcelKey) {
		this.referenceCropParcelKey = referenceCropParcelKey;
	}



	/**
	 * Gets the labor type.
	 *
	 * @return the labor type
	 */
	public Integer getLaborType() {
		return laborType;
	}



	/**
	 * Sets the laborType.
	 *
	 * @param laborType
	 *            the new laborType
	 */
	@JsonProperty(value = "LaborType")
	public void setLaborType(Integer laborType) {
		this.laborType = laborType;
	}



	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate == null ? null : (Date) startDate.clone();
	}



	/**
	 * Sets the startDate.
	 *
	 * @param startDate
	 *            the new startDate
	 */
	@JsonProperty(value = "StartDate")
	public void setStartDate(final Date startDate) {
		this.startDate = startDate == null ? null : (Date) startDate.clone();
	}



	/**
	 * Gets the end date.
	 *
	 * @return the end date
	 */
	public Date getEndDate() {
		return endDate == null ? null : (Date) endDate.clone();
	}



	/**
	 * Sets the endDate.
	 *
	 * @param endDate
	 *            the new endDate
	 */
	@JsonProperty(value = "EndDate")
	public void setEndDate(final Date endDate) {
		this.endDate = endDate == null ? null : (Date) endDate.clone();
	}




	/**
	 * Gets the agricultural activity.
	 *
	 * @return the agricultural activity
	 */
	public Boolean getAgriculturalActivity() {
		return agriculturalActivity;
	}

	/**
	 * Sets the agricultural activity.
	 *
	 * @param agriculturalActivity
	 *            the new agricultural activity
	 */
	@JsonProperty(value = "AgriculturalActivity")
	public void setAgriculturalActivity(Boolean agriculturalActivity) {
		this.agriculturalActivity = agriculturalActivity;
	}

	/**
	 * Gets the cultivation system.
	 *
	 * @return the cultivation system
	 */
	public Integer getCultivationSystem() {
		return cultivationSystem;
	}

	/**
	 * Sets the cultivation system.
	 *
	 * @param cultivationSystem
	 *            the new cultivation system
	 */
	@JsonProperty(value = "CultivationSystem")
	public void setCultivationSystem(Integer cultivationSystem) {
		this.cultivationSystem = cultivationSystem;
	}

	/**
	 * Gets the cultivation detail.
	 *
	 * @return the cultivation detail
	 */
	public Integer getCultivationDetail() {
		return cultivationDetail;
	}

	/**
	 * Sets the cultivation detail.
	 *
	 * @param cultivationDetail
	 *            the new cultivation detail
	 */
	@JsonProperty(value = "CultivationDetail")
	public void setCultivationDetail(Integer cultivationDetail) {
		this.cultivationDetail = cultivationDetail;
	}

	/**
	 * Gets the seed type.
	 *
	 * @return the seed type
	 */
	public Integer getSeedType() {
		return seedType;
	}

	/**
	 * Sets the seed type.
	 *
	 * @param seedType
	 *            the new seed type
	 */
	@JsonProperty(value = "SeedType")
	public void setSeedType(Integer seedType) {
		this.seedType = seedType;
	}

	/**
	 * Gets the organic farming.
	 *
	 * @return the organic farming
	 */
	public Integer getOrganicFarming() {
		return organicFarming;
	}

	/**
	 * Sets the organic farming.
	 *
	 * @param organicFarming
	 *            the new organic farming
	 */
	@JsonProperty(value = "OrganicFarming")
	public void setOrganicFarming(Integer organicFarming) {
		this.organicFarming = organicFarming;
	}
}
