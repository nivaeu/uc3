/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.revisions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

/**
 * The Class UserRevisionEntity.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "REV_DREVINFO")
@RevisionEntity(UserRevisionListener.class)
public class UserRevisionEntity {

    /** The rev. */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_REV_ENVERS")
    @SequenceGenerator(name = "SEQ_REV_ENVERS", sequenceName = "SEQ_REV_ENVERS", allocationSize = 1)
    @RevisionNumber
    @Column(name = "rev")
    private Integer rev;

    /** The revtstmp. */
    @RevisionTimestamp
    @Column(name = "rev_tstmp")
    private Long revtstmp;

    /** The revuser. */
    @Column(name = "rev_user")
    private Integer revuser;

    /**
     * Gets the rev.
     *
     * @return the rev
     */
    public Integer getRev() {
        return rev;
    }

    /**
     * Gets the revtstmp.
     *
     * @return the revtstmp
     */
    public Long getRevtstmp() {
        return revtstmp;
    }

    /**
     * Gets the revuser.
     *
     * @return the revuser
     */
    public Integer getRevuser() {
        return revuser;
    }

    /**
     * Sets the rev.
     *
     * @param rev
     *            the new rev
     */
    public void setRev(final Integer rev) {
        this.rev = rev;
    }

    /**
     * Sets the revtstmp.
     *
     * @param revtstmp
     *            the new revtstmp
     */
    public void setRevtstmp(final Long revtstmp) {
        this.revtstmp = revtstmp;
    }

    /**
     * Sets the revuser.
     *
     * @param revUser
     *            the new revuser
     */
    public void setRevuser(final Integer revUser) {
        this.revuser = revUser;
    }
}