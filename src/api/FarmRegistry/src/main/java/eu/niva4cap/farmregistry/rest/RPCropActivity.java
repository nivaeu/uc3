/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropActivity;
import eu.niva4cap.farmregistry.repositories.IRpCropActivity;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class RPCropActivity.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_CROP_ACTIVITY, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Crop Activities")
@ApiIgnore
public class RPCropActivity
{
	
	/** The i crop activity. */
	@Autowired
	private IRpCropActivity iCropActivity;

	/**
	 * Obtiene el.
	 *
	 * @return the response entity
	 */
	@GetMapping
	@ApiOperation(value = "Returns all Crop Activities" , authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropActivity>> get()
	{
		try
		{
			return ResponseEntity.ok(iCropActivity.findAll());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}
}