/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import eu.niva4cap.farmregistry.beans.RpCropActivity;
import eu.niva4cap.farmregistry.beans.RpCropActivityMessage;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.utils.JsonViews;

@JsonInclude(Include.NON_NULL)

/**
 * The Class CropActivityMessage.
 *
 * @author irodri11
 * @version 1.0.0,  10-jun-2021
 * @since JDK 1.6
 */
public class CropActivityMessage {
	
	/** The crop activity. */
	@JsonView(JsonViews.FullDetailed.class)
	@NotEmpty(message = "RpCropActivityMessage is null or empty")
	private List<RpCropActivityMessage> rpcropActivityMessage;
	

	/**
	 * Gets the crop activity
	 *
	 * @return the crop activity
	 */
	public List<RpCropActivityMessage> getRpcropActivityMessage(){
		return rpcropActivityMessage;
	}
	
	@JsonProperty(value = "CropActivities")
	public void setPRpcropActivityMassage(List<RpCropActivityMessage> rpcropActivityMessage) {
		this.rpcropActivityMessage = rpcropActivityMessage;
	
	}
	
}
