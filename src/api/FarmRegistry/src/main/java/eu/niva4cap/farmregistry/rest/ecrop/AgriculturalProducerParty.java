/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.ecrop;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrFarmerData;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProductionUnit;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class AgriculturalProducerParty.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping(value = ConstantesRest.REST_QUERY_AGRICULTURAL_PRODUCER_PARTIES, produces = {ConstantesRest.REST_PRODUCES})
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
@Api(tags = "Agricultural Producer Parties")
public class AgriculturalProducerParty {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AgriculturalProducerParty.class);

    /** The i farmer. */
    @Autowired
    private IPrFarmer iFarmer;

    /**
     * Delete.
     *
     * @param code the code
     * @return the response entity
     */
    @DeleteMapping(value = "{code}")
    @ApiIgnore
    @ApiOperation(value = "Deletes an Agricultural Producer Party by its code", authorizations = { @Authorization(value="BearerToken") })
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Response> delete(@PathVariable("code") final String code) {
        final Response response = new Response();

        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCode(code);

            if (!farmer.isPresent()) {
                response.addError("Agricultural Producer Party not found: " + code);

                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return ResponseEntity.badRequest().body(response);
            }

            else {
                iFarmer.delete(farmer.get());

                response.setStatus(HttpStatus.OK.value());
                return ResponseEntity.ok().body(response);
            }
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            response.addError("Internal error in WebService");
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    /**
     * Obtiene el.
     *
     * @param params the params
     * @return the response entity
     */
    @GetMapping
	@RequestMapping(method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Agricultural Producer Parties codes",  authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.Simple.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty>>
            get(@RequestParam final Map<String, String> params) {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty> agriculturalProducerParties = new ArrayList<>();

            final PrFarmer farmer = new PrFarmer();
            farmer.setFarmerData(new PrFarmerData());

            if (params.get("Country") != null) {
                farmer.setIdCountry(params.get("Country"));
            }

            if (params.get("Name") != null) {
                farmer.getFarmerData().setName(params.get("Name"));
            }

            if (params.get("Classification") != null) {
                farmer.getFarmerData()
                        .setIdFarmerType(Integer.parseInt(params.get("Classification")));

            }

            if (params.get("PersonalID") != null) {
                farmer.getFarmerData().setPersonalId(params.get("PersonalID"));
            }

            final List<PrFarmer> prFarmers = iFarmer.findAll(Example.of(farmer));

            for (final PrFarmer prFarmer : prFarmers) {
                final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty agriculturalProducerParty = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty();
                agriculturalProducerParties.add(agriculturalProducerParty);

                agriculturalProducerParty.parseFarmerCodeList(prFarmer);
            }

            return ResponseEntity.ok(agriculturalProducerParties);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    /**
     * Obtiene el.
     *
     * @param params the params
     * @return the response entity
     */
    @GetMapping(value = "/details")
	@RequestMapping(value = "details", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Agricultural Producer Parties main details", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty>>
            getDetails(@RequestParam final Map<String, String> params) {
        try {
            final List<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty> agriculturalProducerParties = new ArrayList<>();

            final PrFarmer farmer = new PrFarmer();
            farmer.setFarmerData(new PrFarmerData());

            if (params.get("Country") != null) {
                farmer.setIdCountry(params.get("Country"));
            }

            if (params.get("Name") != null) {
                farmer.getFarmerData().setName(params.get("Name"));
            }

            if (params.get("Classification") != null) {
                farmer.getFarmerData()
                        .setIdFarmerType(Integer.parseInt(params.get("Classification")));

            }

            if (params.get("PersonalID") != null) {
                farmer.getFarmerData().setPersonalId(params.get("PersonalID"));
            }

            final List<PrFarmer> prFarmers = iFarmer.findAll(Example.of(farmer));

            for (final PrFarmer prFarmer : prFarmers) {
                final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty agriculturalProducerParty = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty();
                agriculturalProducerParties.add(agriculturalProducerParty);

                agriculturalProducerParty.parseFarmerCodeList(prFarmer);
            }

            return ResponseEntity.ok(agriculturalProducerParties);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
    
    /**
     * Gets the agricultural production unit by code.
     *
     * @param code the code
     * @param productionUnitKey the production unit key
     * @return the agricultural production unit by code
     */
    @GetMapping(value = "/code/{code}/AgriculturalProductionUnits/{productionUnitKey}")
	@RequestMapping(value = "/code/{code}/AgriculturalProductionUnits/{productionUnitKey}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Agricultural Production Unit by its code of an Agricultural Producer Party by its key", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<AgriculturalProductionUnit> getAgriculturalProductionUnitByCode(
            @PathVariable("code") final String code,
            @PathVariable("productionUnitKey") final String productionUnitKey) {
        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCode(code);

            if (!farmer.isPresent()) {
                return ResponseEntity.ok(null);
            }

            final PrFarmer prFarmer = farmer.get();

            final PrProductionUnit prProductionUnit = prFarmer
                    .getProductionUnitByKey(productionUnitKey);

            if (prProductionUnit == null) {
                return ResponseEntity.ok(null);
            }

            final AgriculturalProductionUnit agriculturalProductionUnit = new AgriculturalProductionUnit();

            agriculturalProductionUnit.parseProductionUnit(prProductionUnit);

            return ResponseEntity.ok(agriculturalProductionUnit);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    /**
     * Gets the agricultural production units by code.
     *
     * @param code the code
     * @return the agricultural production units by code
     */
    @GetMapping(value = "/code/{code}/AgriculturalProductionUnits")
	@RequestMapping(value = "/code/{code}/AgriculturalProductionUnits", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns all Agricultural Production Units of an Agricultural Producer Party by its code", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<AgriculturalProductionUnit>>
            getAgriculturalProductionUnitsByCode(@PathVariable("code") final String code) {
        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCode(code);

            if (!farmer.isPresent()) {
                return ResponseEntity.ok(null);
            }

            final PrFarmer prFarmer = farmer.get();

            final List<AgriculturalProductionUnit> agriculturalProductionUnits = new ArrayList<>();

            for (final PrProductionUnit prProductionUnit : prFarmer.getProductionUnits()) {
                final AgriculturalProductionUnit agriculturalProductionUnit = new AgriculturalProductionUnit();
                agriculturalProductionUnits.add(agriculturalProductionUnit);

                agriculturalProductionUnit.parseProductionUnit(prProductionUnit);
            }

            return ResponseEntity.ok(agriculturalProductionUnits);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    /**
     * Gets the by code.
     *
     * @param code the code
     * @return the by code
     */
    @GetMapping(value = "/code/{code}")
	@RequestMapping(value = "/code/{code}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Agricultural Producer Party by its Farm Registry Code", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty>
            getByCode(@PathVariable("code") final String code) {
        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCode(code);

            if (!farmer.isPresent()) {
                return ResponseEntity.ok(null);
            }

            final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty agriculturalProducerParty = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty();

            agriculturalProducerParty.parseFarmer(farmer.get());

            return ResponseEntity.ok(agriculturalProducerParty);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    /**
     * Gets the by personal id country.
     *
     * @param country the country
     * @return the country
     */
    @GetMapping(value = "/country/{country}")
	@RequestMapping(value = "/country/{country}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Agricultural Producer Party by its Country and Personal ID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.Detailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<List<PrFarmer>> getByCountry(@PathVariable("country") final String country) {
        try {
			Optional<List<PrFarmer>> beanDB = iFarmer.findByCountry(country);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }    
    
    /**
     * Gets the by personal id country.
     *
     * @param country the country
     * @param personalID the personal ID
     * @return the by personal id country
     */
    @GetMapping(value = "/country/{country}/personalID/{personalID}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Agricultural Producer Party by its Country and Personal ID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
    public ResponseEntity<eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty>
            getByPersonalIdCountry(@PathVariable("country") final String country,
                    @PathVariable("personalID") final String personalID) {
        try {
            final Optional<PrFarmer> farmer = iFarmer.findByCountryAndPersonalID(country,
                    personalID);

            if (!farmer.isPresent()) {
                return ResponseEntity.ok(null);
            }

            final eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty agriculturalProducerParty = new eu.niva4cap.farmregistry.beans.ecrop.AgriculturalProducerParty();

            agriculturalProducerParty.parseFarmer(farmer.get());

            return ResponseEntity.ok(agriculturalProducerParty);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}