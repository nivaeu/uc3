/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoFertilizer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PFERTILIZER")
@JsonPropertyOrder({"id", "name", "id_fertilizer_type", "expiry_date"})
public class CoFertilizer {
    
    /** The id. */
    @Id
    @Column(name = "FER_ID")
    @ApiModelProperty(example = "4")
    @JsonView(JsonViews.Simple.class)
    private Integer id;

    /** The name. */
    @Column(name = "FER_NAME")
    @ApiModelProperty(example = "Organic nitrogen fertiliser of animal origin")
    @JsonView(JsonViews.Simple.class)
    private String name;

    /** The id fertilizer type. */
    @Column(name = "FET_ID")
    @ApiModelProperty(example = "2")
    @JsonView(JsonViews.Simple.class)
    private Integer idFertilizerType;

    /** The expiry date. */
    @Column(name = "FER_EXPIRYDATE")
    @ApiModelProperty(example = "2025-12-31")
    @JsonView(JsonViews.Detailed.class)
    private Date expiryDate;

    /**
     * Gets the expiry date.
     *
     * @return the expiry date
     */
    @JsonProperty(value = "expiry_date")
    public Date getExpiryDate() {
        return expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public Integer getId() {
        return id;
    }

    /**
     * Gets the id fertilizer type.
     *
     * @return the id fertilizer type
     */
    @JsonProperty(value = "id_fertilizer_type")
    public Integer getIdFertilizerType() {
        return idFertilizerType;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
     * Sets the expiry date.
     *
     * @param expiryDate the new expiry date
     */
    public void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Sets the id fertilizer type.
     *
     * @param idFertilizerType the new id fertilizer type
     */
    public void setIdFertilizerType(final Integer idFertilizerType) {
        this.idFertilizerType = idFertilizerType;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }
}