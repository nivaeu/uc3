/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoCountry.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PCOUNTRY")
@JsonPropertyOrder({"id", "name", "expiry_date"})
public class CoCountry {

    /** The id. */
    @Id
    @Column(name = "COU_ID")
    @ApiModelProperty(example = "ES")
    @JsonView(JsonViews.Simple.class)
    private String id;

    /** The name. */
    @Column(name = "COU_NAME")
    @ApiModelProperty(example = "Spain")
    @JsonView(JsonViews.Simple.class)
    private String name;

    /** The expiry date. */
    @Column(name = "COU_EXPIRYDATE")
    @ApiModelProperty(example = "2025-12-31")
    @JsonView(JsonViews.Detailed.class)
    private Date expiryDate;

    /**
     * Gets the expiry date.
     *
     * @return the expiry date
     */
    @JsonProperty(value = "expiry_date")
    public Date getExpiryDate() {
        return expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public String getId() {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
     * Sets the expiry date.
     *
     * @param expiryDate
     *            the new expiry date
     */
    public void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the new name
     */
    public void setName(final String name) {
        this.name = name;
    }
}