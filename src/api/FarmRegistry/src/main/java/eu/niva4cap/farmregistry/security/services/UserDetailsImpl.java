/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.security.services;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import eu.niva4cap.farmregistry.security.beans.Role;
import eu.niva4cap.farmregistry.security.beans.User;

public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 6136647198980822103L;

    public static UserDetailsImpl build(final User user) {
        return new UserDetailsImpl(user.getId(), user.getUsername(), user.getPassword(),
                user.getCountry(), getAuthorities(user));
    }

    private static Set<? extends GrantedAuthority> getAuthorities(final User user) {
        final Set<Role> roles = user.getRoles();
        final Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        for (final Role role : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        }

        return authorities;
    }

    private final Integer id;
    private final String username;
    @JsonIgnore
    private final String password;

    private final String country;

    private final Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(final Integer id, final String username, final String password,
            final String country, final Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.country = country;
        this.authorities = authorities;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getCountry() {
        return country;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}