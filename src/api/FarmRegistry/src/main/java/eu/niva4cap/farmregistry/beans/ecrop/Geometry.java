/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbGeometry;
import eu.niva4cap.farmregistry.beans.RpCropParcelGeometry;
import eu.niva4cap.farmregistry.beans.RpGeometry;
import eu.niva4cap.farmregistry.utils.Coordinates;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class Geometry.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Type", "Coordinates", "Surface", "DateFrom", "DateTo"})
public class Geometry {

    /** The Constant POLYGON. */
    private static final String POLYGON = "Polygon";

    /** The type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "Polygon")
    private String type;
    
    /** The coordinates. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "[\r\n" + 
    		"                        [\r\n" + 
    		"                            [\r\n" + 
    		"                                \"5.41412794564089\",\r\n" + 
    		"                                \"52.1484515916486\"\r\n" + 
    		"                            ],\r\n" + 
    		"                            [\r\n" + 
    		"                                \"5.41784357231444\",\r\n" + 
    		"                                \"52.1547846831759\"\r\n" + 
    		"                            ],\r\n" + 
    		"                            [\r\n" + 
    		"                                \"5.4232748279405\",\r\n" + 
    		"                                \"52.1575155234364\"\r\n" + 
    		"                            ],\r\n" + 
    		"                            [\r\n" + 
    		"                                \"5.41412794564089\",\r\n" + 
    		"                                \"52.1484515916486\"\r\n" + 
    		"                            ]\r\n" + 
    		"                        ]\r\n" + 
    		"                    ]")
    private String[][][] coordinates;
    
    /** The surface. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "9873.25")
    private BigDecimal surface;
    
    /** The date from. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date dateFrom;
    
    /** The date to. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2021-03-24")
    private Date dateTo;

    /**
     * Gets the coordinates.
     *
     * @return the coordinates
     */
    public String[][][] getCoordinates() {
        return coordinates;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the surface.
     *
     * @return the surface
     */
    public BigDecimal getSurface() {
        return surface;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Parsea el geometry.
     *
     * @param geometry the geometry
     */
    public void parseGeometry(final AbGeometry geometry) {
        setType(POLYGON);
        setCoordinates(Coordinates.wkt2GeoJSON(geometry.getGeometry()));
        setSurface(geometry.getSurface());
        setDateFrom(geometry.getDateFrom());
        setDateTo(geometry.getDateTo());
    }

    /**
     * Parsea el geometry.
     *
     * @param geometry the geometry
     */
    public void parseGeometry(final RpCropParcelGeometry geometry) {
        setType(POLYGON);
        setCoordinates(Coordinates.wkt2GeoJSON(geometry.getGeometry()));
        setSurface(geometry.getSurface());
        setDateFrom(geometry.getDateFrom());
        setDateTo(geometry.getDateTo());
    }

    /**
     * Parsea el geometry.
     *
     * @param geometry the geometry
     */
    public void parseGeometry(final RpGeometry geometry) {
        setType(POLYGON);
        setCoordinates(Coordinates.wkt2GeoJSON(geometry.getGeometry()));
        setSurface(geometry.getSurface());
        setDateFrom(geometry.getDateFrom());
        setDateTo(geometry.getDateTo());
    }

    /**
     * Sets the coordinates.
     *
     * @param coordinates the new coordinates
     */
    @JsonProperty(value = "Coordinates")
    public void setCoordinates(final String[][][] coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "DateFrom")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "DateTo")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the surface.
     *
     * @param surface the new surface
     */
    @JsonProperty(value = "Surface")
    public void setSurface(final BigDecimal surface) {
        this.surface = surface;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    @JsonProperty(value = "Type")
    public void setType(final String type) {
        this.type = type;
    }
}