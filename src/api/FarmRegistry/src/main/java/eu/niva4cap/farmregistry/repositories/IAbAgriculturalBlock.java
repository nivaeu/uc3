/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.PrFarmer;

/**
 * The Interface IAbAgriculturalBlock.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IAbAgriculturalBlock extends JpaRepository<AbAgriculturalBlock, UUID>
{

    /**
     * Find by country and personal ID.
     *
     * @param country
     *            the country
     * @param personalID
     *            the personal ID
     * @param apuKey
     *            the apu Key
     * @param key 
     * 			  the agr block key
     * @return the optional
     */
    @Query( value = "SELECT * \r\n" + 
    		"FROM sc_niva.ab_dagrblock A,  \r\n" + 
    		"		sc_niva.pr_dproductionunit P,\r\n" + 
    		"		sc_niva.pr_dfarmer F, \r\n" + 
    		"		sc_niva.pr_dfarmerdata D\r\n" + 
    		"WHERE A.pun_id = P.pun_id AND P.far_id = F.far_id AND F.far_id = D.far_id AND F.cou_id = :country AND D.fad_personalid = :personalID AND P.pun_key = :productUnitKey AND A.abl_key = :key", nativeQuery = true)
    
    List<AbAgriculturalBlock> findByCountryAndPersonalID(@Param("country") String country,
            @Param("personalID") String personalID, @Param("productUnitKey") String productUnitKey,
            @Param("key") String key);
    
    
    /**
     * Find by country and personal ID.
     *
     * @param country
     *            the country
     * @param personalID
     *            the personal ID
     * @param apuKey
     *            the apu Key
     * @return the optional
     */
    @Query( value = "SELECT * \r\n" + 
    		"FROM sc_niva.ab_dagrblock A,  \r\n" + 
    		"		sc_niva.pr_dproductionunit P,\r\n" + 
    		"		sc_niva.pr_dfarmer F, \r\n" + 
    		"		sc_niva.pr_dfarmerdata D\r\n" + 
    		"WHERE A.pun_id = P.pun_id AND P.far_id = F.far_id AND F.far_id = D.far_id AND F.cou_id = :country AND D.fad_personalid = :personalID AND P.pun_key = :productUnitKey", nativeQuery = true)
    
    List<AbAgriculturalBlock> findByCountryAndPersonalID(@Param("country") String country,
            @Param("personalID") String personalID, @Param("productUnitKey") String productUnitKey);
    

}
