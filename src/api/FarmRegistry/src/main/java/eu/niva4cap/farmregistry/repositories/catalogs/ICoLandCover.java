/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories.catalogs;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.catalogs.CoCropProductGroup;
import eu.niva4cap.farmregistry.beans.catalogs.CoLandCover;

/**
 * The Interface ICoLandCover.
 *
 * @author irodri11
 * @version 1.0.0,  27-oct-2021
 * @since JDK 1.6
 */
public interface ICoLandCover extends JpaRepository<CoLandCover, Integer>
{

	@Query( value = "SELECT * FROM  sc_niva.co_plandcover WHERE LAN_ID = :code", nativeQuery = true)


	Optional<List<CoLandCover>> findByCpgId(@Param("code") int i);

	
}