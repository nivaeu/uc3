/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropActivityDetail;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;


/**
 * The Class ActivityDetail.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"LaborType", "StartDate", "EndDate"})
public class ActivityDetail {
    
    /** The labor type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer laborType;
    
    /** The start date. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date startDate;
    
    /** The end date. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2021-03-12")
    private Date endDate;

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the labor type.
     *
     * @return the labor type
     */
    public Integer getLaborType() {
        return laborType;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate == null ? null : (Date) startDate.clone();
    }

    /**
     * Parsea el crop activity detail.
     *
     * @param cropActivityDetail the crop activity detail
     */
    public void parseCropActivityDetail(final RpCropActivityDetail cropActivityDetail) {
        setLaborType(cropActivityDetail.getIdLaborType());
        setStartDate(cropActivityDetail.getStartDate());
        setEndDate(cropActivityDetail.getEndDate());
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    @JsonProperty(value = "EndDate")
    public void setEndDate(final Date endDate) {
        this.endDate = endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Sets the labor type.
     *
     * @param laborType the new labor type
     */
    @JsonProperty(value = "LaborType")
    public void setLaborType(final Integer laborType) {
        this.laborType = laborType;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    @JsonProperty(value = "StartDate")
    public void setStartDate(final Date startDate) {
        this.startDate = startDate == null ? null : (Date) startDate.clone();
    }
}