/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.catalogs;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.beans.catalogs.CoActMats;
import eu.niva4cap.farmregistry.beans.ecrop.ActMatsMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.beans.validators.Validator;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActMats;

import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping(value = ConstantesRest.REST_ACTIVE_MATERIALS, produces = { ConstantesRest.REST_PRODUCES })
@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
@Api(tags = "Active Materials")
public class ActiveMaterials {

	@Autowired
	private Validator validator;

	@Autowired
	private ICoActMats iActMats; 

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ActMatsMessage.class);

	@PostMapping
	@ApiOperation(value = "Inserts data into DB", authorizations = { @Authorization(value="BearerToken") })
	@PreAuthorize("hasRole('WRITE')")
	@Transactional(propagation = Propagation.NEVER)
	public ResponseEntity<Response> create(@RequestBody final ActMatsMessage data) {
		final Response response = new Response();

		try {

			for (final CoActMats coActMats : data.getCoActMats()) {
				CoActMats actMats = new CoActMats();

				if (coActMats.getPhy_id() != null && coActMats.getPhy_id() != null) {
					actMats.setId(coActMats.getId());
					actMats.setPhy_id(coActMats.getPhy_id());
					actMats.setIdCountry(coActMats.getIdCountry());
					actMats.setAcs_id(coActMats.getAcs_id());
					actMats.setAcm_percsacs(coActMats.getAcm_percsacs());
					actMats.setAcm_quanacs(coActMats.getAcm_quanacs());
					actMats.setUni_id(coActMats.getUni_id());

				}
				
				if (validator.validate(actMats)) {                
					iActMats.save(actMats);
                }				
				if (validator.getErrors().size() > 0)
					response.addErrors(validator.getErrors());

			}

			if (response.getErrors() != null) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return ResponseEntity.badRequest().body(response);
			}

			else {
				response.setStatus(HttpStatus.OK.value());
				return ResponseEntity.ok().body(response);
			}
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());

			response.addError("Internal error in WebService");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

}
