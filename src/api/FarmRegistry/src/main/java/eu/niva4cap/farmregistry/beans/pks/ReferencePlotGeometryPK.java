/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class ReferencePlotGeometryPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class ReferencePlotGeometryPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 2069190211219014770L;

    /** The id reference plot. */
    @Column(name = "RFP_id")
    private UUID idReferencePlot;

    /** The date from. */
    @Column(name = "RGE_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.Geometry.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReferencePlotGeometryPK other = (ReferencePlotGeometryPK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idReferencePlot == null) {
            if (other.idReferencePlot != null) {
                return false;
            }
        } else if (!idReferencePlot.equals(other.idReferencePlot)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id reference plot.
     *
     * @return the id reference plot
     */
    public UUID getIdReferencePlot() {
        return idReferencePlot;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result + ((idReferencePlot == null) ? 0 : idReferencePlot.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id reference plot.
     *
     * @param idReferencePlot
     *            the new id reference plot
     */
    public void setIdReferencePlot(final UUID idReferencePlot) {
        this.idReferencePlot = idReferencePlot;
    }
}