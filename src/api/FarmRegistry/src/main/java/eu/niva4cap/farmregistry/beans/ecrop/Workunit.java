/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrProductionWorkunit;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class Workunit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
public class Workunit {
    
    /** The type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer type;
    
    /** The awu. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "8.50")
    private BigDecimal awu;
    
   
    /** The date from. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date dateFrom;
    
    /** The date to. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2021-03-24")
    private Date dateTo;

    /**
     * Gets the awu.
     *
     * @return the awu
     */
    public BigDecimal getAwu() {
        return awu;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * Parsea el production workunit.
     *
     * @param productionWorkunit the production workunit
     */
    public void parseProductionWorkunit(final PrProductionWorkunit productionWorkunit) {
        setType(productionWorkunit.getIdWorkunitType());
        setAwu(productionWorkunit.getAwu());
        setDateFrom(productionWorkunit.getDateFrom());
        setDateTo(productionWorkunit.getDateTo());
    }

    /**
     * Sets the awu.
     *
     * @param awu the new awu
     */
    @JsonProperty(value = "AWU")
    public void setAwu(final BigDecimal awu) {
        this.awu = awu;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "DateFrom")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "DateTo")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    @JsonProperty(value = "Type")
    public void setType(final Integer type) {
        this.type = type;
    }
}