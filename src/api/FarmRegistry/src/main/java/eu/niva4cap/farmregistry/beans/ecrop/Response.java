/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * The Class Response.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"timestamp", "status", "errors"})
public class Response {
    
    /** The timestamp. */
    private Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    /** The status. */
    private Integer status;

    /** The errors. */
    private List<String> errors;

    /**
     * Añade el error.
     *
     * @param error the error
     */
    public void addError(final String error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }

        errors.add(error);
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     */
    public void addErrors(final List<String> errors) {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }

        this.errors.addAll(errors);
    }

    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Chequea por errors.
     *
     * @return true, if successful
     */
    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    /**
     * Sets the errors.
     *
     * @param errors the new errors
     */
    @JsonProperty(value = "errors")
    public void setErrors(final List<String> errors) {
        this.errors = errors;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    @JsonProperty(value = "status")
    public void setStatus(final Integer status) {
        this.status = status;
    }

    /**
     * Sets the timestamp.
     *
     * @param timestamp the new timestamp
     */
    @JsonProperty(value = "timestamp")
    public void setTimestamp(final Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}