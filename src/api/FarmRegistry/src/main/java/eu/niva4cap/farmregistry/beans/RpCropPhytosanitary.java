/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.CropPhytosanitary;
import eu.niva4cap.farmregistry.beans.pks.CropPhytosanitaryPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropPhytosanitary.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPPHYTOSANIT")
@Table(name = "RP_DCROPPHYTOSANIT")
@JsonPropertyOrder({"id_phytosanitary", "id_country", "dose", "id_unit_type",
        "id_phytosanitary_equipment", "start_date", "end_date"})
public class RpCropPhytosanitary {
    
    /** The crop phytosanitary PK. */
    @Valid
    protected @EmbeddedId CropPhytosanitaryPK cropPhytosanitaryPK = new CropPhytosanitaryPK();

    /** The id crop parcel. */
    @Column(name = "RCP_ID", insertable = false, updatable = false)
    private UUID idCropParcel;

    /** The id phytosanitary. */
    @Column(name = "PHY_ID", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private String idPhytosanitary;

    /** The id country. */
    @Column(name = "COU_ID", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private String idCountry;

    /** The dose. */
    @Column(name = "CPH_DOSE")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 6, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.Dose is too long")
    private BigDecimal dose;

    /** The id unit type. */
    @Column(name = "UNI_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idUnitType;

    /** The id phytosanitary equipment. */
    @Column(name = "PHE_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idPhytosanitaryEquipment;

    /** The start date. */
    @Column(name = "CPH_STARTDATE", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date startDate;

    /** The end date. */
    @Column(name = "CPH_ENDDATE")
    @JsonView(JsonViews.Detailed.class)
    private Date endDate;

    /** The crop parcel. */
    @MapsId(value = "idCropParcel")
    @ManyToOne
    @JoinColumn(name = "RCP_ID")
    private RpCropParcel cropParcel;

    /**
     * Gets the crop parcel.
     *
     * @return the crop parcel
     */
    @JsonIgnore
    public RpCropParcel getCropParcel() {
        return cropParcel;
    }

    /**
     * Gets the dose.
     *
     * @return the dose
     */
    public BigDecimal getDose() {
        return dose;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the id country.
     *
     * @return the id country
     */
    public String getIdCountry() {
        return cropPhytosanitaryPK.getIdCountry();
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    @JsonIgnore
    public UUID getIdCropParcel() {
        return cropPhytosanitaryPK.getIdCropParcel();
    }

    /**
     * Gets the id phytosanitary.
     *
     * @return the id phytosanitary
     */
    public String getIdPhytosanitary() {
        return cropPhytosanitaryPK.getIdPhytosanitary();
    }

    /**
     * Gets the id phytosanitary equipment.
     *
     * @return the id phytosanitary equipment
     */
    public Integer getIdPhytosanitaryEquipment() {
        return idPhytosanitaryEquipment;
    }

    /**
     * Gets the id unit type.
     *
     * @return the id unit type
     */
    public Integer getIdUnitType() {
        return idUnitType;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return cropPhytosanitaryPK.getStartDate();
    }

    /**
     * Parsea el phytosanitary.
     *
     * @param cropPhytosanitary the crop phytosanitary
     */
    public void parsePhytosanitary(final CropPhytosanitary cropPhytosanitary) {
        setIdPhytosanitary(cropPhytosanitary.getId());
        setIdCountry(cropPhytosanitary.getCountry());
        setDose(cropPhytosanitary.getDose());
        setIdUnitType(cropPhytosanitary.getUnit());
        setIdPhytosanitaryEquipment(cropPhytosanitary.getEquipment());
        setStartDate(cropPhytosanitary.getStartDate());
        setEndDate(cropPhytosanitary.getEndDate());
    }

    /**
     * Sets the crop parcel.
     *
     * @param cropParcel the new crop parcel
     */
    public void setCropParcel(final RpCropParcel cropParcel) {
        this.cropParcel = cropParcel;
    }

    /**
     * Sets the dose.
     *
     * @param dose the new dose
     */
    @JsonProperty(value = "dose")
    public void setDose(final BigDecimal dose) {
        this.dose = dose;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    @JsonProperty(value = "end_date")
    public void setEndDate(final Date endDate) {
        this.endDate = endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Sets the id country.
     *
     * @param idCountry the new id country
     */
    @JsonProperty(value = "id_country")
    public void setIdCountry(final String idCountry) {
        cropPhytosanitaryPK.setIdCountry(idCountry);
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        cropPhytosanitaryPK.setIdCropParcel(idCropParcel);
    }

    /**
     * Sets the id phytosanitary.
     *
     * @param idPhytosanitary the new id phytosanitary
     */
    @JsonProperty(value = "id_phytosanitary")
    public void setIdPhytosanitary(final String idPhytosanitary) {
        cropPhytosanitaryPK.setIdPhytosanitary(idPhytosanitary);
    }

    /**
     * Sets the id phytosanitary equipment.
     *
     * @param idPhytosanitaryEquipment the new id phytosanitary equipment
     */
    @JsonProperty(value = "id_phytosanitary_equipment")
    public void setIdPhytosanitaryEquipment(final Integer idPhytosanitaryEquipment) {
        this.idPhytosanitaryEquipment = idPhytosanitaryEquipment;
    }

    /**
     * Sets the id unit type.
     *
     * @param idUnitType the new id unit type
     */
    @JsonProperty(value = "id_unit_type")
    public void setIdUnitType(final Integer idUnitType) {
        this.idUnitType = idUnitType;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    @JsonProperty(value = "start_date")
    public void setStartDate(final Date startDate) {
        cropPhytosanitaryPK.setStartDate(startDate);
    }
}