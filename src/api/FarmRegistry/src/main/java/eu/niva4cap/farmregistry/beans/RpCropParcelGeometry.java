/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.pks.CropParcelGeometryPK;
import eu.niva4cap.farmregistry.utils.Coordinates;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropParcelGeometry.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPPARCELGEOM")
@Table(name = "RP_DCROPPARCELGEOM")
@JsonPropertyOrder({"feature", "surface", "date_from", "date_to"})
public class RpCropParcelGeometry {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RpCropParcelGeometry.class);

    /** The crop parcel geometry PK. */
    @Valid
    @EmbeddedId
    protected CropParcelGeometryPK cropParcelGeometryPK = new CropParcelGeometryPK();

    /** The id crop parcel. */
    @Column(name = "RCP_ID", insertable = false, updatable = false)
    private UUID idCropParcel;

    /** The geometry. */
    @Column(name = "CPG_GEOMETRY")
    @JsonView(JsonViews.Detailed.class)
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Geometry.Coordinates is null")
    private Geometry geometry;

    /** The surface. */
    @Column(name = "CPG_SURFACE")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 10, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Geometry.Surface is too long")
    private BigDecimal surface;

    /** The date from. */
    @Column(name = "CPG_DATEFROM", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "CPG_DATETO")
    @JsonView(JsonViews.Detailed.class)
    private Date dateTo;

    /** The crop parcel. */
    @MapsId(value = "idCropParcel")
    @ManyToOne
    @JoinColumn(name = "RCP_ID")
    private RpCropParcel cropParcel;

    /**
     * Gets the crop parcel.
     *
     * @return the crop parcel
     */
    @JsonIgnore
    public RpCropParcel getCropParcel() {
        return cropParcel;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return cropParcelGeometryPK.getDateFrom();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the geometry.
     *
     * @return the geometry
     */
    public String getGeometry() {
        return geometry.toString();
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    @JsonIgnore
    public UUID getIdCropParcel() {
        return cropParcelGeometryPK.getIdCropParcel();
    }

    /**
     * Gets the surface.
     *
     * @return the surface
     */
    public BigDecimal getSurface() {
        return surface;
    }

    /**
     * Parsea el geometry.
     *
     * @param geometry the geometry
     */
    public void parseGeometry(final eu.niva4cap.farmregistry.beans.ecrop.Geometry geometry) {
        setGeometry(geometry.getCoordinates());
        setSurface(geometry.getSurface());
        setDateFrom(geometry.getDateFrom());
        setDateTo(geometry.getDateTo());
    }

    /**
     * Sets the crop parcel.
     *
     * @param cropParcel the new crop parcel
     */
    public void setCropParcel(final RpCropParcel cropParcel) {
        this.cropParcel = cropParcel;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        cropParcelGeometryPK.setDateFrom(dateFrom);
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometry.
     *
     * @param geometry the new geometry
     */
    @JsonProperty(value = "feature")
    public void setGeometry(final String[][][] geometry) {
        try {
            this.geometry = new WKTReader().read(Coordinates.geoJSON2WKT(geometry));
            this.geometry.setSRID(4326);
        }

        catch (final Exception e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        cropParcelGeometryPK.setIdCropParcel(idCropParcel);
    }

    /**
     * Sets the surface.
     *
     * @param surface the new surface
     */
    @JsonProperty(value = "surface")
    public void setSurface(final BigDecimal surface) {
        this.surface = surface;
    }
}