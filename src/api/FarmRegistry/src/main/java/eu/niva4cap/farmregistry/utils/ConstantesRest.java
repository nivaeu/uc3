/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.utils;


public class ConstantesRest {

    
    /** Rest Query AgriculturalProducerParty. */
    public static final String REST_QUERY_AGRICULTURAL_PRODUCER_PARTIES = "/query/agriculturalProducerParties";
    
    /** Rest Query AgriculturalProductionUnit. */
    public static final String REST_QUERY_AGRICULTURAL_PRODUCTION_UNIT = "/query/agriculturalProductionUnits";
    
    /** Rest Query CropActivity. */
    public static final String REST_QUERY_CROP_ACTIVITY = "/query/cropActivities";
    
    /** Rest Query CropActivityDetail. */
    public static final String REST_QUERY_CROP_ACTIVITY_DETAIL = "/query/cropActivityDetails";
    
    /** Rest Query CropFertilizer. */
    public static final String REST_QUERY_CROP_FERTILIZER = "/query/cropFertilizers";
    
    /** Rest Query CropIrrigation. */
    public static final String REST_QUERY_CROP_IRRIGATION = "/query/cropIrrigations";
    
    /** Rest Query CropPhytosanitary. */
    public static final String REST_QUERY_CROP_PHYTOSANITARY = "/query/cropPhytosanitaries";
    
    /** Rest Query  CropPlot. */
    public static final String REST_QUERY_CROP_PLOT= "/query/cropPlots";
    
    /** Rest FieldCrop. */
    public static final String REST_QUERY_FIELD_CROP = "/query/fieldCrops";
    
    /** Rest Query ReferencePlot. */
    public static final String REST_QUERY_REFERENCE_PLOT = "/query/referencePlots";
    
    /** Load Query LoadEcrop. */
    public static final String REST_LOAD_ECROP = "/load/agriculturalProducerParties";
    
    /** Rest Query ABAgriculturalBlock. */
    public static final String REST_AB_AGRICULTURAL_BLOCK = "/query/agriculturalBlocks";
    
    /** Rest Query PRFarmer. */
    public static final String REST_QUERY_PR_FARMER = "/query/farmers";
    
    /** Rest Query PRFarmerContact. */
    public static final String REST_QUERY_PR_FARMER_CONTACT= "/query/farmerContacts";
    
    /** Rest Query PRFarmerData. */
    public static final String REST_QUERY_PR_FARMER_DATA = "/query/farmerData";

    /** Rest Query PRProductionUnit. */
    public static final String REST_QUERY_PR_PRODUCTION_UNIT = "/query/productionUnits";

    /** Rest Query PRProductionUnitFarmingType. */
    public static final String REST_QUERY_PR_PRODUCTION_UNIT_FARMING_TYPE = "/query/productionUnitFarmingTypes";

    /** Rest Query PRProductionUnitWorkunit. */
    public static final String REST_QUERY_PR_PRODUCTION_UNIT_WORKUNIT = "/query/productionUnitWorkunits";

    /** Rest Query RPCropParcel. */
    public static final String REST_QUERY_RP_CROP_PARCEL = "/query/cropParcels";
    
    /** Rest Query QueryCatalogs. */
    public static final String REST_QUERY_QUERY_CATALOGS = "/query/dataList";

    /** Rest Query QuerySurfaces. */
    public static final String REST_QUERY_QUERY_SURFACES = "/query/surfaces";
    
    /** Rest AuthController. */
    public static final String REST_QUERY_AUTH_CONTROLLER = "/auth";

    /** Rest Load ActiveSustances. */
    public static final String REST_ACTIVE_SUBSTANCES = "/load/dataList/ActiveSubstances";
    
    /** Rest Load ActiveMaterials. */
    public static final String REST_ACTIVE_MATERIALS = "/load/dataList/ActiveMaterials";
    
    /** Rest Load CropProduct. */
    public static final String REST_CROP_PRODUCT = "/load/dataList/CropProduct";

    /** Rest Load CropVariety. */
    public static final String REST_CROP_VARIETY = "/load/dataList/CropVariety";

    /** Rest Load CropActivity. */
    public static final String REST_CROP_ACTIVITY = "/load/CropActivity";
    
    /** Rest Load Fertilizer. */
    public static final String REST_FERTILIZER = "/load/Fertilizer";

    /** Rest Load Pythosanitary. */
    public static final String REST_PYTHOSANITARY = "/load/Phytosanitary";

    /** Rest Delete */
    public static final String REST_DELETE = "/delete/agriculturalProducerParties";
    
    /** Rest Produces. */
    public static final String REST_PRODUCES = "application/json";
    
}
