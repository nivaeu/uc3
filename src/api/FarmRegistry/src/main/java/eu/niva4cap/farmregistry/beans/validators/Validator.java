/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.validators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.AbGeometry;
import eu.niva4cap.farmregistry.beans.AbLandCover;
import eu.niva4cap.farmregistry.beans.PrFarmer;
import eu.niva4cap.farmregistry.beans.PrFarmerContact;
import eu.niva4cap.farmregistry.beans.PrFarmerData;
import eu.niva4cap.farmregistry.beans.PrFarmingType;
import eu.niva4cap.farmregistry.beans.PrProductionUnit;
import eu.niva4cap.farmregistry.beans.PrProductionWorkunit;
import eu.niva4cap.farmregistry.beans.RpCropActivity;
import eu.niva4cap.farmregistry.beans.RpCropActivityDetail;
import eu.niva4cap.farmregistry.beans.RpCropActivityMessage;
import eu.niva4cap.farmregistry.beans.RpCropFertilizer;
import eu.niva4cap.farmregistry.beans.RpCropIrrigation;
import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpCropParcelGeometry;
import eu.niva4cap.farmregistry.beans.RpCropPhytosanitary;
import eu.niva4cap.farmregistry.beans.RpGeometry;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.beans.catalogs.CoActMats;
import eu.niva4cap.farmregistry.beans.catalogs.CoActSus;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProduct;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProductGroup;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropVariety;
import eu.niva4cap.farmregistry.beans.catalogs.CoFertilizer;
import eu.niva4cap.farmregistry.beans.catalogs.CoPhytosanitary;
import eu.niva4cap.farmregistry.repositories.IPrFarmer;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActMats;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoActSus;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCountry;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProduct;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProductGroup;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropVariety;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationDetail;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCultivationSystem;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFarmingType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationMethod;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizer;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoFertilizerType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoIrrigationType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLaborType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandCover;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoLandTenure;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoOrganicFarming;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitary;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoPhytosanitaryEquipment;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoSeedType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoUnitType;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWaterOrigin;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoWorkunitType;
import eu.niva4cap.farmregistry.rest.catalogs.CropVariety;

/**
 * The Class Validator.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Component
public class Validator {
    
    /** The i farmer. */
    @Autowired
    private IPrFarmer iFarmer;
    
    /** The i country. */
    @Autowired
    private ICoCountry iCountry;

    /** The i farmer type. */
    @Autowired
    private ICoFarmerType iFarmerType;

    /** The i farming type. */
    @Autowired
    private ICoFarmingType iFarmingType;

    /** The i workunit type. */
    @Autowired
    private ICoWorkunitType iWorkunitType;

    /** The i land cover. */
    @Autowired
    private ICoLandCover iLandCover;

    /** The i land tenure. */
    @Autowired
    private ICoLandTenure iLandTenure;

    /** The i crop product. */
    @Autowired
    private ICoCropProduct iCropProduct;

    /** The i crop product Group. */
    @Autowired
    private ICoCropProductGroup iCropProductGroup;
    
    /** The i crop variety. */
    @Autowired
    private ICoCropVariety iCropVariety;

    /** The i cultivation system. */
    @Autowired
    private ICoCultivationSystem iCultivationSystem;

    /** The i cultivation detail. */
    @Autowired
    private ICoCultivationDetail iCultivationDetail;

    /** The i seed type. */
    @Autowired
    private ICoSeedType iSeedType;

    /** The i organic farming. */
    @Autowired
    private ICoOrganicFarming iOrganicFarming;

    /** The i labor type. */
    @Autowired
    private ICoLaborType iLaborType;

    /** The i irrigation type. */
    @Autowired
    private ICoIrrigationType iIrrigationType;

    /** The i water origin. */
    @Autowired
    private ICoWaterOrigin iWaterOrigin;

    /** The i unit type. */
    @Autowired
    private ICoUnitType iUnitType;

    /** The i phytosanitary. */
    @Autowired
    private ICoPhytosanitary iPhytosanitary;

    /** The i phytosanitary equipment. */
    @Autowired
    private ICoPhytosanitaryEquipment iPhytosanitaryEquipment;

    /** The i fertilizer. */
    @Autowired
    private ICoFertilizer iFertilizer;

    /** The i fertilizer type. */
    @Autowired
    private ICoFertilizerType iFertilizerType;

    /** The i fertilization type. */
    @Autowired
    private ICoFertilizationType iFertilizationType;

    /** The i fertilization method. */
    @Autowired
    private ICoFertilizationMethod iFertilizationMethod;

    /** The i active material. */
    @Autowired
    private ICoActMats iActMats;

    /** The i active substance. */
    @Autowired
    private ICoActSus iActSus;
    
    /** The validation errors. */
    private List<String> validationErrors;

    /**
     * Añade el error.
     *
     * @param error the error
     * @param agriculturalBlock the agricultural block
     */
    private void addError(String error, final AbAgriculturalBlock agriculturalBlock) {
        error = "[#app# / #apu# / #ab#] -> " + error;
        error = replaceAPP(error, agriculturalBlock.getProductionUnit().getFarmer());
        error = replaceAPU(error, agriculturalBlock.getProductionUnit());
        error = replaceAB(error, agriculturalBlock);

        validationErrors.add(error);
    }

    /**
     * Añade el error.
     *
     * @param error the error
     * @param farmer the farmer
     */
    private void addError(String error, final PrFarmer farmer) {
        error = "[#app#] -> " + error;
        error = replaceAPP(error, farmer);

        validationErrors.add(error);
    }

    /**
     * Añade el error.
     *
     * @param error the error
     * @param productionUnit the production unit
     */
    private void addError(String error, final PrProductionUnit productionUnit) {
        error = "[#app# / #apu#] -> " + error;
        error = replaceAPP(error, productionUnit.getFarmer());
        error = replaceAPU(error, productionUnit);

        validationErrors.add(error);
    }

    /**
     * Añade el error.
     *
     * @param error the error
     * @param cropParcel the crop parcel
     */
    private void addError(String error, final RpCropParcel cropParcel) {
        error = "[#app# / #apu# / #ab# / #rp# / #cp#] -> " + error;
        error = replaceAPP(error, cropParcel.getReferencePlot().getAgriculturalBlock()
                .getProductionUnit().getFarmer());
        error = replaceAPU(error,
                cropParcel.getReferencePlot().getAgriculturalBlock().getProductionUnit());
        error = replaceAB(error, cropParcel.getReferencePlot().getAgriculturalBlock());
        error = replaceRP(error, cropParcel.getReferencePlot());
        error = replaceCP(error, cropParcel);

        validationErrors.add(error);
    }

    /**
     * Añade el error.
     *
     * @param error the error
     * @param referencePlot the reference plot
     */
    private void addError(String error, final RpReferencePlot referencePlot) {
        error = "[#app# / #apu# / #ab# / #rp#] -> " + error;
        error = replaceAPP(error,
                referencePlot.getAgriculturalBlock().getProductionUnit().getFarmer());
        error = replaceAPU(error, referencePlot.getAgriculturalBlock().getProductionUnit());
        error = replaceAB(error, referencePlot.getAgriculturalBlock());
        error = replaceRP(error, referencePlot);

        validationErrors.add(error);
    }

    private void addError(String error, final CoCropProduct coCropProduct) {
        error = "[#crprod#] -> " + error;
        error= replaceCProd(error,coCropProduct);

        validationErrors.add(error);
    }  
    /**
     * Añade el error.
     *
     * @param error the error
     * @param cropVariety the crop variety
     */
    private void addError(String error, final CoCropVariety cropVariety) {
        error = "[#" + cropVariety.getIdProduct() + "#] -> " + error;
        validationErrors.add(error);
    }    

    /**
     * Añade el error.
     *
     * @param error the error
     * @param fertilizer the fertilizer
     */
    private void addError(String error, final CoFertilizer fertilizer) {
        error = "[#" + fertilizer.getIdFertilizerType() + "#] -> " + error;
        validationErrors.add(error);
    }    
    
    /**
     * Añade el error.
     *
     * @param error the error
     * @param phytosanitary the phytosanitary
     */
    private void addError(String error, final CoPhytosanitary phytosanitary) {
        error = "[#" + phytosanitary.getId() + "#] -> " + error;
        validationErrors.add(error);
    }    
    
    /**
     * Añade el error.
     *
     * @param error the error
     * @param coActSus the Active Substance
     */
    private void addError(String error, final CoActSus coActSus) {
        error = "[#" + coActSus.getId() + "#] -> " + error;
        validationErrors.add(error);
    }    

    /**
     * Añade el error.
     *
     * @param error the error
     * @param coActMats the Active Material
     */
    private void addError(String error, final CoActMats coActMats) {
        error = "[#" + coActMats.getPhy_id() + "#] -> " + error;
        validationErrors.add(error);
    }    
    
    
    /**
     * Añade el error.
     *
     * @param error the error
     * @param rpcropactivitymessage the rpcropactivitymessage
     */
    private void addError(String error, final RpCropActivityMessage rpcropactivitymessage) {
        error = "[#" + rpcropactivitymessage.getPersonalID() + "#] -> " + error;
        validationErrors.add(error);
    } 
    
    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param agriculturalBlock the agricultural block
     */
    private void addErrors(final Set<ConstraintViolation<AbAgriculturalBlock>> errors,
            final AbAgriculturalBlock agriculturalBlock) {
        final Iterator<ConstraintViolation<AbAgriculturalBlock>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), agriculturalBlock);
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param geometry the geometry
     */
    private void addErrors(final Set<ConstraintViolation<AbGeometry>> errors,
            final AbGeometry geometry) {
        final Iterator<ConstraintViolation<AbGeometry>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), geometry.getAgriculturalBlock());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param landCover the land cover
     */
    private void addErrors(final Set<ConstraintViolation<AbLandCover>> errors,
            final AbLandCover landCover) {
        final Iterator<ConstraintViolation<AbLandCover>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), landCover.getAgriculturalBlock());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param farmer the farmer
     */
    private void addErrors(final Set<ConstraintViolation<PrFarmer>> errors, final PrFarmer farmer) {
        final Iterator<ConstraintViolation<PrFarmer>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), farmer);
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param farmerContact the farmer contact
     */
    private void addErrors(final Set<ConstraintViolation<PrFarmerContact>> errors,
            final PrFarmerContact farmerContact) {
        final Iterator<ConstraintViolation<PrFarmerContact>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            if (farmerContact.getFarmer() != null) {
                addError(iterator.next().getMessage(), farmerContact.getFarmer());
            } else {
                addError(iterator.next().getMessage(), farmerContact.getProductionUnit());
            }
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param farmerData the farmer data
     */
    private void addErrors(final Set<ConstraintViolation<PrFarmerData>> errors,
            final PrFarmerData farmerData) {
        final Iterator<ConstraintViolation<PrFarmerData>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), farmerData.getFarmer());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param farmingType the farming type
     */
    private void addErrors(final Set<ConstraintViolation<PrFarmingType>> errors,
            final PrFarmingType farmingType) {
        final Iterator<ConstraintViolation<PrFarmingType>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), farmingType.getProductionUnit());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param productionUnit the production unit
     */
    private void addErrors(final Set<ConstraintViolation<PrProductionUnit>> errors,
            final PrProductionUnit productionUnit) {
        final Iterator<ConstraintViolation<PrProductionUnit>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), productionUnit);
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param productionWorkunit the production workunit
     */
    private void addErrors(final Set<ConstraintViolation<PrProductionWorkunit>> errors,
            final PrProductionWorkunit productionWorkunit) {
        final Iterator<ConstraintViolation<PrProductionWorkunit>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), productionWorkunit.getProductionUnit());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param cropActivity the crop activity
     */
    private void addErrors(final Set<ConstraintViolation<RpCropActivity>> errors,
            final RpCropActivity cropActivity) {
        final Iterator<ConstraintViolation<RpCropActivity>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), cropActivity.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param cropActivityDetail the crop activity detail
     */
    private void addErrors(final Set<ConstraintViolation<RpCropActivityDetail>> errors,
            final RpCropActivityDetail cropActivityDetail) {
        final Iterator<ConstraintViolation<RpCropActivityDetail>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), cropActivityDetail.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param fertilizer the fertilizer
     */
    private void addErrors(final Set<ConstraintViolation<RpCropFertilizer>> errors,
            final RpCropFertilizer fertilizer) {
        final Iterator<ConstraintViolation<RpCropFertilizer>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), fertilizer.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param cropIrrigation the crop irrigation
     */
    private void addErrors(final Set<ConstraintViolation<RpCropIrrigation>> errors,
            final RpCropIrrigation cropIrrigation) {
        final Iterator<ConstraintViolation<RpCropIrrigation>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), cropIrrigation.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param cropParcel the crop parcel
     */
    private void addErrors(final Set<ConstraintViolation<RpCropParcel>> errors,
            final RpCropParcel cropParcel) {
        final Iterator<ConstraintViolation<RpCropParcel>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), cropParcel.getReferencePlot());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param geometry the geometry
     */
    private void addErrors(final Set<ConstraintViolation<RpCropParcelGeometry>> errors,
            final RpCropParcelGeometry geometry) {
        final Iterator<ConstraintViolation<RpCropParcelGeometry>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), geometry.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param phytosanitary the phytosanitary
     */
    private void addErrors(final Set<ConstraintViolation<RpCropPhytosanitary>> errors,
            final RpCropPhytosanitary phytosanitary) {
        final Iterator<ConstraintViolation<RpCropPhytosanitary>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), phytosanitary.getCropParcel());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param geometry the geometry
     */
    private void addErrors(final Set<ConstraintViolation<RpGeometry>> errors,
            final RpGeometry geometry) {
        final Iterator<ConstraintViolation<RpGeometry>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), geometry.getReferencePlot());
        }
    }

    /**
     * Añade el errors.
     *
     * @param errors the errors
     * @param referencePlot the reference plot
     */
    private void addErrors(final Set<ConstraintViolation<RpReferencePlot>> errors,
            final RpReferencePlot referencePlot) {
        final Iterator<ConstraintViolation<RpReferencePlot>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), referencePlot);
        }
    }

    
    private void addErrors(final Set<ConstraintViolation<CoCropProduct>> errors,
            final CoCropProduct coCropProduct) {
        final Iterator<ConstraintViolation<CoCropProduct>> iterator = errors.iterator();

        while (iterator.hasNext()) {
            addError(iterator.next().getMessage(), coCropProduct);
        }
    }
    
    /**
     * Gets the errors.
     *
     * @return the errors
     */
    public List<String> getErrors() {
        return validationErrors;
    }

    /**
     * Replace AB.
     *
     * @param error the error
     * @param agriculturalBlock the agricultural block
     * @return the string
     */
    private String replaceAB(String error, final AbAgriculturalBlock agriculturalBlock) {
        String key = "???";

        if (agriculturalBlock.getKey() != null) {
            key = agriculturalBlock.getKey();
        }

        error = error.replace("#ab#", key);

        return error;
    }

    /**
     * Replace APP.
     *
     * @param error the error
     * @param farmer the farmer
     * @return the string
     */
    private String replaceAPP(String error, final PrFarmer farmer) {
        String key = "???";

        if (farmer.getFarmerData().getPersonalId() != null) {
            key = farmer.getFarmerData().getPersonalId();
        }

        error = error.replace("#app#", key);

        return error;
    }

    /**
     * Replace APU.
     *
     * @param error the error
     * @param productionUnit the production unit
     * @return the string
     */
    private String replaceAPU(String error, final PrProductionUnit productionUnit) {
        String key = "???";

        if (productionUnit.getKey() != null) {
            key = productionUnit.getKey();
        }

        error = error.replace("#apu#", key);

        return error;
    }

    /**
     * Replace CP.
     *
     * @param error the error
     * @param cropParcel the crop parcel
     * @return the string
     */
    private String replaceCP(String error, final RpCropParcel cropParcel) {
        String key = "???";

        if (cropParcel.getKey() != null) {
            key = cropParcel.getKey();
        }

        error = error.replace("#cp#", key);

        return error;
    }

    /**
     * Replace RP.
     *
     * @param error the error
     * @param referencePlot the reference plot
     * @return the string
     */
    private String replaceRP(String error, final RpReferencePlot referencePlot) {
        String key = "???";

        if (referencePlot.getKey() != null) {
            key = referencePlot.getKey();
        }

        error = error.replace("#rp#", key);

        return error;
    }
    
    private String replaceCProd(String error, final CoCropProduct coCropProduct) {
        String key = "???";

        if (coCropProduct.getId() != null) {
            key = coCropProduct.getId();
        }

        error = error.replace("#crprod#", key);

        return error;
    }

    /**
     * Validate.
     *
     * @param agriculturalBlock the agricultural block
     * @return true, if successful
     */
    public boolean validate(final AbAgriculturalBlock agriculturalBlock) {
        if (agriculturalBlock != null) {
            final Set<ConstraintViolation<AbAgriculturalBlock>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(agriculturalBlock);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, agriculturalBlock);

                return false;
            }

            final ListIterator<AbLandCover> iteratorLC = agriculturalBlock.getLandCovers()
                    .listIterator();
            while (iteratorLC.hasNext()) {
                if (!validate(iteratorLC.next())) {
                    iteratorLC.remove();
                }
            }

            final ListIterator<AbGeometry> iteratorG = agriculturalBlock.getGeometries()
                    .listIterator();
            while (iteratorG.hasNext()) {
                if (!validate(iteratorG.next())) {
                    iteratorG.remove();
                }
            }

            final ListIterator<RpReferencePlot> iteratorRP = agriculturalBlock.getReferencePlots()
                    .listIterator();
            while (iteratorRP.hasNext()) {
                if (!validate(iteratorRP.next())) {
                    iteratorRP.remove();
                }
            }

        }

        return true;
    }

    /**
     * Validate.
     *
     * @param geometry the geometry
     * @return true, if successful
     */
    public boolean validate(final AbGeometry geometry) {
        if (geometry != null) {
            final Set<ConstraintViolation<AbGeometry>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(geometry);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, geometry);

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param landCover the land cover
     * @return true, if successful
     */
    public boolean validate(final AbLandCover landCover) {
        if (landCover != null) {
            final Set<ConstraintViolation<AbLandCover>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(landCover);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, landCover);

                return false;
            }

            if (!iLandCover.findById(landCover.getIdLandCover()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.SoilType.Type is not valid",
                        landCover.getAgriculturalBlock());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param farmer the farmer
     * @return true, if successful
     */
    public boolean validate(final PrFarmer farmer) {
        validationErrors = new ArrayList<>();

        final Set<ConstraintViolation<PrFarmer>> sErrors = Validation.buildDefaultValidatorFactory()
                .getValidator().validate(farmer);

        if (!sErrors.isEmpty()) {
            addErrors(sErrors, farmer);

            return false;
        }

        if (!iCountry.findById(farmer.getIdCountry()).isPresent()) {
            addError("AgriculturalProducerParty.Country is not valid", farmer);

            return false;
        }

        if (!validate(farmer.getFarmerData())) {
            farmer.setFarmerData(null);
        }

        if (!validate(farmer.getFarmerContact())) {
            farmer.setFarmerContact(null);
        }

        final ListIterator<PrProductionUnit> iterator = farmer.getProductionUnits().listIterator();
        while (iterator.hasNext()) {
            if (!validate(iterator.next())) {
                iterator.remove();
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param farmerContact the farmer contact
     * @return true, if successful
     */
    public boolean validate(final PrFarmerContact farmerContact) {
        if (farmerContact != null) {
            final Set<ConstraintViolation<PrFarmerContact>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(farmerContact);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, farmerContact);

                return false;
            }

            if (farmerContact.getIdCountry() != null
                    && !iCountry.findById(farmerContact.getIdCountry()).isPresent()) {
                if (farmerContact.getFarmer() != null) {
                    addError("StructuredAddress.Country is not valid", farmerContact.getFarmer());
                } else {
                    addError("StructuredAddress.Country is not valid",
                            farmerContact.getProductionUnit());
                }

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param farmerData the farmer data
     * @return true, if successful
     */
    public boolean validate(final PrFarmerData farmerData) {
        if (farmerData != null) {
            final Set<ConstraintViolation<PrFarmerData>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(farmerData);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, farmerData);

                return false;
            }

            if (farmerData.getIdFarmerType() != null
                    && !iFarmerType.findById(farmerData.getIdFarmerType()).isPresent()) {
                addError("AgriculturalProducerParty.Classification is not valid",
                        farmerData.getFarmer());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param farmingType the farming type
     * @return true, if successful
     */
    public boolean validate(final PrFarmingType farmingType) {
        if (farmingType != null) {
            final Set<ConstraintViolation<PrFarmingType>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(farmingType);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, farmingType);

                return false;
            }

            if (!iFarmingType.findById(farmingType.getIdFarmingType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.FarmingType.Type is not valid",
                        farmingType.getProductionUnit());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param productionUnit the production unit
     * @return true, if successful
     */
    public boolean validate(final PrProductionUnit productionUnit) {
        if (productionUnit != null) {
            final Set<ConstraintViolation<PrProductionUnit>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(productionUnit);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, productionUnit);

                return false;
            }

            if (!iCountry.findById(productionUnit.getIdCountry()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.Country is not valid",
                        productionUnit);

                return false;
            }

            if (!validate(productionUnit.getFarmerContact())) {
                productionUnit.setFarmerContact(null);
            }

            final ListIterator<PrFarmingType> iteratorFT = productionUnit.getFarmingTypes()
                    .listIterator();
            while (iteratorFT.hasNext()) {
                if (!validate(iteratorFT.next())) {
                    iteratorFT.remove();
                }
            }

            final ListIterator<PrProductionWorkunit> iteratorPW = productionUnit
                    .getProductionWorkunits().listIterator();
            while (iteratorPW.hasNext()) {
                if (!validate(iteratorPW.next())) {
                    iteratorPW.remove();
                }
            }

            final ListIterator<AbAgriculturalBlock> iteratorAB = productionUnit
                    .getAgriculturalBlocks().listIterator();
            while (iteratorAB.hasNext()) {
                if (!validate(iteratorAB.next())) {
                    iteratorAB.remove();
                }
            }

        }

        return true;
    }

    /**
     * Validate.
     *
     * @param productionWorkunit the production workunit
     * @return true, if successful
     */
    public boolean validate(final PrProductionWorkunit productionWorkunit) {
        if (productionWorkunit != null) {
            final Set<ConstraintViolation<PrProductionWorkunit>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(productionWorkunit);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, productionWorkunit);

                return false;
            }

            if (!iWorkunitType.findById(productionWorkunit.getIdWorkunitType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.Workunit.Type is not valid",
                        productionWorkunit.getProductionUnit());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param cropActivity the crop activity
     * @return true, if successful
     */
    public boolean validate(final RpCropActivity cropActivity) {
        if (cropActivity != null) {
            final Set<ConstraintViolation<RpCropActivity>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(cropActivity);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, cropActivity);

                return false;
            }

            if (cropActivity.getIdCultivationSystem() != null && !iCultivationSystem
                    .findById(cropActivity.getIdCultivationSystem()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.CultivationSystem is not valid",
                        cropActivity.getCropParcel());

                return false;
            }

            if (cropActivity.getIdCultivationDetail() != null && !iCultivationDetail
                    .findById(cropActivity.getIdCultivationDetail()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.CultivationDetail is not valid",
                        cropActivity.getCropParcel());

                return false;
            }

            if (cropActivity.getIdSeedType() != null
                    && !iSeedType.findById(cropActivity.getIdSeedType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.SeedType is not valid",
                        cropActivity.getCropParcel());

                return false;
            }

            if (cropActivity.getIdOrganicFarming() != null
                    && !iOrganicFarming.findById(cropActivity.getIdOrganicFarming()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.OrganicFarming is not valid",
                        cropActivity.getCropParcel());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param cropActivityDetail the crop activity detail
     * @return true, if successful
     */
    public boolean validate(final RpCropActivityDetail cropActivityDetail) {
        if (cropActivityDetail != null) {
            final Set<ConstraintViolation<RpCropActivityDetail>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(cropActivityDetail);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, cropActivityDetail);

                return false;
            }

            if (!iLaborType.findById(cropActivityDetail.getIdLaborType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.ActivityDetail.LaborType is not valid",
                        cropActivityDetail.getCropParcel());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param fertilizer the fertilizer
     * @return true, if successful
     */
    public boolean validate(final RpCropFertilizer fertilizer) {
        if (fertilizer != null) {
            final Set<ConstraintViolation<RpCropFertilizer>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(fertilizer);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, fertilizer);

                return false;
            }

            if (!iFertilizerType.findById(fertilizer.getIdFertilizerType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.Type is not valid",
                        fertilizer.getCropParcel());

                return false;
            }

            if (fertilizer.getIdFertilizer() != null
                    && !iFertilizer.findById(fertilizer.getIdFertilizer()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.ID is not valid",
                        fertilizer.getCropParcel());

                return false;
            }

            if (fertilizer.getIdFertilizationType() != null && !iFertilizationType
                    .findById(fertilizer.getIdFertilizationType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.FertilizationType is not valid",
                        fertilizer.getCropParcel());

                return false;
            }

            if (fertilizer.getIdFertilizationMethod() != null && !iFertilizationMethod
                    .findById(fertilizer.getIdFertilizationMethod()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.FertilizationMethod is not valid",
                        fertilizer.getCropParcel());

                return false;
            }

            if (fertilizer.getIdUnitType() != null
                    && !iUnitType.findById(fertilizer.getIdUnitType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.Unit is not valid",
                        fertilizer.getCropParcel());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param cropIrrigation the crop irrigation
     * @return true, if successful
     */
    public boolean validate(final RpCropIrrigation cropIrrigation) {
        if (cropIrrigation != null) {
            final Set<ConstraintViolation<RpCropIrrigation>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(cropIrrigation);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, cropIrrigation);

                return false;
            }

            if (!iIrrigationType.findById(cropIrrigation.getIdIrrigationType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.Type is not valid",
                        cropIrrigation.getCropParcel());

                return false;
            }

            if (cropIrrigation.getIdWaterOrigin() != null
                    && !iWaterOrigin.findById(cropIrrigation.getIdWaterOrigin()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.WaterOrigin is not valid",
                        cropIrrigation.getCropParcel());

                return false;
            }

            if (cropIrrigation.getIdUnitType() != null
                    && !iUnitType.findById(cropIrrigation.getIdUnitType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropIrrigation.Unit is not valid",
                        cropIrrigation.getCropParcel());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param cropParcel the crop parcel
     * @return true, if successful
     */
    public boolean validate(final RpCropParcel cropParcel) {
        if (cropParcel != null) {
            final Set<ConstraintViolation<RpCropParcel>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(cropParcel);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, cropParcel);

                return false;
            }

            final ListIterator<RpCropParcelGeometry> iteratorCPG = cropParcel.getGeometries()
                    .listIterator();
            while (iteratorCPG.hasNext()) {
                if (!validate(iteratorCPG.next())) {
                    iteratorCPG.remove();
                }
            }

            if (cropParcel.getIdLandTenure() != null
                    && !iLandTenure.findById(cropParcel.getIdLandTenure()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.LandTenure is not valid",
                        cropParcel);

                return false;
            }

            if (!iCropProduct.findById(cropParcel.getIdCropProduct()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropProduct is not valid",
                        cropParcel);

                return false;
            }

            if (cropParcel.getIdCropVariety() != null && !iCropVariety
                    .findByIdAndProductAndCountry(cropParcel.getIdCropVariety(),
                            cropParcel.getIdCropProduct(), cropParcel.getReferencePlot()
                                    .getAgriculturalBlock().getProductionUnit().getIdCountry())
                    .isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropVariety is not valid",
                        cropParcel);
            }

            if (!validate(cropParcel.getCropActivity())) {
                cropParcel.setCropActivity(null);
            }

            final ListIterator<RpCropActivityDetail> iteratorCAD = cropParcel
                    .getCropActivityDetails().listIterator();
            while (iteratorCAD.hasNext()) {
                if (!validate(iteratorCAD.next())) {
                    iteratorCAD.remove();
                }
            }

            final ListIterator<RpCropIrrigation> iteratorCI = cropParcel.getCropIrrigations()
                    .listIterator();
            while (iteratorCI.hasNext()) {
                if (!validate(iteratorCI.next())) {
                    iteratorCI.remove();
                }
            }

            final ListIterator<RpCropPhytosanitary> iteratorCP = cropParcel.getCropPhytosanitaries()
                    .listIterator();
            while (iteratorCP.hasNext()) {
                if (!validate(iteratorCP.next())) {
                    iteratorCP.remove();
                }
            }

            final ListIterator<RpCropFertilizer> iteratorCF = cropParcel.getCropFertilizers()
                    .listIterator();
            while (iteratorCF.hasNext()) {
                if (!validate(iteratorCF.next())) {
                    iteratorCF.remove();
                }
            }

        }

        return true;
    }

    /**
     * Validate.
     *
     * @param geometry the geometry
     * @return true, if successful
     */
    public boolean validate(final RpCropParcelGeometry geometry) {
        if (geometry != null) {
            final Set<ConstraintViolation<RpCropParcelGeometry>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(geometry);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, geometry);

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param phytosanitary the phytosanitary
     * @return true, if successful
     */
    public boolean validate(final RpCropPhytosanitary phytosanitary) {
        if (phytosanitary != null) {
            final Set<ConstraintViolation<RpCropPhytosanitary>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(phytosanitary);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, phytosanitary);

                return false;
            }

            if (!iCountry.findById(phytosanitary.getIdCountry()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.Country is not valid",
                        phytosanitary.getCropParcel());

                return false;
            }

            if (!iPhytosanitary.findById(phytosanitary.getIdPhytosanitary()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ID is not valid",
                        phytosanitary.getCropParcel());

                return false;
            }

            if (phytosanitary.getIdPhytosanitaryEquipment() != null && !iPhytosanitaryEquipment
                    .findById(phytosanitary.getIdPhytosanitaryEquipment()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.Equipment is not valid",
                        phytosanitary.getCropParcel());

                return false;
            }

            if (phytosanitary.getIdUnitType() != null
                    && !iUnitType.findById(phytosanitary.getIdUnitType()).isPresent()) {
                addError(
                        "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.Unit is not valid",
                        phytosanitary.getCropParcel());

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param geometry the geometry
     * @return true, if successful
     */
    public boolean validate(final RpGeometry geometry) {
        if (geometry != null) {
            final Set<ConstraintViolation<RpGeometry>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(geometry);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, geometry);

                return false;
            }
        }

        return true;
    }

    /**
     * Validate.
     *
     * @param referencePlot the reference plot
     * @return true, if successful
     */
    public boolean validate(final RpReferencePlot referencePlot) {
        if (referencePlot != null) {
            final Set<ConstraintViolation<RpReferencePlot>> sErrors = Validation
                    .buildDefaultValidatorFactory().getValidator().validate(referencePlot);

            if (!sErrors.isEmpty()) {
                addErrors(sErrors, referencePlot);

                return false;
            }

            final ListIterator<RpGeometry> iteratorG = referencePlot.getGeometries().listIterator();
            while (iteratorG.hasNext()) {
                if (!validate(iteratorG.next())) {
                    iteratorG.remove();
                }
            }

            final ListIterator<RpCropParcel> iteratorCP = referencePlot.getCropParcels()
                    .listIterator();
            while (iteratorCP.hasNext()) {
                if (!validate(iteratorCP.next())) {
                    iteratorCP.remove();
                }
            }

        }

        return true;
    }
    
    /**
     * Validate.
     *
     * @param cropVariety the crop variety
     * @return true, if successful
     */
    public boolean validate(final CoCropVariety coCropVariety) {
    	
        validationErrors = new ArrayList<>();

    	if (coCropVariety.getIdProduct() != null && !iCropProduct
                .findById(coCropVariety.getIdProduct()).isPresent()) {
            addError("AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropProduct is not valid", coCropVariety);
            return false;
    	}

    	if ((coCropVariety.getIdCountry() == null) || (coCropVariety.getIdCountry() != null && !iCountry
                .findById(coCropVariety.getIdCountry()).isPresent())) {
            addError("AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropVariety.Country is not valid", coCropVariety);
            return false;
    	}
    	
    	return true;
    }
    
    /**
     * Validate.
     *
     * @param fertilizer the fertilizer
     * @return true, if successful
     */
    public boolean validate(final CoFertilizer coFertilizer) {
    	
        validationErrors = new ArrayList<>();

    	if (coFertilizer.getIdFertilizerType() != null && !iFertilizerType
                .findById(coFertilizer.getIdFertilizerType()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.Type is not valid",
                    coFertilizer);

            return false;
    	}
    	
    	return true;
    }
    
    public boolean validate(CoCropProduct coCropProduct) {
    	
    	validationErrors = new ArrayList<>();
    	
    	if (coCropProduct.getIdGroup() != null && !(iCropProductGroup
           .findByCpgId(coCropProduct.getIdGroup())).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropProduct is not valid",
                    coCropProduct);

            return false;
    	}
    	
    	if (coCropProduct.getIdLandCover() != null && !(iLandCover
           .findByCpgId(coCropProduct.getIdLandCover())).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.CropProduct is not valid",
                    coCropProduct);

            return false;
    	}
    	
    	return true;
    }
    
    /**
     * Validate.
     *
     * @param phytosanitary the phytosanitary
     * @return true, if successful
     */
    public boolean validate(final CoPhytosanitary coPhytosanitary) {
    	
        validationErrors = new ArrayList<>();

    	if (coPhytosanitary.getIdCountry() != null && !iCountry
                .findById(coPhytosanitary.getIdCountry()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ID is not valid",
                    coPhytosanitary);

            return false;
    	}
    	
    	return true;
    }    
    
    /**
     * Validate.
     *
     * @param coactsus the active substance
     * @return true, if successful
     */
    public boolean validate(final CoActSus actSus) {
    	
        validationErrors = new ArrayList<>();

    	if (actSus.getName() == null) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ActiveSubstance is not valid",
                    actSus);

            return false;
    	}
    	
    	return true;
    }    

    /**
     * Validate.
     *
     * @param coactmats the active material
     * @return true, if successful
     */
    public boolean validate(final CoActMats actMats) {
    	
        validationErrors = new ArrayList<>();

    	if (actMats.getIdCountry() != null && !iCountry
                .findById(actMats.getIdCountry()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ActiveMaterial.Country is not valid",
                    actMats);

            return false;
    	}
    	
    	if (actMats.getPhy_id() != null && !iPhytosanitary
                .findById(actMats.getPhy_id()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ActiveMaterial.Phytosanitary is not valid",
                    actMats);

            return false;
    	}

    	if (actMats.getAcs_id() != null && !iActSus
                .findById(actMats.getAcs_id()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ActiveMaterial.ActiveSubstance is not valid",
                    actMats);

            return false;
    	}
    	
    	if (actMats.getUni_id() != null && !iUnitType
                .findById(actMats.getUni_id()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Phytosanitary.ActiveMaterial.UnitType is not valid",
                    actMats);

            return false;
    	}
    	
    	return true;
    }    
    
    
    /**
     * Validate.
     *
     * @param rpCropActivityMessage the rpCropActivityMessage
     * @return true, if successful
     */
    public boolean validate(final RpCropActivityMessage rpCropActivityMessage) {
    	
        validationErrors = new ArrayList<>();

        // validate CoCountry
    	if (rpCropActivityMessage.getCountry() != null && !iCountry
                .findById(rpCropActivityMessage.getCountry()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.Country is not valid",
                    rpCropActivityMessage);

            return false;
    	}
    	
        // validate CoCultivationSystem
    	if (rpCropActivityMessage.getCultivationSystem() != null && !iCultivationSystem
                .findById(rpCropActivityMessage.getCultivationSystem()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.CultivationSystem is not valid",
                    rpCropActivityMessage);

            return false;
    	}                
        
        // validate CoCultivationDetail
    	if (rpCropActivityMessage.getCultivationDetail() != null && !iCultivationDetail
                .findById(rpCropActivityMessage.getCultivationDetail()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.CultivationDetail is not valid",
                    rpCropActivityMessage);

            return false;
    	}                
    	
        // validate CoSeedType
    	if (rpCropActivityMessage.getSeedType() != null && !iSeedType
                .findById(rpCropActivityMessage.getSeedType()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.SeedType is not valid",
                    rpCropActivityMessage);

            return false;
    	}                
    	
        // validate CoOrganicFarming
    	if (rpCropActivityMessage.getOrganicFarming() != null && !iOrganicFarming
                .findById(rpCropActivityMessage.getOrganicFarming()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Activity.OrganicFarming is not valid",
                    rpCropActivityMessage);

            return false;
    	}
    	                
        // validate CoLaborType
    	if (rpCropActivityMessage.getLaborType() != null && !iLaborType
                .findById(rpCropActivityMessage.getLaborType()).isPresent()) {
            addError(
                    "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.ActivityDetail.LaborType is null",
                    rpCropActivityMessage);

            return false;
    	}      
    	
        
        // farmer by country and personalID
        PrFarmer prFarmer = new PrFarmer();

        if (rpCropActivityMessage.getCountry() != null
                && rpCropActivityMessage.getPersonalID() != null) {
        	

        	
            final Optional<PrFarmer> farmer = iFarmer.findByCountryAndPersonalID(
            		rpCropActivityMessage.getCountry(),
            		rpCropActivityMessage.getPersonalID());

            System.out.println("Farmer check " + farmer.toString());
            
            if (farmer.isPresent()) {
                prFarmer = farmer.get();

            	
            }
            else {
                addError(
                        "RpCropActivityMessage is not valid",
                        rpCropActivityMessage);      
                return false;
            }
        }
    	
    	return true;
    }
    
}