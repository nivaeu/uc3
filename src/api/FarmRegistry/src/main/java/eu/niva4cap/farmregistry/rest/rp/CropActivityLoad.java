/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest.rp;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.niva4cap.farmregistry.beans.RpCropActivity;
import eu.niva4cap.farmregistry.beans.RpCropActivityDetail;
import eu.niva4cap.farmregistry.beans.RpCropActivityMessage;
import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.beans.catalogs.CoCropProduct;
import eu.niva4cap.farmregistry.beans.ecrop.CropActivityMessage;
import eu.niva4cap.farmregistry.beans.ecrop.CropProductMessage;
import eu.niva4cap.farmregistry.beans.ecrop.Response;
import eu.niva4cap.farmregistry.beans.validators.Validator;
import eu.niva4cap.farmregistry.repositories.IRpCropActivity;
import eu.niva4cap.farmregistry.repositories.IRpCropActivityDetail;
import eu.niva4cap.farmregistry.repositories.IRpCropParcel;
import eu.niva4cap.farmregistry.repositories.IRpReferencePlot;
import eu.niva4cap.farmregistry.repositories.catalogs.ICoCropProduct;

import eu.niva4cap.farmregistry.utils.ConstantesRest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@RestController
@RequestMapping(value = ConstantesRest.REST_CROP_ACTIVITY, produces = { ConstantesRest.REST_PRODUCES })
@CrossOrigin(origins = "*", methods = { RequestMethod.POST })
@Api(tags = "Crop Activities")
public class CropActivityLoad {

	@Autowired
	private Validator validator;

	@Autowired
	private IRpCropActivity iCropActivity;

	@Autowired
	private IRpCropActivityDetail iCropActivityDetail;
	
	@Autowired
	private IRpReferencePlot iReferencePlot;
	
	@Autowired
	private IRpCropParcel iCropParcel;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CropActivityMessage.class);

	@PostMapping
	@ApiOperation(value = "Inserts data into DB", authorizations = { @Authorization(value="BearerToken") })
	@PreAuthorize("hasRole('WRITE')")
	@Transactional(propagation = Propagation.NEVER)
	public ResponseEntity<Response> create(@RequestBody final CropActivityMessage data) {
		final Response response = new Response();

		try {

			for (final RpCropActivityMessage rpcropActivity : data.getRpcropActivityMessage()) {
				
				if (validator.validate(rpcropActivity)) {

					RpCropActivity cropActivity = new RpCropActivity();
					
					cropActivity.setAgriculturalActivity(rpcropActivity.getAgriculturalActivity());
					cropActivity.setIdCultivationSystem(rpcropActivity.getCultivationSystem());
					cropActivity.setIdCultivationDetail(rpcropActivity.getCultivationDetail());
					cropActivity.setIdSeedType(rpcropActivity.getSeedType());
					cropActivity.setIdOrganicFarming(rpcropActivity.getOrganicFarming());
					
					RpCropActivityDetail cropActivityDetail = new RpCropActivityDetail();
					cropActivityDetail.setIdLaborType(rpcropActivity.getLaborType());
					
					cropActivityDetail.setStartDate(rpcropActivity.getStartDate());
					cropActivityDetail.setEndDate(rpcropActivity.getEndDate());
					
					Optional<List<RpReferencePlot>> rpreferenceplot = iReferencePlot.findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(rpcropActivity.getCountry(), 
							rpcropActivity.getPersonalID(), rpcropActivity.getProductionUnitKey(), rpcropActivity.getAgriculturalBlockKey(), 
							rpcropActivity.getReferenceCropParcelKey());
					
					Optional<List<RpCropParcel>> rpcropparcel = iCropParcel.findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(rpcropActivity.getCountry(), 
							rpcropActivity.getPersonalID(), rpcropActivity.getProductionUnitKey(), rpcropActivity.getAgriculturalBlockKey(), 
							rpcropActivity.getReferenceCropParcelKey());
					
					if (rpcropparcel.isPresent() && rpreferenceplot.isPresent()) {
						List<RpCropParcel> rpcp = rpcropparcel.get();
						UUID uuid = rpcp.get(0).getId();
						
						cropActivity.setIdCropParcel(uuid);
						cropActivity.setCropParcel(rpcp.get(0));
						cropActivityDetail.setIdCropParcel(uuid);
						cropActivityDetail.setCropParcel(rpcp.get(0));
						
						iCropActivityDetail.save(cropActivityDetail);

						rpcp.get(0).setCropActivity(cropActivity);
						rpreferenceplot.get().get(0).setCropParcels(rpcp);

						iReferencePlot.save(rpreferenceplot.get().get(0));
					}
					else {
						System.out.println("rpcropparcel UUID no encontrado");
					}
					
                }
				else {
					System.out.println("rpCropActivityMessage not ok");
				}
				
				if (validator.getErrors().size() > 0)
					response.addErrors(validator.getErrors());
				

			} // for (final RpCropActivityMessage rpcropActivity : data.getRpcropActivityMessage()) {
			
			if (response.getErrors() != null) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return ResponseEntity.badRequest().body(response);
			}

			else {
				response.setStatus(HttpStatus.OK.value());
				return ResponseEntity.ok().body(response);
			}
		}

		catch (final Exception e) {
			LOGGER.error(e.getMessage());

			response.addError("Internal error in WebService");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
	}

}

