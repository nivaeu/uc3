/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpGeometry;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class ReferencePlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Key", "ReferenceParcel", "DateFrom", "DateTo", "Geometry", "FieldCrop"})
public class ReferencePlot {
    
    /** The key. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "E45:41:0:0:19:255:1S")
    private String key;
    
    /** The date from. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date dateFrom;
    
    /** The date to. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2021-03-24")
    private Date dateTo;
    
    /** The reference parcel. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "02079A038001230000EJ")
    private String referenceParcel;
    
    /** The geometries. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<Geometry> geometries;
    
    
    /** The field crops. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<FieldCrop> fieldCrops;

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the field crops.
     *
     * @return the field crops
     */
    public List<FieldCrop> getFieldCrops() {
        return fieldCrops;
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<Geometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the reference parcel.
     *
     * @return the reference parcel
     */
    public String getReferenceParcel() {
        return referenceParcel;
    }

    /**
     * Parsea el reference plot.
     *
     * @param referencePlot the reference plot
     */
    public void parseReferencePlot(final RpReferencePlot referencePlot) {
        setKey(referencePlot.getKey());
        setDateFrom(referencePlot.getDateFrom());
        setDateTo(referencePlot.getDateTo());
        setReferenceParcel(referencePlot.getReferenceParcel());

        if (referencePlot.getGeometries() != null) {
            setGeometries(new ArrayList<Geometry>());

            for (final RpGeometry rpGeometry : referencePlot.getGeometries()) {
                final Geometry geometry = new Geometry();
                getGeometries().add(geometry);

                geometry.parseGeometry(rpGeometry);
            }
        }

        if (referencePlot.getCropParcels() != null) {
            setFieldCrops(new ArrayList<FieldCrop>());

            for (final RpCropParcel rpCropParcel : referencePlot.getCropParcels()) {
                final FieldCrop fieldCrop = new FieldCrop();
                getFieldCrops().add(fieldCrop);

                fieldCrop.parseCropParcel(rpCropParcel);
            }
        }
    }
    
    /**
     * Parsea el reference plot.
     *
     * @param referencePlot the reference plot
     */
    public void parseReferencePlotCodeList(final RpReferencePlot referencePlot) {
        setKey(referencePlot.getKey());
        setDateFrom(referencePlot.getDateFrom());
        setDateTo(referencePlot.getDateTo());
        setReferenceParcel(referencePlot.getReferenceParcel());

    }    

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "DateFrom")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "DateTo")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the field crops.
     *
     * @param fieldCrops the new field crops
     */
    @JsonProperty(value = "FieldCrop")
    public void setFieldCrops(final List<FieldCrop> fieldCrops) {
        this.fieldCrops = fieldCrops;
    }

    /**
     * Sets the geometries.
     *
     * @param geometries the new geometries
     */
    @JsonProperty(value = "Geometry")
    public void setGeometries(final List<Geometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    @JsonProperty(value = "Key")
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the reference parcel.
     *
     * @param referenceParcel the new reference parcel
     */
    @JsonProperty(value = "ReferenceParcel")
    public void setReferenceParcel(final String referenceParcel) {
        this.referenceParcel = referenceParcel;
    }
}