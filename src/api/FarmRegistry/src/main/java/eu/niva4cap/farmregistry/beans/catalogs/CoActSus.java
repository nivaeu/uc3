/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoActSus.
 *
 * @author irodri11
 * @version 1.0.0,  27-ago-2021
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PACTSUS")
@JsonPropertyOrder({"id", "name", "nameeu"})
public class CoActSus {
    
    /** The id. */
    @Id
    @Column(name = "ACS_ID")
    @ApiModelProperty(example = "1")
    @JsonView(JsonViews.Simple.class)
    private Integer id;

    /** The name. */
    @Column(name = "ACS_NAME")
    @ApiModelProperty(example = "SULPHUR")
    @JsonView(JsonViews.Simple.class)
    private String name;

    /** The european name. */
    @Column(name = "ACS_NAMEEU")
    @ApiModelProperty(example = "SULPHUR")
    @JsonView(JsonViews.Simple.class)
    private String nameeu;
    

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public Integer getId() {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
     * Gets the nameeu.
     *
     * @return the nameeu
     */
    @JsonProperty(value = "nameeu")
    public String getNameeu() {
        return nameeu;
    }    
    
    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Integer id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }
    
    /**
     * Sets the nameeu.
     *
     * @param name the new nameeu
     */
    public void setNameeu(final String nameeu) {
        this.nameeu = nameeu;
    }    
    
}