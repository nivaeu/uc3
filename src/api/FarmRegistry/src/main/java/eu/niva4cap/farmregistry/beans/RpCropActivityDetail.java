/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.ActivityDetail;
import eu.niva4cap.farmregistry.beans.pks.CropActivityDetailPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropActivityDetail.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPACTIVITYDET")
@Table(name = "RP_DCROPACTIVITYDET")
@JsonPropertyOrder({"id_labor_type", "start_date", "end_date"})
public class RpCropActivityDetail {
    
    /** The crop activity detail PK. */
    @Valid
    protected @EmbeddedId CropActivityDetailPK cropActivityDetailPK = new CropActivityDetailPK();

    /** The id crop parcel. */
    @Column(name = "RCP_ID", insertable = false, updatable = false)
    private UUID idCropParcel;

    /** The id labor type. */
    @Column(name = "LAB_ID", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Integer idLaborType;

    /** The start date. */
    @Column(name = "CAD_STARTDATE", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date startDate;

    /** The end date. */
    @Column(name = "CAD_ENDDATE")
    @JsonView(JsonViews.Detailed.class)
    private Date endDate;

    /** The crop parcel. */
    @MapsId(value = "idCropParcel")
    @ManyToOne
    @JoinColumn(name = "RCP_ID")
    private RpCropParcel cropParcel;

    /**
     * Gets the crop parcel.
     *
     * @return the crop parcel
     */
    @JsonIgnore
    public RpCropParcel getCropParcel() {
        return cropParcel;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    @JsonIgnore
    public UUID getIdCropParcel() {
        return cropActivityDetailPK.getIdCropParcel();
    }

    /**
     * Gets the id labor type.
     *
     * @return the id labor type
     */
    public Integer getIdLaborType() {
        return cropActivityDetailPK.getIdLaborType();
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return cropActivityDetailPK.getStartDate();
    }

    /**
     * Parsea el activity detail.
     *
     * @param activityDetail the activity detail
     */
    public void parseActivityDetail(final ActivityDetail activityDetail) {
        setIdLaborType(activityDetail.getLaborType());
        setStartDate(activityDetail.getStartDate());
        setEndDate(activityDetail.getEndDate());
    }

    /**
     * Sets the crop parcel.
     *
     * @param cropParcel the new crop parcel
     */
    public void setCropParcel(final RpCropParcel cropParcel) {
        this.cropParcel = cropParcel;
    }

    /**
     * Sets the end date.
     *
     * @param date the new end date
     */
    @JsonProperty(value = "end_date")
    public void setEndDate(final java.util.Date date) {
        this.endDate = date == null ? null : (Date) date.clone();
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        cropActivityDetailPK.setIdCropParcel(idCropParcel);
    }

    /**
     * Sets the id labor type.
     *
     * @param idLaborType the new id labor type
     */
    @JsonProperty(value = "id_labor_type")
    public void setIdLaborType(final Integer idLaborType) {
        cropActivityDetailPK.setIdLaborType(idLaborType);
    }

    /**
     * Sets the start date.
     *
     * @param date the new start date
     */
    @JsonProperty(value = "start_date")
    public void setStartDate(final java.util.Date date) {
        cropActivityDetailPK.setStartDate(date);
    }


}