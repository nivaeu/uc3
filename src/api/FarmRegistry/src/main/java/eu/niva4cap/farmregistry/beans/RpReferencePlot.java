/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.FieldCrop;
import eu.niva4cap.farmregistry.beans.ecrop.Geometry;
import eu.niva4cap.farmregistry.beans.ecrop.ReferencePlot;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpReferencePlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HREFPLOT")
@Table(name = "RP_DREFPLOT")
@JsonPropertyOrder({"key", "reference_parcel", "date_from", "date_to", "geometry", "crop_parcel"})
public class RpReferencePlot {
    
    /** The id. */
    @Id
    @Column(name = "RFP_ID")
    @GeneratedValue(generator = "UUID")
    private UUID id;

    /** The key. */
    @Column(name = "RFP_KEY")
    @JsonView(JsonViews.Simple.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.Key is null or empty")
    @Size(max = 50,
          message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.Key is too long")
    private String key;

    /** The id agricultural block. */
    @Column(name = "ABL_ID", insertable = false, updatable = false)
    private UUID idAgriculturalBlock;

    /** The reference parcel. */
    @Column(name = "RFP_REFPARCEL")
    @JsonView(JsonViews.FullDetailed.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.ReferenceParcel is null or empty")
    @Size(max = 28,
          message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.ReferenceParcel is too long")
    private String referenceParcel;

    /** The date from. */
    @Column(name = "RFP_DATEFROM")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "RFP_DATETO")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateTo;

    /** The agricultural block. */
    @ManyToOne
    @JoinColumn(name = "ABL_ID")
    private AbAgriculturalBlock agriculturalBlock;

    /** The geometries. */
    @OneToMany(mappedBy = "referencePlot", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "geometry")
    private List<RpGeometry> geometries = new ArrayList<>();

    /** The crop parcels. */
    @OneToMany(mappedBy = "referencePlot", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "crop_parcel")
    private List<RpCropParcel> cropParcels = new ArrayList<>();

    /**
     * Gets the agricultural block.
     *
     * @return the agricultural block
     */
    @JsonIgnore
    public AbAgriculturalBlock getAgriculturalBlock() {
        return agriculturalBlock;
    }

    /**
     * Gets the crop parcel by key.
     *
     * @param key the key
     * @return the crop parcel by key
     */
    public RpCropParcel getCropParcelByKey(final String key) {
        for (final RpCropParcel cropParcel : getCropParcels()) {
            if (cropParcel.getKey().equals(key)) {
                return cropParcel;
            }
        }

        return null;
    }

    /**
     * Gets the crop parcels.
     *
     * @return the crop parcels
     */
    public List<RpCropParcel> getCropParcels() {
        return cropParcels;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<RpGeometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the geometry.
     *
     * @param date the date
     * @return the geometry
     */
    public RpGeometry getGeometry(final Date date) {
        for (final RpGeometry geometry : getGeometries()) {
            if (geometry.getDateFrom().toString().equals(date.toString())) {
                return geometry;
            }
        }

        return null;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonIgnore
    public UUID getId() {
        return id;
    }

    /**
     * Gets the id agricultural block.
     *
     * @return the id agricultural block
     */
    @JsonIgnore
    public UUID getIdAgriculturalBlock() {
        return agriculturalBlock.getId();
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the reference parcel.
     *
     * @return the reference parcel
     */
    public String getReferenceParcel() {
        return referenceParcel;
    }

    /**
     * Parsea el reference plot.
     *
     * @param referencePlot the reference plot
     */
    public void parseReferencePlot(final ReferencePlot referencePlot) {
        setKey(referencePlot.getKey());

        if (referencePlot.getReferenceParcel() != null) {
            setReferenceParcel(referencePlot.getReferenceParcel());
        }

        if (referencePlot.getDateFrom() != null) {
            setDateFrom(referencePlot.getDateFrom());
        }

        if (referencePlot.getDateTo() != null) {
            setDateTo(referencePlot.getDateTo());
        }

        if (referencePlot.getGeometries() != null) {
            for (final Geometry geometry : referencePlot.getGeometries()) {
                RpGeometry rpGeometry = new RpGeometry();

                final RpGeometry wanted = getGeometry(geometry.getDateFrom());

                if (wanted != null) {
                    rpGeometry = wanted;
                }

                else {
                    getGeometries().add(rpGeometry);

                    rpGeometry.setReferencePlot(this);
                }

                rpGeometry.parseGeometry(geometry);
            }
        }

        if (referencePlot.getFieldCrops() != null) {
            for (final FieldCrop fieldCrop : referencePlot.getFieldCrops()) {
                RpCropParcel rpCropParcel = new RpCropParcel();

                final RpCropParcel wanted = getCropParcelByKey(fieldCrop.getKey());

                if (wanted != null) {
                    rpCropParcel = wanted;
                }

                else {
                    getCropParcels().add(rpCropParcel);

                    rpCropParcel.setReferencePlot(this);
                }

                rpCropParcel.parseFieldCrop(fieldCrop);
            }
        }
    }

    /**
     * Sets the agricultural block.
     *
     * @param agriculturalBlock the new agricultural block
     */
    public void setAgriculturalBlock(final AbAgriculturalBlock agriculturalBlock) {
        this.agriculturalBlock = agriculturalBlock;
    }

    /**
     * Sets the crop parcels.
     *
     * @param cropParcels the new crop parcels
     */
    public void setCropParcels(final List<RpCropParcel> cropParcels) {
        this.cropParcels = cropParcels;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometries.
     *
     * @param geometries the new geometries
     */
    public void setGeometries(final List<RpGeometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Sets the id agricultural block.
     *
     * @param idAgriculturalBlock the new id agricultural block
     */
    public void setIdAgriculturalBlock(final UUID idAgriculturalBlock) {
        agriculturalBlock.setId(idAgriculturalBlock);
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the reference parcel.
     *
     * @param referenceParcel the new reference parcel
     */
    @JsonProperty(value = "reference_parcel")
    public void setReferenceParcel(final String referenceParcel) {
        this.referenceParcel = referenceParcel;
    }
}