/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.utils;

/**
 * The Class JsonViews.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public class JsonViews
{
	
	/**
	 * The Interface Simple.
	 *
	 * @author rcampana
	 * @version 1.0.0,  26-nov-2020
	 * @since JDK 1.6
	 */
	public interface Simple { }
	
	/**
	 * The Interface Detailed.
	 *
	 * @author rcampana
	 * @version 1.0.0,  26-nov-2020
	 * @since JDK 1.6
	 */
	public interface Detailed extends Simple { }
	
	/**
	 * The Interface FullDetailed.
	 *
	 * @author rcampana
	 * @version 1.0.0,  26-nov-2020
	 * @since JDK 1.6
	 */
	public interface FullDetailed extends Detailed { }
}