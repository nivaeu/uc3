/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.AbAgriculturalBlock;
import eu.niva4cap.farmregistry.beans.AbGeometry;
import eu.niva4cap.farmregistry.beans.AbLandCover;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CropPlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Key", "Start", "End", "SoilType", "Geometry", "ReferencePlot"})
public class CropPlot {
    
    /** The key. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "9174226")
    private String key;
    
    /** The start. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date start;
    
    /** The end. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2021-03-12")
    private Date end;
    
    /** The soil types. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<SoilType> soilTypes;
    
    /** The geometries. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<Geometry> geometries;
    
    /** The reference plots. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<ReferencePlot> referencePlots;

    /**
     * Gets the end.
     *
     * @return the end
     */
    public Date getEnd() {
        return end == null ? null : (Date) end.clone();
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<Geometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the reference plots.
     *
     * @return the reference plots
     */
    public List<ReferencePlot> getReferencePlots() {
        return referencePlots;
    }

    /**
     * Gets the soil types.
     *
     * @return the soil types
     */
    public List<SoilType> getSoilTypes() {
        return soilTypes;
    }

    /**
     * Gets the start.
     *
     * @return the start
     */
    public Date getStart() {
        return start == null ? null : (Date) start.clone();
    }

    /**
     * Parsea el agricultural block.
     *
     * @param agriculturalBlock the agricultural block
     */
    public void parseAgriculturalBlock(final AbAgriculturalBlock agriculturalBlock) {
        setKey(agriculturalBlock.getKey());
        setStart(agriculturalBlock.getDateFrom());
        setEnd(agriculturalBlock.getDateTo());

        if (agriculturalBlock.getLandCovers() != null) {
            setSoilTypes(new ArrayList<SoilType>());

            for (final AbLandCover abLandCover : agriculturalBlock.getLandCovers()) {
                final SoilType soilType = new SoilType();
                getSoilTypes().add(soilType);

                soilType.parseLandCover(abLandCover);
            }
        }

        if (agriculturalBlock.getGeometries() != null) {
            setGeometries(new ArrayList<Geometry>());

            for (final AbGeometry abGeometry : agriculturalBlock.getGeometries()) {
                final Geometry geometry = new Geometry();
                getGeometries().add(geometry);

                geometry.parseGeometry(abGeometry);
            }
        }

        if (agriculturalBlock.getReferencePlots() != null) {
            setReferencePlots(new ArrayList<ReferencePlot>());

            for (final RpReferencePlot rpReferencePlot : agriculturalBlock.getReferencePlots()) {
                final ReferencePlot referencePlot = new ReferencePlot();
                referencePlots.add(referencePlot);

                referencePlot.parseReferencePlot(rpReferencePlot);
                
            }
        }
    }
    
    
    /**
     * Parsea el agricultural block.
     *
     * @param agriculturalBlock the agricultural block
     */
    public void parseAgriculturalBlockCodeList(final AbAgriculturalBlock agriculturalBlock) {
        setKey(agriculturalBlock.getKey());
        setStart(agriculturalBlock.getDateFrom());
        setEnd(agriculturalBlock.getDateTo());

    }    

    /**
     * Sets the end.
     *
     * @param end the new end
     */
    @JsonProperty(value = "End")
    public void setEnd(final Date end) {
        this.end = end == null ? null : (Date) end.clone();
    }

    /**
     * Sets the geometries.
     *
     * @param geometries the new geometries
     */
    @JsonProperty(value = "Geometry")
    public void setGeometries(final List<Geometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    @JsonProperty(value = "Key")
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the reference plots.
     *
     * @param referencePlots the new reference plots
     */
    @JsonProperty(value = "ReferencePlot")
    public void setReferencePlots(final List<ReferencePlot> referencePlots) {
        this.referencePlots = referencePlots;
    }

    /**
     * Sets the soil types.
     *
     * @param soilTypes the new soil types
     */
    @JsonProperty(value = "SoilType")
    public void setSoilTypes(final List<SoilType> soilTypes) {
        this.soilTypes = soilTypes;
    }

    /**
     * Sets the start.
     *
     * @param start the new start
     */
    @JsonProperty(value = "Start")
    public void setStart(final Date start) {
        this.start = start == null ? null : (Date) start.clone();
    }
}