/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class ProductionWorkunitPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class ProductionWorkunitPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -132069712240944650L;

    /** The id production unit. */
    @Column(name = "PUN_id")
    private UUID idProductionUnit;

    /** The id workunit type. */
    @Column(name = "WUT_id")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.Workunit.Type is null")
    private Integer idWorkunitType;

    /** The date from. */
    @Column(name = "PUW_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.Workunit.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductionWorkunitPK other = (ProductionWorkunitPK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idProductionUnit == null) {
            if (other.idProductionUnit != null) {
                return false;
            }
        } else if (!idProductionUnit.equals(other.idProductionUnit)) {
            return false;
        }
        if (idWorkunitType == null) {
            if (other.idWorkunitType != null) {
                return false;
            }
        } else if (!idWorkunitType.equals(other.idWorkunitType)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id production unit.
     *
     * @return the id production unit
     */
    public UUID getIdProductionUnit() {
        return idProductionUnit;
    }

    /**
     * Gets the id workunit type.
     *
     * @return the id workunit type
     */
    public Integer getIdWorkunitType() {
        return idWorkunitType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result + ((idProductionUnit == null) ? 0 : idProductionUnit.hashCode());
        result = prime * result + ((idWorkunitType == null) ? 0 : idWorkunitType.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id production unit.
     *
     * @param idProductionUnit
     *            the new id production unit
     */
    public void setIdProductionUnit(final UUID idProductionUnit) {
        this.idProductionUnit = idProductionUnit;
    }

    /**
     * Sets the id workunit type.
     *
     * @param idWorkunitType
     *            the new id workunit type
     */
    public void setIdWorkunitType(final Integer idWorkunitType) {
        this.idWorkunitType = idWorkunitType;
    }
}