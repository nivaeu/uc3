/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropActivityDetail;
import eu.niva4cap.farmregistry.beans.RpCropFertilizer;
import eu.niva4cap.farmregistry.beans.RpCropIrrigation;
import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpCropParcelGeometry;
import eu.niva4cap.farmregistry.beans.RpCropPhytosanitary;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class FieldCrop.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Key", "Irrigation", "CropProduct", "CropVariety", "LandscapeFeatures",
        "LandTenure", "DateFrom", "DateTo", "Geometry", "Activity", "ActivityDetail",
        "CropIrrigation", "Phytosanitary", "Fertilizer"})
public class FieldCrop {
    
    /** The key. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "FC_123330")
    private String key;
    
    /** The irrigation. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "true")
    private Boolean irrigation;
    
    /** The crop product. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "HORVX")
    private String cropProduct;
    
    /** The crop variety. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "")
    private String cropVariety;
    
    /** The landscape features. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "true")
    private Boolean landscapeFeatures;
    
    /** The land tenure. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "3")
    private Integer landTenure;
    
    /** The date from. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2020-01-01")
    private Date dateFrom;
    
    /** The date to. */
    @JsonView(JsonViews.FullDetailed.class)
    @ApiModelProperty(example = "2021-03-12")
    private Date dateTo;
    
    /** The geometries. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<Geometry> geometries;
    
    /** The activity. */
    @JsonView(JsonViews.FullDetailed.class)
    private Activity activity;
    
    /** The activity details. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<ActivityDetail> activityDetails;
    
    /** The crop irrigations. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<CropIrrigation> cropIrrigations;
    
    /** The crop phytosanitaries. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<CropPhytosanitary> cropPhytosanitaries;
    
    /** The crop fertilizers. */
    @JsonView(JsonViews.FullDetailed.class)
    private List<CropFertilizer> cropFertilizers;

    /**
     * Gets the activity.
     *
     * @return the activity
     */
    public Activity getActivity() {
        return activity;
    }

    /**
     * Gets the activity details.
     *
     * @return the activity details
     */
    public List<ActivityDetail> getActivityDetails() {
        return activityDetails;
    }

    /**
     * Gets the crop fertilizers.
     *
     * @return the crop fertilizers
     */
    public List<CropFertilizer> getCropFertilizers() {
        return cropFertilizers;
    }

    /**
     * Gets the crop irrigations.
     *
     * @return the crop irrigations
     */
    public List<CropIrrigation> getCropIrrigations() {
        return cropIrrigations;
    }

    /**
     * Gets the crop phytosanitaries.
     *
     * @return the crop phytosanitaries
     */
    public List<CropPhytosanitary> getCropPhytosanitaries() {
        return cropPhytosanitaries;
    }

    /**
     * Gets the crop product.
     *
     * @return the crop product
     */
    public String getCropProduct() {
        return cropProduct;
    }

    /**
     * Gets the crop variety.
     *
     * @return the crop variety
     */
    public String getCropVariety() {
        return cropVariety;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<Geometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the irrigation.
     *
     * @return the irrigation
     */
    public Boolean getIrrigation() {
        return irrigation;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the landscape features.
     *
     * @return the landscape features
     */
    public Boolean getLandscapeFeatures() {
        return landscapeFeatures;
    }

    /**
     * Gets the land tenure.
     *
     * @return the land tenure
     */
    public Integer getLandTenure() {
        return landTenure;
    }

    /**
     * Parsea el crop parcel.
     *
     * @param cropParcel the crop parcel
     */
    public void parseCropParcel(final RpCropParcel cropParcel) {
        setKey(cropParcel.getKey());
        setIrrigation(cropParcel.getIrrigation());
        setCropProduct(cropParcel.getIdCropProduct());
        setCropVariety(cropParcel.getIdCropVariety());
        setLandscapeFeatures(cropParcel.getLandscapeFeatures());
        setLandTenure(cropParcel.getIdLandTenure());
        setDateFrom(cropParcel.getDateFrom());
        setDateTo(cropParcel.getDateTo());

        if (cropParcel.getGeometries() != null) {
            setGeometries(new ArrayList<Geometry>());

            for (final RpCropParcelGeometry rpGeometry : cropParcel.getGeometries()) {
                final Geometry geometry = new Geometry();
                getGeometries().add(geometry);

                geometry.parseGeometry(rpGeometry);
            }
        }

        if (cropParcel.getCropActivity() != null) {
            final Activity act = new Activity();
            setActivity(act);

            act.parseActivity(cropParcel.getCropActivity());
        }

        if (cropParcel.getCropActivityDetails() != null) {
            setActivityDetails(new ArrayList<ActivityDetail>());

            for (final RpCropActivityDetail rpCropActivityDetail : cropParcel
                    .getCropActivityDetails()) {
                final ActivityDetail activityDetail = new ActivityDetail();
                getActivityDetails().add(activityDetail);

                activityDetail.parseCropActivityDetail(rpCropActivityDetail);
            }
        }

        if (cropParcel.getCropIrrigations() != null) {
            setCropIrrigations(new ArrayList<CropIrrigation>());

            for (final RpCropIrrigation rpCropIrrigation : cropParcel.getCropIrrigations()) {
                final CropIrrigation cropIrrigation = new CropIrrigation();
                getCropIrrigations().add(cropIrrigation);

                cropIrrigation.parseCropIrrigation(rpCropIrrigation);
            }
        }

        if (cropParcel.getCropPhytosanitaries() != null) {
            setCropPhytosanitaries(new ArrayList<CropPhytosanitary>());

            for (final RpCropPhytosanitary rpCropPhytosanitary : cropParcel
                    .getCropPhytosanitaries()) {
                final CropPhytosanitary cropPhytosanitary = new CropPhytosanitary();
                getCropPhytosanitaries().add(cropPhytosanitary);

                cropPhytosanitary.parseCropPhytosanitary(rpCropPhytosanitary);
            }
        }

        if (cropParcel.getCropFertilizers() != null) {
            setCropFertilizers(new ArrayList<CropFertilizer>());

            for (final RpCropFertilizer rpCropFertilizer : cropParcel.getCropFertilizers()) {
                final CropFertilizer cropFertilizer = new CropFertilizer();
                getCropFertilizers().add(cropFertilizer);

                cropFertilizer.parseCropFertilizer(rpCropFertilizer);
            }
        }
    }
    
    /**
     * Parsea el crop parcel.
     *
     * @param cropParcel the crop parcel
     */
    public void parseCropParcelCodeList(final RpCropParcel cropParcel) {
        setKey(cropParcel.getKey());
        setIrrigation(cropParcel.getIrrigation());
        setCropProduct(cropParcel.getIdCropProduct());
        setCropVariety(cropParcel.getIdCropVariety());
        setLandscapeFeatures(cropParcel.getLandscapeFeatures());
        setLandTenure(cropParcel.getIdLandTenure());
        setDateFrom(cropParcel.getDateFrom());
        setDateTo(cropParcel.getDateTo());

    }

    /**
     * Sets the activity.
     *
     * @param activity the new activity
     */
    @JsonProperty(value = "Activity")
    public void setActivity(final Activity activity) {
        this.activity = activity;
    }

    /**
     * Sets the activity details.
     *
     * @param activityDetails the new activity details
     */
    @JsonProperty(value = "ActivityDetail")
    public void setActivityDetails(final List<ActivityDetail> activityDetails) {
        this.activityDetails = activityDetails;
    }

    /**
     * Sets the crop fertilizers.
     *
     * @param cropFertilizers the new crop fertilizers
     */
    @JsonProperty(value = "Fertilizer")
    public void setCropFertilizers(final List<CropFertilizer> cropFertilizers) {
        this.cropFertilizers = cropFertilizers;
    }

    /**
     * Sets the crop irrigations.
     *
     * @param cropIrrigations the new crop irrigations
     */
    @JsonProperty(value = "CropIrrigation")
    public void setCropIrrigations(final List<CropIrrigation> cropIrrigations) {
        this.cropIrrigations = cropIrrigations;
    }

    /**
     * Sets the crop phytosanitaries.
     *
     * @param cropPhytosanitaries the new crop phytosanitaries
     */
    @JsonProperty(value = "Phytosanitary")
    public void setCropPhytosanitaries(final List<CropPhytosanitary> cropPhytosanitaries) {
        this.cropPhytosanitaries = cropPhytosanitaries;
    }

    /**
     * Sets the crop product.
     *
     * @param cropProduct the new crop product
     */
    @JsonProperty(value = "CropProduct")
    public void setCropProduct(final String cropProduct) {
        this.cropProduct = cropProduct;
    }

    /**
     * Sets the crop variety.
     *
     * @param cropVariety the new crop variety
     */
    @JsonProperty(value = "CropVariety")
    public void setCropVariety(final String cropVariety) {
        this.cropVariety = cropVariety;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom the new date from
     */
    @JsonProperty(value = "DateFrom")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo the new date to
     */
    @JsonProperty(value = "DateTo")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometries.
     *
     * @param geometries the new geometries
     */
    @JsonProperty(value = "Geometry")
    public void setGeometries(final List<Geometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the irrigation.
     *
     * @param irrigation the new irrigation
     */
    @JsonProperty(value = "Irrigation")
    public void setIrrigation(final Boolean irrigation) {
        this.irrigation = irrigation;
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    @JsonProperty(value = "Key")
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the landscape features.
     *
     * @param landscapeFeatures the new landscape features
     */
    @JsonProperty(value = "LandscapeFeatures")
    public void setLandscapeFeatures(final Boolean landscapeFeatures) {
        this.landscapeFeatures = landscapeFeatures;
    }

    /**
     * Sets the land tenure.
     *
     * @param landTenure the new land tenure
     */
    @JsonProperty(value = "LandTenure")
    public void setLandTenure(final Integer landTenure) {
        this.landTenure = landTenure;
    }
}