/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.Optional;
import java.util.UUID;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.beans.ecrop.ReferencePlot;

/**
 * The Interface IRpReferencePlot.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IRpReferencePlot extends JpaRepository<RpReferencePlot, UUID>
{
	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country",
	           nativeQuery = true)
	
	    Optional<List<RpReferencePlot>> findByCountryAndPersonalID(@Param("country") String country,
	            @Param("personalID") String personalID);
	
	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and \r\n"
			+ "pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :key",
	           nativeQuery = true)
	
	    Optional<List<RpReferencePlot>> findByCountryAndPersonalIDAndKey(@Param("country") String country,
	            @Param("personalID") String personalID, 
	            @Param("key") String key);
	
	
	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and \r\n"
			+ "pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :key and ab.ABL_key= :AgriculturalBlock",
	           nativeQuery = true)
	
	    Optional<List<RpReferencePlot>> findByCountryAndPersonalIDAndKeyAndBlock(@Param("country") String country,
	            @Param("personalID") String personalID, 
	            @Param("key") String key,
    			@Param("AgriculturalBlock") String AgriculturalBlock);


	@Query(value = "SELECT* \r\n"
			+ "FROM RP_DCROPPARCEL cp, RP_DREFPLOT dp, AB_DAGRBLOCK ab, PR_DPRODUCTIONUNIT pu, PR_DFARMERDATA fd, PR_DFARMER fr \r\n"
			+ "WHERE cp.RFP_ID = dp.RFP_ID AND dp.ABL_ID=ab.ABL_id and ab.PUN_id=pu.PUN_id and pu.FAR_id=fd.FAR_id and fd.FAD_personalid= :personalID and pu.FAR_id=fr.FAR_id and fr.COU_id= :country and pu.PUN_key= :agriculturalProductionUnitKey and ab.ABL_KEY = :agriculturalBlock and dp.RFP_KEY = :referencePlot",
	           nativeQuery = true)

    Optional<List<RpReferencePlot>> findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(@Param("country") String country,
            @Param("personalID") String personalID, @Param("agriculturalProductionUnitKey") String agriculturalProductionUnitKey,
            @Param("agriculturalBlock") String agriculturalBlock, @Param("referencePlot") String referencePlot);	
		
}