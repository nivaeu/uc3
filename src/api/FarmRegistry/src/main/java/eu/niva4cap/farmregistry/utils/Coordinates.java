/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.utils;

/**
 * The Class Coordinates.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public class Coordinates {
    
    /**
     * Geo JSON 2 WKT.
     *
     * @param geometry the geometry
     * @return the string
     */
    public static String geoJSON2WKT(final String[][][] geometry) {
        final StringBuilder coordinates = new StringBuilder();
        String newcoordinates = new String();

        for (final String[][] str1 : geometry) {
            if (geometry.length > 1) {
            	newcoordinates += "(";                
            }
            for (final String[] str2 : str1) {
                newcoordinates += ",";
                newcoordinates += str2[0];
                newcoordinates += " ";
                newcoordinates += str2[1];
            }
            if (geometry.length > 1) {
            	newcoordinates += ")";
            }

        }
        
        newcoordinates = "POLYGON((" + newcoordinates.toString().substring(1) + "))";
        
        if (geometry.length > 1) {
        	newcoordinates = newcoordinates.replace("POLYGON((,", "MULTIPOLYGON(((");
        	newcoordinates = newcoordinates.replace(")(,", ")),((");
        }

        return newcoordinates;
    }

    /**
     * Wkt 2 geo JSON.
     *
     * @param geometry the geometry
     * @return the string[][][]
     */
    public static String[][][] wkt2GeoJSON(String geometry) {
        geometry = geometry.substring(10, geometry.length() - 2);
        geometry = geometry.replace(", ", ",");
        final String[] aCoordinatesTMP = geometry.split(",");

        final String[][][] aCoordinates = new String[1][aCoordinatesTMP.length][];

        for (int i = 0; i < aCoordinatesTMP.length; i++) {
            aCoordinates[0][i] = aCoordinatesTMP[i].split(" ");
        }

        return aCoordinates;
    }
}