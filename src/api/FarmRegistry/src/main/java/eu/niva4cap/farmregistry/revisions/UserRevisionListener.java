/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.revisions;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.context.SecurityContextHolder;

import eu.niva4cap.farmregistry.security.services.UserDetailsImpl;

/**
 * The listener interface for receiving userRevision events.
 * The class that is interested in processing a userRevision
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addUserRevisionListener<code> method. When
 * the userRevision event occurs, that object's appropriate
 * method is invoked.
 *
 * @see UserRevisionEvent
 */
public class UserRevisionListener implements RevisionListener
{
    
    /* (non-Javadoc)
     * @see org.hibernate.envers.RevisionListener#newRevision(java.lang.Object)
     */
    @Override
    public void newRevision(Object revisionEntity) {
        ((UserRevisionEntity)revisionEntity).setRevuser(((UserDetailsImpl)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId());
    }
}