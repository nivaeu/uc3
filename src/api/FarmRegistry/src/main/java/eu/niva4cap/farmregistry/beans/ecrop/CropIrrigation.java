/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import java.math.BigDecimal;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropIrrigation;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CropIrrigation.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"Type", "WaterOrigin", "Volume", "Unit", "Date"})
public class CropIrrigation {
    
    /** The type. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer type;
    
    /** The water origin. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
    private Integer waterOrigin;
    
    /** The volume. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "18.56")
    private BigDecimal volume;
    
    /** The unit. */
    @JsonView(JsonViews.Detailed.class) 
    @ApiModelProperty(example = "1")
    private Integer unit;
    
    /** The date. */
    @JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2021-12-31")
    private Date date;

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date == null ? null : (Date) date.clone();
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * Gets the unit.
     *
     * @return the unit
     */
    public Integer getUnit() {
        return unit;
    }

    /**
     * Gets the volume.
     *
     * @return the volume
     */
    public BigDecimal getVolume() {
        return volume;
    }

    /**
     * Gets the water origin.
     *
     * @return the water origin
     */
    public Integer getWaterOrigin() {
        return waterOrigin;
    }

    /**
     * Parsea el crop irrigation.
     *
     * @param cropIrrigation the crop irrigation
     */
    public void parseCropIrrigation(final RpCropIrrigation cropIrrigation) {
        setType(cropIrrigation.getIdIrrigationType());
        setWaterOrigin(cropIrrigation.getIdWaterOrigin());
        setVolume(cropIrrigation.getVolume());
        setUnit(cropIrrigation.getIdUnitType());
        setDate(cropIrrigation.getIrrigationDate());
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    @JsonProperty(value = "Date")
    public void setDate(final Date date) {
        this.date = date == null ? null : (Date) date.clone();
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    @JsonProperty(value = "Type")
    public void setType(final Integer type) {
        this.type = type;
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    @JsonProperty(value = "Unit")
    public void setUnit(final Integer unit) {
        this.unit = unit;
    }

    /**
     * Sets the volume.
     *
     * @param volume the new volume
     */
    @JsonProperty(value = "Volume")
    public void setVolume(final BigDecimal volume) {
        this.volume = volume;
    }

    /**
     * Sets the water origin.
     *
     * @param waterOrigin the new water origin
     */
    @JsonProperty(value = "WaterOrigin")
    public void setWaterOrigin(final Integer waterOrigin) {
        this.waterOrigin = waterOrigin;
    }
}