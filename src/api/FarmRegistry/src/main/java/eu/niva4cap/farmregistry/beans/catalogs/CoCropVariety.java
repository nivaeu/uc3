/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.catalogs;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.pks.CropVarietyPK;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class CoCropVariety.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Table(name = "CO_PCROPVARIETY")
@JsonPropertyOrder({"id", "name", "id_product", "id_country", "expiry_date"})
public class CoCropVariety {
    
    /** The crop variety PK. */
    @Valid
    protected @EmbeddedId CropVarietyPK cropVarietyPK = new CropVarietyPK();

    /** The id. */
    @Column(name = "CRV_ID", insertable = false, updatable = false)
    @ApiModelProperty(example = "1393")
    @JsonView(JsonViews.Simple.class)
    private String id;

    /** The id product. */
    @Column(name = "CRP_ID", insertable = false, updatable = false)
    @ApiModelProperty(example = "BRSNN")
    @JsonView(JsonViews.Simple.class)
    private String idProduct;

    /** The name. */
    @Column(name = "CRV_NAME")
    @ApiModelProperty(example = "ALFYBRID")
    @JsonView(JsonViews.Simple.class)
    private String name;

    /** The id country. */
    @Column(name = "COU_ID", insertable = false, updatable = false)
    @ApiModelProperty(example = "ES")
    @JsonView(JsonViews.Simple.class)
    private String idCountry;

    /** The expiry date. */
    @Column(name = "CRV_EXPIRYDATE")
    @ApiModelProperty(example = "2025-12-31")
    @JsonView(JsonViews.Detailed.class)
    private Date expiryDate;

    /**
     * Gets the expiry date.
     *
     * @return the expiry date
     */
    @JsonProperty(value = "expiry_date")
    public Date getExpiryDate() {
        return expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonProperty(value = "id")
    public String getId() {
        return cropVarietyPK.getIdVariety();
    }

    /**
     * Gets the id country.
     *
     * @return the id country
     */
    @JsonProperty(value = "id_country")
    public String getIdCountry() {
        return cropVarietyPK.getIdCountry();
    }

    /**
     * Gets the id product.
     *
     * @return the id product
     */
    @JsonProperty(value = "id_product")
    public String getIdProduct() {
        return cropVarietyPK.getIdProduct();
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonProperty(value = "name")
    public String getName() {
        return name;
    }

    /**
     * Sets the expiry date.
     *
     * @param expiryDate the new expiry date
     */
    public void setExpiryDate(final Date expiryDate) {
        this.expiryDate = expiryDate == null ? null : (Date) expiryDate.clone();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final String id) {
        cropVarietyPK.setIdVariety(id);
    }

    /**
     * Sets the id country.
     *
     * @param idCountry the new id country
     */
    public void setIdCountry(final String idCountry) {
        cropVarietyPK.setIdCountry(idCountry);
    }

    /**
     * Sets the id product.
     *
     * @param idProduct the new id product
     */
    public void setIdProduct(final String idProduct) {
        cropVarietyPK.setIdProduct(idProduct);
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }
}