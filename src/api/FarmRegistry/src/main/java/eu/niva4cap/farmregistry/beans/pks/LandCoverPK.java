/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

/**
 * The Class LandCoverPK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class LandCoverPK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -132069712240944650L;

    /** The id agricultural block. */
    @Column(name = "ABL_id")
    private UUID idAgriculturalBlock;

    /** The id land cover. */
    @Column(name = "LAN_id")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.SoilType.Type is null")
    private Integer idLandCover;

    /** The date from. */
    @Column(name = "ALC_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.SoilType.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LandCoverPK other = (LandCoverPK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idAgriculturalBlock == null) {
            if (other.idAgriculturalBlock != null) {
                return false;
            }
        } else if (!idAgriculturalBlock.equals(other.idAgriculturalBlock)) {
            return false;
        }
        if (idLandCover == null) {
            if (other.idLandCover != null) {
                return false;
            }
        } else if (!idLandCover.equals(other.idLandCover)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id agricultural block.
     *
     * @return the id agricultural block
     */
    public UUID getIdAgriculturalBlock() {
        return idAgriculturalBlock;
    }

    /**
     * Gets the id land cover.
     *
     * @return the id land cover
     */
    public Integer getIdLandCover() {
        return idLandCover;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result
                + ((idAgriculturalBlock == null) ? 0 : idAgriculturalBlock.hashCode());
        result = prime * result + ((idLandCover == null) ? 0 : idLandCover.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id agricultural block.
     *
     * @param idAgriculturalBlock
     *            the new id agricultural block
     */
    public void setIdAgriculturalBlock(final UUID idAgriculturalBlock) {
        this.idAgriculturalBlock = idAgriculturalBlock;
    }

    /**
     * Sets the id land cover.
     *
     * @param idLandCover
     *            the new id land cover
     */
    public void setIdLandCover(final Integer idLandCover) {
        this.idLandCover = idLandCover;
    }
}