/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import eu.niva4cap.farmregistry.beans.PrProductionWorkunit;

/**
 * The Interface IPrProductionWorkunit.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
public interface IPrProductionWorkunit extends JpaRepository<PrProductionWorkunit, String>
{
	
	/**
	 * Find by code.
	 *
	 * @param code the code
	 * @return the optional
	 */
	@Query(value="SELECT pw.* FROM PR_DPRODWORKUNIT pw INNER JOIN PR_DPRODUCTIONUNIT pu ON pw.PUN_id = pu.PUN_id AND pu.PUN_code = :code", nativeQuery=true) Optional<List<PrProductionWorkunit>> findByCode(@Param("code") String code);
}