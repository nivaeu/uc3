/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.CropFertilizer;
import eu.niva4cap.farmregistry.beans.pks.CropFertilizerPK;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class RpCropFertilizer.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "RP_HCROPFERTILIZER")
@Table(name = "RP_DCROPFERTILIZER")
@JsonPropertyOrder({"id_fertilizer", "dose", "id_unit_type", "id_fertilizer_type",
        "id_fertilization_type", "id_fertilization_method", "start_date", "end_date"})
public class RpCropFertilizer {
    
    /** The crop fertilizer PK. */
    @Valid
    protected @EmbeddedId CropFertilizerPK cropFertilizerPK = new CropFertilizerPK();

    /** The id crop parcel. */
    @Column(name = "RCP_ID", insertable = false, updatable = false)
    private UUID idCropParcel;

    /** The id fertilizer. */
    @Column(name = "FER_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idFertilizer;

    /** The id fertilizer type. */
    @Column(name = "FET_ID", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Integer idFertilizerType;

    /** The id fertilization type. */
    @Column(name = "FRT_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idFertilizationType;

    /** The id fertilization method. */
    @Column(name = "FEM_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idFertilizationMethod;

    /** The dose. */
    @Column(name = "CFE_DOSE")
    @JsonView(JsonViews.Detailed.class)
    @Digits(integer = 6, fraction = 2,
            message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.ReferencePlot.FieldCrop.Fertilizer.Dose is too long")
    private BigDecimal dose;

    /** The id unit type. */
    @Column(name = "UNI_ID")
    @JsonView(JsonViews.Detailed.class)
    private Integer idUnitType;

    /** The start date. */
    @Column(name = "CFE_STARTDATE", insertable = false, updatable = false)
    @JsonView(JsonViews.Detailed.class)
    private Date startDate;

    /** The end date. */
    @Column(name = "CFE_ENDDATE")
    @JsonView(JsonViews.Detailed.class)
    private Date endDate;

    /** The crop parcel. */
    @MapsId(value = "idCropParcel")
    @ManyToOne
    @JoinColumn(name = "RCP_ID")
    private RpCropParcel cropParcel;

    /**
     * Gets the crop parcel.
     *
     * @return the crop parcel
     */
    @JsonIgnore
    public RpCropParcel getCropParcel() {
        return cropParcel;
    }

    /**
     * Gets the dose.
     *
     * @return the dose
     */
    public BigDecimal getDose() {
        return dose;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Gets the id crop parcel.
     *
     * @return the id crop parcel
     */
    @JsonIgnore
    public UUID getIdCropParcel() {
        return cropFertilizerPK.getIdCropParcel();
    }

    /**
     * Gets the id fertilization method.
     *
     * @return the id fertilization method
     */
    public Integer getIdFertilizationMethod() {
        return idFertilizationMethod;
    }

    /**
     * Gets the id fertilization type.
     *
     * @return the id fertilization type
     */
    public Integer getIdFertilizationType() {
        return idFertilizationType;
    }

    /**
     * Gets the id fertilizer.
     *
     * @return the id fertilizer
     */
    public Integer getIdFertilizer() {
        return idFertilizer;
    }

    /**
     * Gets the id fertilizer type.
     *
     * @return the id fertilizer type
     */
    public Integer getIdFertilizerType() {
        return cropFertilizerPK.getIdFertilizerType();
    }

    /**
     * Gets the id unit type.
     *
     * @return the id unit type
     */
    public Integer getIdUnitType() {
        return idUnitType;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return cropFertilizerPK.getStartDate();
    }

    /**
     * Parsea el fertilizer.
     *
     * @param cropFertilizer the crop fertilizer
     */
    public void parseFertilizer(final CropFertilizer cropFertilizer) {
        setIdFertilizer(cropFertilizer.getId());
        setIdFertilizerType(cropFertilizer.getFertilizerType());
        setIdFertilizationType(cropFertilizer.getFertilizationType());
        setIdFertilizationMethod(cropFertilizer.getFertilizationMethod());
        setDose(cropFertilizer.getDose());
        setIdUnitType(cropFertilizer.getUnit());
        setStartDate(cropFertilizer.getStartDate());
        setEndDate(cropFertilizer.getEndDate());
    }

    /**
     * Sets the crop parcel.
     *
     * @param cropParcel the new crop parcel
     */
    public void setCropParcel(final RpCropParcel cropParcel) {
        this.cropParcel = cropParcel;
    }

    /**
     * Sets the dose.
     *
     * @param dose the new dose
     */
    @JsonProperty(value = "dose")
    public void setDose(final BigDecimal dose) {
        this.dose = dose;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    @JsonProperty(value = "end_date")
    public void setEndDate(final Date endDate) {
        this.endDate = endDate == null ? null : (Date) endDate.clone();
    }

    /**
     * Sets the id crop parcel.
     *
     * @param idCropParcel the new id crop parcel
     */
    public void setIdCropParcel(final UUID idCropParcel) {
        cropFertilizerPK.setIdCropParcel(idCropParcel);
    }

    /**
     * Sets the id fertilization method.
     *
     * @param idFertilizationMethod the new id fertilization method
     */
    @JsonProperty(value = "id_fertilization_method")
    public void setIdFertilizationMethod(final Integer idFertilizationMethod) {
        this.idFertilizationMethod = idFertilizationMethod;
    }

    /**
     * Sets the id fertilization type.
     *
     * @param idFertilizationType the new id fertilization type
     */
    @JsonProperty(value = "id_fertilization_type")
    public void setIdFertilizationType(final Integer idFertilizationType) {
        this.idFertilizationType = idFertilizationType;
    }

    /**
     * Sets the id fertilizer.
     *
     * @param idFertilizer the new id fertilizer
     */
    @JsonProperty(value = "id_fertilizer")
    public void setIdFertilizer(final Integer idFertilizer) {
        this.idFertilizer = idFertilizer;
    }

    /**
     * Sets the id fertilizer type.
     *
     * @param idFertilizerType the new id fertilizer type
     */
    @JsonProperty(value = "id_fertilizer_type")
    public void setIdFertilizerType(final Integer idFertilizerType) {
        cropFertilizerPK.setIdFertilizerType(idFertilizerType);
    }

    /**
     * Sets the id unit type.
     *
     * @param idUnitType the new id unit type
     */
    @JsonProperty(value = "id_unit_type")
    public void setIdUnitType(final Integer idUnitType) {
        this.idUnitType = idUnitType;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    @JsonProperty(value = "start_date")
    public void setStartDate(final Date startDate) {
        cropFertilizerPK.setStartDate(startDate);
    }
}