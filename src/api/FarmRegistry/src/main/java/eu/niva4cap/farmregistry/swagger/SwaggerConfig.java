/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.swagger;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletContext;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.Tag;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;




/**
 * The Class SwaggerConfig.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@Configuration
@EnableSwagger2

public class SwaggerConfig
{
	
	
	    /**
	 * Ui config.
	 *
	 * @return the ui configuration
	 */
	@Bean
	public UiConfiguration uiConfig() {
	    return UiConfigurationBuilder
	            .builder()
	            .operationsSorter(OperationsSorter.METHOD)
	            .build();
	}
	

	/**
	 * Api.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket api(ServletContext servletContext)
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("eu.niva4cap.farmregistry"))
				.paths(PathSelectors.any())
				.build()         
				.apiInfo(apiInfo())
		        .useDefaultResponseMessages(false)
				.securitySchemes(Collections.singletonList(apiKey()))
		        .securityContexts(Collections.singletonList(securityContext()))
		        .genericModelSubstitutes(Optional.class);
			
	}
	
	
	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder()
	            .title("eu.niva4cap.farmregistry")
	            .termsOfServiceUrl("localhost")
	            .version("1.0")
	            .build();
	}
	
	private ApiKey apiKey() {
	    return new ApiKey("BearerToken", "Authorization", "header");
	}
	
	

	private SecurityContext securityContext() {
		    return SecurityContext.builder()
		        .securityReferences(defaultAuth())
		        .forPaths(PathSelectors.none())
		        .build();
		  }

	private List<SecurityReference> defaultAuth() {
		    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessRestricted");
		    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		    authorizationScopes[0] = authorizationScope;
		    return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
		  }

	
}