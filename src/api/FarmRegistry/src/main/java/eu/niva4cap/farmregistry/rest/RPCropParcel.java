/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropParcel;
import eu.niva4cap.farmregistry.beans.RpReferencePlot;
import eu.niva4cap.farmregistry.repositories.IRpCropParcel;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class RPCropParcel.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_RP_CROP_PARCEL, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Crop Parcels")
@ApiIgnore
public class RPCropParcel
{
	
	/** The i crop parcel. */
	@Autowired
	private IRpCropParcel iCropParcel;

	/**
	 * Obtiene el.
	 *
	 * @return the response entity
	 */
	@GetMapping
	@ApiOperation(value = "Returns all Crop Parcels" , authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.Detailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropParcel>> get()
	{
		try
		{
			return ResponseEntity.ok(iCropParcel.findAll());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}

	@GetMapping(value = "/country/{country}/personalID/{personalID}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Crop Parcel by its Country and Personal ID", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropParcel>> getByCountryAndPersonalID(@PathVariable("country") String country, @PathVariable("personalID") String personalID) {
        try {
            Optional<List<RpCropParcel>> cropParcel = iCropParcel.findByCountryAndPersonalID(country, personalID);

        	if (!cropParcel.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(cropParcel.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }
	
	@GetMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Crop Parcel by its Country, Personal ID and APU Key", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropParcel>> getByCountryAndPersonalIDAPUKey(@PathVariable("country") String country, @PathVariable("personalID") String personalID, @PathVariable("agriculturalProductionUnitKey") String agriculturalProductionUnitKey) {
        try {
            Optional<List<RpCropParcel>> cropParcel = iCropParcel.findByCountryAndPersonalIDAndAPUKey(country, personalID, agriculturalProductionUnitKey);

        	if (!cropParcel.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(cropParcel.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }

	@GetMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/agriculturalBlock/{agriculturalBlock}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/agriculturalBlock/{agriculturalBlock}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Crop Parcel by its Country, Personal ID, APU Keya and Agricultural Block", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropParcel>> getByCountryAndPersonalIDAPUKeyAgrBlock(@PathVariable("country") String country, @PathVariable("personalID") String personalID, 
			@PathVariable("agriculturalProductionUnitKey") String agriculturalProductionUnitKey, @PathVariable("agriculturalBlock") String agriculturalBlock) {
        try {
            Optional<List<RpCropParcel>> cropParcel = iCropParcel.findByCountryAndPersonalIDAPUKeyAgrBlock(country, personalID, agriculturalProductionUnitKey, agriculturalBlock);

        	if (!cropParcel.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(cropParcel.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }
	

	
	@GetMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/agriculturalBlock/{agriculturalBlock}/referencePlot/{referencePlot}")
	@RequestMapping(value = "/country/{country}/personalID/{personalID}/agriculturalProductionUnitKey/{agriculturalProductionUnitKey}/agriculturalBlock/{agriculturalBlock}/referencePlot/{referencePlot}", method = RequestMethod.GET, produces = {ConstantesRest.REST_PRODUCES})
    @ApiOperation(value = "Returns an Crop Parcel by its Country, Personal ID, APU Keya, Agricultural Block and Reference Plot", authorizations = { @Authorization(value="BearerToken") })
    @JsonView(JsonViews.FullDetailed.class)
    @PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<RpCropParcel>> getByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(@PathVariable("country") String country, @PathVariable("personalID") String personalID, 
			@PathVariable("agriculturalProductionUnitKey") String agriculturalProductionUnitKey, @PathVariable("agriculturalBlock") String agriculturalBlock,
			@PathVariable("referencePlot") String referencePlot) {
        try {
            Optional<List<RpCropParcel>> cropParcel = iCropParcel.findByCountryAndPersonalIDAPUKeyAgrBlockRefPlot(country, personalID, agriculturalProductionUnitKey, agriculturalBlock, referencePlot);

        	if (!cropParcel.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(cropParcel.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}       
    }	
	
}