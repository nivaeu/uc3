/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.ecrop;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.RpCropActivity;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class Activity.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "AgriculturalActivity", "CultivationSystem", "CultivationDetail", "SeedType", "OrganicFarming" })
public class Activity
{
	
	/** The agricultural activity. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "true")
	private Boolean agriculturalActivity;
	
	/** The cultivation system. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "2")
	private Integer cultivationSystem;
	
	/** The cultivation detail. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "5")
	private Integer cultivationDetail;
	
	/** The seed type. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "6")
	private Integer seedType;
	
	/** The organic farming. */
	@JsonView(JsonViews.Detailed.class)
    @ApiModelProperty(example = "1")
	private Integer organicFarming;

	/**
	 * Parsea el activity.
	 *
	 * @param cropActivity the crop activity
	 */
	public void parseActivity(RpCropActivity cropActivity) {
		setAgriculturalActivity(cropActivity.getAgriculturalActivity());
		setCultivationSystem(cropActivity.getIdCultivationSystem());
		setCultivationDetail(cropActivity.getIdCultivationDetail());
		setSeedType(cropActivity.getIdSeedType());
		setOrganicFarming(cropActivity.getIdOrganicFarming());
	}

	/**
	 * Gets the agricultural activity.
	 *
	 * @return the agricultural activity
	 */
	public Boolean getAgriculturalActivity() {
		return agriculturalActivity;
	}

	/**
	 * Sets the agricultural activity.
	 *
	 * @param agriculturalActivity the new agricultural activity
	 */
	@JsonProperty(value = "AgriculturalActivity")
	public void setAgriculturalActivity(Boolean agriculturalActivity) {
		this.agriculturalActivity = agriculturalActivity;
	}

	/**
	 * Gets the cultivation system.
	 *
	 * @return the cultivation system
	 */
	public Integer getCultivationSystem() {
		return cultivationSystem;
	}

	/**
	 * Sets the cultivation system.
	 *
	 * @param cultivationSystem the new cultivation system
	 */
	@JsonProperty(value = "CultivationSystem")
	public void setCultivationSystem(Integer cultivationSystem) {
		this.cultivationSystem = cultivationSystem;
	}

	/**
	 * Gets the cultivation detail.
	 *
	 * @return the cultivation detail
	 */
	public Integer getCultivationDetail() {
		return cultivationDetail;
	}

	/**
	 * Sets the cultivation detail.
	 *
	 * @param cultivationDetail the new cultivation detail
	 */
	@JsonProperty(value = "CultivationDetail")
	public void setCultivationDetail(Integer cultivationDetail) {
		this.cultivationDetail = cultivationDetail;
	}

	/**
	 * Gets the seed type.
	 *
	 * @return the seed type
	 */
	public Integer getSeedType() {
		return seedType;
	}

	/**
	 * Sets the seed type.
	 *
	 * @param seedType the new seed type
	 */
	@JsonProperty(value = "SeedType")
	public void setSeedType(Integer seedType) {
		this.seedType = seedType;
	}

	/**
	 * Gets the organic farming.
	 *
	 * @return the organic farming
	 */
	public Integer getOrganicFarming() {
		return organicFarming;
	}

	/**
	 * Sets the organic farming.
	 *
	 * @param organicFarming the new organic farming
	 */
	@JsonProperty(value = "OrganicFarming")
	public void setOrganicFarming(Integer organicFarming) {
		this.organicFarming = organicFarming;
	}
}