/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.PrFarmingType;
import eu.niva4cap.farmregistry.repositories.IPrFarmingType;
import eu.niva4cap.farmregistry.utils.ConstantesRest;
import eu.niva4cap.farmregistry.utils.JsonViews;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

/**
 * The Class PRProductionUnitFarmingType.
 *
 * @author rcampana
 * @version 1.0.0,  26-nov-2020
 * @since JDK 1.6
 */
@RestController
@RequestMapping( value = ConstantesRest.REST_QUERY_PR_PRODUCTION_UNIT_FARMING_TYPE, produces = {ConstantesRest.REST_PRODUCES} )
@CrossOrigin(origins = "*", methods= { RequestMethod.GET })
@Api(tags = "Production Unit Farming Types")
@ApiIgnore
public class PRProductionUnitFarmingType
{
	
	/** The i farming type. */
	@Autowired
	private IPrFarmingType iFarmingType;

	/**
	 * Gets the by code.
	 *
	 * @param code the code
	 * @return the by code
	 */
	@GetMapping(value="{code}")
	@ApiOperation(value = "Returns Production Unit Farming Types by production unit code", authorizations = { @Authorization(value="BearerToken") })
	@JsonView(JsonViews.FullDetailed.class)
	@PreAuthorize("hasRole('READ')")
	public ResponseEntity<List<PrFarmingType>> getByCode(@PathVariable("code") String code)
	{
		try
		{
			Optional<List<PrFarmingType>> beanDB = iFarmingType.findByCode(code);

			if (!beanDB.isPresent())
				return ResponseEntity.ok(null);

			return ResponseEntity.ok(beanDB.get());
		}

		catch (Exception e)
		{
			return ResponseEntity.ok(null);
		}
	}
}