/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans.pks;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * The Class FarmingTypePK.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class FarmingTypePK implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -132069712240944650L;

    /** The id production unit. */
    @Column(name = "PUN_id")
    private UUID idProductionUnit;

    /** The id farming type. */
    @Column(name = "FRT_id")
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.FarmingType.Type is null or empty")
    private String idFarmingType;

    /** The date from. */
    @Column(name = "PFT_datefrom")
    @NotNull(message = "AgriculturalProducerParty.AgriculturalProductionUnit.FarmingType.DateFrom is null")
    private Date dateFrom;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FarmingTypePK other = (FarmingTypePK) obj;
        if (dateFrom == null) {
            if (other.dateFrom != null) {
                return false;
            }
        } else if (!dateFrom.equals(other.dateFrom)) {
            return false;
        }
        if (idFarmingType == null) {
            if (other.idFarmingType != null) {
                return false;
            }
        } else if (!idFarmingType.equals(other.idFarmingType)) {
            return false;
        }
        if (idProductionUnit == null) {
            if (other.idProductionUnit != null) {
                return false;
            }
        } else if (!idProductionUnit.equals(other.idProductionUnit)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the id farming type.
     *
     * @return the id farming type
     */
    public String getIdFarmingType() {
        return idFarmingType;
    }

    /**
     * Gets the id production unit.
     *
     * @return the id production unit
     */
    public UUID getIdProductionUnit() {
        return idProductionUnit;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
        result = prime * result + ((idFarmingType == null) ? 0 : idFarmingType.hashCode());
        result = prime * result + ((idProductionUnit == null) ? 0 : idProductionUnit.hashCode());
        return result;
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = (Date) dateFrom.clone();
    }

    /**
     * Sets the id farming type.
     *
     * @param idFarmingType
     *            the new id farming type
     */
    public void setIdFarmingType(final String idFarmingType) {
        this.idFarmingType = idFarmingType;
    }

    /**
     * Sets the id production unit.
     *
     * @param idProductionUnit
     *            the new id production unit
     */
    public void setIdProductionUnit(final UUID idProductionUnit) {
        this.idProductionUnit = idProductionUnit;
    }
}