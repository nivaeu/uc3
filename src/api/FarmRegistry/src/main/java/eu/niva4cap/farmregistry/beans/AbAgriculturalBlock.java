/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;

import eu.niva4cap.farmregistry.beans.ecrop.CropPlot;
import eu.niva4cap.farmregistry.beans.ecrop.Geometry;
import eu.niva4cap.farmregistry.beans.ecrop.ReferencePlot;
import eu.niva4cap.farmregistry.beans.ecrop.SoilType;
import eu.niva4cap.farmregistry.utils.JsonViews;

/**
 * The Class AbAgriculturalBlock. que la he cambiado para pruebas
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@Entity
@Audited
@AuditTable(value = "AB_HAGRBLOCK")
@Table(name = "AB_DAGRBLOCK")
@JsonPropertyOrder({"key", "date_from", "date_to", "land_cover", "geometry", "reference_plot"})
public class AbAgriculturalBlock {

    /** The id. */
    @Id
    @Column(name = "ABL_id")
    @GeneratedValue(generator = "UUID")
    private UUID id;

    /** The key. */
    @Column(name = "ABL_key")
    @JsonView(JsonViews.Simple.class)
    @NotEmpty(message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.Key is null or empty")
    @Size(max = 50,
          message = "AgriculturalProducerParty.AgriculturalProductionUnit.CropPlot.Key is too long")
    private String key;

    /** The id production unit. */
    @Column(name = "PUN_id", insertable = false, updatable = false)
    private UUID idProductionUnit;

    /** The date from. */
    @Column(name = "ABL_datefrom")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateFrom;

    /** The date to. */
    @Column(name = "ABL_dateto")
    @JsonView(JsonViews.FullDetailed.class)
    private Date dateTo;

    /** The production unit. */
    @ManyToOne
    @JoinColumn(name = "PUN_id")
    private PrProductionUnit productionUnit;

    /** The land covers. */
    @OneToMany(mappedBy = "agriculturalBlock", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "land_cover")
    private List<AbLandCover> landCovers = new ArrayList<>();

    /** The geometries. */
    @OneToMany(mappedBy = "agriculturalBlock", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "geometry")
    private List<AbGeometry> geometries = new ArrayList<>();

    /** The reference plots. */
    @OneToMany(mappedBy = "agriculturalBlock", cascade = CascadeType.ALL)
    @JsonView(JsonViews.FullDetailed.class)
    @JsonProperty(value = "reference_plot")
    private List<RpReferencePlot> referencePlots = new ArrayList<>();

    /**
     * Gets the date from.
     *
     * @return the date from
     */
    public Date getDateFrom() {
        return dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Gets the date to.
     *
     * @return the date to
     */
    public Date getDateTo() {
        return dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Gets the geometries.
     *
     * @return the geometries
     */
    public List<AbGeometry> getGeometries() {
        return geometries;
    }

    /**
     * Gets the geometry.
     *
     * @param date
     *            the date
     * @return the geometry
     */
    public AbGeometry getGeometry(final Date date) {
        for (final AbGeometry geometry : getGeometries()) {
            if (geometry.getDateFrom().toString().equals(date.toString())) {
                return geometry;
            }
        }

        return null;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    @JsonIgnore
    public UUID getId() {
        return id;
    }

    /**
     * Gets the id production unit.
     *
     * @return the id production unit
     */
    @JsonIgnore
    public UUID getIdProductionUnit() {
        return productionUnit.getId();
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the land cover.
     *
     * @param type
     *            the type
     * @param date
     *            the date
     * @return the land cover
     */
    public AbLandCover getLandCover(final Integer type, final Date date) {
        for (final AbLandCover landCover : getLandCovers()) {
            if (landCover.getIdLandCover() == type
                    && landCover.getDateFrom().toString().equals(date.toString())) {
                return landCover;
            }
        }

        return null;
    }

    /**
     * Gets the land covers.
     *
     * @return the land covers
     */
    public List<AbLandCover> getLandCovers() {
        return landCovers;
    }

    /**
     * Gets the production unit.
     *
     * @return the production unit
     */
    @JsonIgnore
    public PrProductionUnit getProductionUnit() {
        return productionUnit;
    }

    /**
     * Gets the reference plot by key.
     *
     * @param key
     *            the key
     * @return the reference plot by key
     */
    public RpReferencePlot getReferencePlotByKey(final String key) {
        for (final RpReferencePlot referencePlot : getReferencePlots()) {
            if (referencePlot.getKey().equals(key)) {
                return referencePlot;
            }
        }

        return null;
    }

    /**
     * Gets the reference plots.
     *
     * @return the reference plots
     */
    public List<RpReferencePlot> getReferencePlots() {
        return referencePlots;
    }

    /**
     * Parsea el crop plot.
     *
     * @param cropPlot
     *            the crop plot
     */
    public void parseCropPlot(final CropPlot cropPlot) {
        setKey(cropPlot.getKey());

        if (cropPlot.getStart() != null) {
            setDateFrom(cropPlot.getStart());
        }

        if (cropPlot.getEnd() != null) {
            setDateTo(cropPlot.getEnd());
        }

        if (cropPlot.getSoilTypes() != null) {
            for (final SoilType landCover : cropPlot.getSoilTypes()) {
                AbLandCover abLandCover = new AbLandCover();

                final AbLandCover wanted = getLandCover(landCover.getType(),
                        landCover.getDateFrom());

                if (wanted != null) {
                    abLandCover = wanted;
                }

                else {
                    getLandCovers().add(abLandCover);

                    abLandCover.setAgriculturalBlock(this);
                }

                abLandCover.parseLandCover(landCover);
            }
        }

        if (cropPlot.getGeometries() != null) {
            for (final Geometry geometry : cropPlot.getGeometries()) {
                AbGeometry abGeometry = new AbGeometry();

                final AbGeometry wanted = getGeometry(geometry.getDateFrom());

                if (wanted != null) {
                    abGeometry = wanted;
                }

                else {
                    getGeometries().add(abGeometry);

                    abGeometry.setAgriculturalBlock(this);
                }

                abGeometry.parseGeometry(geometry);
            }
        }

        if (cropPlot.getReferencePlots() != null) {
            for (final ReferencePlot referencePlot : cropPlot.getReferencePlots()) {
                RpReferencePlot rpReferencePlot = new RpReferencePlot();

                final RpReferencePlot wanted = getReferencePlotByKey(referencePlot.getKey());

                if (wanted != null) {
                    rpReferencePlot = wanted;
                }

                else {
                    getReferencePlots().add(rpReferencePlot);

                    rpReferencePlot.setAgriculturalBlock(this);
                }

                rpReferencePlot.parseReferencePlot(referencePlot);
            }
        }
    }

    /**
     * Sets the date from.
     *
     * @param dateFrom
     *            the new date from
     */
    @JsonProperty(value = "date_from")
    public void setDateFrom(final Date dateFrom) {
        this.dateFrom = dateFrom == null ? null : (Date) dateFrom.clone();
    }

    /**
     * Sets the date to.
     *
     * @param dateTo
     *            the new date to
     */
    @JsonProperty(value = "date_to")
    public void setDateTo(final Date dateTo) {
        this.dateTo = dateTo == null ? null : (Date) dateTo.clone();
    }

    /**
     * Sets the geometries.
     *
     * @param geometries
     *            the new geometries
     */
    public void setGeometries(final List<AbGeometry> geometries) {
        this.geometries = geometries;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    public void setId(final UUID id) {
        this.id = id;
    }

    /**
     * Sets the id production unit.
     *
     * @param idProductionUnit
     *            the new id production unit
     */
    public void setIdProductionUnit(final UUID idProductionUnit) {
        productionUnit.setId(idProductionUnit);
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the new key
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the land covers.
     *
     * @param landCovers
     *            the new land covers
     */
    public void setLandCovers(final List<AbLandCover> landCovers) {
        this.landCovers = landCovers;
    }

    /**
     * Sets the production unit.
     *
     * @param productionUnit
     *            the new production unit
     */
    public void setProductionUnit(final PrProductionUnit productionUnit) {
        this.productionUnit = productionUnit;
    }

    /**
     * Sets the reference plots.
     *
     * @param referencePlots
     *            the new reference plots
     */
    public void setReferencePlots(final List<RpReferencePlot> referencePlots) {
        this.referencePlots = referencePlots;
    }
}