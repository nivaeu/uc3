/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry;

/**
 * The Class Constantes.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class Constantes {

    /** The Constant USU_REST. */
    public static final String USU_REST = "rcampana";

    /** The Constant CONTR_REST. */
    public static final String CONTR_REST = "rcampana";

    /** The Constant END_POINT. */
    // PREPRODUCCION
    public static final String END_POINT = "http://svjc-pre-niva.ttec.es:8080";

    /** The Constant URL_LOGIN. */
    public static final String URL_LOGIN = END_POINT + "/FarmRegistry/auth/login";

    /** The Constant NAME. */
    public static final String NAME = "Name";

    /** The Constant BUILDING_NUMBER. */
    public static final String BUILDING_NUMBER = "BuildingNumber";

    /** The Constant CITY_NAME. */
    public static final String CITY_NAME = "CityName";

    /** The Constant CLASSIFICATION. */
    public static final String CLASSIFICATION = "Classification";

    /** The Constant COUNTRY. */
    public static final String COUNTRY = "Country";

    /** The Constant DATE_TO. */
    public static final String DATE_TO = "DateTo";

    /** The Constant DATE_FROM. */
    public static final String DATE_FROM = "DateFrom";

    /** The Constant EMAIL1. */
    public static final String EMAIL1 = "Email1";

    /** The Constant PERSONAL_ID. */
    public static final String PERSONAL_ID = "PersonalID";

    /** The Constant PHONE1. */
    public static final String PHONE1 = "Phone1";

    /** The Constant PHONE2. */
    public static final String PHONE2 = "Phone2";

    /** The Constant POSTCODE. */
    public static final String POSTCODE = "Postcode";

    /** The Constant STREET_NAME. */
    public static final String STREET_NAME = "StreetName";

    /** The Constant STRUCTURED_ADDRESS. */
    public static final String STRUCTURED_ADDRESS = "StructuredAddress";

    /** The Constant TYPE. */
    public static final String TYPE = "Type";

    /** The Constant FARMING_TYPE. */
    public static final String FARMING_TYPE = "FarmingType";

    /** The Constant AWU. */
    public static final String AWU = "AWU";

    /** The Constant WORKUNIT. */
    public static final String WORKUNIT = "Workunit";

    /** The Constant START. */
    public static final String START = "Start";

    /** The Constant END. */
    public static final String END = "End";

    /** The Constant SOIL_TYPE. */
    public static final String SOIL_TYPE = "SoilType";

    /** The Constant REFERENCE_PARCEL. */
    public static final String REFERENCE_PARCEL = "ReferenceParcel";

    /** The Constant IRRIGATION. */
    public static final String IRRIGATION = "Irrigation";

    /** The Constant CROP_PRODUCT. */
    public static final String CROP_PRODUCT = "CropProduct";

    /** The Constant LANDSCAPE_FEATURES. */
    public static final String LANDSCAPE_FEATURES = "LandscapeFeatures";

    /** The Constant LAND_TENURE. */
    public static final String LAND_TENURE = "LandTenure";

    /** The Constant CULTIVATION_SYSTEM. */
    public static final String CULTIVATION_SYSTEM = "CultivationSystem";

    /** The Constant CULTIVATION_DETAIL. */
    public static final String CULTIVATION_DETAIL = "CultivationDetail";

    /** The Constant SEED_TYPE. */
    public static final String SEED_TYPE = "SeedType";

    /** The Constant ORGANIC_FARMING. */
    public static final String ORGANIC_FARMING = "OrganicFarming";

    /** The Constant ACTIVITY. */
    public static final String ACTIVITY = "Activity";

    /** The Constant LABOR_TYPE. */
    public static final String LABOR_TYPE = "LaborType";

    /** The Constant START_DATE. */
    public static final String START_DATE = "StartDate";

    /** The Constant END_DATE. */
    public static final String END_DATE = "EndDate";

    /** The Constant ACTIVITY_DETAIL. */
    public static final String ACTIVITY_DETAIL = "ActivityDetail";

    /** The Constant FIELD_CROP. */
    public static final String FIELD_CROP = "FieldCrop";

    /** The Constant REFERENCE_PLOT. */
    public static final String REFERENCE_PLOT = "ReferencePlot";

    /** The Constant CROP_PLOT. */
    public static final String CROP_PLOT = "CropPlot";

    /** The Constant AGRICULTURAL_PRODUCTION_UNIT. */
    public static final String AGRICULTURAL_PRODUCTION_UNIT = "AgriculturalProductionUnit";

    /** The Constant AGRICULTURAL_PRODUCER_PARTY. */
    public static final String AGRICULTURAL_PRODUCER_PARTY = "AgriculturalProducerParty";

    /** The Constant ADDITIONAL. */
    public static final String ADDITIONAL = "Additional";

    /** The Constant KEY. */
    public static final String KEY = "Key";

    /** The Constant AGRICULTURAL_ACTIVITY. */
    public static final String AGRICULTURAL_ACTIVITY = "AgriculturalActivity";

    /** The Constant COORDINATES. */
    public static final String COORDINATES = "Coordinates";

    /** The Constant GEOMETRY. */
    public static final String GEOMETRY = "Geometry";

    /** The Constant SURFACE. */
    public static final String SURFACE = "Surface";

    /** The Constant POLYGON. */
    public static final String POLYGON = "Polygon";

    /** The Constant CROP_IRRIGATION. */
    public static final String CROP_IRRIGATION = "CropIrrigation";

    /** The Constant WATER_ORIGIN. */
    public static final String WATER_ORIGIN = "WaterOrigin";

    /** The Constant VOLUME. */
    public static final String VOLUME = "Volume";

    /** The Constant UNIT. */
    public static final String UNIT = "Unit";

    /** The Constant DATE. */
    public static final String DATE = "Date";

    /** The Constant PHYTOSANITARY. */
    public static final String PHYTOSANITARY = "Phytosanitary";

    /** The Constant ID. */
    public static final String ID = "ID";

    /** The Constant DOSE. */
    public static final String DOSE = "Dose";

    /** The Constant EQUIPMENT. */
    public static final String EQUIPMENT = "Equipment";

    /** The Constant FERTILIZER. */
    public static final String FERTILIZER = "Fertilizer";

    /** The Constant FERTILIZATION_TYPE. */
    public static final String FERTILIZATION_TYPE = "FertilizationType";

    /** The Constant FERTILIZATION_METHOD. */
    public static final String FERTILIZATION_METHOD = "FertilizationMethod";

    /** The Constant CROP_REPORT_DOCUMENT. */
    public static final String CROP_REPORT_DOCUMENT = "CropReportDocument";

    /** The Constant REPORT_COUNT. */
    public static final String REPORT_COUNT = "ReportCount";

    /** The Constant DESCRIPCION. */
    public static final String DESCRIPCION = "Description";

    /** The Constant COPY. */
    public static final String COPY = "Copy";

    /** The Constant CONTROL_REQUIREMENT. */
    public static final String CONTROL_REQUIREMENT = "ControlRequirement";

    /** The Constant PURPOSE. */
    public static final String PURPOSE = "Purpose";

    /** The Constant LINE_COUNT. */
    public static final String LINE_COUNT = "LineCount";

    /** The Constant INFORMATION. */
    public static final String INFORMATION = "Information";

    /** The Constant STATUS. */
    public static final String STATUS = "Status";

    /** The Constant SEQUENCE. */
    public static final String SEQUENCE = "Sequence";

    /** The Constant FECHA_INICIO. */
    public static final String FECHA_INICIO = "2020-01-01";

    /** The Constant TEST_PERSONAL_ID. */
    public static final String TEST_PERSONAL_ID = "JUnitPersonalID";

    /** The Constant TEST_KEY_PRODUCTION_UNIT. */
    public static final String TEST_KEY_PRODUCTION_UNIT = "JUnitKeyProductionUnit";
    
}
