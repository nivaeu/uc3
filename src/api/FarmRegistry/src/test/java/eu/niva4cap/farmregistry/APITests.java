/*
 * #
 * # Copyright (c) David Sanchez, Tragsatec 2019 -- 2021.
 * # This file belongs to subproject UC3 of project NIVA (www.niva4cap.eu)
 * # All rights reserved

 * #
 * # Project and code is made available under the EU-PL v 1.2 license.
 * #
 * @proyecto: NIVA NIVA (NEW IACS VISION IN ACTION)
 * @empresa: FEGA
*/
package eu.niva4cap.farmregistry;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;

import eu.niva4cap.farmregistry.*;
//import eu.niva4cap.farmregistry.rest.RestfulCallerService;
import eu.niva4cap.farmregistry.rest.RestfulCallerService;

/**
 * The Class APITests.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
@SpringBootTest
@TestMethodOrder(value = OrderAnnotation.class)
class APITests {

    /** The llamada rest. */
    private static RestfulCallerService llamadaRest = null;

    /** The respuesta login. */
    private static JSONObject respuestaLogin = null;

    /**
     * Obtener crop report document.
     *
     * @return the JSON object
     */
    @SuppressWarnings({"unchecked"})
    private static JSONObject obtenerCropReportDocument() {
        final JSONObject cropReportDocument = new JSONObject();
        cropReportDocument.put(Constantes.REPORT_COUNT, 1);
        cropReportDocument.put(Constantes.DESCRIPCION, "eCrop report message");
        cropReportDocument.put(Constantes.TYPE, 1);
        cropReportDocument.put(Constantes.COPY, false);
        cropReportDocument.put(Constantes.CONTROL_REQUIREMENT, false);
        cropReportDocument.put(Constantes.PURPOSE, 1);
        cropReportDocument.put(Constantes.LINE_COUNT, 440);
        cropReportDocument.put(Constantes.INFORMATION, "Some random information text");
        cropReportDocument.put(Constantes.STATUS, 1);
        cropReportDocument.put(Constantes.SEQUENCE, 1);
        return cropReportDocument;
    }

    /**
     * Set up.
     */
    @SuppressWarnings("static-access")
    @BeforeAll
    private static void setUp() {
        llamadaRest = new RestfulCallerService();
        respuestaLogin = llamadaRest.obtenerRespuestaLogin();
        llamadaRest.setRespuestaLogin(respuestaLogin);
    }

    /**
     * Crear agricultural producer party.
     *
     * @return the JSON object
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private JSONObject crearAgriculturalProducerParty() {

        final JSONObject jsonPrincipal = new JSONObject();

        final Date fechaActualDate = new Date();
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        final String fechaActual = df.format(fechaActualDate);

        final List<JSONObject> lista = new ArrayList<>();

        final JSONObject jsonPropiedades = new JSONObject();

        jsonPropiedades.put(Constantes.NAME, "JUnitName");

        jsonPropiedades.put(Constantes.COUNTRY, "ES");

        jsonPropiedades.put(Constantes.PERSONAL_ID, Constantes.TEST_PERSONAL_ID);

        jsonPropiedades.put(Constantes.CLASSIFICATION, 1);

        final JSONObject structuredAddress = new JSONObject();

        structuredAddress.put(Constantes.COUNTRY, "ES");

        structuredAddress.put(Constantes.CITY_NAME, "JUnitCity");

        structuredAddress.put(Constantes.STREET_NAME, "JUnitStreet");

        structuredAddress.put(Constantes.BUILDING_NUMBER, "1");

        structuredAddress.put(Constantes.POSTCODE, "11111");

        structuredAddress.put(Constantes.EMAIL1, "JUnitEmail");

        structuredAddress.put(Constantes.PHONE1, "111111111");

        structuredAddress.put(Constantes.PHONE2, "111111111");

        jsonPropiedades.put(Constantes.STRUCTURED_ADDRESS, structuredAddress);

        final List<JSONObject> listaAgriculturalProductionUnit = new ArrayList<>();

        final JSONObject agriculturalProductionUnit = new JSONObject();
        agriculturalProductionUnit.put(Constantes.KEY, Constantes.TEST_KEY_PRODUCTION_UNIT);
        agriculturalProductionUnit.put(Constantes.COUNTRY, "ES");
        agriculturalProductionUnit.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        agriculturalProductionUnit.put(Constantes.DATE_TO, fechaActual);

        final JSONObject structuredAddressAPU = new JSONObject();
        structuredAddressAPU.put(Constantes.COUNTRY, "ES");
        structuredAddressAPU.put(Constantes.CITY_NAME, "JUnitCity");
        structuredAddressAPU.put(Constantes.STREET_NAME, "JUnitStret");
        structuredAddressAPU.put(Constantes.BUILDING_NUMBER, "1");
        structuredAddressAPU.put(Constantes.POSTCODE, "11111");
        structuredAddressAPU.put(Constantes.ADDITIONAL, "parcel");
        structuredAddressAPU.put(Constantes.EMAIL1, "JUnitEmail");
        structuredAddressAPU.put(Constantes.PHONE1, "111111111");
        structuredAddressAPU.put(Constantes.PHONE2, "111111111");
        agriculturalProductionUnit.put(Constantes.STRUCTURED_ADDRESS, structuredAddressAPU);

        final List<JSONObject> listaFarmingType = new ArrayList<>();
        final JSONObject farmingType = new JSONObject();
        farmingType.put(Constantes.TYPE, "900");
        farmingType.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        farmingType.put(Constantes.DATE_TO, fechaActual);
        listaFarmingType.add(farmingType);
        agriculturalProductionUnit.put(Constantes.FARMING_TYPE, listaFarmingType);

        final List<JSONObject> listaWorkUnit = new ArrayList<>();
        final JSONObject workUnit = new JSONObject();
        workUnit.put(Constantes.TYPE, 1);
        workUnit.put(Constantes.AWU, 8.5);
        workUnit.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        workUnit.put(Constantes.DATE_TO, fechaActual);
        listaWorkUnit.add(workUnit);
        agriculturalProductionUnit.put(Constantes.WORKUNIT, listaWorkUnit);

        final List<JSONObject> listaCropPlot = new ArrayList<>();
        final JSONObject cropPlot = new JSONObject();
        cropPlot.put(Constantes.KEY, "1111111");
        cropPlot.put(Constantes.START, Constantes.FECHA_INICIO);
        cropPlot.put(Constantes.END, fechaActual);

        final List<JSONObject> listaGeometryCP = new ArrayList<>();
        final JSONObject geometryCP = new JSONObject();
        geometryCP.put(Constantes.TYPE, Constantes.POLYGON);
        final List listaCoordinatesCP0 = new ArrayList();
        final List listaCoordinatesCP1 = new ArrayList();
        final List listaCoordinatesCP20 = Arrays.asList(5.41412794564089, 52.148451591648602);
        final List listaCoordinatesCP21 = Arrays.asList(5.41784357231444, 52.154784683175897);
        final List listaCoordinatesCP22 = Arrays.asList(5.4232748279405, 52.157515523436402);
        final List listaCoordinatesCP23 = Arrays.asList(5.41412794564089, 52.148451591648602);
        listaCoordinatesCP1.add(listaCoordinatesCP20);
        listaCoordinatesCP1.add(listaCoordinatesCP21);
        listaCoordinatesCP1.add(listaCoordinatesCP22);
        listaCoordinatesCP1.add(listaCoordinatesCP23);
        listaCoordinatesCP0.add(listaCoordinatesCP1);
        geometryCP.put(Constantes.COORDINATES, listaCoordinatesCP0);
        geometryCP.put(Constantes.SURFACE, 9873.25);
        geometryCP.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        geometryCP.put(Constantes.DATE_TO, fechaActual);
        listaGeometryCP.add(geometryCP);

        cropPlot.put(Constantes.GEOMETRY, listaGeometryCP);

        final List<JSONObject> listaSoilType = new ArrayList<>();
        final JSONObject soilType = new JSONObject();
        soilType.put(Constantes.TYPE, 1);
        soilType.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        soilType.put(Constantes.DATE_TO, fechaActual);
        listaSoilType.add(soilType);

        cropPlot.put(Constantes.SOIL_TYPE, listaSoilType);

        final List<JSONObject> listaReferencePlot = new ArrayList<>();
        final JSONObject referencePlot = new JSONObject();
        referencePlot.put(Constantes.KEY, "11:11:11:11:11:11:11");
        referencePlot.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        referencePlot.put(Constantes.DATE_TO, fechaActual);
        referencePlot.put(Constantes.REFERENCE_PARCEL, "JUnitReferenceParcel");

        final List<JSONObject> listaGeometryRP = new ArrayList<>();
        final JSONObject geometryRP = new JSONObject();
        geometryRP.put(Constantes.TYPE, Constantes.POLYGON);
        final List listaCoordinatesRP0 = new ArrayList();
        final List listaCoordinatesRP1 = new ArrayList();
        final List listaCoordinatesRP20 = Arrays.asList(5.49469492804353, 52.124697887324601);
        final List listaCoordinatesRP21 = Arrays.asList(5.49691768162249, 52.119557486966002);
        final List listaCoordinatesRP22 = Arrays.asList(5.5053539453497, 52.119750446627997);
        final List listaCoordinatesRP23 = Arrays.asList(5.49469492804353, 52.124697887324601);
        listaCoordinatesRP1.add(listaCoordinatesRP20);
        listaCoordinatesRP1.add(listaCoordinatesRP21);
        listaCoordinatesRP1.add(listaCoordinatesRP22);
        listaCoordinatesRP1.add(listaCoordinatesRP23);
        listaCoordinatesRP0.add(listaCoordinatesRP1);
        geometryRP.put(Constantes.COORDINATES, listaCoordinatesRP0);
        geometryRP.put(Constantes.SURFACE, 5688.45);
        geometryRP.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        geometryRP.put(Constantes.DATE_TO, fechaActual);
        listaGeometryRP.add(geometryRP);

        referencePlot.put(Constantes.GEOMETRY, listaGeometryRP);

        final List<JSONObject> listaFieldCrop = new ArrayList<>();
        final JSONObject fieldCrop = new JSONObject();
        fieldCrop.put(Constantes.KEY, "FC_123330");
        fieldCrop.put(Constantes.IRRIGATION, true);
        fieldCrop.put(Constantes.CROP_PRODUCT, "B18");
        fieldCrop.put(Constantes.LANDSCAPE_FEATURES, true);
        fieldCrop.put(Constantes.LAND_TENURE, 3);
        fieldCrop.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        fieldCrop.put(Constantes.DATE_TO, fechaActual);

        final List<JSONObject> listaGeometryFC = new ArrayList<>();
        final JSONObject geometryFC = new JSONObject();
        geometryFC.put(Constantes.TYPE, Constantes.POLYGON);
        final List listaCoordinatesFC0 = new ArrayList();
        final List listaCoordinatesFC1 = new ArrayList();
        final List listaCoordinatesFC20 = Arrays.asList(5.47582548628898, 52.112071103593401);
        final List listaCoordinatesFC21 = Arrays.asList(5.47603713431143, 52.105012153653398);
        final List listaCoordinatesFC22 = Arrays.asList(5.47370706432387, 52.104759628267502);
        final List listaCoordinatesFC23 = Arrays.asList(5.47582548628898, 52.112071103593401);
        listaCoordinatesFC1.add(listaCoordinatesFC20);
        listaCoordinatesFC1.add(listaCoordinatesFC21);
        listaCoordinatesFC1.add(listaCoordinatesFC22);
        listaCoordinatesFC1.add(listaCoordinatesFC23);
        listaCoordinatesFC0.add(listaCoordinatesFC1);
        geometryFC.put(Constantes.COORDINATES, listaCoordinatesFC0);
        geometryFC.put(Constantes.SURFACE, 754.12);
        geometryFC.put(Constantes.DATE_FROM, Constantes.FECHA_INICIO);
        geometryFC.put(Constantes.DATE_TO, fechaActual);
        listaGeometryFC.add(geometryFC);

        fieldCrop.put(Constantes.GEOMETRY, listaGeometryRP);

        final JSONObject activity = new JSONObject();
        activity.put(Constantes.AGRICULTURAL_ACTIVITY, true);
        activity.put(Constantes.CULTIVATION_SYSTEM, 2);
        activity.put(Constantes.CULTIVATION_DETAIL, 5);
        activity.put(Constantes.SEED_TYPE, 3);
        activity.put(Constantes.ORGANIC_FARMING, 1);
        fieldCrop.put(Constantes.ACTIVITY, activity);

        final List<JSONObject> listaActivityDetail = new ArrayList<>();
        final JSONObject activityDetail = new JSONObject();
        activityDetail.put(Constantes.LABOR_TYPE, 1);
        activityDetail.put(Constantes.START_DATE, Constantes.FECHA_INICIO);
        activityDetail.put(Constantes.END_DATE, fechaActual);
        listaActivityDetail.add(activityDetail);
        fieldCrop.put(Constantes.ACTIVITY_DETAIL, listaActivityDetail);

        final List<JSONObject> listaCropIrrigation = new ArrayList<>();
        final JSONObject cropIrrigation = new JSONObject();
        cropIrrigation.put(Constantes.TYPE, 1);
        cropIrrigation.put(Constantes.WATER_ORIGIN, 1);
        cropIrrigation.put(Constantes.VOLUME, 49.99);
        cropIrrigation.put(Constantes.UNIT, 1);
        cropIrrigation.put(Constantes.DATE, "2021-12-31");
        listaCropIrrigation.add(cropIrrigation);
        fieldCrop.put(Constantes.CROP_IRRIGATION, listaCropIrrigation);

        final List<JSONObject> listaPhytosanitary = new ArrayList<>();
        final JSONObject phytosanitary = new JSONObject();
        phytosanitary.put(Constantes.ID, "0");
        phytosanitary.put(Constantes.COUNTRY, "ES");
        phytosanitary.put(Constantes.DOSE, 55.99);
        phytosanitary.put(Constantes.UNIT, 1);
        phytosanitary.put(Constantes.EQUIPMENT, 1);
        phytosanitary.put(Constantes.START_DATE, Constantes.FECHA_INICIO);
        phytosanitary.put(Constantes.END_DATE, fechaActual);
        listaPhytosanitary.add(phytosanitary);
        fieldCrop.put(Constantes.PHYTOSANITARY, listaPhytosanitary);

        final List<JSONObject> listaFertilizer = new ArrayList<>();
        final JSONObject fertilizer = new JSONObject();
        fertilizer.put(Constantes.ID, 5);
        fertilizer.put(Constantes.TYPE, 1);
        fertilizer.put(Constantes.FERTILIZATION_TYPE, 2);
        fertilizer.put(Constantes.FERTILIZATION_METHOD, 1);
        fertilizer.put(Constantes.DOSE, 25.52);
        fertilizer.put(Constantes.UNIT, 1);
        fertilizer.put(Constantes.START_DATE, Constantes.FECHA_INICIO);
        fertilizer.put(Constantes.END_DATE, fechaActual);
        listaFertilizer.add(fertilizer);
        fieldCrop.put(Constantes.FERTILIZER, listaFertilizer);

        listaFieldCrop.add(fieldCrop);

        referencePlot.put(Constantes.FIELD_CROP, listaFieldCrop);

        listaReferencePlot.add(referencePlot);

        cropPlot.put(Constantes.REFERENCE_PLOT, listaReferencePlot);

        listaCropPlot.add(cropPlot);

        agriculturalProductionUnit.put(Constantes.CROP_PLOT, listaCropPlot);

        listaAgriculturalProductionUnit.add(agriculturalProductionUnit);

        jsonPropiedades.put(Constantes.AGRICULTURAL_PRODUCTION_UNIT,
                listaAgriculturalProductionUnit);

        lista.add(jsonPropiedades);

        final JSONObject cropReportDocument = obtenerCropReportDocument();
        jsonPrincipal.put(Constantes.CROP_REPORT_DOCUMENT, cropReportDocument);
        jsonPrincipal.put(Constantes.AGRICULTURAL_PRODUCER_PARTY, lista);

        return jsonPrincipal;
    }

    /**
     * Obtener code agricultural producer party.
     *
     * @return the string
     */
    private String obtenerCodeAgriculturalProducerParty() {
        String code = null;
        final String country = "ES";
        final String personalID = Constantes.TEST_PERSONAL_ID;
        final String url = Constantes.END_POINT
                + "/FarmRegistry/eCROP/agricultural-producer-parties/country/" + country
                + "/personalID/" + personalID;
        try {
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
            code = respuestaLlamada.get("Code").toString();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return code;
    }

    /**
     * Obtener code agricultural production unit.
     *
     * @return the string
     */
    private String obtenerCodeAgriculturalProductionUnit() {
        String code = null;
        try {
            final String codeAPP = obtenerCodeAgriculturalProducerParty();
            final String url = Constantes.END_POINT
                    + "/FarmRegistry/eCROP/agricultural-producer-parties/" + codeAPP
                    + "/agricultural-production-units";
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONArray respuestaLlamada = (JSONArray) parser.parse(respuesta);
            code = ((JSONObject) respuestaLlamada.get(0)).get("Code").toString();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return code;

    }

    /**
     * Test agricultural producer parties by code.
     */
    @Test
    @Order(2)
    void testAgriculturalProducerPartiesByCode() {
        try {
            final String code = obtenerCodeAgriculturalProducerParty();
            final String url = Constantes.END_POINT
                    + "/FarmRegistry/eCROP/agricultural-producer-parties/code/" + code;
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada2 = (JSONObject) parser.parse(respuesta);
            assertEquals(respuestaLlamada2.get("Code").toString(), code);
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

    /**
     * Test agricultural producer parties by code agricultural production units.
     */
    @Test
    @Order(4)
    void testAgriculturalProducerPartiesByCodeAgriculturalProductionUnits() {
        try {
            final String code = obtenerCodeAgriculturalProducerParty();
            final String url = Constantes.END_POINT
                    + "/FarmRegistry/eCROP/agricultural-producer-parties/" + code
                    + "/agricultural-production-units";
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONArray respuestaLlamada = (JSONArray) parser.parse(respuesta);
            assertEquals(((JSONObject) respuestaLlamada.get(0)).get("Key").toString(),
                    Constantes.TEST_KEY_PRODUCTION_UNIT);
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

    // @Test
    // @Order(7)
    // void testAgriculturalProductionUnitsDeleteByCode() {
    // try {
    // final String codeAPU = obtenerCodeAgriculturalProductionUnit();
    // final String url = Constantes.END_POINT +
    // "/FarmRegistry/eCROP/agricultural-production-units/" + codeAPU;
    // final String respuesta = llamadaRest.callRestfulDelete(url);
    // final JSONParser parser = new JSONParser();
    // final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
    // assertEquals(respuestaLlamada.get("status"), Long.valueOf(200));
    // } catch (final Exception e) {
    // fail(e.getMessage());
    // }
    //
    // }
    //
    // @Test
    // @Order(8)
    // void testAgriculturalProducerPartiesDeleteByCode() {
    // try {
    // final String codeAPP = obtenerCodeAgriculturalProducerParty();
    // final String url = Constantes.END_POINT +
    // "/FarmRegistry/eCROP/agricultural-producer-parties/" + codeAPP;
    // final String respuesta = llamadaRest.callRestfulDelete(url);
    // final JSONParser parser = new JSONParser();
    // final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
    // assertEquals(respuestaLlamada.get("status"), Long.valueOf(200));
    // } catch (final Exception e) {
    // fail(e.getMessage());
    // }
    //
    // }

    /**
     * Test agricultural producer parties by code agricultural production units
     * by production unit key.
     */
    @Test
    @Order(5)
    void testAgriculturalProducerPartiesByCodeAgriculturalProductionUnitsByProductionUnitKey() {
        try {
            final String codeAPP = obtenerCodeAgriculturalProducerParty();
            final String keyAPU = Constantes.TEST_KEY_PRODUCTION_UNIT;
            final String url = Constantes.END_POINT
                    + "/FarmRegistry/eCROP/agricultural-producer-parties/" + codeAPP
                    + "/agricultural-production-units/" + keyAPU;
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
            assertEquals(respuestaLlamada.get("Key").toString(),
                    Constantes.TEST_KEY_PRODUCTION_UNIT);
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

    /**
     * Test agricultural producer parties by country personal ID.
     */
    @Test
    @Order(3)
    void testAgriculturalProducerPartiesByCountryPersonalID() {
        final String country = "ES";
        final String personalID = Constantes.TEST_PERSONAL_ID;
        final String url = Constantes.END_POINT
                + "/FarmRegistry/eCROP/agricultural-producer-parties/country/" + country
                + "/personalID/" + personalID;
        try {
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
            assertEquals(respuestaLlamada.get("PersonalID").toString(),
                    Constantes.TEST_PERSONAL_ID);
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

    /**
     * Test agricultural production units by code.
     */
    @Test
    @Order(6)
    void testAgriculturalProductionUnitsByCode() {
        try {
            final String codeAPU = obtenerCodeAgriculturalProductionUnit();
            final String url = Constantes.END_POINT
                    + "/FarmRegistry/eCROP/agricultural-production-units/" + codeAPU;
            final String respuesta = llamadaRest.callRestfulGetJson(url);
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
            assertEquals(respuestaLlamada.get("Key").toString(),
                    Constantes.TEST_KEY_PRODUCTION_UNIT);
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

    /**
     * Test ECROP.
     */
    @Test
    @Order(1)
    void testECROP() {
        final String url = Constantes.END_POINT + "/FarmRegistry/eCROP";
        try {
            final JSONObject agriculturalProducerParty = crearAgriculturalProducerParty();
            final String respuesta = llamadaRest.callRestfulPostJson(url,
                    agriculturalProducerParty.toJSONString());
            final JSONParser parser = new JSONParser();
            final JSONObject respuestaLlamada = (JSONObject) parser.parse(respuesta);
            assertEquals(respuestaLlamada.get("status"), Long.valueOf(200));
        } catch (final Exception e) {
            fail(e.getMessage());
        }

    }

}
