/*
 * @proyecto: PAC 2015
 * @empresa: Tragsatec
 */
package eu.niva4cap.farmregistry.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import eu.niva4cap.farmregistry.Constantes;

/**
 * The Class RestfulCallerService.
 *
 * @author rcampana
 * @version 1.0.0, 26-nov-2020
 * @since JDK 1.6
 */
public class RestfulCallerService {

    /** The Constant POST. */
    private static final String POST = "POST";

    /** The Constant DELETE. */
    private static final String DELETE = "DELETE";

    /** The Constant CONTENT_TYPE. */
    private static final String CONTENT_TYPE = "Content-Type";

    /** The Constant CONTENT_TYPE_JSON. */
    private static final String CONTENT_TYPE_JSON = "application/json";

    /** The Constant AUTHORIZATION. */
    private static final String AUTHORIZATION = "Authorization";

    /** The Constant ENCODING. */
    private static final String ENCODING = "UTF-8";

    /** The Constant ACCEPT. */
    private static final String ACCEPT = "Accept";

    /**
     * Obtener respuesta login.
     *
     * @return the JSON object
     */
    @SuppressWarnings({"unchecked"})
    public static JSONObject obtenerRespuestaLogin() {
        JSONObject respuestaLogin = null;
        final RestfulCallerService llamadaRest = new RestfulCallerService();
        final JSONObject jsonLogin = new JSONObject();
        jsonLogin.put("password", Constantes.USU_REST);
        jsonLogin.put("username", Constantes.CONTR_REST);
        try {
            final String respuesta = llamadaRest.callRestfulPostJson(Constantes.URL_LOGIN,
                    jsonLogin.toJSONString());
            final JSONParser parser = new JSONParser();
            respuestaLogin = (JSONObject) parser.parse(respuesta);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        System.out.println("Token obtenido para el usuario " + Constantes.USU_REST);
        return respuestaLogin;
    }

    /** The respuesta login. */
    JSONObject respuestaLogin;

    /**
     * Call restful delete.
     *
     * @param urlServicio
     *            the url servicio
     * @return the string
     * @throws Exception
     *             the exception
     */
    public String callRestfulDelete(final String urlServicio) throws Exception {

        String respuesta = null;

        try {
            final URL url = new URL(urlServicio);
            final HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpCon.addRequestProperty(AUTHORIZATION,
                    respuestaLogin.get("tokenType") + " " + respuestaLogin.get("accessToken"));
            httpCon.setRequestMethod(DELETE);
            httpCon.connect();
            final BufferedReader br = new BufferedReader(
                    new InputStreamReader(httpCon.getInputStream()));

            respuesta = br.readLine();
        } catch (final Exception e) {
            e.printStackTrace();
            throw new Exception(
                    "Error inesperado en la llamada al servicio de SgaGenMdg " + urlServicio, e);
        }

        return respuesta;
    }

    /**
     * Call restful get json.
     *
     * @param urlServicio
     *            the url servicio
     * @return the string
     * @throws Exception
     *             the exception
     */
    public String callRestfulGetJson(final String urlServicio) throws Exception {

        String respuesta = null;
        try {
            final URL url = new URL(urlServicio);
            final URLConnection conn = url.openConnection();
            setToken(conn);
            conn.setDoOutput(true);

            final BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));

            respuesta = br.readLine();

        } catch (final Exception e) {
            e.printStackTrace();
            throw new Exception(
                    "Error inesperado en la llamada al servicio de SgaGenMdg " + urlServicio, e);
        }
        return respuesta;
    }

    /**
     * Call restful post json.
     *
     * @param urlServicio
     *            the url servicio
     * @param json
     *            the json
     * @return the string
     * @throws Exception
     *             the exception
     */
    public String callRestfulPostJson(final String urlServicio, final String json)
            throws Exception {

        String respuesta = null;
        try {
            final HttpURLConnection conn = (HttpURLConnection) new URL(urlServicio)
                    .openConnection();
            conn.setDoOutput(true);
            postJson(conn, urlServicio);
            final Writer out = new OutputStreamWriter(conn.getOutputStream(), ENCODING);
            if (json != null) {
                out.write(json);
            }
            out.close();

            final BufferedReader in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), ENCODING));

            respuesta = in.readLine();

        } catch (final Exception e) {
            e.printStackTrace();
            throw new Exception(
                    "Error inesperado en la llamada al servicio de SgaGenMdg " + urlServicio, e);
        }
        return respuesta;
    }

    /**
     * Gets the respuesta login.
     *
     * @return the respuesta login
     */
    public JSONObject getRespuestaLogin() {
        return respuestaLogin;
    }

    /**
     * Post json.
     *
     * @param conn
     *            El par�metro conn indica
     * @param urlServicio
     *            the url servicio
     * @throws ProtocolException
     *             the protocol exception
     */
    private void postJson(final HttpURLConnection conn, final String urlServicio)
            throws ProtocolException {
        if (!urlServicio.contains("login")) {
            conn.addRequestProperty(AUTHORIZATION,
                    respuestaLogin.get("tokenType") + " " + respuestaLogin.get("accessToken"));
        }
        conn.addRequestProperty(CONTENT_TYPE, CONTENT_TYPE_JSON);
        conn.setRequestProperty(ACCEPT, CONTENT_TYPE_JSON);
        conn.setRequestMethod(POST);

    }

    /**
     * Sets the respuesta login.
     *
     * @param respuestaLogin
     *            the new respuesta login
     */
    public void setRespuestaLogin(final JSONObject respuestaLogin) {
        this.respuestaLogin = respuestaLogin;
    }

    /**
     * Get.
     *
     * @param conn
     *            El par�metro conn indica
     * @throws ProtocolException
     *             the protocol exception
     */
    private void setToken(final URLConnection conn) throws ProtocolException {
        conn.addRequestProperty(AUTHORIZATION,
                respuestaLogin.get("tokenType") + " " + respuestaLogin.get("accessToken"));

    }

}